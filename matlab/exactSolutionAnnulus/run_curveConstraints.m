% From
%   http://mathworld.wolfram.com/BiharmonicEquation.html
%       (cf. Kaplan 1984, ed. 5, p. 143)
% we know that the bilaplace equation for radially symmetric functions
% reads
%   u_rrrr + 2/r u_rrr - 1/r^2 u_rr + 1/r^3 u_r = 0
% which according to
%   http://www.wolframalpha.com/input/?i=f%27%27%27%27(x)+%2B+2%2Fx*f%27%27%27(x)+-+1%2Fx%5E2*f%27%27(x)+%2B+1%2Fx%5E3*f%27(x)+%3D+0
% has the solution
%   u(r) = c1 + c2*r^2 + c3*log(r) + c4*r^2*(log(r)-1/2).
% This script computes the coefficients c(i) for the bilaplace equation on
% an annulus with constant inner boundary conditions and Dirichlet outer
% boundary conditions.

clear variables;
close all;

% Inner height/slope:
h   = (exp(2)-3)/4;
s   = -exp(1);

% Radii of inner and outer circle.
r0  = exp(-1);
r1  = 1;

% Linear system.
A   = [1 r0^2 log(r0) r0^2*(log(r0)-1/2); ...
       0 2*r0 1/r0    2*r0*log(r0); ...
       1 r1^2 log(r1) r1^2*(log(r1)-1/2); ...
       0 2*r1 1/r1    2*r1*log(r1) ];
b   = [h s 0 0].';

% Solve.
c   = A\b;

% Plot
f = @(r) c(1) + c(2)*r.^2 + c(3) * log(r) + c(4)*r.^2.*(log(r)-1/2);
R = linspace(r0,r1,200);
plot(R, f(R));


% To-do: Could now do the same with symbolic values.
% error('Do you want to invoke the symbolic toolbox?');
syms r0_ r1_ h_ s_;
A_  = [1 r0_^2 log(r0_) r0_^2*(log(r0_)-1/2); ...
       0 2*r0_ 1/r0_    2*r0_*log(r0_); ...
       1 r1_^2 log(r1_) r1_^2*(log(r1_)-1/2); ...
       0 2*r1_ 1/r1_    2*r1_*log(r1_) ];
b_  = [h_ s_ 0 0].';
c_  = A_\b_;

% By looking at the terms we get an idea for what expressions we expect
% "pretty" terms:
% 
syms d % dummy
s1 = simplify( subs(c_, [r0_, r1_], [exp(d), 1]) );
s2 = simplify( subs(s1, d, -1) );
s3 = simplify( subs(subs(s2, s_, -4*exp(d)*h_/(exp(2*d)-3)),d,1) );
s4 = simplify( subs(subs(s3, h_, (exp(d)-3)/4),d,2) );

% -> Results suggest:
%   r0 = 1/e, r1 = 1, h = (e^2-3)/4, s = -e
% Yields: c = [e^2/4 0 0 e^2/2]