% This scripts investigates a little the Sobolev-regularity of the solution
% for the point constraint problem.
% Basically all terms there are okay except the r^2*log(r) part.
% Let's have a closer look at that one.

clear variables;
syms r x y

f = r^2*log(r);
F = subs(f,r,sqrt(x^2+y^2));

% Integrand for function itself, norm of gradient, Hessian and third derivative.
int_0  = simplify(F^2);
int_1  = simplify(diff(F,x)^2 + diff(F,y)^2);
int_2  = simplify(diff(F,x,x)^2 + diff(F,x,y)^2 ...
            + diff(F,y,x)^2 + diff(F,y,y)^2);
int_3  = simplify(diff(F,x,x,x)^2 + diff(F,x,x,y)^2 + diff(F,x,y,x)^2 ...
            + diff(F,x,y,y)^2 + diff(F,y,x,x)^2 + diff(F,y,x,y)^2 ...
            + diff(F,y,y,x)^2 + diff(F,y,y,y)^2);

% Integrands in polar coordinates. (Angle is already integrated out here.
rint_0  = 2*pi*simplify(subs(int_0,x^2+y^2,r^2))*r;
rint_1  = 2*pi*simplify(subs(int_1,x^2+y^2,r^2))*r;
rint_2  = 2*pi*simplify(subs(int_2,x^2+y^2,r^2))*r;
rint_3  = 2*pi*simplify(subs(int_3,x^2+y^2,r^2))*r;

% Values of the integrals:
syms R
I0  = int(rint_0,0,R);
I1  = int(rint_1,0,R);
I2  = int(rint_2,0,R);
I3  = int(rint_3,0,R);

% We see that everything works out just fine, except for I3, which is not
% defined. (This already could clearly be seen from rint_3 as well.)

% So, we have that the solution is in H^2 but not in H^3. This would not
% justify any convergence of our discretization. Therefore, we ask
% ourselves: Is there any more regularity? To this end we investigate the
% second derivatives with respect to the Sobolev-Slobodeckij norm.

% Substitute x and y by v and w in F and compute the numerator appearing in
% the norm.
syms v w
G = subs(F,[x y], [v w]);
numer  = simplify(...
              (diff(F,x,x)-diff(G,v,v))^2 ...
            + (diff(F,x,y)-diff(G,v,w))^2 ...
            + (diff(F,y,x)-diff(G,w,v))^2 ...
            + (diff(F,y,y)-diff(G,w,w))^2 ...
         );
     
% Derive the corresponding transformed integrand.
syms t s
int_s = simplify(subs(numer,[x^2+y^2, v^2+w^2],[r^2 t^2]));

% Add some zeros to aid the simplification.
% int_s = simplify(int_s - 4*x^2*log(r^2)/r^2 - 4*y^2*log(r^2)/r^2 + 4*log(r^2));
% int_s = simplify(int_s + 4*v^2*log(r^2)/t^2 + 4*w^2*log(r^2)/t^2 - 4*log(r^2));
% int_s = simplify(int_s + 4*x^2*log(t^2)/r^2 + 4*y^2*log(t^2)/r^2 - 4*log(t^2));
% int_s = simplify(int_s - 4*v^2*log(t^2)/t^2 - 4*w^2*log(t^2)/t^2 + 4*log(t^2));
% 
% int_s = simplify(int_s + expand(2*(x^2+y^2)*(v^2+w^2)/(r^2*t^2)) - 2);
% 
% int_s = simplify(int_s - 4*x^4/r^4 - 4*y^4/r^4 + expand(4*(v^2+w^2)^2*x^4/(r^4*t^4) + 4*(v^2+w^2)^2*y^4/(r^4*t^4)));
% int_s = simplify(int_s - expand(4*(v^2+w^2)^2*(x^2+y^2)^2/(r^4*t^4)) + 4);
% 
% int_s = simplify(int_s - 4*v^4/t^4 - 4*w^4/t^4 + expand(4*(x^2+y^2)^2*v^4/(r^4*t^4) + 4*(x^2+y^2)^2*w^4/(r^4*t^4)));
% int_s = simplify(int_s - expand(4*(x^2+y^2)^2*(v^2+w^2)^2/(r^4*t^4)) + 4);
% 
% int_s = simplify(int_s - (-6*v^2*x^2 + 2*v^2*y^2 + 2*w^2*x^2 - 6*w^2*y^2)/(r^2*t^2) + expand((x^2+y^2)*(v^2+w^2)*(-6*v^2*x^2 + 2*v^2*y^2 + 2*w^2*x^2 - 6*w^2*y^2)/(r^4*t^4)));
% int_s = simplify(int_s + 6*expand((x^2+y^2)^2*(v^2+w^2)^2/(r^4*t^4)) - 6);

% int_s = expand(int_s);
% int_s = int_s - (2*log(r^2)^2 + 2*log(t^2)^2 - 4*log(r^2)*log(t^2));
% int_s = int_s * r^4 * t^4;
% int_s = subs(int_s, [r t], [x^2+y^2, v^2+w^2]);
% int_s = expand(int_s);
% int_s = simplify(int_s);
% pretty(int_s);
% int_s = subs(int_s, [x^2+y^2 v^2+w^2], [r t]);
% int_s = int_s + (2*log(r^2)^2 + 2*log(t^2)^2 - 4*log(r^2)*log(t^2));

% int_s = int_s / (r-t)^(2*(s+1)) * r * t;

pretty(expand(int_s));