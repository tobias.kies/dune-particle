% This is the analogue of run_curveConstraints.m but with point
% constraints. The essence here is that in the limit r0->0 the solution may
% no longer contain the "log(r)"-term and therefore our solution space has
% a dimension less. This is quite convenient as we also have a boundary
% condition less to fulfill.

clear variables;
close all;

% Height at r=0.
h   = 1;

% Radius of outer circle.
R   = 1;

% Linear system.
A   = [1 0 0; ...
       1 R^2 R^2*(log(R)-1/2); ...
       0 2*R 2*R*log(R) ];
b   = [h 0 0].';

% Solve.
c   = A\b;

% Plot
f = @(r) c(1) + c(2)*r.^2 + c(3)*r.^2.*(log(r)-1/2);
I = linspace(0,R,200);
plot(I, f(I));


% To-do: Could now do the same with symbolic values.
error('Do you want to invoke the symbolic toolbox?');
syms h_ R_;
A_  = [1 0 0; ...
       1 R_^2 R_^2*(log(R_)-1/2); ...
       0 2*R_ 2*R_*log(R_) ];
b_  = [h_ 0 0].';
c_  = A_\b_;

% By looking at the terms we get an idea for what expressions we expect
% "pretty" terms:
%
s1 = simplify( subs(c_,R_,1) );

% -> Results suggest: For R=1 we may choose an arbitrary h and obtain
%       c = [h 0 2*h].
% For general R we have
%       c = [h 2*h*log(R)/R^2 2*h/R^2].
