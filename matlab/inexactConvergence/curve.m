% Get the convergence rate of the method with inexact solution.
clear variables;
close all;

data = importdata('../../../../srv/particle/tests/convergence_curve_inexact2.txt');
% Data structure:
%   grid_size, fe_approximation_error (L2, H1, H2)

h = data(:,1);
errors = data(:,2:4);
names = {'L2', 'H1', 'H2'};

for i = 1:3
  % Extract relevant errors.
  err = errors(:,i);

  % Compute pre-factor for reference slope.
  [minVal,id] = min(err./h.^(5-i));
  c   = err(id)/h(id)^(5-i);
  
  % Actual plot.
  fig = figure;
  loglog(h, err, 'color', 'blue');
  hold on;
  loglog(h, c*h.^(5-i), 'color', 'black');
  eoc = log(err(end)/err(1))/log(h(end)/h(1));
  title(sprintf('%s-Error of example with inexact solution, EOC=%.2g',names{i},eoc));
  legend('error', sprintf('reference line (slope %d)',5-i));
  print(fig, '-dpng', sprintf('convergence_curve_inexact_%s.png',names{i}));
end
