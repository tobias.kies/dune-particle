clear variables;
close all;

% Load data.
data = importdata('../../../../srv/particle/tests/egg_carton_curve.txt');

% Data structure:
% #step time position energy gradient noise
% position/gradient/noise are 6*nParticles vectors
nParticles = (size(data,2)-3)/(3*6);


t = data(:,2);
energy = data(:,3+nParticles*6);

h = figure;
plot(t,energy);
title('energy behavior over time');
