% Load convergence data from the derivative test and visualize the error behavior.
% 

clear variables;
close all;

data = importdata('../../../../srv/particle/tests/derivative_convergence_curve2.txt');

% Data shape:
% h, gradient (n*6), errors (n*6), total error

nParticles = (size(data,2)-2)/12;

referenceGradient = data(1,2:(1+6*nParticles));
data  = data(2:end,:);

h         = data(:,1);
gradients = data(:,2:(nParticles*6+1));
errors    = abs(data(:,(nParticles*6+2):(end-1)));
errTotal  = data(:,end);

h0 = figure;
loglog(h,errTotal);
hold on;
loglog(h,h.^2*errTotal(1)/h(1)^2);
close(h0);

% Plot data.
for dir = 1:6
  % Estimate rate of convergence. (Based on "exact" reference value.)
  eoc1 = log(errors(end,dir)/errors(1,dir))/log(h(end)/h(1));
  
  h1 = figure;
  Y = errors(:,dir)+1e-15;
  loglog(h,Y);
  hold on;
  loglog(h,h.^2*Y(1)/h(1)^2);
  title(sprintf('Errors in direction %d; EOC=%.2g',dir,eoc1));
  close(h1);

  % Estmate rate of convergence. (Based on progress of "analytic" values.)
  eoc2  = NaN; % TODO
  h2 = figure;
  Y = abs(gradients(:,dir)-gradients(end,dir))+1e-10;
  loglog(h,Y);
  hold on;
  loglog(h,h.^2 * Y(1)/h(1)^2);
  title(sprintf('Errors in direction %d; EOC=%.2g',dir,eoc2));
  close(h2);
  
  % Just plot the evolution of the derivative.
  h3 = figure;
  plot(h,gradients(:,dir));
  close(h3);
end
