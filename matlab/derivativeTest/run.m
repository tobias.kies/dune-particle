% The goal is to show how the function values develop along a given trajectory and how the computed derivatives compare with finite difference approximations.

clear variables;
close all;

tplFname  = '../../../../srv/particle/tests/derivative_curve_%d.txt';
dirs      = [0 3];


finiteDifferences = @(x,y) (y(3:end)-y(1:end-2)) ./ (x(3:end) - x(1:end-2));

for dir = dirs
  fname = sprintf(tplFname, dir);
  D = importdata(fname);
  N = size(D,1);
  
  % First n*6 columns are the position, then we have the function value and then the derivative.
  positions   = D(:,1:end-2);
  fvals       = D(:,end-1);
  derivatives = D(:,end);
  
  % Compute distances.
  center    = positions(ceil(N/2),:);
  sgn       = @(i) 1 - 2*(i < ceil(N/2));
  distances = arrayfun( @(i) sgn(i)*norm(center - positions(i,:)), (1:N).');
  
  % Compute finite differences.
  fin_distances   = distances(2:end-1);
  fin_derivatives = finiteDifferences(distances,fvals);
  I = abs(fin_derivatives-derivatives(2:end-1))./abs(derivatives(2:end-1)) < 2;
  fin_distances = fin_distances(I);
  fin_derivatives = fin_derivatives(I);
  
  % Plot function values.
  h1  = figure;
  plot(distances, fvals);
  title(sprintf('Function values direction %d',dir));
  %close(h1);
  
  % Plot derivatives.
  h2  = figure;
  plot(distances, derivatives, 'color', 'blue');
  hold on;
  plot(fin_distances, fin_derivatives, 'color', 'red');
  legend('shape calc.', 'diff. quot.');
  title(sprintf('Derivatives direction %d',dir));
  %close(h2);

  h3  = figure;
  semilogy(fin_distances, abs(fin_derivatives-derivatives([false; I; false])));
  title(sprintf('Derivative mismatch direction %d',dir));
  close(h3);
end
