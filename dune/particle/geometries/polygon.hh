#ifndef DUNE_GEOMETRIES_POLYGON_HH
#define DUNE_GEOMETRIES_POLYGON_HH

//~ #include <dune/membrane/helper/helper.hh>

/**
 *  This class provides an implementation of a polygon in 2 dimensions.
 *  The polygon is represented by four vectors a_1,\dots,a_k \in R^2,
 *  where k depends on the number of points that were supplied.
 */

template<class float_x = double>
class Polygon : public std::vector<Dune::FieldVector<float_x,2>>
{
    private:
    
        static const size_t dim = 2;

    
    public:
    
        typedef Dune::FieldVector<float_x,dim> Vector;
        typedef std::vector<Vector> Vectors;
        typedef Vectors Base;
        
        using Base::Base;
        
        template<class Element>
        static Polygon createPolygonFromElement( const Element& element )
        {
            const auto& geometry        = element.geometry();
            const auto& elementCenter   = geometry.center();

            Polygon polygon;
            for( size_t i = 0; i < geometry.corners(); ++i )
                polygon.push_back( geometry.corner(i) );
                
            // Sort points by center.
            // This is the functionality from GeometryHelper.
            // However, we can not invoke the GeometryHelper yet because it again depends on the geometries.
            // GeometryHelper::sortPointsByCenter( polygon, elementCenter );
            typedef Vector Point;
            std::sort( polygon.begin(), polygon.end(),
                        [elementCenter]( const Point& a, const Point& b ){
                            return ( std::atan2( a[1]-elementCenter[1], a[0]-elementCenter[0] ) < std::atan2( b[1]-elementCenter[1], b[0]-elementCenter[0] ) );
                        } );
            
            return polygon;
        }
};

#endif
