% Define ellipse.
syms a b phi x y cx cy
assume( [a b phi x y cx cy], 'real');
assume( a > 0 & b > 0 );
xy = [x;y];
c = [cx;cy];
A = [1/a^2, 0; 0, 1/b^2];
R = [cos(phi), -sin(phi); sin(phi), cos(phi)];
ellipse = (R*(xy-c))'*A*(R*(xy-c))-1;


% Define line p*t + q
syms px py qx qy t
assume( [px py qx qy t], 'real');
p = [px;py];
q = [qx;qy];
line = p*t + q;


% Compute intersection t
assume( px ~= 0 );
ct = coeffs( subs( ellipse, [x,y], [line(1), line(2)] ), t );
ccode(ct, 'file', 'ct.c');
% This leads to a quadratic equation that can easily be invastigated using the discriminant disc = ct(2)^2 - 4*ct(3)*ct(1);
