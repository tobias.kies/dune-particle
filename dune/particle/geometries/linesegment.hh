#ifndef DUNE_GEOMETRIES_LINE_SEGMENT_HH
#define DUNE_GEOMETRIES_LINE_SEGMENT_HH

#include "line.hh"

/**
 *  This class provides an implementation of a line segment in 2 dimensions.
 *  The line is represented by
 *      { t*b + c | t \in [0,1] }
 *  given b, c \in R^2.
 *  We call b the *direction* and c the *shift* of the given line.
 */
template<class float_x = double>
class LineSegment : public Line<float_x>
{
    public:
    
        typedef Line<float_x> Base;
        using Base::Base;
};

#endif
