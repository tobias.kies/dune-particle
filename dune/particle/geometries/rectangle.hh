#ifndef DUNE_GEOMETRIES_RECTANGLE_HH
#define DUNE_GEOMETRIES_RECTANGLE_HH

/// Note: Technically this describes a quadrangle.

/**
 *  This class provides an implementation of a 2D rectangle in n dimensions.
 *  The rectangle is represented by four vectors a,b,d,c \in R^n.
 */
template<size_t dim = 2, class float_x = double>
class Rectangle
{
    public:
    
        typedef Dune::FieldVector<float_x,dim> Vector;
        
    
    private:

        Vector a_,b_,c_,d_;
        
    
    public:
    
        Rectangle( const Vector& a__, const Vector& b__, const Vector& c__, const Vector& d__ )
            : a_(a__), b_(b__), c_(c__), d_(d__)
        {}


        const Vector& a() const
        {
            return a_;
        }
        
        void a( const Vector& new_a )
        {
            a_ = new_a;
        }

        
        const Vector& b() const
        {
            return b_;
        }
        
        void b( const Vector& new_b )
        {
            b_ = new_b;
        }

        
        const Vector& c() const
        {
            return c_;
        }
        
        void c( const Vector& new_c )
        {
            c_ = new_c;
        }

        
        const Vector& d() const
        {
            return d_;
        }
        
        void d( const Vector& new_d )
        {
            d_ = new_d;
        }
};

#endif
