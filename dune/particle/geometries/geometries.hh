#ifndef DUNE_GEOMETRIES_HH
#define DUNE_GEOMETRIES_HH

/**
 * This file's only purpose is to give a single file that can be included in order to include all geometries at once.
 */

namespace Geometries
{
    #include <dune/particle/geometries/ellipse.hh>
    #include <dune/particle/geometries/line.hh>
    #include <dune/particle/geometries/linesegment.hh>
    #include <dune/particle/geometries/polygon.hh>
    #include <dune/particle/geometries/realrectangle.hh>
    #include <dune/particle/geometries/rectangle.hh>
}

#endif
