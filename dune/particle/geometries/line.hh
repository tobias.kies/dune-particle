#ifndef DUNE_GEOMETRIES_LINE_HH
#define DUNE_GEOMETRIES_LINE_HH


/**
 *  This class provides an implementation of a line in 2 dimensions.
 *  The line is represented by
 *      { t*b + c | t \in R }
 *  given b, c \in R^2.
 *  We call b the *direction* and c the *shift* of the given line.
 */
template<class float_x = double>
class Line
{
    public:
    
        static const size_t dim = 2;
        typedef Dune::FieldVector<float_x,dim> Vector;
    
    
    private:
    
        Vector b_, c_;
    
    public:
    
        Line( const Vector& b__, const Vector& c__ )
            : b_(b__), c_(c__)
        {}


        const Vector& direction() const
        {
            return b_;
        }
        
        void direction( const Vector& b_new )
        {
            b_  = b_new;
        }
        
        
        const Vector& shift() const
        {
            return c_;
        }
        
        void shift( const Vector& c_new )
        {
            c_ = c_new;
        }
};

#endif
