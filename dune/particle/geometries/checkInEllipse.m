% This script derives a formula for checking whether a point is inside an ellipse or not.%
% The ellipse is given by the ellipse described by the length axes a and b (w.r.t x and y), rotated by the angle phi (radians) around the center c = [cx;cy].
% The point to be tested is in the following given by [x;y].
syms a b phi x y cx cy

assume( [a b phi x y cx cy], 'real');
assume( a > 0 & b > 0 );

xy = [x;y];
c = [cx;cy];
A = [1/a^2, 0; 0, 1/b^2];
R = [cos(phi), -sin(phi); sin(phi), cos(phi)];

% We define ellipse in such a way, that ellipse < 0 on the inside and > 0 outside.
ellipse = (R*(xy-c))'*A*(R*(xy-c)) - 1;

% Now all we have to do is to export this expression into a c file.
ccode( ellipse, 'file', 'inEllipse.c' );
