#ifndef DUNE_GEOMETRIES_ELLIPSE_HH
#define DUNE_GEOMETRIES_ELLIPSE_HH


/**
 *  This class provides an implementation of an ellipse in 2D.
 *  The ellipse is characterized by
 *      (R (x-c_x,y-c_y)')' * A * R(x-c_x,y-c_y)' = 1
 *  where
 *      A = [1/a^2, 0; 0, 1/b^2]
 *      R = [cos(-phi), -sin(-phi); sin(-phi), cos(-phi)].
 *  We require a,b \in R_{>0}, center = (c_x,c_y) \in R^2 and
 *  rotation = phi \in R.
 * 
 *  This means the ellipse is given by
 *      
 */
template<class float_x = double>
class Ellipse
{
    public:
        typedef Dune::FieldVector<float_x,2> Center;
        typedef Center Vector;
        
    private:

        float_x a_, b_;
        Center center_;
        float_x rotation_;
    
    public:
    
        Ellipse( const float_x& a__, const float_x& b__, const float_x& rotation__ = 0, const Center& center__ = {0,0} )
            : a_(a__), b_(b__), rotation_(rotation__), center_(center__)
        {}
        
        Ellipse( const std::array<float_x,2>& r, const float_x& rotation__ = 0, const Center& center__ = {0,0} )
            : Ellipse( r[0], r[1], rotation__, center__ )
        {}
        
        /**
         *  This version of a constructor exists for compatibility reasons with the EllipticMembraneParticle interface.
         *  This constructor may be removed in future.
         */ 
        template<class T>
        Ellipse( const T& ellipse, const float_x& rotation__ = 0, const Center& center__ = {0,0} )
            : Ellipse( ellipse.get_a(), ellipse.get_b(), rotation__, center__ )
        {}


        const float_x& a() const
        {
            return a_;
        }
        
        void a( const float_x& new_a )
        {
            if( new_a <= 0 )
                DUNE_THROW( Dune::RangeError, "Ellipse parameters must be positive." );
            a_ = new_a;
        }

        
        const float_x& b() const
        {
            return b_;
        }
        
        void b( const float_x& new_b )
        {
            if( new_b <= 0 )
                DUNE_THROW( Dune::RangeError, "Ellipse parameters must be positive." );
            b_ = new_b;
        }

        
        const Center& center() const
        {
            return center_;
        }
        
        void center( const Center& new_center )
        {
            center_ = new_center;
        }
        
        
        /**
         * Checks if a point is contained in the ellipse.
         * Returns
         *  true if point is inside or on the boundary
         *  false if point is outside
         */
        template<class Point>
        bool contains( const Point& point ) const
        {
            const float_x
                x = point[0],
                y = point[1],
                phi = rotation_,
                cx = center_[0],
                cy = center_[1];
                
            const float_x
                t3 = cos(phi),
                t4 = cy-y,
                t5 = sin(phi),
                t6 = cx-x,
                t2 = t3*t6-t4*t5,
                t7 = t3*t4+t5*t6,
                t0 = 1.0/(a_*a_)*(t2*t2)+1.0/(b_*b_)*(t7*t7)-1.0;

            return (t0 <= 0 );
        }
        
        
        const float_x& rotation() const
        {
            return rotation_;
        }
        
        void rotation( const float_x& new_rotation )
        {
            rotation_ = new_rotation;
        }
};

#endif
