#ifndef DUNE_GEOMETRIES_REAL_RECTANGLE_HH
#define DUNE_GEOMETRIES_REAL_RECTANGLE_HH

/**
 *  This class provides an implementation of a 2D rectangle in n dimensions.
 *  The rectangle is represented by three vectors in R^n.
 */
template<class float_x = double>
class RealRectangle
{
    public:
    
        static const size_t dim = 2;
        typedef Dune::FieldVector<float_x,dim> Vector;
        
    
    private:

        Vector bottomLeft_, bottomRight_, upperRight_;
        mutable Vector upperLeft_;
        
    
    public:
    
        RealRectangle()
        {}
    
        RealRectangle( const Vector& bottomLeft__, const Vector& bottomRight__, const Vector& upperRight__ )
        : bottomLeft_(bottomLeft__), bottomRight_(bottomRight__), upperRight_(upperRight__)
        {}
        
        RealRectangle( const Vector& center, const float_x a, const float_x b, const float_x alpha )
        :
            bottomLeft_ ( {center[0] - cos(alpha)*a + sin(alpha)*b, center[1] - sin(alpha)*a - cos(alpha)*b} ),
            bottomRight_( {center[0] + cos(alpha)*a + sin(alpha)*b, center[1] + sin(alpha)*a - cos(alpha)*b} ),
            upperRight_ ( {center[0] + cos(alpha)*a - sin(alpha)*b, center[1] + sin(alpha)*a + cos(alpha)*b} )
        {}

        const Vector& corner( const size_t i ) const
        {
            switch(i)
            {
                case 0:
                    return bottomLeft();
                    
                case 1:
                    return bottomRight();
                    
                case 2:
                    return upperRight();
                    
                case 3:
                    return upperLeft();
                    
                default:
                    DUNE_THROW( Dune::RangeError, "Invalid corner id." );
            }
            return bottomLeft();
        }

        const Vector& bottomLeft() const
        {
            return bottomLeft_;
        }
        
        void bottomLeft( const Vector& new_bottomLeft )
        {
            bottomLeft_ = new_bottomLeft;
        }


        const Vector& bottomRight() const
        {
            return bottomRight_;
        }
        
        void bottomRight( const Vector& new_bottomRight )
        {
            bottomRight_ = new_bottomRight;
        }


        const Vector& upperLeft() const
        {
            upperLeft_ = upperRight_ + bottomLeft_ - bottomRight_;
            return upperLeft_;
        }


        const Vector& upperRight() const
        {
            return upperRight_;
        }
        
        void upperRight( const Vector& new_upperRight )
        {
            upperRight_ = new_upperRight;
        }

};

#endif
