#ifndef DUNE_FICTITIOUS_NITSCHE_DOMAIN_INFORMATION_HH
#define DUNE_FICTITIOUS_NITSCHE_DOMAIN_INFORMATION_HH

#include <algorithm>
#include <cmath>

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parametertree.hh>

#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>

#include <dune/particle/quadrature/localparameterizationquadrature.hh>

#include <dune/particle/helper/bisection.hh>


/** 
 *  The big question here is: Should we save EntitySeeds or rather set up a BitSetVector?
 *  It would probably be more memory efficient to use BitSetVectors, but in case of many loops over those entities, it may be beneficial to avoid a lot of trivial cycles in those loops.
 * 
 *  Also, the overall organization in this class ended up being enormously chaotic.
 * */
template<class Basis_, class LevelSetFunctions, class float_x = double>
class DomainInformation
{
    public:    
        typedef Basis_ Basis;


    private:
        typedef typename LevelSetFunctions::value_type::element_type LevelSetFunction;
        typedef typename LevelSetFunction::RangeType LSRT;
        typedef std::vector<std::vector<LSRT>> LevelSetValues;
        
        typedef typename Basis::GridView GridView;
        typedef typename GridView::Grid Grid;
        
        typedef typename GridView::template Codim<0>::Entity Element;
        typedef typename Element::EntitySeed ElementSeed;
        typedef std::vector<ElementSeed> ElementSeedContainer;
        
        typedef typename GridView::template Codim<1>::Entity Face;
        typedef typename Face::EntitySeed FaceSeed;
        typedef std::vector<FaceSeed> FaceSeedContainer;


        enum ElementPosition {inside, outside, boundary};
        typedef std::vector<ElementPosition> ElementPositionContainer;
        
        typedef std::map<size_t,size_t> InverseBoundaryDOFs;
        
        typedef Dune::BitSetVector<1> BitSetVector;
        
        typedef typename LevelSetFunction::DomainType DomainType;
        
        typedef std::map<size_t,std::vector<std::vector<DomainType>>> ElementIntersections;
        
        typedef std::map<size_t,std::vector<std::vector<std::pair<DomainType,DomainType>>>> ElementIntersectionPairs;

    public:
    
        // Empty constructor.
        DomainInformation( ){}
        
        DomainInformation( const Basis& basis_, const LevelSetFunctions& levelSetFunctions_, const Dune::ParameterTree& params )
        : DomainInformation(basis_, levelSetFunctions_, params.template get<float_x>("stabilizationThreshold"))
        {}
        
        DomainInformation( const Basis& basis_, const LevelSetFunctions& levelSetFunctions_, const float_x stabilizationFaceThreshold_ )
        : basis(basis_)
        , levelSetFunctions(levelSetFunctions_)
        , stabilizationFaceThreshold(stabilizationFaceThreshold_)
        {
            // Initialize containers.
            initContainers(levelSetFunctions);
            
            // Do a step that does stuff that does the main work.
            doWork();
            
            // A little post-processing: Ensure that the vectors containing the boundary-DOFs have unique entries.
            makeBoundaryDOFsUnique();
            
            // Create an inverse map for the boundary DOFs.
            updateInverseBoundaryDOFs();
            
            // For convenience, create a unified vector of boundary element seeds.
            // Note that the way it is done here is memory inefficient and could be improved (but then we'd lose the vector structure.
            updateUnifiedBoundaryElementSeeds();
            
            // Determine feasibility.
            determineFeasibility();
            
            // Determine which cut faces we actually use for stabilization.
            updateStabilizationFaces( );
        }

        DomainInformation( const Basis& basis_, const LevelSetFunctions& levelSetFunctions_ )
        : DomainInformation(basis_,levelSetFunctions_, float_x(1))
        {
        }
        
        
        const auto& boundaryDOFs( size_t levelSetId ) const
        {
            return boundaryDOFs_[levelSetId];
        }
        
        const auto& boundaryDOFsBitVector( size_t levelSetId ) const
        {
            return boundaryDOFsBitVector_[levelSetId];
        }
        
        const auto& boundaryElementSeeds( ) const
        {
            return unifiedBoundaryElementSeeds_;
        }
        
        const auto& boundaryElementSeeds( size_t levelSetId = 0 ) const
        {
            return boundaryElementSeeds_[levelSetId];
        }
        
        auto elementId( const Element& element ) const
        {
            return basis.getGridView().indexSet().index(element);
        }
        
        template<class IS>
        auto intersectionId( const IS& is ) const
        {
            //~ return basis.getGridView().indexSet().subIndex( is.inside(), is.indexInInside(), 1 );
            //if( basis.getGridView().grid().globalIdSet().subId( is.inside(), is.indexInInside(), 1 ) != basis.getGridView().grid().globalIdSet().subId( is.outside(), is.indexInOutside(), 1 ) )
            //    DUNE_THROW( Dune::Exception, "IDs do not match?" );
            return basis.getGridView().grid().globalIdSet().subId( is.inside(), is.indexInInside(), 1 );
        }
        
        const auto& insideElementSeeds( ) const
        {
            return insideElementSeeds_;
        }
        
        const auto& getBasis( ) const
        {
            return basis;
        }
        
        bool isBoundary( const Element& element ) const
        {
            return isBoundary( elementId(element) );
        }
        
        bool isBoundary( const size_t elementId ) const
        {
            return (elementPositions_[elementId] == ElementPosition::boundary);
        }
        
        bool isBoundary( const Element& element, const size_t levelSetId ) const
        {
            return isBoundary( elementId(element), levelSetId );
        }
        
        bool isBoundary( const size_t elementId, const size_t levelSetId ) const
        {
            return isBoundaryElement_[levelSetId][elementId][0];
        }
        
        bool isFeasible( ) const
        {
            return fIsFeasible_;
        }
        
        bool isInside( const Element& element ) const
        {
            return isInside( elementId(element) );
        }
        
        bool isInside( const size_t elementId ) const
        {
            return (elementPositions_[elementId] == ElementPosition::inside);
        }
        
        bool isOutside( const Element& element ) const
        {
            return isOutside( elementId(element) );
        }
        
        bool isOutside( const size_t elementId ) const
        {
            return (elementPositions_[elementId] == ElementPosition::outside);
        }
        
        template<class IS>
        bool isStabilizationFace( const IS& is ) const
        {
            if( basis.getGridView().grid().globalIdSet().subId( is.inside(), is.indexInInside(), 1 ) != basis.getGridView().grid().globalIdSet().subId( is.outside(), is.indexInOutside(), 1 ) )
                return false; // This happens with non-conforming grids.
            return isStabilizationFace_.at( intersectionId(is) );
        }
        
        const auto& levelSetFunction( const Element& element ) const
        {
            const auto elementId_ = elementId(element);
            for( size_t levelSetId = 0; levelSetId < levelSetFunctions.size(); ++levelSetId )
                if( isBoundary( elementId_, levelSetId ) )
                    return *levelSetFunctions[levelSetId];
            DUNE_THROW( Dune::Exception, "Requested level set function on non-boundary element." );
            return *levelSetFunctions[0];
        }
        
        const auto& levelSetFunction( const size_t levelSetId ) const
        {
            return *levelSetFunctions[levelSetId];
        }
        
        
        const auto numLevelSets( ) const
        {
            return levelSetFunctions.size();
        }

        const auto& intersectionPairs( const Element& element, const size_t levelSetId ) const
        {
            return (elementIntersectionPairs.at(elementId(element)))[levelSetId];
            //~ elementIntersectionPairs[elementId(element)].resize(std::max(levelSetId,elementIntersectionPairs[elementId(element)].size()));
            //~ return (elementIntersectionPairs[elementId(element)])[levelSetId];
        }

    private:
    
        const Basis& basis;
        const LevelSetFunctions& levelSetFunctions;

        ElementSeedContainer insideElementSeeds_;
        FaceSeedContainer cutFaceSeeds_;

        std::vector<ElementSeedContainer> boundaryElementSeeds_;
        std::vector<std::vector<size_t>> boundaryDOFs_; /// ToDo: may want to use a set instead of a vector.
        std::vector<InverseBoundaryDOFs> inverseBoundaryDOFs_;
        std::vector<BitSetVector> isBoundaryElement_;
        std::vector<BitSetVector> boundaryDOFsBitVector_;
        
        std::map<int,bool> isStabilizationFace_;
        float_x stabilizationFaceThreshold = 1;
        
        bool fIsFeasible_ = true;
        
        ElementPositionContainer elementPositions_;
        ElementSeedContainer unifiedBoundaryElementSeeds_;
        
        ElementIntersections elementIntersections;
        mutable ElementIntersectionPairs elementIntersectionPairs;
        
    
        void doWork( )
        {
            // Iterate over all elements and intersections.
            const auto& gridView    = basis.getGridView();
            for( const auto& element : elements(gridView) )
            {
                LevelSetValues levelSetValues(levelSetFunctions.size());
                std::vector<DomainType> pointsOfEvaluation;

                for( const auto& is : intersections(gridView,element) )
                {
                    // Evaluate the level set functions.
                    // Note: We perform a lot of unnecessary evaluations because we very often will evaluate the same functions in the same points for different elements.
                    evaluateLevelSetFunctionsOnInterface( is, levelSetFunctions, levelSetValues, pointsOfEvaluation );
                }
                    
                // Determine the element's position.
                const auto&& elementPosition = determineElementPosition(element, levelSetValues);
                
                // If the element is on an interface, then we compute the intersections.
                if( elementPosition == ElementPosition::boundary )
                    updateElementIntersections(element, levelSetValues, pointsOfEvaluation);
                
                // Finally, update the element position in some more containers.
                updateElementPositions(element, elementPosition);
            }
        }
        
        
        void updateElementIntersections( const Element& element, const LevelSetValues& levelSetValues, const std::vector<DomainType>& pointsOfEvaluation )
        {
            // Prepare index map.
            std::vector<size_t> indices(pointsOfEvaluation.size());
            for( size_t i = 0; i < indices.size(); ++i )
                indices[i]  = i;
            
            // First find a permutation that orders the points of evaluation.
            const auto&& center = element.geometry().center();
            sort( indices.begin(), indices.end(),
                [&center, &pointsOfEvaluation]( const size_t& i, const size_t& j ){
                        const auto& a = pointsOfEvaluation[i];
                        const auto& b = pointsOfEvaluation[j];
                        double angle_a = std::atan2( a[1]-center[1], a[0]-center[0] ),
                               angle_b = std::atan2( b[1]-center[1], b[0]-center[0] );
                        return ( angle_a < angle_b );
                    }
            );
            
            // Then remove duplicates.
            const double tolDuplicates = 1.0e-8;
            indices.erase(
                unique(
                    indices.begin(),
                    indices.end(),
                    [tolDuplicates, &pointsOfEvaluation]( const size_t& i, const size_t& j )
                    {
                        const auto& a = pointsOfEvaluation[i];
                        const auto& b = pointsOfEvaluation[j];
                        const auto&& c = a-b;
                        return ( c.one_norm() < tolDuplicates );
                    }
                ),
                indices.end()
            );
            
            // Now iterate over all points, detect sign changes and find the intersections.
            const size_t N = indices.size();
            const auto&& next = [&N]( size_t id )
                {
                    return (id+1) % N;
                };
            const auto&& point = [&indices, &pointsOfEvaluation]( const size_t i )
                {
                    return pointsOfEvaluation[indices[i]];
                };
            
            const auto eId = elementId(element);
            const double tolPerpendicular = 1.0e-8;
            elementIntersections[eId].resize(levelSetFunctions.size());
            for( size_t levelSetId = 0; levelSetId < levelSetFunctions.size(); ++levelSetId )
            {
                const auto&& value = [&indices, &levelSetValues, &levelSetId]( size_t i )
                    {
                        return levelSetValues[levelSetId][indices[i]];
                    };
                const auto&& f  = [this, &levelSetId](const DomainType& x)
                    {
                        return levelSetFunctions[levelSetId]->evaluate(x);
                    };
                    
                std::vector<size_t> intersectionIds;
                for( size_t i = 0; i < N; ++i )
                {
                    #warning Extreme case: One of the values equals zero. Do we fear that?
                    // Continue if signs do not change.
                    if( !(value(i) * value(next(i)) <= 0) )
                        continue;
                        
                    // Compute the intersection.
                    const auto& a = point(i);
                    const auto& b = point(next(i));
                    const auto&& intersection = Bisection::bisectionMethod( f, a, b );
                    
                    // Add it to the list.
                    elementIntersections[eId][levelSetId].push_back( intersection );
                    intersectionIds.push_back(i);
                }
                
                #warning TODO: Need to add the feature where perpendicular points can be removed.
                /*
                // If there is an odd number of intersections, then we check if some intersectoins are orthogonal. In that case we drop them.
                Dune::BitSetVector<1> dropable(intersectionIds.size(), false);
                if( elementIntersections[eId][levelSetId].size() % 2 == 1 )
                {
                    for( size_t k = 0; k < intersectionIds.size(); ++k )
                    {
                        const auto i = intersectionIds[k];
                        
                        const auto& a = point(i);
                        const auto& b = point(next(i));
                        auto&& c = b-a;

                        // Compute the derivative of f in the intersection.
                        const auto&& gradient = levelSetFunctions[levelSetId]->evaluateGradient(elementIntersections[eId][levelSetId][k]);
                        
                        // Determine if the gradient is perpendicular to the face that it is on.
                        // (Here we make use that the corners of the element are always a subset
                        // of the points of evaluation.)
                        bool fIsPerpendicular = ( std::abs(c*gradient) < tolPerpendicular );
                        
                        // If the gradient in the intersection is perpendicular to the face that it is on
                        // we remove it.
                        if( fIsPerpendicular )
                            dropable[k] = true;
                    }
                }
                
                auto& v = elementIntersections[eId][levelSetId];
                auto pos = remove_if(v.begin(), v.end(), [&dropable, &v](const DomainType& x){return dropable[std::distance(&v[0], &x)]; }));
                v.erase(pos, v.end());*/
            }
            
            // Finally, we compute the intersection pairs.
            // Those are pairs of intersections that we expect to be
            // endpoints of a curve that goes through the element.
            #warning This is not optimal yet. Also it may break if there is an odd number of intersections on the element.
            elementIntersectionPairs[eId].resize(levelSetFunctions.size());
            for( size_t levelSetId = 0; levelSetId < levelSetFunctions.size(); ++levelSetId )
            {
                const int M = elementIntersections[eId][levelSetId].size();
                if( M < 2 )
                    continue;
                
                int pointId = 0;

                const auto&& intersection = [&M, &levelSetId, this, &eId]( const size_t i )
                {
                    return elementIntersections[eId][levelSetId][i % M];
                };
                
                if( M > 2 )
                {
                    // Check if the level set rather corresponds to [a,b] or to [a,c].
                    const auto& a = point(0);
                    const auto& b = point(1);
                    const auto& c = point(M-1);
                    
                    const auto&& ab = b-a;
                    const auto&& ac = c-a;
                    
                    const auto& gradient = levelSetFunctions[levelSetId]->evaluateGradient(a);
                    
                    const auto gab = std::abs(gradient*ab) / ab.two_norm();
                    const auto gac = std::abs(gradient*ac) / ac.two_norm();
                    bool fSkip  = ( gac < gab );
                    
                    // If the latter is the case (and M is even), then we let the pointId's start at 1.
                    if( fSkip && (M % 2 == 0) )
                        pointId = 1;
                        
                    // If M equals 3, we additionally check out bc.
                    if( M == 3 )
                    {
                        const auto&& bc = b-c;
                        const auto gbc = std::abs(gradient*bc) / bc.two_norm();
                        
                        if( gbc < std::min(gab, gac) && gbc > 0)
                            pointId = 1;
                        else if( gab < gac )
                            pointId = 0;
                        else // gac
                            pointId = 2;
                    }
                }
                
                // Add intersection pairs.
                // Note again that this may behave oddly for odd M.
                elementIntersectionPairs[eId][levelSetId].resize(0);
                for( size_t i = 0; i < M/2; ++i )
                {
                    elementIntersectionPairs[eId][levelSetId].push_back( std::make_pair<DomainType,DomainType>(intersection(pointId), intersection(pointId+1)) );
                    pointId += 2;
                }
            }
        }

        // This method does way more than just checking the element's position. Should consider to split this part up, somehow.
        auto determineElementPosition( const Element& element, const LevelSetValues& levelSetValues ) //const
        {
            ElementPosition elementPosition = ElementPosition::inside;
            
            const auto&& mini = [&levelSetValues]( size_t id )
                {
                    typename LevelSetValues::value_type::value_type val = INFINITY;
                    for( const auto& c : levelSetValues[id] )
                        val = (std::isnan(c)) ? val : std::min( val, c );
                    return val;
                };
            const auto&& maxi = [&levelSetValues]( size_t id )
                {
                    typename LevelSetValues::value_type::value_type val = -INFINITY;
                    for( const auto& c : levelSetValues[id] )
                        val = (std::isnan(c)) ? val : std::max( val, c );
                    return val;
                };
                
            const auto&& allnan = [&levelSetValues](size_t id)
                {
                    for( const auto& c : levelSetValues[id] )
                        if(!std::isnan(c))
                          return false;
                    return true;
                };

            // Iterate over all level sets and check their position.
            for( size_t levelSetId = 0; levelSetId < levelSetValues.size(); ++levelSetId )
            {
                const auto&& minVal = mini(levelSetId);
                const auto&& maxVal = maxi(levelSetId);
                if(allnan(levelSetId))
                  continue;

                // If the sign of the two values is different, then the current element is a boundary element for the given level set.
                if( minVal * maxVal < 0 )
                {
                    #warning ToDo: Move these parts somewhere else.
                    // Add boundary element seed.
                    boundaryElementSeeds_[levelSetId].push_back( element.seed( ) );
                    
                    // Add interface DOFs.
                    updateBoundaryDOFs( basis, element, levelSetId );
                    
                    // Set bit.
                    const auto elementId    = basis.getGridView().indexSet().index(element);
                    isBoundaryElement_[levelSetId][elementId]   = true;
                    
                    // Update element position.
                    if( elementPosition != ElementPosition::outside )
                    {
                        elementPosition = ElementPosition::boundary;
                        //~ std::cerr << "Adding element on id " << levelSetId << ": "
                          //~ << element.geometry().corner(0) << "; "
                          //~ << element.geometry().corner(1) << "; "
                          //~ << element.geometry().corner(2) << "; "
                          //~ << element.geometry().corner(3) << "; "
                          //~ << "values " << levelSetValues[levelSetId].size()
                          //~ << std::endl;
                        //~ Debug::dprintVector(levelSetValues[levelSetId]);
                    }
                }

                // If both values are non-negative, we know that the element lies outside.
                if( minVal >= 0 )
                    elementPosition = ElementPosition::outside;
            }

            return elementPosition;
        }


        auto computeGlobalIndices( const Element& element ) const
        {
            const auto& indexSet    = basis.getGridView().indexSet();
            const auto&& geometry   = element.geometry();
            const auto&& nCorners   = geometry.corners();
            const size_t dim        = GridView::dimensionworld;

            // Compute all global indices associated to this element.
            std::vector<size_t> globalIndices(nCorners);
            for( size_t cornerId = 0; cornerId < nCorners; ++cornerId )
                globalIndices[cornerId] = indexSet.subIndex(element, cornerId, dim);

            return globalIndices;
        }
        
        
        void determineFeasibility( )
        {
            const size_t nLevelSets = levelSetFunctions.size();
            const size_t nDOFs      = basis.size();
            for( size_t dofId = 0; dofId < nDOFs; ++dofId )
            {
                size_t counter = 0;
                for( size_t levelSetId = 0; levelSetId < nLevelSets; ++levelSetId )
                    if( boundaryDOFsBitVector_[levelSetId][dofId][0] )    // Note: it is not just enough to check if two particles share the same element... we really want to know if they share the same DOF
                        counter++;
                
                if( counter > 1 )
                {
                    fIsFeasible_ = false;
                    break;
                }
            }
        }


        /**template<class GlobalIndices>
        auto evaluateLevelSetFunctions( const GlobalIndices& globalIndices, const Element& element, const LevelSetFunctions& levelSetFunctions, LevelSetValues& levelSetValues ) const
        {
            const auto&& geometry = element.geometry();
            const auto&& nCorners = geometry.corners();

            // Iterate over all corners and get their global index.
            // Update levelSetValues if value is still missing.
            for( size_t cornerId = 0; cornerId < nCorners; ++cornerId )
            {
                // Get global index and global position of current corner.
                const auto& globalIdx  = globalIndices[cornerId];
                const auto&& globalPos  = geometry.corner(cornerId);  // [remark] Code would be more efficient if we would compute the position only when we actually need to evaluate any level sets.

                // Iterate over all level sets and evaluate, if necessary.
                for( size_t levelSetId = 0; levelSetId < levelSetValues.size(); ++levelSetId )
                {
                    const auto& levelSet = levelSetFunctions[levelSetId];
                    auto& levelSetVector = levelSetValues[levelSetId];
                    if( !std::isnan(levelSetVector[globalIdx]) )
                        continue;

                    levelSetVector[globalIdx]   = levelSet->evaluate(globalPos);
                }
            }
        }*/
        
        template<class Interface>
        void evaluateLevelSetFunctionsOnInterface( const Interface& is, const LevelSetFunctions& levelSetFunctions, LevelSetValues& levelSetValues, std::vector<DomainType>& pointsOfEvaluation ) const
        {
            static_assert( Interface::mydimension == 1, "We only support one-dimensional interfaces." );
            
            // To-do: Make the refinement factor a parameter of something.
            const size_t m = 3;
            
            const auto&& a = is.geometry().corner(0);
            const auto&& b = is.geometry().corner(1);
            
            for( size_t i = 0; i <= m; ++i )
            {
                DomainType x(0);
                double lambda = ((double) i) / ((double) m);
                x.axpy(1-lambda,a);
                x.axpy(lambda,b);
                pointsOfEvaluation.push_back(x);
                
                for( size_t levelSetId = 0; levelSetId < levelSetFunctions.size(); ++levelSetId) 
                {
                    //~ std::cerr << "checking levelSet " << levelSetId << " in point " << x << std::endl;
                    const auto value = levelSetFunctions[levelSetId]->evaluate(x);
                    levelSetValues[levelSetId].push_back(value);
                    
                    
                    //~ // DEBUG PART
                    //~ if( (x[0] < -1.5 && x[1] > 1.5 && value > 0) || (x[0] < -1.5 && x[1] < -1.5 && value > 0) )
                    //~ {
                      //~ std::cerr << "Problems from levelSetId " << levelSetId << " value " << value << std::endl;
                      //~ Debug::dprintVector(x, "position");
                      //~ DUNE_THROW(Dune::Exception, "Stop.");
                    //~ }
                }
            }
        }


        auto initContainers( const LevelSetFunctions& levelSetFunctions )
        {
            const auto& gridView        = basis.getGridView();
            const size_t nLevelSets     = levelSetFunctions.size();
            const size_t dim            = GridView::dimensionworld;
            const size_t nGridElements  = gridView.size(0);
            const size_t nGridNodes     = gridView.size(dim);
            const size_t nDOFs          = basis.size();
            
            boundaryElementSeeds_.resize(nLevelSets);
            boundaryDOFs_.resize(nLevelSets);
            inverseBoundaryDOFs_.resize(nLevelSets);
            isBoundaryElement_.resize(nLevelSets);
            boundaryDOFsBitVector_.resize(nLevelSets);
            
            elementIntersections.clear();
                
            // Reserve memory for levelSet-wise boundary information.
            for( auto& vector : isBoundaryElement_ )
                vector.resize(nGridElements, false);
                
            // Reserve memory for boundary DOF bit set vectors.
            for( auto& vector : boundaryDOFsBitVector_ )
                vector.resize(nDOFs, false);
                
            // Reserve memory for element positions.
            elementPositions_.resize(nGridElements, ElementPosition::inside );
        }
        
        
        void makeBoundaryDOFsUnique( )
        {
            for( auto& boundaryDOFs_Vector : boundaryDOFs_ )
            {
                sort( boundaryDOFs_Vector.begin(), boundaryDOFs_Vector.end() );
                boundaryDOFs_Vector.erase(
                    std::unique( boundaryDOFs_Vector.begin(), boundaryDOFs_Vector.end() ),
                    boundaryDOFs_Vector.end()
                );
            }
        }


        // Computing the indices for various levelSetId's may be inefficient. But we assume that there is at most one level set per element for which we actually have to compute those indices.
        void updateBoundaryDOFs( const Basis& basis, const Element& element, const size_t levelSetId )
        {
            const auto& localBasis          = basis.getLocalFiniteElement(element);
            auto& boundaryDOFs_Vector       = boundaryDOFs_[levelSetId];
            auto& boundaryDOFs_BitVector    = boundaryDOFsBitVector_[levelSetId];

            // Get global indices and add them to the vector.
            for( size_t i = 0; i < localBasis.size(); ++i )
            {
                auto globalIndex    = basis.index(element,i);
                if( basis.isConstrained(globalIndex) )
                {
                    const auto& constraints = basis.constraints(globalIndex);
                    for( const auto& linearFactor : constraints )
                    {
                        boundaryDOFs_BitVector[linearFactor.index]   = true;
                        boundaryDOFs_Vector.push_back(linearFactor.index);
                    }
                }
                else
                {
                    boundaryDOFs_BitVector[basis.index(element,i)]   = true;
                    boundaryDOFs_Vector.push_back( basis.index(element,i) );
                }
            }
        }
        
        
        void updateStabilizationFaces( )
        {
            const auto& gridView = basis.getGridView();
            
            isStabilizationFace_.clear();
            
            // First compute the quadrature sums for all elements.
            std::map<int,float_x> quadratureSums;
            for( const auto& element : elements(gridView) )
            {
                // Only consider boundary elements.
                if( !isBoundary(element) )
                    continue;
                    
                // Get volume quadrature for the current point.
                const auto& fe = basis.getLocalFiniteElement(element);
                QuadratureRuleKey quadFE(fe);
                QuadratureRuleKey quadKey   = quadFE.square();
                Dune::QuadratureRule<float_x,Grid::dimension> quadratureRule;
                #warning We have the assumption that there is at most one level set function per element.
                quadratureRule  = LocalParameterizationQuadrature<float_x>::rule( quadKey, element, *this );
                
                // Sum up the weights.
                float_x sum = 0;
                for( const auto& point : quadratureRule )
                    sum += point.weight();
                    
                quadratureSums[elementId(element)] = sum;
            }
            
            // Now determine which faces are used for stabilization.
            for( const auto& element : elements(gridView) )
            {
                // Only consider boundary elements.
                if( !isBoundary(element) )
                    continue;
                    
                const auto eid = elementId(element);
                    
                float_x maxSum = 0;
                int maxId = -1;
                bool fFoundInside = false;
                for( const auto& is : intersections(gridView,element) )
                {
                    const auto iid = intersectionId(is);
                    isStabilizationFace_[iid]  = false;
                    
                    if( quadratureSums[eid] > stabilizationFaceThreshold )
                        continue;
                    
                    if( is.boundary() )
                        continue;
                    
                    isStabilizationFace_[iid]  = true;
                    fFoundInside = true;
                    continue;
                        
                    if( isInside( is.outside() ) )
                    {
                        isStabilizationFace_[iid]  = true;
                        fFoundInside = true;
                    }
                    else if( isBoundary( is.outside() ) )
                    {
                        if( quadratureSums[elementId(is.outside())] > maxSum )
                        {
                            maxId   = iid;
                            maxSum  = quadratureSums[elementId(is.outside())];
                        }
                    }
                }
                
                if( !fFoundInside )
                    isStabilizationFace_[maxId]  = true;
            }
        }
        
        
        void updateElementPositions( const Element& element, const ElementPosition& elementPosition )
        {
            const auto& indexSet    = basis.getGridView().indexSet();

            // Write the element position into the positions vector.
            const auto&& elementIndex       = indexSet.index(element);
            elementPositions_[elementIndex] = elementPosition;

            // If element is inside, save the element seed.
            if( elementPosition != ElementPosition::outside )
            {
                const auto&& elementSeed = element.seed();
                insideElementSeeds_.push_back( elementSeed );
            }
        }
        
        
        void updateInverseBoundaryDOFs( )
        {
            const size_t nLevelSets = boundaryDOFs_.size();
            for( size_t levelSetId = 0; levelSetId < nLevelSets; ++levelSetId )
            {
                const auto& boundaryDOFsVector   = boundaryDOFs_[levelSetId];
                auto& invBoundaryDOFsVector = inverseBoundaryDOFs_[levelSetId];
                
                for( size_t i = 0; i < boundaryDOFsVector.size(); ++i )
                    invBoundaryDOFsVector[boundaryDOFsVector[i]]    = i;
            }
        }


        void updateUnifiedBoundaryElementSeeds( )
        {
            for( const auto& boundaryElementSeedsVector : boundaryElementSeeds_ )
                unifiedBoundaryElementSeeds_.insert( unifiedBoundaryElementSeeds_.end(), boundaryElementSeedsVector.begin(), boundaryElementSeedsVector.end() );
        }

};

#endif
