#ifndef DUNE_MISTMACH_MINIMIZATION_PROBLEM_HH
#define DUNE_MISMATCH_MINIMIZATION_PROBLEM_HH


#include "IpTNLP.hpp"
#include "IpIpoptApplication.hpp"
#include "IpSolveStatistics.hpp"


// Projects a vector onto a level set intersected with a hyper cube using ipopt.
template<class Function, class float_x = double>
class MismatchMinimizationProblem : public Ipopt::TNLP
{
  private:
  
    typedef typename Function::DomainType DomainType;
    
    typedef std::pair<typename std::tuple_element<2,DomainType>::type, typename std::tuple_element<3,DomainType>::type> ReducedDomainType;


  public:
  
    MismatchMinimizationProblem(const Function& function, const DomainType& x0, ReducedDomainType& x_final, Ipopt::Number& objectiveValue )
    : function(function), x0(x0), x_final(x_final), objectiveValue(objectiveValue)
    {
      nDomainType   = 3;
      nConstraints  = 0;
    }
    
    
    /**@name Overloaded from TNLP */
    //@{
    /** Method to return some info about the nlp */
    virtual bool get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
                              Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style)
    {
      n = nDomainType;
      
      m = nConstraints;
      
      nnz_jac_g = m*n;
      nnz_h_lag = n*(n+1)/2;
      
      index_style = Ipopt::TNLP::C_STYLE;
      
      return true;
    }


    /** Method to return the bounds for my problem */
    virtual bool get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
                                 Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u)
    {
      // Set lower/upper bounds on x.
      for( size_t i = 0; i < n; ++i )
      {
        x_l[i]  = -std::numeric_limits<double>::max();
        x_u[i]  = +std::numeric_limits<double>::max();
      }

      return true;
    }


    /** Method to return the starting point for the algorithm */
    virtual bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
                                    bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
                                    Ipopt::Index m, bool init_lambda,
                                    Ipopt::Number* lambda)
    {
      if( init_x )
        toIpoptType(x0, x);
      
      if( init_z )
        DUNE_THROW( Dune::NotImplemented, "Can not supply z." );
        
      if( init_lambda )
        DUNE_THROW( Dune::NotImplemented, "Can not supply lambda." );
      
      return true;
    }


    /** Method to return the objective value */
    virtual bool eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value)
    {
      DomainType&& x_hat = toDomainType(x);
      obj_value = 0.5*function.evaluate(x_hat).two_norm();
          
      return true;
    }


    /** Method to return the gradient of the objective */
    virtual bool eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f)
    {
      DomainType&& x_hat    = toDomainType(x);
      const auto&& fval     = function.evaluate(x_hat);
      const auto&& jacobian = function.jacobianInLastTwoComponents(x_hat);
      for(int j = 0; j < 3; ++j)
      {
        grad_f[j] = 0;
        for(int i = 0; i < 3; ++i)
          grad_f[j] += jacobian[i][j] * fval[i];
      }

      return true;
    }


    /** Method to return the constraint residuals */
    virtual bool eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g)
    {
      return true;
    }


    /** Method to return:
     *   1) The structure of the jacobian (if "values" is NULL)
     *   2) The values of the jacobian (if "values" is not NULL)
     */
    virtual bool eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                            Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow, Ipopt::Index *jCol,
                            Ipopt::Number* values)
    {
      return true;
    }

    /** Method to return:
     *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
     *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
     */
    virtual bool eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                        Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
                        bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
                        Ipopt::Index* jCol, Ipopt::Number* values)
    {
      if( values == NULL )
      {
        int counter = 0;
        for(int i = 0; i < nDomainType; ++i)
        {
          for(int j = 0; j <= i; ++j)
          {
            iRow[counter] = i;
            jCol[counter] = j;
            ++counter;
          }
        }
          
      }
      else
      {
        const auto&& x_hat    = toDomainType(x);
        const auto&& fval     = function.evaluate(x_hat);
        const auto&& jacobian = function.jacobianInLastTwoComponents(x_hat);
        const auto&& hessians = function.hessiansInLastTwoComponents(x_hat);
        
        int counter = 0;
        for(int i = 0; i < nDomainType; ++i)
        {
          for(int j = 0; j <= i; ++j)
          {
            values[counter] = 0;
            for(int k = 0; k < nDomainType; ++k)
              values[counter] += hessians[k][i][j] * fval[k] + jacobian[k][i] * jacobian[k][j];
            ++counter;
          }
        }
      }
      
      return true;
    }

    //@}

    /** @name Solution Methods */
    //@{
    /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
    virtual void finalize_solution(Ipopt::SolverReturn status,
                                   Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
                                   Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
                                   Ipopt::Number obj_value,
                                   const Ipopt::IpoptData* ip_data,
                                   Ipopt::IpoptCalculatedQuantities* ip_cq)
    {
      //~ Debug::dprint( "[MismatchMinimization] ErrorCodes:", Ipopt::SUCCESS, Ipopt::MAXITER_EXCEEDED, Ipopt::CPUTIME_EXCEEDED, Ipopt::STOP_AT_TINY_STEP, Ipopt::STOP_AT_ACCEPTABLE_POINT, Ipopt::LOCAL_INFEASIBILITY, Ipopt::USER_REQUESTED_STOP, Ipopt::DIVERGING_ITERATES, Ipopt::RESTORATION_FAILURE, Ipopt::ERROR_IN_STEP_COMPUTATION, Ipopt::INVALID_NUMBER_DETECTED, Ipopt::INTERNAL_ERROR );
      //~ Debug::dprint( "[MismatchMinimization] Summary: nVars =", n, ", nConstraints =", m, ", objectiveValue =", obj_value, ", returnCode=", status );
      
      const auto&& x_final_ = toDomainType(x);
      x_final = std::make_pair( std::get<2>(x_final_), std::get<3>(x_final_) );
      objectiveValue = obj_value;
    }
    //@}
    
    
  private:
  
    const Function& function;
    const DomainType& x0;
    
    ReducedDomainType& x_final;
    Ipopt::Number& objectiveValue;
    
    size_t nDomainType;
    size_t nConstraints;
  
    inline DomainType toDomainType( const Ipopt::Number* x ) const
    {
      // Convert supplied input into domain type.
      DomainType pos;
      std::get<0>(pos)    = std::get<0>(x0);
      std::get<1>(pos)    = std::get<1>(x0);
      std::get<2>(pos)[0] = x[0];
      std::get<2>(pos)[1] = x[1];
      std::get<3>(pos)    = x[2];
      return pos;
    }
    
    template<class Input>
    inline void toIpoptType( const Input& pos, Ipopt::Number* x ) const
    {
      x[0]  = std::get<2>(pos)[0];
      x[1]  = std::get<2>(pos)[1];
      x[2]  = std::get<3>(pos);
    }
};


#endif
