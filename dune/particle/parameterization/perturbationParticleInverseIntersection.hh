#ifndef DUNE_PERTURBATION_PARTICLE_INVERSE_INTERSECTION_HH
#define DUNE_PERTURBATION_PARTICLE_INVERSE_INTERSECTION_HH

#include <tuple>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

template<class Surface, class Particle, class Transformation, class float_x = double>
class PerturbationParticleInverseIntersection
  : public Dune::VirtualMultiDifferentiableFunction
      <
        std::pair<typename Transformation::Descriptor,
                  typename Particle::ParameterizedPoint>,
        std::pair<typename Surface::Point2D,
                  typename Surface::Point1D>,
        std::pair<typename Transformation::DescriptorOT,
                  typename Particle::HeightProfileFunction::OrderType>
      >
{
  
  public:
  
    typedef std::pair<typename Transformation::Descriptor,
                      typename Particle::ParameterizedPoint> DomainType;
    typedef std::pair<typename Surface::Point2D,
                      typename Surface::Point1D> RangeType;
    typedef std::pair<typename Transformation::DescriptorOT,
                      typename Particle::HeightProfileFunction::OrderType> OrderType;

    typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;

    using BaseType::evaluate;

    PerturbationParticleInverseIntersection(const Surface& surface, const Particle& particle, const Transformation& transformation)
      : surface(surface), particle(particle), transformation(transformation)
    {}


    RangeType evaluate(const DomainType& z, const OrderType& d) const
    {
      RangeType y;

      const auto& p = z.first;
      const auto& x = z.second;

      const auto& f = surface.parameterizedProjectionDistanceFunction();
      const auto& Phi = transformation;

      // Compute point on particle-surface.
      typename Surface::Point3D gamma_x;
      gamma_x[0]  = x[0];
      gamma_x[1]  = x[1];
      gamma_x[2]  = particle.heightProfileFunction().evaluate(x);
      
      // Compute transformed point.
      const auto Phi_x = Phi.apply(p,gamma_x);
      
      // Return function value or derivative.
      const auto order = orderOf(d);
      
      const auto&& df = [&f, &Phi_x](const auto& x_, const int i)
      {
        assert(i >= 0 && i <=2);
        return f.evaluate(Phi_x, Dune::FieldVector<size_t,3>({i==0,i==1,i==2}));
      };
      
      switch(order)
      {
        case 0:
        {
          // Project transformed point onto reference surface.
          y = f.evaluate(Phi_x);
          break;
        }
          
        case 1:
        {
          const auto index = indexOf(d);
          
          if(index.first == 0) // a p-derivative
          {
            typename Transformation::OrderType d_ex;
            d_ex.first  = d.first;
            const auto dPhi = Phi.evaluate(std::make_pair(z.first,gamma_x),d_ex);
            
            for(int k = 0; k < 3; ++k)
            {
              const auto df_x = df(Phi_x,k);
              y.first[0]  += df_x.first[0] * dPhi[k];
              y.first[1]  += df_x.first[1] * dPhi[k];
              y.second    += df_x.second   * dPhi[k];
            }
          }
          else if(index.first == 1) // an x-derivative
          {
            typename Transformation::OrderType d_ex1, d_ex2;
            d_ex1.second[index.second]  = 1;
            d_ex2.second[2] = 1;

            const auto dPhi1 = Phi.evaluate(std::make_pair(z.first,gamma_x),d_ex1);
            const auto dPhi2 = Phi.evaluate(std::make_pair(z.first,gamma_x),d_ex2);
            
            const auto dgamma = particle.heightProfileFunction().evaluate(x,d.second);
            
            for(int k = 0; k < 3; ++k)
            {
              const auto df_x = df(Phi_x,k);
              const auto factor = (dPhi1[k] + dPhi2[k] * dgamma);
              y.first[0]  += df_x.first[0] * factor;
              y.first[1]  += df_x.first[1] * factor;
              y.second    += df_x.second   * factor;
            }

          }
          else
            DUNE_THROW(Dune::Exception, "Something went wrong.");
          
          break;
        }
          
        default:
        {
          DUNE_THROW(Dune::NotImplemented, "Not implemented.");
          break;
        }
      }
      
      return y;
    }
  
  private:
  
    const Surface& surface;
    const Particle& particle;
    const Transformation& transformation;
    
    static auto orderOf(const OrderType& d)
    {
      int order = 0;

      const auto& d0 = std::get<0>(d);
      for(int i = 0; i < d0.size(); ++i)
        order += (d0[i] != 0);

      const auto& d1 = std::get<1>(d);
      for(int i = 0; i < d1.size(); ++i)
        order += (d1[i] != 0);
        
      return order;
    }
    
    static auto indexOf(const OrderType& d)
    {
      assert(orderOf(d) == 1);
      std::pair<int,int> index = std::make_pair(-1,-1);
      
      const auto& d0 = std::get<0>(d);
      for(int i = 0; i < d0.size(); ++i)
        if(d0[i] != 0)
          index = std::make_pair(0,i);
          
      const auto& d1 = std::get<1>(d);
      for(int i = 0; i < d1.size(); ++i)
        if(d1[i] != 0)
          index = std::make_pair(1,i);
          
      return index;
    }
};

#endif
