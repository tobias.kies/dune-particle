#ifndef DUNE_MEMBRANE_PARTICLE_MISMATCH_HH
#define DUNE_MEMBRANE_PARTICLE_MISMATCH_HH

#include <tuple>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

template<class Surface, class Particle, class Transformation, class float_x = double>
class MembraneParticleMismatch
 : public Dune::VirtualMultiDifferentiableFunction
      <
        std::tuple<
          typename Surface::Point2D,
          typename Transformation::Descriptor,
          typename Particle::ParameterizedPoint,
          Dune::FieldVector<float_x,1>
        >,
        Dune::FieldVector<float_x,3>,
        std::tuple<
          Dune::FieldVector<size_t,2>,
          typename std::tuple_element<0,typename Transformation::OrderType>::type,
          Dune::FieldVector<size_t,2>,
          Dune::FieldVector<size_t,1>
        >
      >
{
  public:
  
    typedef typename Surface::Point2D XMembrane; // A point in the parameterization of the membrane
    typedef typename Transformation::Descriptor ParticlePosition; // Describes the particle position
    typedef typename Particle::ParameterizedPoint XParticle; // A point in the parameterization of the particle
    typedef Dune::FieldVector<float_x,1> T; // A peturbation distance
    
    typedef Dune::FieldVector<size_t,2> OT_XMembrane;
    typedef typename Transformation::OrderType OT_Transformation;
    typedef typename std::tuple_element<0,OT_Transformation>::type OT_ParticlePosition;
    typedef Dune::FieldVector<size_t,2> OT_XParticle;
    typedef Dune::FieldVector<size_t,1> OT_T;
    
    typedef std::tuple<XMembrane,ParticlePosition,XParticle,T> DomainType;
    typedef Dune::FieldVector<float_x,3> RangeType;
    typedef std::tuple<OT_XMembrane,OT_ParticlePosition,OT_XParticle,OT_T> OrderType;
    
    typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;
    
    using BaseType::evaluate;
  
    typedef Dune::FieldMatrix<float_x,3,3> Matrix;
    typedef std::array<int,2> Index;
  
    MembraneParticleMismatch(const Surface& surface, const Particle& particle, const Transformation& transformation)
      : surface(surface), particle(particle), transformation(transformation)
    {}
    
    
    RangeType evaluate(const DomainType& z, const OrderType& d) const
    {
      RangeType y;
      
      const auto order = orderOf(d);
      
      switch(order)
      {
        case 0:
          y = evaluateFunction(z);
          break;
          
        case 1:
          y = evaluateFirstDerivative(z, indexOf(d));
          break;
          
        default:
          DUNE_THROW(Dune::NotImplemented, "Not implemented.");
          break;
      }
      
      return y;
    }
    
    Matrix jacobianInLastTwoComponents(const DomainType& z) const
    {
      Matrix A;
      
      for(int j = 0; j < 3; ++j)
      {
        const auto&& y = evaluateFirstDerivative(z, {(j<2)?2:3,(j==1)?1:0});
        for(int i = 0; i < 3; ++i)
          A[i][j] = y[i];
      }
      
      return A;
    }
    
    auto hessiansInLastTwoComponents(const DomainType& z) const
    {
      std::array<Matrix,3> A; // A[k] is the Hessian of the k-th component of the mismatch function.
      
      for(int i = 0; i < 3; ++i)
      {
        for(int j = 0; j <= i; ++j)
        {
          const auto&& sec = evaluateSecondDerivative(z,{(i<2)?2:3,(i==1)?1:0}, {(j<2)?2:3,(j==1)?1:0});
          for(int k = 0; k < 3; ++k)
          {
            A[k][i][j]  = sec[k];
            A[k][j][i]  = A[k][i][j];
          }
        }
      }
      
      return A;
    }
    
    const Particle& getParticle() const
    {
      return particle;
    }
    
    const Surface& getSurface() const
    {
      return surface;
    }
    
    const Transformation& getTransformation() const
    {
      return transformation;
    }
    
    
    mutable bool fDebug = false;
    
  private:
  
    const Surface& surface;
    const Particle& particle;
    const Transformation& transformation;
    
    
    static auto orderOf(const OrderType& d)
    {
      int order = 0;

      const auto& d0 = std::get<0>(d);
      for(int i = 0; i < d0.size(); ++i)
        order += (d0[i] != 0);

      const auto& d1 = std::get<1>(d);
      for(int i = 0; i < d1.size(); ++i)
        order += (d1[i] != 0);

      const auto& d2 = std::get<2>(d);
      for(int i = 0; i < d2.size(); ++i)
        order += (d2[i] != 0);

      const auto& d3 = std::get<3>(d);
      for(int i = 0; i < d3.size(); ++i)
        order += (d3[i] != 0);
        
      return order;
    }
    
    
    static auto indexOf(const OrderType& d)
    {
      if(orderOf(d) != 1)
        DUNE_THROW(Dune::Exception, "Wrong order.");
        
      std::array<int,2> index = {-1,-1};

      bool flag = false;

      const auto& d0 = std::get<0>(d);
      for(int i = 0; i < d0.size(); ++i)
        if(d0[i] != 0)
          index = {0,i};

      const auto& d1 = std::get<1>(d);
      for(int i = 0; i < d1.size(); ++i)
        if(d1[i] != 0)
          index = {1,i};

      const auto& d2 = std::get<2>(d);
      for(int i = 0; i < d2.size(); ++i)
        if(d2[i] != 0)
          index = {2,i};

      const auto& d3 = std::get<3>(d);
      for(int i = 0; i < d3.size(); ++i)
        if(d3[i] != 0)
          index = {3,i};
          
      return index;
    }
    
    
    const auto evaluateFunction(const DomainType& z) const
    {
      RangeType y;
      
      // Extract variables.
      const auto& x_tilda = std::get<0>(z);
      const auto& p       = std::get<1>(z);
      const auto& x       = std::get<2>(z);
      const auto& t       = std::get<3>(z);
      
      // Build a 3D point from x and the height profile function.
      RangeType gamma_x;
      gamma_x[0]  = x[0];
      gamma_x[1]  = x[1];
      gamma_x[2]  = particle.heightProfileFunction().evaluate(x);
      
      // Evaluate transformation at this point.
      const auto&& Phi_z    = transformation.apply(p,gamma_x);
      
      // Evaluate parameterization.
      const auto&& varphi_z = surface.parameterizationFunction().evaluate(x_tilda);
      
      // Evaluate normal.
      const auto&& normal   = surface.parameterizedNormal().evaluate(x_tilda);
      
      // Put everything together.
      y = Phi_z;
      y -= varphi_z;
      y.axpy(-t, normal);
      
      
      if(fDebug)
      {
        Debug::dprint("Mismatch debug information:");
        Debug::dprint("z:", x_tilda, "; ", p, "; ", x, "; ", t);
        Debug::dprintVector(gamma_x, "gamma_x");
        Debug::dprintVector(Phi_z, "Phi_z");
        Debug::dprintVector(varphi_z, "varphi_z");
        Debug::dprintVector(normal, "nu");
        Debug::dprintVector(y, "y");
      }
      
      return y;
    }
    
    
    const auto evaluateFirstDerivative(const DomainType& z, const Index& index) const
    {
      RangeType y(0);
      
      // Extract variables.
      const auto& x_tilda = std::get<0>(z);
      const auto& p       = std::get<1>(z);
      const auto& x       = std::get<2>(z);
      const auto& t       = std::get<3>(z);
      
      switch(index[0])
      {
        case 0:
        {
          OT_XMembrane d;
          d[index[1]] = 1;
          //~ std::cerr << "Mismatch differentiation x_tilda in direction " << d << std::endl;
          y -= surface.parameterizationFunction().evaluate(x_tilda, d);
          //~ std::cerr << "y1 = " << y << std::endl;
          y.axpy(-t, surface.parameterizedNormal().evaluate(x_tilda,d));
          //~ std::cerr << "y2 = " << y << std::endl;
          break;
        }
          
        case 1:
        {
          // Build a 3D point from x and the height profile function.
          RangeType gamma_x;
          gamma_x[0]  = x[0];
          gamma_x[1]  = x[1];
          gamma_x[2]  = particle.heightProfileFunction().evaluate(x);
          
          OT_Transformation d;
          d.first[index[1]] = 1;
          y = transformation.evaluate(std::make_pair(p, gamma_x), d);          
          break;
        }
          
        case 2:
        {
          // Build a 3D point from x and the height profile function.
          RangeType gamma_x;
          gamma_x[0]  = x[0];
          gamma_x[1]  = x[1];
          gamma_x[2]  = particle.heightProfileFunction().evaluate(x);
          
          OT_Transformation d1;
          d1.second[index[1]] = 1;
          y = transformation.evaluate(std::make_pair(p, gamma_x), d1);
          
          OT_Transformation d2;
          d2.second[2]  = 1;
          
          OT_XParticle d3;
          d3[index[1]]  = 1;
          
          y.axpy( particle.heightProfileFunction().evaluate(x,d3), transformation.evaluate(std::make_pair(p,gamma_x), d2));
          break;
        }
          
        case 3:
        {
          y.axpy(-1, surface.parameterizedNormal().evaluate(x_tilda));
          break;
        }
                
        default:
          DUNE_THROW(Dune::Exception, "Unexpected error.");
      }
      
      return y;
    }

    const auto evaluateSecondDerivative(const DomainType& z, const Index& index1, const Index& index2) const
    {
      RangeType y(0);
      
      // Mixed differentiation is only supported for the last two arguments.
      if(index1[0] < 2 || index2[0] < 2)
        DUNE_THROW(Dune::NotImplemented, "Not implemented.");
        
      // Second order differentiation in time results in 0.
      if(index1[0] == 3 || index2[0] == 3)
        return y;
        
      // Remaining case: We have a second order derivative in the x-component.
      
      // Extract variables.
      const auto& x_tilda = std::get<0>(z);
      const auto& p       = std::get<1>(z);
      const auto& x       = std::get<2>(z);
      const auto& t       = std::get<3>(z);

      const auto& gamma   = particle.heightProfileFunction();
      const auto& Phi     = transformation;
      
      const auto i        = index1[1];
      const auto j        = index2[1];

      // Build a 3D point from x and the height profile function.
      RangeType gamma_x;
      gamma_x[0]  = x[0];
      gamma_x[1]  = x[1];
      gamma_x[2]  = particle.heightProfileFunction().evaluate(x);
      
      const auto w        = std::make_pair(p,gamma_x);
      
      // Set differentiation multi-indices.
      OT_XParticle d_gamma1(0), d_gamma2(0), d_gamma12(0);
      OT_Transformation d_Phi3, d_Phi12, d_Phi13, d_Phi23, d_Phi33;
      
      d_gamma1[i]   = 1;
      
      d_gamma2[j]   = 1;
      
      d_gamma12[i]  += 1;
      d_gamma12[j]  += 1;
      
      d_Phi3.second[2]  = 1;
      
      d_Phi12.second[i] += 1;
      d_Phi12.second[j] += 1;
      
      d_Phi13.second[i] = 1;
      d_Phi13.second[2] = 1;
      
      d_Phi23.second[j] = 1;
      d_Phi23.second[2] = 1;
      
      d_Phi33.second[2] = 2;
      
      // Populate y.
      const auto&& gamma_1  = gamma.evaluate(x,d_gamma1);
      const auto&& gamma_2  = gamma.evaluate(x,d_gamma2);
      const auto&& gamma_12 = gamma.evaluate(x,d_gamma12);
      
      const auto&& Phi_3    = Phi.evaluate(w,d_Phi3);
      const auto&& Phi_12   = Phi.evaluate(w,d_Phi12);
      const auto&& Phi_13   = Phi.evaluate(w,d_Phi13);
      const auto&& Phi_23   = Phi.evaluate(w,d_Phi23);
      const auto&& Phi_33   = Phi.evaluate(w,d_Phi33);
      
      y = Phi_12;
      y.axpy(gamma_2, Phi_13);
      y.axpy(gamma_1, Phi_23);
      y.axpy(gamma_1*gamma_2, Phi_33);
      if(y.two_norm() > 1e-10)
      {
        Debug::dprint("12:", Phi_12, ", 13:", Phi_13, ", 23:", Phi_23, ", 33:", Phi_33);
        Debug::dprint("12:", d_Phi12.second, ", 13:", d_Phi13.second, ", 23:", d_Phi23.second, ", 33:", d_Phi33.second);
        DUNE_THROW(Dune::Exception, "stop.");
      }
      y.axpy(gamma_12, Phi_3);
      
      return y;
    }
};

#endif
