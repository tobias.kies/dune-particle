#ifndef DUNE_MEMBRANE_PARTICLE_PARAMETERIZATION_HH
#define DUNE_MEMBRANE_PARTICLE_PARAMETERIZATION_HH

#include <dune/istl/bvector.hh>

#include <dune/particle/particle/particleTypes.hh>
#include <dune/particle/parameterization/membraneParticleMismatch.hh>
#include <dune/particle/parameterization/perturbationParticleIntersection.hh>
#include <dune/particle/functions/transformedParticleLevelSet.hh>
#include <dune/particle/functions/transformedConstraintFunction.hh>
#include <dune/particle/helper/functionshelper.hh>

template<class Surface_, class Particle_, class Transformation_>
class ParameterizedMembraneParticleProblem
{
  public:
  
    typedef Surface_ Surface;
    typedef Particle_ Particle;
    typedef Transformation_ Transformation;
  
    typedef double float_x;
    typedef Dune::BlockVector<typename Transformation::Descriptor> Position;
    typedef std::pair<std::vector<Dune::FieldVector<float_x,2>>, std::vector<Dune::FieldVector<float_x,1>>> ParameterizedPoints;
  
    typedef typename FunctionsHelper::template Function<float_x,2> Function;
    typedef typename std::shared_ptr<Function> FunctionPtr; // Sort of need that the surface level set is compatible with this.
    typedef std::vector<FunctionPtr> LevelSetFunctions;
    
    typedef LevelSetFunctions ConstraintFunctions;
    
    typedef MembraneParticleMismatch<Surface,Particle,Transformation,float_x> Mismatch;
    typedef PerturbationParticleIntersection<Mismatch,float_x> Intersection;
    
    typedef TransformedParticleLevelSet<typename Particle::LevelSetFunction,Intersection,Surface,Transformation> TransformedParticleLevelSet_;
    typedef TransformedConstraintFunction<typename Particle::HeightProfileFunction,Intersection> TransformedConstraintFunction_;
    
    ParameterizedMembraneParticleProblem(const Surface_& surface, const Particle_& particle, const Transformation_& transformation)
      : surface(surface), particle(particle), transformation(transformation),
        mismatch(surface, particle, transformation), intersection(mismatch)
    {}
    
    
    auto& boundingBox() const
    {
      return surface.boundingBox();
    }
    
    void setNumberOfParticles(const int n)
    {
      nParticles  = n;
      position.resize(n);
    }
    
    void setPosition(const int id, const typename Transformation::Descriptor& subposition)
    {
      assert(id < nParticles);
      position[id] = subposition;
    }
    
    void setPosition(const Position& position_)
    {
      assert(position_.size() == nParticles);
      position = position_;
    }
    
    void setPeriodicity(const bool flag)
    {
      fPeriodic = flag;
    }
    
    auto transformedParameterizedPoints( ) const
    {
      ParameterizedPoints points;
      for( int i = 0; i < nParticles; ++i )
      {
        const auto& points_i = transformedParameterizedPoints(i);
        points.first.insert( points.first.end(), points_i.first.begin(), points_i.first.end() );
        points.second.insert( points.second.end(), points_i.second.begin(), points_i.second.end() );
      }
      return points;
    }
    
    auto transformedParameterizedPoints(unsigned int particleId) const
    {
      //~ assert(Particle::type == ParticleType::Point); 
      
      const auto& particlePoints = particle.points();
      ParameterizedPoints points;
      if(Particle::type != ParticleType::Point)
        return points;
      
      points.first.reserve(particlePoints.size());
      points.second.reserve(particlePoints.size());
      for( int i = 0; i < particlePoints.size(); ++i )
      {
        const auto&& point = transformation.apply(position[particleId], particlePoints[i]);
        const auto&& parameterizedPoint     = surface.parameterizedProjection(point);
        const auto&& parameterizedDistance  = surface.parameterizedDistance(point);
        points.first.push_back(parameterizedPoint);
        points.second.push_back(parameterizedDistance);
      }
      
      return points;
    }
    
    bool pointIndexBelongsToLevelSet(const int pointId, const int levelSetId) const
    {
      const auto n = particle.points().size();
      return (pointId >= levelSetId*n) && (pointId < (levelSetId+1)*n);
    }
    
    auto transformedLevelSets() const
    {
      LevelSetFunctions levelSetFunctions;
      levelSetFunctions.push_back( std::make_shared<typename std::decay<decltype(surface.levelSetFunction())>::type>(surface.levelSetFunction()) );
      for(int i = 0; i < nParticles; ++i )
        levelSetFunctions.push_back(
          std::make_shared<TransformedParticleLevelSet_>(
            particle.levelSetFunction(), intersection, position[i], surface, transformation, fPeriodic
          )
        );
      return levelSetFunctions;
    }
    
    auto transformedConstraintFunctions() const
    {
      ConstraintFunctions constraintFunctions;
      constraintFunctions.push_back( std::make_shared<typename std::decay<decltype(surface.constraintFunction())>::type>(surface.constraintFunction()) );
      for(int i = 0; i < nParticles; ++i )
        constraintFunctions.push_back(
          std::make_shared<TransformedConstraintFunction_>(
            particle.heightProfileFunction(), intersection, position[i]
          )
        );
      return constraintFunctions;
    }


    const auto& getPosition() const
    {
      return position;
    }
    
    const auto& getMismatchFunction() const
    {
      return mismatch;
    }
    
    const auto& getSurface() const
    {
      return surface;
    }

    const auto& getParticle() const
    {
      return particle;
    }
    
    const auto& getTransformation() const
    {
      return transformation;
    }
    
    const auto getPeriodicity() const
    {
      return fPeriodic;
    }

  private:
  
    const Surface&        surface;
    const Particle&       particle;
    const Transformation& transformation;
    
    const Mismatch        mismatch;
    const Intersection    intersection;

    int nParticles = 0;
    Position position;
    
    bool fPeriodic = false;
};

#endif
