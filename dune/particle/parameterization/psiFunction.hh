#ifndef DUNE_PSI_FUNCTION_HH
#define DUNE_PSI_FUNCTION_HH

#include <tuple>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

template<class Positions, class Intersection, class InverseIntersection, class float_x = double>
class PsiFunctionDerivative
  : public Dune::VirtualMultiDifferentiableFunction
      <
        Dune::FieldVector<float_x,2>,
        Dune::FieldVector<float_x,2>,
        Dune::FieldVector<size_t,2>
      >
{
  
  public:

    typedef typename Positions::value_type Position;
  
    typedef Dune::FieldVector<float_x,2> DomainType;
    typedef Dune::FieldVector<float_x,2> RangeType;
    typedef Dune::FieldVector<size_t,2> OrderType;
    
    typedef Dune::FieldVector<size_t,Position::dimension> Direction;
    
    typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;
    
    
    using BaseType::evaluate;
  
    PsiFunctionDerivative(const Positions& positions, const Intersection& intersection, const InverseIntersection& inverseIntersection)
      : positions(positions), intersection(intersection), inverseIntersection(inverseIntersection)
    {
      assert(DomainType::dimension == 2);
    }
  
  
    // Todo: At least use a finite difference scheme to approximate derivatives.
    RangeType evaluate(const DomainType& x, const OrderType& d) const
    { 
      assert(orderOf(direction) == 0);
      assert(positionId >= 0 && positionId < positions.size());

      // Return function value or derivative.
      const auto order = d[0] + d[1];
      if(order == 0)
        return evaluateFunction(x);
      else if(order == 1 && d[0] == 1)
      {
        return finiteDifference(x,0);
      } else if( order == 1 && d[1] == 1)
      {
        return finiteDifference(x,1);
      } else if( d[0] == 1 && d[1] == 1 )
      {
        return finiteDifference(x,0,1);
      } else
        DUNE_THROW(Dune::NotImplemented, "Not Implemented.");
    }
    
    
    void setDirection(const Direction& d)
    {
      direction = d;
    }
    
    void setPositionId(const int id)
    {
      positionId = id;
    }
  
  private:
  
    template<class OT>
    static auto orderOf(const OT& d)
    {
      int order = 0;
      for(int i = 0; i < d.size(); ++i)
        order += d[i];
      return order;
    }
    
    auto evaluateFunction(const DomainType& x) const
    {
      RangeType y(0);
      const auto& position = positions[positionId];
      
      typename Intersection::DomainType xp;
      xp.first  = x;
      xp.second = position;
      
      typename InverseIntersection::DomainType xp_;
      xp_.first = position;
      xp_.second = intersection.evaluate(xp).first;
      
      typename InverseIntersection::OrderType d_;
      d_.first = direction;
      d_.second = 0;
      y = inverseIntersection.evaluate(xp_,d_).first;
      
      /*
      #warning Could save some resources here if we forward xp_.second to the following call.
      typename Intersection::OrderType d__;
      d__.first   = 0;
      d__.second  = direction;
      const auto drho_x = intersection.evaluate(xp,d__).first;
      for(int i = 0; i < DomainType::dimension; ++i)
      {
        typename InverseIntersection::OrderType d___;
        d___.first      = 0;
        d___.second[i]  = 1;
        y.axpy(drho_x[i], inverseIntersection.evaluate(xp_,d___).first);
      }
      */

      return y;
    }
    
    auto finiteDifference(const DomainType& x, int dir) const
    {
      auto x_m(x), x_p(x);
      x_m[dir] -= h;
      x_p[dir] += h;
      
      RangeType y = evaluateFunction(x_p);
      y -= evaluateFunction(x_m);
      y /= (2*h);
      
      return y;
    }
    
    auto finiteDifference(const DomainType& x, int d1, int d2) const
    {
      // Only do mixed derivatives so far.
      assert(d1 != d2);

      auto xpp(x), xmp(x), xpm(x), xmm(x);
      for(int i = 0; i < 2; ++i)
      {
        xpp[i]  += h;
        xmm[i]  -= h;
        xmp[i]  += (i==0) ? -h : +h;
        xpm[i]  += (i==0) ? +h : -h;
      }
      
      RangeType y = evaluateFunction(xpp);
      y -= evaluateFunction(xmp);
      y += evaluateFunction(xmm);
      y -= evaluateFunction(xpm);
      y /= (4*h*h);
      
      return y;
    }
  
    const Positions& positions;
          Direction direction;
    const Intersection& intersection;
    const InverseIntersection& inverseIntersection;
    
    const double h = 1e-3;
    
    int positionId = -1;
};

#endif
