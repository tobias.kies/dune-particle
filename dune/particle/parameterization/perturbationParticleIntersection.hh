#ifndef DUNE_PERTURBATION_PARTICLE_INTERSECTION_HH
#define DUNE_PERTURBATION_PARTICLE_INTERSECTION_HH

#include <tuple>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>
#include <dune/particle/parameterization/mismatchMinimizationProblem.hh>

#include "IpTNLP.hpp"
#include "IpIpoptApplication.hpp"
#include "IpSolveStatistics.hpp"

#warning This function relies in the intersection2() function explicitly on the type of transformation.

template<class MismatchFunction, class float_x = double>
class PerturbationParticleIntersection
  : public Dune::VirtualMultiDifferentiableFunction
      <
        std::pair<typename std::tuple_element<0,typename MismatchFunction::DomainType>::type,
                  typename std::tuple_element<1,typename MismatchFunction::DomainType>::type>,
        std::pair<typename std::tuple_element<2,typename MismatchFunction::DomainType>::type,
                  typename std::tuple_element<3,typename MismatchFunction::DomainType>::type>,
        std::pair<typename std::tuple_element<0,typename MismatchFunction::OrderType>::type,
                  typename std::tuple_element<1,typename MismatchFunction::OrderType>::type>
      >
{
  
  public:
  
    typedef std::pair<typename std::tuple_element<0,typename MismatchFunction::DomainType>::type,
                      typename std::tuple_element<1,typename MismatchFunction::DomainType>::type> DomainType;
    typedef std::pair<typename std::tuple_element<2,typename MismatchFunction::DomainType>::type,
                      typename std::tuple_element<3,typename MismatchFunction::DomainType>::type> RangeType;
    typedef std::pair<typename std::tuple_element<0,typename MismatchFunction::OrderType>::type,
                      typename std::tuple_element<1,typename MismatchFunction::OrderType>::type> OrderType;
    
    typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;
    
    using BaseType::evaluate;
  
    PerturbationParticleIntersection(const MismatchFunction& mismatch)
      : mismatch(mismatch)
    {}
  
  
    RangeType evaluate(const DomainType& z, const OrderType& d) const
    {
      RangeType y;
      
      // Compute "intersection".
      const auto&& isec = intersection2(z);
      
      // Return function value or derivative.
      const auto order = orderOf(d);
      
      switch(order)
      {
        case 0:
        {
          y = isec;
          break;
        }
          
        case 1:
        {
          typename MismatchFunction::DomainType z_ex;
          std::get<0>(z_ex) = z.first;
          std::get<1>(z_ex) = z.second;
          std::get<2>(z_ex) = isec.first;
          std::get<3>(z_ex) = isec.second;
          
          typename MismatchFunction::OrderType d_ex;
          std::get<0>(d_ex) = d.first;
          std::get<1>(d_ex) = d.second;

          auto b = mismatch.evaluate(z_ex,d_ex);
        
          auto A = mismatch.jacobianInLastTwoComponents(z_ex);
            //~ Debug::dprintVector(b);
            //~ Debug::dprintDMatrix(A);
          try{
            A.invert();
          } catch( ... ){
            //~ #warning Disabled error for non-well-defined perturbation particle intersection.
            //~ const double NaN = std::numeric_limits<double>::quiet_NaN();
            //~ for(int i = 0; i < 3; ++i) for(int j = 0; j < 3; ++j) A[i][j] = NaN;
            std::cerr << std::endl << " === Error trace ===" << std::endl << std::endl;
            fDebug = true;
            intersection2(z);
            std::cerr << "z_ex: " << std::get<0>(z_ex)
             << ";" << std::get<1>(z_ex)
             << ";" << std::get<2>(z_ex)
             << ";" << std::get<3>(z_ex)
            << std::endl;
            std::cerr << "d_ex: " << std::get<0>(d_ex)
             << ";" << std::get<1>(d_ex)
             << ";" << std::get<2>(d_ex)
             << ";" << std::get<3>(d_ex)
            << std::endl;
            Debug::dprintVector(b, "b");
            Debug::dprintDMatrix(mismatch.jacobianInLastTwoComponents(z_ex));
            //~ fDebug = false;
            DUNE_THROW(Dune::Exception, "Probably could not invert this matrix.");
          }
          
          Dune::FieldVector<float_x,3> v(0);
          A.mmv(b,v);
          
          y.first[0]  = v[0];
          y.first[1]  = v[1];
          y.second    = v[2];
          
          #warning Kept this isnan comment around because I feel like I might need it later.
          if(fDebug)
          {
            if(std::isnan(v[0]) || std::isnan(v[1]) || std::isnan(v[2]))
            //~ if(d.second[5]==1)
            {
              std::cerr << "From InverseIntersection Class:" << std::endl;
              std::cerr << z.first << "; " << z.second << "; " << isec.first << "; " << isec.second << std::endl;
              std::cerr << d.first << "; " << d.second << std::endl;
              Debug::dprintDMatrix(mismatch.jacobianInLastTwoComponents(z_ex), "A");
              Debug::dprintDMatrix(A, "Ainv");
              Debug::dprintVector(b, "b");
              Debug::dprintVector(v, "v");
            }
          }
          
          break;
        }
          
        default:
        {
          DUNE_THROW(Dune::NotImplemented, "Not implemented.");
          break;
        }
      }
      
      return y;
    }

    mutable bool fDebug = false;

  private:
  
    const MismatchFunction& mismatch;
    typedef MismatchMinimizationProblem<MismatchFunction> Problem;
    
    static auto orderOf(const OrderType& d)
    {
      int order = 0;

      const auto& d0 = std::get<0>(d);
      for(int i = 0; i < d0.size(); ++i)
        order += (d0[i] != 0);

      const auto& d1 = std::get<1>(d);
      for(int i = 0; i < d1.size(); ++i)
        order += (d1[i] != 0);
        
      return order;
    }
    
    RangeType intersection2(const DomainType& z) const
    {
      // Apply an optimization method to the mismatch function.
      RangeType finalResult;
      Ipopt::Number objectiveValue;
      
      // Set up initial iterate.
      typename MismatchFunction::DomainType x0;
      std::get<0>(x0) = z.first;
      std::get<1>(x0) = z.second;
      
      //~ if(fDebug)
        //~ mismatch.fDebug = true;
      const auto&& y = mismatch.getTransformation().applyInverse(
                        z.second,
                        mismatch.getSurface().parameterizationFunction().evaluate(z.first));
      
      // Variant 2: Assume that the heightProfileFunction is constant. Compute the intersection of y + alpha*nu and (*,*,gamma). Therefore, we have alpha = (gamma-y_3)/nu_3, where nu is the back-rotated normal.
      // Slightly different take even: Assume that gamma should be (almost) zero at the point of interest; that is because most of the heightProfileFunctions we use are supposed to be almost zero around the associated zero level set.
      const auto&& nu    = mismatch.getSurface().parameterizedNormal().evaluate(z.first);
      auto p_ = z.second;
      for(int i = 0; i < 3; ++i)
        p_[i] = 0;
      const auto&& nu_y  = mismatch.getTransformation().applyInverse(p_,nu);
      const auto&& gamma = mismatch.getParticle().heightProfileFunction().evaluate({y[0], y[1]})-y[2];
      //~ const double alpha = (gamma-y[2])/nu_y[2];
      const double alpha = (0-y[2])/nu_y[2];

      if(std::isfinite(alpha))
      {
        std::get<2>(x0)[0]  = y[0]+alpha*nu_y[0];
        std::get<2>(x0)[1]  = y[1]+alpha*nu_y[1];
        std::get<3>(x0)     = gamma;
      }
      else // fallback to old variant 1
      {
        // Variant 1: Just assume that y_(1,2) is close enough to the real deal.
        std::get<2>(x0)[0]  = y[0];
        std::get<2>(x0)[1]  = y[1];
        std::get<3>(x0)     = gamma-y[2];
      }

      //~ std::cerr << "Starting iterate from " << z.first << "; " << z.second << " is " << std::get<0>(x0) << "; " << std::get<1>(x0) << "; " << std::get<2>(x0) << "; " << std::get<3>(x0) << " as of parameterization " << mismatch.getSurface().parameterizationFunction().evaluate(z.first) << std::endl;
      
      // Evaluation of important values.
      const auto&& costFunction = [&](const auto& x, const auto& y)
      {
        return 0.5*y.two_norm2();
      };
      
      const auto&& costFunctionD = [&](const auto& x, const auto& fval, const auto& jacobian)
      {
        Dune::FieldVector<double,3> grad_f;
        for(int j = 0; j < 3; ++j)
        {
          grad_f[j] = 0;
          for(int i = 0; i < 3; ++i)
            grad_f[j] += jacobian[i][j] * fval[i];
        }
        
        if(fDebug)
        {
          Dune::FieldVector<double,3> grad_f_;
          for(int i = 0; i < 3; ++i)
          {
            auto xm = x;
            auto xp = x;
            
            const double h = 1e-5;
            if(i < 2)
            {
              std::get<2>(xm)[i] -= h;
              std::get<2>(xp)[i] += h;
            }
            else
            {
              std::get<3>(xm) -= h;
              std::get<3>(xp) += h;
            }
          
            auto ym = mismatch.evaluate(xm);
            auto yp = mismatch.evaluate(xp);
            grad_f_[i]  = (costFunction(xp,yp)-costFunction(xm,ym))/(2*h);
          }
          Debug::dprint("Comparison grad_f:", grad_f, "and", grad_f_);
          grad_f_ -= grad_f;
          Debug::dprint("Error", grad_f_.two_norm());
        }
        
        return grad_f;
      };
      
      const auto&& costFunctionD2 = [&](const auto& x, const auto& fval, const auto& jacobian, const auto& hessians)
      {
        Dune::FieldMatrix<double,3,3> hess;
        for(int i = 0; i < 3; ++i)
        {
          for(int j = 0; j <= i; ++j)
          {
            hess[i][j] = 0;
            for(int k = 0; k < 3; ++k)
              hess[i][j] += hessians[k][i][j] * fval[k] + jacobian[k][i] * jacobian[k][j];
            hess[j][i]  = hess[i][j];
          }
        }
        
        /*for(int k = 0; k < 3; ++k)
          if(fDebug)
          {
            const auto& hess = hessians[k];
            Dune::FieldMatrix<double,3,3> hess_;
            for(int i = 0; i < 3; ++i)
            {
              for(int j = 0; j < 3; ++j)
              {              
                auto xm = x;
                auto xp = x;
                
                const double h = 1e-5;
                if(i < 2)
                {
                  std::get<2>(xm)[i] -= h;
                  std::get<2>(xp)[i] += h;
                }
                else
                {
                  std::get<3>(xm) -= h;
                  std::get<3>(xp) += h;
                }

                // Approximate partial_j xm
                auto ymm = xm;
                auto ymp = xm;
                if(j < 2)
                {
                  std::get<2>(ymm)[j] -= h;
                  std::get<2>(ymp)[j] += h;
                }
                else
                {
                  std::get<3>(ymm) -= h;
                  std::get<3>(ymp) += h;
                }
                auto zmm = mismatch.evaluate(ymm);
                auto zmp = mismatch.evaluate(ymp);
                auto dxm = (zmp[k]-zmm[k])/(2*h);

                // Approximate partial_j xm
                auto ypm = xp;
                auto ypp = xp;
                if(j < 2)
                {
                  std::get<2>(ypm)[j] -= h;
                  std::get<2>(ypp)[j] += h;
                }
                else
                {
                  std::get<3>(ypm) -= h;
                  std::get<3>(ypp) += h;
                }
                auto zpm = mismatch.evaluate(ypm);
                auto zpp = mismatch.evaluate(ypp);
                auto dxp = (zpp[k]-zpm[k])/(2*h);
                
                hess_[i][j]  = (dxp-dxm)/(2*h);
              }
            }
            Debug::dprintDMatrix(hess, "hessk", k);
            Debug::dprintDMatrix(hess_, "hessk_", k);
            double error = 0;
            for(int i = 0; i < 3; ++i)
              for(int j = 0; j < 3; ++j)  
                error += (hess[i][j]-hess_[i][j])*(hess[i][j]-hess_[i][j]);
            error = sqrt(error);
            Debug::dprint("Error", error);
          }*/
        
        
        /*if(fDebug)
        {
          Dune::FieldMatrix<double,3,3> hess_;
          for(int i = 0; i < 3; ++i)
          {
            for(int j = 0; j < 3; ++j)
            {              
              auto xm = x;
              auto xp = x;
              
              const double h = 1e-5;
              if(i < 2)
              {
                std::get<2>(xm)[i] -= h;
                std::get<2>(xp)[i] += h;
              }
              else
              {
                std::get<3>(xm) -= h;
                std::get<3>(xp) += h;
              }

              // Approximate partial_j xm
              auto ymm = xm;
              auto ymp = xm;
              if(j < 2)
              {
                std::get<2>(ymm)[j] -= h;
                std::get<2>(ymp)[j] += h;
              }
              else
              {
                std::get<3>(ymm) -= h;
                std::get<3>(ymp) += h;
              }
              auto zmm = mismatch.evaluate(ymm);
              auto zmp = mismatch.evaluate(ymp);
              auto dxm = (costFunction(ymp,zmp)-costFunction(ymm,zmm))/(2*h);

              // Approximate partial_j xm
              auto ypm = xp;
              auto ypp = xp;
              if(j < 2)
              {
                std::get<2>(ypm)[j] -= h;
                std::get<2>(ypp)[j] += h;
              }
              else
              {
                std::get<3>(ypm) -= h;
                std::get<3>(ypp) += h;
              }
              auto zpm = mismatch.evaluate(ypm);
              auto zpp = mismatch.evaluate(ypp);
              auto dxp = (costFunction(ypp,zpp)-costFunction(ypm,zpm))/(2*h);
              
              hess_[i][j]  = (dxp-dxm)/(2*h);
            }
          }
          Debug::dprintDMatrix(hess, "hess");
          Debug::dprintDMatrix(hess_, "hess_");
          double error = 0;
          for(int i = 0; i < 3; ++i)
            for(int j = 0; j < 3; ++j)  
              error += (hess[i][j]-hess_[i][j])*(hess[i][j]-hess_[i][j]);
          error = sqrt(error);
          Debug::dprint("Error", error);
        }*/

        return hess;
      };
      
      const auto&& searchDirection = [&](const auto& grad_f, auto& hess, auto& delta_x)
      {
        hess.invert();
        hess.mv(grad_f,delta_x);
        delta_x *= -1;
      };
      
      const auto&& armijoRule = [&](const auto& x0, const auto& f0, const auto& grad_f, const auto& p)
      {
        const double sigma = 1e-4;
        const double gamma = 2;
        const int iMax = 10;
        
        const double directional_derivative = (grad_f*p);

        const auto&& x  = [&](const double t)
        {
          auto xt = x0;
          std::get<2>(xt)[0] = std::get<2>(x0)[0] + t*p[0];
          std::get<2>(xt)[1] = std::get<2>(x0)[1] + t*p[1];
          std::get<3>(xt)    = std::get<3>(x0)    + t*p[2];
          return xt;
        };
        
        const auto&& phi  = [&](const double t)
        {
          const auto&& xt = x(t);
          const auto&& fval     = mismatch.evaluate(xt);
          return costFunction(xt, fval);
        };
        
        const auto&& psi = [&](const double t)
        {
          return phi(t) - f0 - sigma*t*directional_derivative;
        };
        
        double t = 1;
        for(int i = 0; i <= iMax; ++i)
        {
          if(i == iMax)
          {
            if(fDebug)
            {
              Debug::dprint("Armijo rule failed in", std::get<0>(x0), ";", std::get<1>(x0), ";", std::get<2>(x0), ";", std::get<3>(x0));
              Debug::dprint("With values", psi(0), psi(0.125), psi(0.25), psi(0.5), psi(1));
              Debug::dprint("With gradient", grad_f);
              Debug::dprint("With search direction", p);
            }
            return std::numeric_limits<double>::quiet_NaN();
            
            const auto&& dphi = [&](const double t)
            {
              const auto&& xt = x(t);
              const auto&& fval     = mismatch.evaluate(xt);
              const auto&& jacobian = mismatch.jacobianInLastTwoComponents(xt);
              return -( costFunctionD(xt, fval, jacobian) * grad_f );
            };
            
            const auto&& dpsi = [&](const double t)
            {
              return dphi(t) - sigma*directional_derivative;
            };
                
            std::ofstream file;
            file.open("debug.txt");
            for(int k = -1000; k <= 1000; ++k)
            {
              t = k*0.001;
              file << t << "\t" << phi(t) << "\t" << dphi(t) << "\t" << psi(t) << "\t" << dpsi(t) << std::endl;
            }
            file.close();
            
            mismatch.fDebug = true;
            mismatch.evaluate(x(0));
            mismatch.fDebug = false;
            
            DUNE_THROW(Dune::Exception, "Armijo rule failed.");
          }
            
          if(psi(t) <= 0)
            break;
            
          t /= gamma;
        }
        
        return t;
      };
      
      const auto&& strictWolfeRule = [&](const auto& x0, const auto& f0, const auto& grad_f, const auto& p)
      {
        const double sigma = 1e-4;
        const double rho = 0.9;
        const double gamma = 2;
        const double tau_1 = 0.25;
        const double tau_2 = 0.25;
        const int iMax = 10;
        
        double t = 1;
        double a = 0, b = 0;
        const auto directional_derivative = grad_f*p;
        if(directional_derivative >= 0)
        {
          if(fDebug)
            Debug::dprint("Directional derivative is not negative:", directional_derivative, "for grad_f =", grad_f, " and p =", p);
          return std::numeric_limits<double>::quiet_NaN();
        }
          //~ DUNE_THROW(Dune::Exception, "Invalid directional derivative ");
          //~ DUNE_THROW(Dune::Exception, "Invalid directional derivative " + std::to_string(directional_derivative));
        
        const auto&& x  = [&](const double t)
        {
          auto xt = x0;
          std::get<2>(xt)[0] = std::get<2>(x0)[0] + t*p[0];
          std::get<2>(xt)[1] = std::get<2>(x0)[1] + t*p[1];
          std::get<3>(xt)    = std::get<3>(x0)    + t*p[2];
          return xt;
        };
        
        const auto&& phi  = [&](const double t)
        {
          const auto&& xt = x(t);
          const auto&& fval     = mismatch.evaluate(xt);
          return costFunction(xt, fval);
        };
        
        const auto&& dphi = [&](const double t)
        {
          const auto&& xt = x(t);
          const auto&& fval     = mismatch.evaluate(xt);
          const auto&& jacobian = mismatch.jacobianInLastTwoComponents(xt);
          return costFunctionD(xt, fval, jacobian) * p;
        };
        
        const auto&& psi = [&](const double t)
        {
          return phi(t) - f0 - sigma*t*directional_derivative;
        };
        
        const auto&& dpsi = [&](const double t)
        {
          return dphi(t) - sigma*directional_derivative;
        };
        
        // Phase A.
        for(int i = 0; i <= iMax; ++i)
        {
          if(i == iMax)
          {
            if(fDebug)
              Debug::dprint("Phase A failed");
            return std::numeric_limits<double>::quiet_NaN();
            //~ DUNE_THROW(Dune::Exception, "Phase A failed.");
          }
            
          const auto psi_t = psi(t);
          
          if(psi_t >= 0)
          {
            a = 0;
            b = t;
            break;
          }
          
          const auto dpsi_t = dpsi(t);
          if(std::abs(dpsi_t) <= (rho-sigma)*std::abs(directional_derivative))
          {
            return t;
          }
          
          if(dpsi_t > 0)
          {
            a = t;
            b = 0;
            break;
          }
          
          t = gamma*t;
        }
        
        // Phase B.
        auto psi_a = psi(a);
        for(int i = 0; i <= iMax; ++i)
        {
          if(i == iMax)
          {
            if(fDebug)
              Debug::dprint("Phase B failed", i, a, t, b, psi_a, psi(t), dpsi(t), grad_f.two_norm());
            // Do nothing for now and hope for the best.
            //~ Debug::dprint(i, a, t, b, psi_a, psi(t), dpsi(t));
            //~ DUNE_THROW(Dune::Exception, "Phase B failed.");
            //~ return std::numeric_limits<double>::quiet_NaN();
          }
            
          t = (a+tau_1*(b-a) + b-tau_2*(b-a))/2;
          const auto psi_t = psi(t);
          if(psi_t >= psi_a)
          {
            b = t;
            continue;
          }
          
          const auto dpsi_t = dpsi(t);
          if(std::abs(dpsi_t) <= (rho-sigma) * std::abs(directional_derivative))
            return t;
            
          if((t-a)*dpsi_t > 0)
            b = a;
          a = t;
          psi_a = psi_t;
        }
        
        return t;
      };
      
      // Newton scheme.
      const auto&& newtonStep = [&](auto& x, auto& gradNorm)
      {
        // Compute search direction/correction.
        const auto&& fval     = mismatch.evaluate(x);
        const auto&& jacobian = mismatch.jacobianInLastTwoComponents(x);
        const auto&& hessians = mismatch.hessiansInLastTwoComponents(x);

        const auto&& oldVal = costFunction(x, fval);
        const auto&& grad_f = costFunctionD(x, fval, jacobian);
              auto&& hess   = costFunctionD2(x, fval, jacobian, hessians);
            
        // If gradient is small enough, we return zero. <- Why?
        //~ if(grad_f.two_norm() < 1e-10)
          //~ return 0.;
        gradNorm = grad_f.two_norm();

        Dune::FieldVector<double,3> delta_x(0);
        try{
          searchDirection(grad_f, hess, delta_x);
        } catch(...) {
          if(fDebug)
          {
            std::cerr << "Error during evaluation in " << z.first << "; " << z.second << " given newton iterate " << std::get<0>(x) << "; " << std::get<1>(x) << "; " << std::get<2>(x) << "; " << std::get<3>(x) << std::endl;
            Debug::dprint(fval, "fval");
            Debug::dprintDMatrix(jacobian, "jacobian");
            Debug::dprintVector(grad_f, "Gradient.");
            Dune::FieldMatrix<double,3,3> hess_;
            for(int i = 0; i < 3; ++i)
            {
              for(int j = 0; j <= i; ++j)
              {
                hess_[i][j] = 0;
                for(int k = 0; k < 3; ++k)
                  hess_[i][j] += hessians[k][i][j] * fval[k] + jacobian[k][i] * jacobian[k][j];
                hess_[j][i]  = hess_[i][j];
              }
            }
            Debug::dprintDMatrix(hess_, "Probably could not invert this matrix.");
          }          
          //~ return std::numeric_limits<double>::quiet_NaN();
        }
        
        // Find suitable step length;
        //~ double t = 1;
        //~ double t = armijoRule(x, oldVal, grad_f, delta_x);
        double t = (delta_x.one_norm() > 0) ? strictWolfeRule(x, oldVal, grad_f, delta_x) : std::numeric_limits<double>::quiet_NaN();
        if(std::isnan(t))
        {
          auto p = grad_f;
          p *= -1;
          t = armijoRule(x, oldVal, grad_f, p);
          std::get<2>(x)[0] += t*p[0];
          std::get<2>(x)[1] += t*p[1];
          std::get<3>(x)    += t*p[2];
        }
        else
        {
          std::get<2>(x)[0] += t*delta_x[0];
          std::get<2>(x)[1] += t*delta_x[1];
          std::get<3>(x)    += t*delta_x[2];
        }

        if(fDebug)
          if(!std::isfinite(mismatch.evaluate(x).two_norm()))
            Debug::dprint("Got norm", mismatch.evaluate(x).two_norm(), "in t =", t, "for delta_x =", delta_x);
            
        return mismatch.evaluate(x).two_norm(); // Here we decide to measure the distance to the "optimal" value instead ofd the norm of the derivative.
      };
      
      
      double norm = mismatch.evaluate(x0).two_norm();
      if(fDebug)
        Debug::dprint("Initial step has norm", norm);
      double gradNorm = std::numeric_limits<double>::infinity();
      for(int i = 0; i < 30; ++i)
      {
        if(norm < 1e-8 || std::isnan(norm) || gradNorm < 1e-10)
          break;
        norm = newtonStep(x0, gradNorm);
        if(fDebug)
          Debug::dprint("Newton step", i, "has cost function value", norm, "and gradNorm", gradNorm);
      }
      
      finalResult.first   = std::get<2>(x0);
      finalResult.second  = std::get<3>(x0);
      
      if(norm > 1e-7 || std::isnan(norm) || finalResult.second < mismatch.getSurface().minDistance() )
      {
        if(fDebug)
          Debug::dprint("Evaluation in", z.first, ";", z.second, "set to NaN because of norm =", norm, " with final result =", finalResult.first, ";", finalResult.second);
        std::get<0>(finalResult) *= std::numeric_limits<float_x>::quiet_NaN();
        std::get<1>(finalResult)  = std::numeric_limits<float_x>::quiet_NaN();
      }
      //~ else
        //~ std::cerr << "Final iterate with norm " << norm << " from " << z.first << "; " << z.second << " is " << std::get<0>(x0) << "; " << std::get<1>(x0) << "; " << std::get<0>(finalResult) << "; " << std::get<1>(finalResult) << " as of parameterization " << mismatch.getSurface().parameterizationFunction().evaluate(z.first) << std::endl;

      //~ std::cerr << "Isec in " << z.first << "; " << z.second << " returning " << finalResult.first << "; " << finalResult.second << std::endl;
      return finalResult;
    }
    
    RangeType intersection(const DomainType& z) const
    {
      // Apply an optimization method to the mismatch function.
      RangeType finalResult;
      Ipopt::Number objectiveValue;
      
      // Set up initial iterate.
      typename MismatchFunction::DomainType x0;
      std::get<0>(x0) = z.first;
      std::get<1>(x0) = z.second;
      
      const auto&& y = mismatch.getTransformation().applyInverse(
                        z.second,
                        mismatch.getSurface().parameterizationFunction().evaluate(z.first));
      std::get<2>(x0)[0]  = y[0];
      std::get<2>(x0)[1]  = y[1];
      std::get<3>(x0)     = mismatch.getParticle().heightProfileFunction().evaluate(std::get<2>(x0))-y[2];
      
      // Prepare problem and set config.
      Ipopt::SmartPtr<Ipopt::TNLP> nlp = new Problem(mismatch, x0, finalResult, objectiveValue);
      Ipopt::SmartPtr<Ipopt::IpoptApplication> app = new Ipopt::IpoptApplication();
      
      //~ app->Options()->SetIntegerValue("print_level", 12);
      //~ app->Options()->SetStringValue("derivative_test", "second-order");
      //~ app->Options()->SetNumericValue("derivative_test_tol", 1.e-2);
      app->Options()->SetIntegerValue("print_level", 0);
      //~ app->Options()->SetIntegerValue("print_frequency_iter", 100);
      //~ app->Options()->SetStringValue("hessian_approximation", "limited-memory");
      //~ app->Options()->SetIntegerValue("limited_memory_max_history", 3);
      app->Options()->SetIntegerValue("max_iter", 5);
      app->Options()->SetNumericValue("tol",1e-3);

      // Initialize solver.
      auto status = app->Initialize();
      if( status != Ipopt::Solve_Succeeded)
        DUNE_THROW(Dune::Exception, "IPOpt: Error during initialization!");
      
      // Apply solver.
      app->OptimizeTNLP(nlp);
      
      // Invalidate result if its energy is too large.
      if(objectiveValue > 1e-2)
      {
        std::get<0>(finalResult) *= std::numeric_limits<float_x>::quiet_NaN();
        std::get<1>(finalResult)  = std::numeric_limits<float_x>::quiet_NaN();
      }
      
      // Return solution.
      return finalResult;
    }
};

#endif
