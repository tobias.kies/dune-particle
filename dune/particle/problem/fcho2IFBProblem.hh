#ifndef DUNE_FCHO2_IFB_PROBLEM
#define DUNE_FCHO2_IFB_PROBLEM

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/surface/planarRectangularPeriodicSurface.hh>

#include <dune/particle/particle/FCHo2Particle.hh>
#include <dune/particle/integrand/mongeGaugeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/particle/derivative/derivativeVectorField_MongeGaugeSpecialization.hh>
#include <dune/particle/derivative/derivativeVectorField_MongeGaugePlanarSpecialization.hh>
#include <dune/particle/solver/membraneMultiGridSolver.hh>

class FCHo2IFBProblem
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef PlanarRectangularPeriodicSurface<> Surface;
    typedef FCHo2Particle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef typename Grid::LevelGridView LevelGridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef HermiteKNodalBasis<LevelGridView,2> LevelBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef MongeGaugeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;
    //~ typedef MembraneMultiGridSolver<Model,Basis,LevelBasis> Solver; // Multigrid solver does not yet work grid-independently.

    typedef Dune::FieldVector<double,2> QuadPoint;
    typedef std::vector<QuadPoint> QuadPoints;

    //~ static constexpr double length    = 5000;
    static constexpr double length    = 2000;
    //~ static constexpr double length    = 3000;

    const bool fRefineGrid = true;
    int nMaxGridSize = 60e3;
    const double refinementDistance = 10;
    double repulsionConstant = 1;
    
    const bool fFixedRefinementSteps = true; // Usually the grid is refined until nMaxGridSize is exceeded. If this flag is true, then a fixed number of refinement steps is performed instead.
    int nRefinementSteps = 11; // Number of fixed refinement steps if fFixedRefinementSteps == true.
    const bool fRefineContainingOnly = true; // Usually the grid is refined in elements that are close enough to atoms (cf. refinementDistance). If this flag is active, then only elements containing the atoms are refined.

    FCHo2IFBProblem()
      : u(0), surface(length), particle(), transformation()
    {
      particle.makePeriodic(surface);
      transformation.makePeriodic(surface);
      initQuadrature();
    }

    auto initialPosition() const
    {
      //~ const int nParticles = 20;
      //~ const double r = 2000;
      const int nParticles = 24;
      const double r = 750;
      
      Position x(nParticles);
      
      for(int i = 0; i < nParticles; ++i)
      {
        const double theta = i*(2.*M_PI/nParticles);
        x[i]  = {(9+(i%3))/10.*r*cos(theta), 1.25*r*sin(theta), 0, 0, 0, theta};
      }
      
      return x;
    }

    auto upperBound() const
    {
      auto up = initialPosition();
      up  = 0;

      for(int i = 0; i < up.size(); ++i)
      {
        up[i][0] = std::numeric_limits<double>::max();
        up[i][1] = std::numeric_limits<double>::max();
        up[i][5] = std::numeric_limits<double>::max();
      }

      Debug::dprintVector(up, "up");
      return up;
    }

    auto lowerBound() const
    {
      auto lo = upperBound();
      lo *= -1;
      Debug::dprintVector(lo, "lo");
      return lo;
    }


    auto solve(const double h, const Model::Position& position)
    {
      lastPosition = position;

      if(!isFeasible(position))
      {
        energy_u = std::numeric_limits<double>::infinity();
        return energy_u;
      }

      // Delete grid (if there is one).
      pGrid.reset();

      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      pModel->setPeriodicity(true);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);
      if(fRefineGrid)
        gridRefine(*pGrid, position);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      ConstraintsMachine::updateConstraints(*pBasis, surface, model.transformedParameterizedPoints().first);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();
      Debug::dprint("Solved.");

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      energy_u = 0.5*(u*Au);
      auto rep = repulsionEnergy(position);
      Debug::dprint("Partial energies:", energy_u, "and", rep);
      energy_u += rep;
      Debug::dprint("Got energy", energy_u);
      return energy_u;
    }

    auto energy() const
    {
      return energy_u;
    }

    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      #if USE_PLANAR_SPECIALIZATION
      typedef DerivativeVectorField_MongeGaugePlanarSpecialization<DomainInfo,Model,Solver> VectorField;
      #else
      typedef DerivativeVectorField_MongeGaugeSpecialization<DomainInfo,Model,Solver> VectorField;
      #endif
      //~ typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      auto dif = derivative.evaluateGradient(lastPosition);
      Debug::dprint("Got membrane gradient.");
      Debug::dprintVector(dif, "Membrane gradient");
      const auto gradRep = repulsionGradient(lastPosition);
      Debug::dprint("Got repulsion gradient.");
      Debug::dprintVector(gradRep, "Repulsion gradient");
      dif += gradRep;
      Debug::dprint("Evaluated gradient.");

      return dif;
    }
    
    auto partialDerivativeWoRepulsion(const int i)
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      typedef DerivativeVectorField_MongeGaugePlanarSpecialization<DomainInfo,Model,Solver> VectorField;
      //~ typedef DerivativeVectorField_MongeGaugeSpecialization<DomainInfo,Model,Solver> VectorField;
      //~ typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      auto dif = lastPosition[i];
      dif *= 0;
      for(int j = 0; j < dif.size(); ++j)
      {
        Debug::dprint("Evaluating directional derivative.", i, j);
        dif[j] = derivative.evaluate(i,j);
      }
      return dif;
    }
    
    void saveGrid(const std::string& fileName)
    {
      NonconformingBasis basis(pGrid->leafGridView());

      Vector zero(basis.size(),0);
      typedef VTKBasisGridFunction<NonconformingBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(basis, zero, "zero");

      Dune::SubsamplingVTKWriter<typename NonconformingBasis::GridView> writer(basis.getGridView(), 0);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    void saveMembrane(const std::string& fileName, const int nRefinement = 0)
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;
            
      if(u.size() != basis.size())
      {
        Debug::dprint("Warning! Calling saveMembrane() without feasible finite element vector. Saving grid instead.");
        saveGrid(fileName);
        return;
      }

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), nRefinement);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }

    auto& solver() const
    {
      return *pMembraneSolver;
    }

    const auto& getTransformation() const
    {
      return transformation;
    }


  protected:

    Dune::ParameterTree params;

    Surface        surface;
    Particle       particle;
    Transformation transformation;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Position lastPosition;
    Vector u;
    double energy_u;


    bool isFeasible(const Position& pos) const
    {
      return true;
    }

    // Repulsion energy at a given position.
    // The parameter only restricts evaluation to a single particle (if only >=0);
    // this is mostly used for the lazy finite difference implementation.
    // This implementation makes only sense of the Transformation is an orthogonal
    // transformation and if no rotation around the x1 or x2 axis is present.
    double repulsionEnergy(const Model::Position& position, int only = -1) const
    {
      // Abbreviation if repulsionConstant is negligible.
      if(repulsionConstant == 0.)
        return 0.;
      
      // Transform quadrature points. Works essentially like transforming atoms.
      const auto&& transformedQuadPoints = transformQuadPoints(position);

      // Evaluate interaction integrals.
      double res = 0;
      for(int i = 0; i < position.size(); ++i)
      {
        for(int j = 0; j < i; ++j)
        {
          // Skip iteration if parameter only is set and only \notin {i,j}.
          if(only >= 0 && i != only && j != only)
            continue;

          // Skip iteration if particles are too far away from each other.
          #warning // Warning: This makes the transformation hard coded!
          double d2 = 0;
          for(int k = 0; k < 3; ++k) d2 += (position[i][k]-position[j][k])*(position[i][k]-position[j][k]);
          if( d2 >= 200*200 ) continue;

          // Evaluate smoothed integral.
          res += interactionIntegral(transformedQuadPoints[i], transformedQuadPoints[j]);
        }
      }
      return res*repulsionConstant;
    }

    double interactionIntegral(const QuadPoints& X, const QuadPoints& Y) const
    {
      // int f(|X-Y|) * g(X_0) * g(Y_0)
      // f(r) = 1/(1+r^2)

      // Define distance function.
      auto length0 = length;
      const auto&& distance2 = [&length0](const auto& x, const auto& y)
      {
        auto z = x;
        z -= y;

        // Apply periodicity.
        for(int i = 0; i < 2; ++i)
          z[i] = (fmod(z[i]+length,2*length)-length) + (z[i] < -length)*(2*length);

        return z.two_norm2();
      };

      // Evaluate actual integral.
      double res = 0;
      for(int i = 0; i < quadWeights.size(); ++i)
      {
        //~ std::cerr << "\r" << i << "/" << quadWeights.size();
        for(int j = 0; j < quadWeights.size(); ++j)
          res += quadWeights[i] * quadWeights[j] / (100 + distance2(X[i],Y[j]));
      }
      //~ std::cerr << std::endl;

      return res;
    }

    Model::Position repulsionGradient(const Model::Position& position) const
    {
      // Lazy finite difference implementation.
      auto gradient = position; gradient *= 0;

      const double t = 0.5e-4;
      #pragma omp parallel for
      for(int i = 0; i < position.size(); ++i)
      {
        for(int j = 0; j < position[i].size(); ++j)
        {
          auto pp = position;
          pp[i][j] += t;

          auto pm = position;
          pm[i][j] -= t;

          gradient[i][j] = (repulsionEnergy(pp,i) - repulsionEnergy(pm,i))/(2*t);
        }
      }

      return gradient;
    }


    void gridRefine(Grid& grid, const Model::Position& p) const
    {
      //~ DUNE_THROW(Dune::Exception, "Grid refinement might not work yet as intended because it does not lead to a conforming basis. This is maybe because either the ConformingBasis itself is erroneous or because the incorporation of point value constraints is not yet compatible with the constraints from the ConformingBasis. To-do: Check results for plausibility and fix errors if necessary.");
      Debug::dprint("Number of elements before marking:", grid.size(0));

      // Get constraint positions in dependence of p.
      // TODO: Make this less hard coded.
      const auto& residuals = FCHo2ParticleRaw::residualPositions;
      std::vector<Dune::FieldVector<double,2>> atoms;
      for(int i = 0; i < p.size(); ++i)
      {
        for(const auto& residual : residuals)
        {
          const auto&& q = transformation.apply(p[i], residual);
          atoms.push_back({
            (fmod(q[0]+length,2*length)-length) + (q[0] < -length)*(2*length),
            (fmod(q[1]+length,2*length)-length) + (q[1] < -length)*(2*length)
          });
        }
      }

      // Mark relevant grid elements until threshold is reached.
      int markCounter = 0;
      int refinementStepCounter = 0;
      //~ while(markCounter < nMaxRefinementMarks)
      while( (/*!fFixedRefinementSteps &&*/ grid.size(0) < nMaxGridSize) && (fFixedRefinementSteps && refinementStepCounter < nRefinementSteps) )
      {
        int oldMarkCounter = markCounter;
        ++refinementStepCounter;
        
        // Check if a point is inside an element.
        // Attention: Only works with tensor-rectangular elements right now!
        const auto&& inside = [](const auto& e, const auto& x)
        {
          Dune::FieldVector<double,2> min, max;
          min = std::numeric_limits<double>::infinity();
          max = -std::numeric_limits<double>::infinity();
          const auto& g = e.geometry();
          for(int i = 0; i < g.corners(); ++i)
          {
            const auto& y = g.corner(i);
            for(int j = 0; j < 2; ++j)
            {
              min[j]  = std::min(min[j], y[j]);
              max[j]  = std::max(max[j], y[j]);
            }
          }
          
          bool f = true;
          for(int j = 0; j < 2; ++j)
            f &= (x[j] >= min[j] && x[j] <= max[j]);
          return f;
        };
          
        // Choose candidates.
        if( !fRefineContainingOnly )
        {
          for(const auto& e : elements(grid.leafGridView()))
          {
            // Mark element if it is close enough to one of the points.
            for(const auto& x : atoms)
            {
              auto z = e.geometry().center();
              z -= x;
              // Apply periodicity.
              for(int i = 0; i < 2; ++i)
                z[i] = (fmod(z[i]+length,2*length)-length) + (z[i] < -length)*(2*length);

              //~ if(z.two_norm() < refinementDistance
              if(z.one_norm() < refinementDistance
                || inside(e,x) ) // Refine in any case when the element contains the given point.
              {
                ++markCounter;
                grid.mark(1,e);
                break; // Elements can/need at most be marked once.
              }
            }
          }
        }
        else
        {
          for(const auto& x : atoms)
          {
            for(const auto& e : elements(grid.leafGridView())) // Could be even faster if only elements from the highest level view would be considered.
            //~ for(const auto& e : elements(grid.levelGridView(grid.maxLevel()))) // <-- Don't use this without care. Might be broken. (So, compare if results stay the same.)
            {
              if(inside(e,x))
              {
                ++markCounter; // May double-mark elements, actually.
                grid.mark(1,e);
                break;
              }
            }
          }
        }
        
        // If nothing was marked, then mark everything.
        if(markCounter == oldMarkCounter)
        {
          for(const auto& e : elements(grid.leafGridView()))
          {
            ++markCounter;
            grid.mark(1,e);
          }
        }

        // Apply marking.
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();

        bool fNeedsPostprocessing = true;
        while(fNeedsPostprocessing)
        {
          /*// For simplicity (with respect to periodic boundary conditions):
          // Also make sure that all boundary elements are uniformly refined.
          BoundaryPatch<typename std::decay<decltype(grid.leafGridView())>::type> boundaryPatch(grid.leafGridView(), true);

          int maxLevel = 0;
          for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
            maxLevel = std::max(it->inside().level(), maxLevel);

          bool fUniform = false;
          while(fUniform == false)
          {
            fUniform = true;
            for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
            {
              const auto& e = it->inside();
              if(e.level() < maxLevel)
              {
                fUniform = false;
                ++markCounter;
                grid.mark(1,e);
              }
            }

            // Apply marking.
            if(!fUniform)
            {
              grid.preAdapt();
              grid.adapt();
              grid.postAdapt();
            }
          }*/
          
          fNeedsPostprocessing = false;
          const int nBorderElements = gridRefineElementsWithoutBuddies(grid);
          fNeedsPostprocessing |= (nBorderElements > 0);

          // Ensure that refinement is not too steep.
          const int nClosureElements = gridClose(grid);
          markCounter += nClosureElements;
          fNeedsPostprocessing |= (nClosureElements > 0);
          
          Debug::dprint("nBorderElements =", nBorderElements, "and nClosureElements =", nClosureElements);
          
          /*// Check if this affected again the boundary.
          int maxLevelNew = 0;
          for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
            maxLevelNew = std::max(it->inside().level(), maxLevelNew);
          fNeedsPostProcessing = (maxLevel != maxLevelNew);*/
          
          ///Debug::dprint("Elements marked by postprocessing:", nBorderElements, nClosureElements);
        }

        Debug::dprint("Intermediate grid size:", grid.size(0));
      }

      Debug::dprint("Number of elements after marking:", grid.size(0));
    }


    // Does *one* run of refinements for all elements without buddies.
    // Essentially copies the code from planarRectangularPeriodicSurface.hh
    //  ... hence, not a really pretty design choice here.
    int gridRefineElementsWithoutBuddies(Grid& grid) const
    {
      ///std::cerr << "Called gREWB." << std::endl;
      ///Debug::dprintVector(surface.r, "r");
      int nMarkCounter = 0;
      const size_t dim = 2;

      const auto&& feq = [](double a, double b){ return ( std::abs(a-b) < 128*1024*std::numeric_limits<double>::epsilon() ); };
      
      std::map<int,std::vector<int>> buddies;
      
      BoundaryPatch<GridView> boundaryPatch(grid.leafGridView(), true);

      //  (coarsified corner localId)  |->  vector of (non-coarse corner component, x/y dimension)
      std::map<long int,std::vector<std::tuple<double,int>>> indexMapX, indexMapY;

      int nPre = 0;
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& g = it->inside().geometry();
        for(int i = 0; i < g.corners(); ++i)
          for(int j = 0; j < 2; ++j)
            if(std::abs(g.corner(i)[j]) > 1e-15)
              nPre = std::max( nPre, (int) std::ceil(std::log10(std::abs(g.corner(i)[j]))) );
      }
      const int nMult = 9 - nPre;
      const auto&& coarseCoord = [&nMult](const double& x)
      {
        long int coarse = x * std::pow(10,nMult);
        return coarse;
      };
      
      std::vector<std::pair<Dune::FieldVector<double,2>,int>> visitedCorners; // Not very elegant... but I am too clumsy to use proper index sets right now.

      // Iterate over all boundary interfaces.
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        // Check all corners.
        const int direction = (feq(it->geometry().corner(0)[0],it->geometry().corner(1)[0])) ? 1 : 0;
        ///Debug::dprint("Set direction to", direction, "as of", it->geometry().corner(0)[0], it->geometry().corner(1)[0]);
        ///Debug::dprint("Conversely,", it->geometry().corner(0)[1], it->geometry().corner(1)[1]);
        for(int i = 0; i < it->geometry().corners(); ++i)
        {
          const auto& corner = it->geometry().corner(i);
          
          bool fVisited = false;
          for(const auto& other : visitedCorners)
            if(other.first == corner && other.second == direction)
            {
              ///Debug::dprint("Visited already", corner[0], corner[1], "in direction", direction);
              fVisited = true;
              break;
            }
          if(fVisited)
            break;
          visitedCorners.push_back(std::make_pair(corner,direction));
          
          // Check if corner is on boundary and which periodic boundary it belongs to.
          if(feq(abs(corner[0]),surface.r[0]))
            indexMapX[coarseCoord(corner[1])].push_back(std::make_tuple<double,int>((double)corner[1],1));
          if(feq(abs(corner[1]),surface.r[1]))
            indexMapY[coarseCoord(corner[0])].push_back(std::make_tuple<double,int>((double)corner[0],0));
        }
      }
      
      ///Debug::dprint("MapSizes", indexMapX.size(), indexMapY.size());
      
      
      
      std::set<std::tuple<double,int>> danglingPoints;
      for(const auto& ids : indexMapX)
      {
        ///Debug::dprint(ids.first, std::get<0>(ids.second[0]), ids.second.size());
        if(ids.second.size() == 1){
          // Buddy finding failed in a recoverable way.
          Debug::dprint("Got dangling X", std::get<0>(ids.second[0]));
          danglingPoints.insert(ids.second[0]);
        }///else if(ids.second.size() > 2)
          ///Debug::dprint("Got critical X", std::get<0>(ids.second[0]), "with size", ids.second.size());
      }
      Debug::dprint(" --- ");
      
      for(const auto& ids : indexMapY)
      {
        ///Debug::dprint(ids.first, std::get<0>(ids.second[0]), ids.second.size());
        if(ids.second.size() == 1){
          // Buddy finding failed in a recoverable way.
          Debug::dprint("Got dangling Y", std::get<0>(ids.second[0]));
          danglingPoints.insert(ids.second[0]);
        }///else if(ids.second.size() > 2)
          ///Debug::dprint("Got critical Y", std::get<0>(ids.second[0]), "with size", ids.second.size());
      }
      
      // Mark and refine elements.
      const double eps = std::numeric_limits<double>::epsilon() * 1024 * 1024;
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        // Extract relevant corners.
        Dune::FieldVector<double,2> start, end;
        start = std::numeric_limits<double>::infinity();
        end   = -std::numeric_limits<double>::infinity();
        const auto& g = it->geometry();
        for(int i = 0; i < g.corners(); ++i)
        {
          auto corner = g.corner(i);
          for(int j = 0; j < 2; ++j)
          {
            start[j]  = std::min(corner[j], start[j]);
            end[j]    = std::max(corner[j], end[j]);
          }
        }
        
        // Check all dangling points.
        for(const auto& candidate : danglingPoints)
        {
          // Check if point is between the corners of the boundary patch.
          // If so, refine and break.
          const auto& c = std::get<0>(candidate);
          const auto& d = std::get<1>(candidate);
          if(start[d]+eps <= c && c <= end[d]-eps)
          {
            ///Debug::dprint("Found element for candidate", c, d);
            grid.mark(1,it->inside());
            ++nMarkCounter;
            break;
          }
        }
      }
  
      grid.preAdapt();
      grid.adapt();
      grid.postAdapt();
      
      
      // Expensive marker when everything else fails (without me knowing why).
      nMarkCounter += gridRefineElementsWithoutBuddiesWithFakeBasis(grid);
      
      return nMarkCounter;
    }
    
    // Modification of the "constructBoundaryDofs" in fufem/functiontools/boundarydofs.hh
    // To-do: Replace the class conditional by the appropriate function object.
    template <class GridView, class Basis, class Conditional, int blocksize>
    static void constructConditonalBoundaryDOFs(
        const BoundaryPatch<GridView>& boundaryPatch,
        const Basis& basis,
        Dune::BitSetVector<blocksize>& boundaryDofs,
        const Conditional& conditional)
    {
      boundaryDofs.resize(basis.size());
      boundaryDofs.unsetAll();

      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& inside = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(inside).localCoefficients();

        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          unsigned int entity = localCoefficients.localKey(i).subEntity();
          unsigned int codim  = localCoefficients.localKey(i).codim();

          if(it.containsInsideSubentity(entity, codim))
            if(conditional(*it, inside, localCoefficients, i))
              boundaryDofs[basis.index(inside, i)] = true;
        }
      }
    }
    
    int gridRefineElementsWithoutBuddiesWithFakeBasis(Grid& grid) const
    {
      const auto& r = surface.r;
      NonconformingBasis ncBasis(grid.leafGridView());
      Basis basis(ncBasis);
      int nMarkCounter = 0;
      const size_t dim = 2;

      const auto&& feq = [](double a, double b){ return ( std::abs(a-b) < 128*1024*std::numeric_limits<double>::epsilon() ); };
      
      const auto&& determinePeriodicDOFs = [&basis, &feq, &r]()
      {
        const auto&& condition = [&](const auto& intersection, const auto& element, const auto& localCoefficients, const int id)
        {
          const auto& g = intersection.geometry();
          const int n   = g.corners();
          for(int i = 0; i < n; ++i )
          {
            const auto&& corner = g.corner(i);
            if( !feq(abs(corner[0]),r[0]) && !feq(abs(corner[1]),r[1]) )
              return false;
          }
          return true;
        };
        Dune::BitSetVector<1> periodicDofs;
        BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
        constructConditonalBoundaryDOFs(boundaryPatch, basis, periodicDofs, condition);
        return periodicDofs;
      };
      auto periodicDOFs = determinePeriodicDOFs();


      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
      std::map<std::pair<long int,size_t>,std::vector<std::pair<int,double>>> indexMapX, indexMapY;

      int nPre = 0;
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& g = it->inside().geometry();
        for(int i = 0; i < g.corners(); ++i)
          for(int j = 0; j < 2; ++j)
            if(std::abs(g.corner(i)[j]) > 1e-15)
              nPre = std::max( nPre, (int) std::ceil(std::log10(std::abs(g.corner(i)[j]))) );
      }
      const int nMult = 9 - nPre;
      const auto&& coarseCoord = [&nMult](const double& x)
      {
        long int coarse = x * std::pow(10,nMult);
        return coarse;
      };

      // Iterate over all boundary interfaces.
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        // Get inside element and local coefficients.
        const auto& element = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(element).localCoefficients();

        // For each local basis function (identified by a local coefficient)...
        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          // ... skip if the basis function is not periodic. (E.g. because it is not really on the boundary.)
          const auto idx = basis.index(element, i);
          if(periodicDOFs[idx].none())
            continue;

          // Extract geometric information.
          const auto entity = localCoefficients.localKey(i).subEntity();
          const auto codim  = localCoefficients.localKey(i).codim();
          const auto index  = localCoefficients.localKey(i).index();
          
          if( codim != dim )
            DUNE_THROW(Dune::Exception, "Only point-based finite elements are supported.");

          const auto&& subEntity  = element.template subEntity<dim>(entity);
          const auto&& corner = subEntity.geometry().corner(0);
          
          // Check if corner is on boundary and which periodic boundary it belongs to.
          if(feq(abs(corner[0]),r[0]))
            indexMapX[std::make_pair(coarseCoord(corner[1]),index)].push_back(std::make_pair(idx,corner[1]));
          if(feq(abs(corner[1]),r[1]))
            indexMapY[std::make_pair(coarseCoord(corner[0]),index)].push_back(std::make_pair(idx,corner[0]));
          
          // Avoid revisiting this basis function.
          periodicDOFs[idx] = false;
        }
      }
      
      const double eps = std::numeric_limits<double>::epsilon() * 1024 * 1024;

      for(const auto& ids : indexMapX)
      {
        if(ids.second.size() != 2){
          Debug::dprint("Fake XBuddy finding failed, got size", ids.second.size());
          // Find element that needs to be refined.
          const auto c = ids.second[0].second;
          for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
          {
            const auto& g = it->geometry();
            double a = std::numeric_limits<double>::infinity(),
                   b = -std::numeric_limits<double>::infinity();
            for(int i = 0; i < g.corners(); ++i)
            {
              a = std::min(g.corner(i)[1], a);
              b = std::max(g.corner(i)[1], b);
            }
            
            if(a+eps <= c && c <= b-eps)
            {
              grid.mark(1,it->inside());
              ++nMarkCounter;
              break;
            }
          }
        }
      }
      
      for(const auto& ids : indexMapY)
      {
        if(ids.second.size() != 2){
          Debug::dprint("Fake YBuddy finding failed, got size", ids.second.size());
          // Find element that needs to be refined.
          const auto c = ids.second[0].second;
          for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
          {
            const auto& g = it->geometry();
            double a = std::numeric_limits<double>::infinity(),
                   b = -std::numeric_limits<double>::infinity();
            for(int i = 0; i < g.corners(); ++i)
            {
              a = std::min(g.corner(i)[0], a);
              b = std::max(g.corner(i)[0], b);
            }
            
            if(a+eps <= c && c <= b-eps)
            {
              grid.mark(1,it->inside());
              ++nMarkCounter;
              break;
            }
          }
        }
      }
  
      grid.preAdapt();
      grid.adapt();
      grid.postAdapt();
      
      return nMarkCounter;
    }

    int gridClose(Grid& grid) const
    {
      // Mark all elements that have more than two elements on one of their edges.
      const int conformityLevel = 1;
      int markCounter = 0;
      bool fClosable = true;

      while(fClosable)
      {
        fClosable = false;
        const auto gridView = grid.leafGridView();
        // Iterate over all elements...
        for( const auto& e : elements(gridView) )
        {
          // Refine if too many finer elements are around.
          const auto iitEnd = gridView.iend(e);
          size_t finerCounter = 0;
          for( auto iit = gridView.ibegin(e); iit != iitEnd; iit++ )
          {
            // Skip if there is no neighbor on edge.
            if( !iit->neighbor() )
              continue;

            // Increase counter if level of neighbor is finter.
            if( iit->outside().level() > e.level() )
              finerCounter++;
          }

          /*if( finerCounter > 2 )
          //~ if( finerCounter > 3 )
          {
            ++markCounter;
            fClosable = true;
            grid.mark(1, e);
            continue;
          }*/

          Dune::BitSetVector<1> checked( grid.size(0), false );
          const int nClosable = gridCheckNeighbors(grid, e, e, conformityLevel, checked); // Does not count elements properly. Also does not exclude double-counting elements.
          markCounter += nClosable;
          fClosable |= (nClosable > 0);
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
      }

      return markCounter;
    }

    template<class Element>
    int gridCheckNeighbors(Grid& grid, const Element& e, const Element& origin, const size_t depth, Dune::BitSetVector<1>& checked) const
    {
      int nMarks = 0;
      
      // Skip if element has been checked already; else check it.
      const auto gridView = grid.leafGridView();
      const auto elementIndex = gridView.indexSet().index(e);
      if( checked[elementIndex].any() )
        return 0;
      checked[elementIndex] = true;

      // Iterate over all neighbors of the current element e.
      const auto&& g    = e.geometry();
      const auto iitEnd = gridView.iend(e);
      for( auto iit = gridView.ibegin(e); iit != iitEnd; iit++ )
      {
        // Skip if there is no neighbor on edge.
        if( !iit->neighbor() )
          continue;

        // Mark origin if current element is on a level that is too high.
        if( iit->outside().level() > origin.level()+1 )
        {
          grid.mark(1,origin);
          ++nMarks;
          return nMarks;
        }

        // Else: Check the current element's neighbors, if we did not yet reach depth=0.
        if( depth > 0 )
        {
          nMarks += gridCheckNeighbors( grid, iit->outside(), origin, depth-1, checked );
          if(nMarks > 0)
            return nMarks;
        }
      }

      return 0;
    }


    /// Quadrature logic.
    QuadPoints quadPoints;
    std::vector<double> quadWeights;

    void initQuadrature()
    {
      /// ToDo: Should turn this into a piecewise quadrature rather due to regularity constraints in the integrand.
      // Get reference quadrature rules in each direction.
      // (Could by the way also use the DUNE functionalities for that but right now I do not remember up to which order is possible to use wihtout crashing it. Rule as generated by R's gauss.quad.)
      // R -> X=gauss.quad(n); for(x in X$nodes) cat(sprintf("%.15f, ",x)); cat('\n')
      // R -> X=gauss.quad(n); for(x in X$weights) cat(sprintf("%.15f, ",x)); cat('\n')
      std::vector<double> pointsX  = {-0.998866404420071, -0.994031969432090, -0.985354084048006, -0.972864385106692, -0.956610955242807, -0.936656618944878, -0.913078556655792, -0.885967979523613, -0.855429769429946, -0.821582070859336, -0.784555832900399, -0.744494302226068, -0.701552468706822, -0.655896465685439, -0.607702927184950, -0.557158304514650, -0.504458144907464, -0.449806334974038, -0.393414311897565, -0.335500245419437, -0.276288193779532, -0.216007236876042, -0.154890589998146, -0.093174701560086, -0.031098338327189, 0.031098338327189, 0.093174701560086, 0.154890589998146, 0.216007236876042, 0.276288193779532, 0.335500245419437, 0.393414311897565, 0.449806334974039, 0.504458144907464, 0.557158304514650, 0.607702927184950, 0.655896465685439, 0.701552468706822, 0.744494302226069, 0.784555832900399, 0.821582070859336, 0.855429769429946, 0.885967979523613, 0.913078556655792, 0.936656618944878, 0.956610955242808, 0.972864385106692, 0.985354084048006, 0.994031969432090, 0.998866404420071, };
      std::vector<double> weightsX = {0.002908622553155, 0.006759799195745, 0.010590548383651, 0.014380822761486, 0.018115560713489, 0.021780243170125, 0.025360673570012, 0.028842993580535, 0.032213728223577, 0.035459835615146, 0.038568756612588, 0.041528463090148, 0.044327504338803, 0.046955051303948, 0.049400938449467, 0.051655703069581, 0.053710621888996, 0.055557744806212, 0.057189925647729, 0.058600849813222, 0.059785058704265, 0.060737970841771, 0.061455899590317, 0.061936067420683, 0.062176616655347, 0.062176616655347, 0.061936067420683, 0.061455899590316, 0.060737970841770, 0.059785058704265, 0.058600849813224, 0.057189925647729, 0.055557744806213, 0.053710621888996, 0.051655703069582, 0.049400938449467, 0.046955051303950, 0.044327504338803, 0.041528463090148, 0.038568756612587, 0.035459835615147, 0.032213728223578, 0.028842993580535, 0.025360673570011, 0.021780243170125, 0.018115560713490, 0.014380822761486, 0.010590548383651, 0.006759799195746, 0.002908622553155, };
      std::vector<double> pointsY  = {-0.993128599185095, -0.963971927277914, -0.912234428251326, -0.839116971822219, -0.746331906460151, -0.636053680726515, -0.510867001950827, -0.373706088715420, -0.227785851141645, -0.076526521133498, 0.076526521133497, 0.227785851141645, 0.373706088715420, 0.510867001950827, 0.636053680726515, 0.746331906460151, 0.839116971822219, 0.912234428251326, 0.963971927277914, 0.993128599185095, };
      std::vector<double> weightsY = {0.017614007139152, 0.040601429800387, 0.062672048334109, 0.083276741576705, 0.101930119817241, 0.118194531961518, 0.131688638449177, 0.142096109318382, 0.149172986472604, 0.152753387130726, 0.152753387130726, 0.149172986472604, 0.142096109318382, 0.131688638449177, 0.118194531961518, 0.101930119817241, 0.083276741576705, 0.062672048334109, 0.040601429800387, 0.017614007139152, };
      const double lengthX = 250;
      const double lengthY = 100;


      // g0(r) = (r^2/eps^2-1)^2/eps^2*c if r^2 <= eps^2 else 0
      const auto g = [](const QuadPoint& x)
      {
        const double eps2 = std::pow(30,2);
        const FCHo2ParticleRaw::ParticleCoordinates& points = FCHo2ParticleRaw::rawMinimalPositions;
        double res = 0;
        for(const auto& point : points)
        {
          double r2 = (x[0] - point[0])*(x[0] - point[0]) + (x[1] - point[1])*(x[1] - point[1]);
          if(r2 < eps2)
            res += (r2/eps2-1)*(r2/eps2-1);
        }
        return res * (3./M_PI/eps2);
      };

      // Join them to one rule and apply an appropriate rescaling.
      // Update the weights furthermore by the smoothed dirac functions g
      quadPoints.resize(0);
      quadPoints.reserve(pointsX.size()*pointsY.size());
      quadWeights.resize(0);
      quadWeights.reserve(pointsX.size()*pointsY.size());
      for(int j = 0; j < pointsY.size(); ++j)
      {
        for(int i = 0; i < pointsX.size(); ++i)
        {
          quadPoints.push_back({pointsX[i]*lengthX/2., pointsY[j]*lengthY/2.});
          quadWeights.push_back(weightsX[i]*lengthX/2.*weightsY[j]*lengthY/2.*g(quadPoints.back()));
        }
      }
    }

    std::vector<QuadPoints> transformQuadPoints(const Model::Position& p) const
    {
      std::vector<QuadPoints> transformedPoints(p.size());
      for(int i = 0; i < p.size(); ++i)
      {
        transformedPoints[i].resize(quadPoints.size());
        for(int j = 0; j < quadPoints.size(); ++j)
        {
          const auto&& z = transformation.apply(p[i], {quadPoints[j][0], quadPoints[j][1], 0.});
          transformedPoints[i][j] = {z[0],z[1]};
        }
      }

      return transformedPoints;
    }
};

#endif
