#ifndef DUNE_DERIVATIVE_CONVERGENCE_TEST_CURVE_PROBLEM2
#define DUNE_DERIVATIVE_CONVERGENCE_TEST_CURVE_PROBLEM2

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/surface/planarRectangularSurface.hh>
#include <dune/particle/particle/ellipsoidalParticle.hh>
#include <dune/particle/integrand/mongeGaugeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>


class DerivativeConvergenceTestCurveProblem2
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef PlanarRectangularSurface<> Surface;
    typedef EllipsoidalParticle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef MongeGaugeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;

    static constexpr double ellipse_a = 1./3;
    static constexpr double ellipse_b = ellipse_a*2./3;
    static constexpr double slope     = 1;
    
    static const int nParticles = 2;

    DerivativeConvergenceTestCurveProblem2()
      : u(0), surface(1), particle(ellipse_a, ellipse_b, 0, slope), transformation()
    {
      basePosition.resize(nParticles);
      basePosition[0] = {-0.4,  0.4, 0.3, 10*M_PI/180, 15*M_PI/180, 30*M_PI/180};
      basePosition[1] = { 0.25, -0.35, 0.3, -10*M_PI/180, -15*M_PI/180, 60*M_PI/180};
    }

    auto solve(const double h, const int perturbId = -1, const int perturbDir = -1, const double perturbVal = 0)
    {
      // Delete grid (if there is one).
      pGrid.reset();
      
      // Set position.
      auto position = basePosition;
      if(perturbId >= 0 && perturbDir >= 0)
        position[perturbId][perturbDir] += perturbVal;
      
      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      surface.updateConstraints(*pBasis);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      const auto energy = 0.5*(u*Au);
      Debug::dprint("Got energy", energy);
      return energy;
    }


    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      const auto dif = derivative.evaluateGradient(basePosition);
      Debug::dprint("Evaluated gradient.");

      return dif;
    }
    
    
    auto differenceQuotients(const double h, const double delta_t = 1e-5)
    {
      auto grad = basePosition;
      for(int i = 0; i < grad.size(); ++i)
        for(int j = 0; j < grad[i].size(); ++j)
          grad[i][j]  = (solve(h,i,j,delta_t)-solve(h,i,j,-delta_t))/(2*delta_t);

      return grad;
    }


    void saveMembrane(const std::string& fileName = "membrane")
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), 0);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }
    
    auto& solver() const
    {
      return *pMembraneSolver;
    }


  protected:

    Dune::ParameterTree params;

          Surface         surface;
    const Particle        particle;
    const Transformation transformation;
    
    typename Model::Position basePosition;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Vector u;
};
#endif
