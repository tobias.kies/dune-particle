#include <dune/particle/debug.hh>

#include <dune/particle/surface/planarRectangularSurface.hh>
#include <dune/particle/surface/tubularSurface.hh>

#include <dune/particle/particle/pointConstraintParticle.hh>
#include <dune/particle/particle/examplePointConstraints.hh>

#include <dune/particle/transformation/spatialTransformation.hh>

#include <dune/particle/parameterization/parameterizedMembraneParticleProblem.hh>

#include <dune/particle/grid/membraneGrid.hh>

#include <dune/particle/basis/hermitek/hermiteknodalbasis.hh>
#include <dune/particle/basis/conforming/conformingkbasis.hh>
#include <dune/particle/basis/cutoff/cutoffbasis.hh>

#include <dune/particle/domaininfo/domaininfo.hh>
#include <dune/particle/functions/constraintFunction.hh>

#include <dune/particle/integrand/mongeGaugeIntegrand.hh>
#include <dune/particle/integrand/tubeIntegrand.hh>

#include <dune/particle/assembler/membraneAssembler.hh>

#include <dune/particle/solver/membraneSolver.hh>

#include <dune/particle/derivative/derivativeVectorField.hh>
#include <dune/particle/derivative/particleDerivative.hh>

#include <dune/particle/functions/parametricPerturbedSurface.hh>
#include <dune/particle/helper/misc.hh>

//~ #include <dune/common/timer.hh>
//~ #include <dune/particle/test.hh>

class MembraneParticleProblem
{
  public:
  
    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    #if SURFACE == 1
      typedef PlanarRectangularSurface<> Surface;
    #else
      typedef TubularSurface<> Surface;
    #endif
    typedef PointConstraintParticle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;
    
    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    #if SURFACE == 1
      typedef MongeGaugeIntegrand<Constraints> Integrand;
    #else
      typedef TubeIntegrand<Constraints> Integrand;
    #endif
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;
      
    MembraneParticleProblem()
    #if SURFACE == 1
      : u(0), surface(1,1), particle(ExamplePointConstraints::testPoints()), transformation()
    #else
      : u(0), surface(5,1), particle(ExamplePointConstraints::testPoints()), transformation()
    #endif        
    {
      // Read configuration.
      Dune::ParameterTreeParser::readINITree("particle.ini", params);
    }
  
    auto solve(Position& position)
    {
      const double h = 1./params.template get<double>("hInv");  // grid size

      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      const auto& model = *pModel;
      
      // Set up grid.
      pGrid = ( Particle::type == ParticleType::Point ) ? MembraneGrid::template makeGridContainingPoints(model, h) : MembraneGrid::template makeGrid(model, h);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      surface.updateConstraints(*pBasis);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      #if SURFACE == 1
        Integrand integrand(1,1,f);
      #else
        Integrand integrand(1,1,radius,f);
      #endif

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      Solver membraneSolver(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = membraneSolver.solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      const auto energy = 0.5*(u*Au);
      Debug::dprint("Got energy", energy);
      return energy;
    }
    
    auto partialDerivative(const int particleId, const int dofId) const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      #if SURFACE == 1
        Integrand integrand(1,1,f);
      #else
        Integrand integrand(1,1,1,f); // third parameter is radius
      #endif

      // Compute derivative.
      typedef DerivativeVectorField<DomainInfo,Model> VectorField;
      VectorField V(domainInfo, model);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo> Derivative;
      Derivative derivative(integrand, V, domainInfo);

      Debug::dprint("Evaluating derivative.");
      const auto dif = derivative.evaluate(u, particleId, dofId);
      std::cerr << "Got derivative: " << dif << std::endl;
      Debug::dprint("Evaluated derivative.");
      
      return dif;
    }
    
    void saveMembrane(const std::string& fileName = "membrane")
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;
      
      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), 2);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");      
    }


  protected:

    Dune::ParameterTree params;
    
    const Surface         surface;
    const Particle        particle;
    const Transformation transformation;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    

    Vector u;
};


  /*std::array<Vector,3> V_components;
  for(int i = 0; i < V_components.size(); ++i)
  {
    auto& Vi = V_components[i];
    Vi.resize(u.size());
    for(int k = 0; k < u.size(); ++k)
      Vi[k] = V.coefficientVector()[k][i];
    std::cerr << "V" << i << " has norm " << Vi.two_norm() << std::endl;
    Misc::writeDiscreteFunction(coBasis, Vi, "V_" + std::to_string(i), 2);
  }*/


  // Other output:
  //~ Test::writeCurveInfo(domainInfo, f);
