#ifndef DUNE_POINT_CONVERGENCE_TEST_PROBLEM_3_HH
#define DUNE_POINT_CONVERGENCE_TEST_PROBLEM_3_HH

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/surface/planarRectangularPeriodicSurface.hh>
#include <dune/particle/particle/fourPointParticle.hh>
#include <dune/particle/integrand/mongeGaugeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/particle/derivative/derivativeVectorField_MongeGaugeSpecialization.hh>

class PointConvergenceTestProblem3
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef PlanarRectangularPeriodicSurface<> Surface;
    typedef FourPointParticle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef MongeGaugeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;

    PointConvergenceTestProblem3()
      : u(0), surface(4), particle(), transformation()
    {
      // Do not need to call "make periodic" here because the particles stay fixed.
    }
    
    auto initialPosition() const
    {
      typename Model::Position position(2);
      const auto DEG = M_PI/180;
      position[0] = { 2, -2, -0.2, 0*DEG, 0*DEG, 30*DEG};
      position[1] = {-2,  2,  0.1, 0*DEG, 0*DEG, 10*DEG};
      return position;
    }

    //~ auto solve(const double h)
    auto solve(const double h, const typename Model::Position& position)
    {
      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      ConstraintsMachine::updateConstraints(*pBasis, surface, model.transformedParameterizedPoints().first);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      const auto energy = 0.5*(u*Au);
      Debug::dprint("Got energy", energy);
      return energy;
    }
    
    auto solve(const double h)
    {
      return solve(h, initialPosition());
    }

    void saveMembrane(const std::string& fileName = "membrane", const int refinement = 2)
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), refinement);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }


    // Approach here: Use a high order quadrature rule on the coarse
    // grid and apply it against the exact solution function.
    template<class OtherProblem>
    auto measureError(const OtherProblem& other) const
    {
      Dune::FieldVector<double,3> error(0);

      const auto& basis       = *pBasis;
      const auto& domainInfo  = *pDomainInfo;

      const auto& o_basis     = other.basis();
      typedef typename std::decay<decltype(o_basis)>::type OBasis;
      const auto& o_u         = other.membrane();

      // Set up functions.
      BasisMultiDifferentiableGridFunction<Basis,Vector,Dune::FieldVector<size_t,2>> func_u(basis, u);
      BasisMultiDifferentiableGridFunction<OBasis,Vector,Dune::FieldVector<size_t,2>> func_ex(o_basis, o_u);


      // Coarse element.
      typedef typename std::decay<decltype(o_basis)>::type OBasis;
      typedef typename OBasis::GridView OGridView;
      typedef typename OGridView::Grid OGrid;
      auto coarseElement = *(o_basis.getGridView().template begin<0>());

      const auto&& findContainingElement = [&o_basis](const auto& x, const auto& e0)
      {
        const auto& gridView = o_basis.getGridView();
        const auto& grid = gridView.grid();

        // Is x in e0 already?
        if(Dune::ReferenceElements<double,2>::general(e0.type()).checkInside(e0.geometry().local(x)))
          return e0;
        
        // Does one of the neighbors have it?
        for(const auto& is : intersections(gridView,e0))
        {
          if(!is.neighbor())
            continue;
            
          const auto& e = is.outside();
          if(Dune::ReferenceElements<double,2>::general(e.type()).checkInside(e.geometry().local(x)))
            return e;
        }
        
        // Is it on the boundary?
        BoundaryPatch<OGridView> boundaryPatch(gridView, true);
        for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
        {
          const auto& inside = it->inside();
          if(Dune::ReferenceElements<double,2>::general(inside.type()).checkInside(inside.geometry().local(x)))
            return inside;
        }
        
        // Last resort: Hierarchical search.
        Dune::HierarchicSearch<OGrid,OGridView> hsearch(grid, gridView);
        return hsearch.findEntity(x);
      };

      // Iterate over all elements.
      for(const auto& e : elements(basis.getGridView()))
      {
        // Get quadrature rule.
        const bool isInside   = domainInfo.isInside(e);
        const bool isOutside  = domainInfo.isOutside(e);

        if(isOutside)
          continue;

        const auto tFE = basis.getLocalFiniteElement(e);
        QuadratureRuleKey quadKey(tFE);
        quadKey.setOrder(tFE.localBasis().order()*2+10);
        auto quadratureRule = (isInside) ? QuadratureRuleCache<double,2>::rule(quadKey) : LocalParameterizationQuadrature<double>::rule(quadKey, e, domainInfo);

        for(const auto& quadPoint : quadratureRule)
        {
          // Extract coordinates.
          const auto& xLocal = quadPoint.position();
          const auto xGlobal = e.geometry().global(xLocal);
          const auto weight = quadPoint.weight()*e.geometry().integrationElement(xLocal);

          // Find containing coarse grid element.
          coarseElement = findContainingElement(xGlobal, coarseElement);
          const auto& xLocalCoarse  = coarseElement.geometry().local(xGlobal);
          
          // Update error term.
          const auto dif_H0   = func_u.evaluateLocal(e,xLocal) - func_ex.evaluateLocal(coarseElement,xLocalCoarse);
          Dune::FieldVector<double,2> dif_H1 = func_u.evaluateLocalGradient(e,xLocal);
          dif_H1  -= func_ex.evaluateLocalGradient(coarseElement,xLocalCoarse);
          const auto dif_H2   = func_u.evaluateLaplacianLocal(e,xLocal) - func_ex.evaluateLaplacianLocal(coarseElement,xLocalCoarse);
          
          error[0] += dif_H0*dif_H0*weight;
          error[1] += (dif_H1*dif_H1)*weight;
          error[2] += dif_H2*dif_H2*weight;
        }
      }

      for(auto& e : error)
        e = sqrt(e);
      return error;
    }

    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      typedef DerivativeVectorField_MongeGaugeSpecialization<DomainInfo,Model,Solver> VectorField;
      //~ typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      const auto dif = derivative.evaluateGradient(model.getPosition());
      Debug::dprint("Evaluated gradient.");

      return dif;
    }


  protected:

    Dune::ParameterTree params;

          Surface         surface;
    const Particle        particle;
    const Transformation transformation;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Vector u;
};
#endif
