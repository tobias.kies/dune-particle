#ifndef DUNE_TEST_PROBLEMS_HH
#define DUNE_TEST_PROBLEMS_HH

#include <dune/particle/problem/annulusCurveConvergenceTestProblem.hh>
#include <dune/particle/problem/curveConvergenceTestProblem2.hh>
#include <dune/particle/problem/curveConvergenceTestProblem3.hh>
#include <dune/particle/problem/derivativeTestCurveProblem.hh>
#include <dune/particle/problem/derivativeConvergenceTestCurveProblem.hh>
#include <dune/particle/problem/derivativeConvergenceTestCurveProblem2.hh>

#include <dune/particle/problem/annulusPointConvergenceTestProblem.hh>
#include <dune/particle/problem/pointConvergenceTestProblem2.hh>
#include <dune/particle/problem/pointConvergenceTestProblem3.hh>
#include <dune/particle/problem/derivativeTestPointProblem.hh>
#include <dune/particle/problem/derivativeConvergenceTestPointProblem.hh>

#include <dune/particle/problem/eggCartonProblem.hh>
#include <dune/particle/problem/eggCartonPeriodicProblem.hh>
#include <dune/particle/problem/eggCartonSaddlePeriodicProblem.hh>
#include <dune/particle/problem/fcho2PeriodicProblem.hh>
#include <dune/particle/functions/rescaledMembraneEnergy.hh>
#include <dune/particle/functions/fcho2projection.hh>
#include <dune/particle/problem/tubeInteractionProblem.hh>

#include <dune/particle/optimization/gradientMethods.hh>

// In hindsight a lot of the routines for test problems could be summarized
// into common routines (with a few (template) parameters).
class TestProblems
{
  public:
  
    static void convergenceTestCurveExact( )
    {
      typedef AnnulusCurveConvergenceTestProblem Problem;
      std::ofstream file;
      file.open("convergence_curve_exact.txt");
      file << std::setprecision(15);
      for(int i = 16; i <= 512; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i;

        Problem problem;
        problem.solve(h);
        if(i < 100)
          problem.saveMembrane("./iterates/curveExactMembrane_0" + std::to_string(i));
        else
          problem.saveMembrane("./iterates/curveExactMembrane_" + std::to_string(i));
        const auto err = problem.measureError();
        std::cerr << "Error " << err << std::endl;

        Problem exact;
        exact.solveExactly(h);
        //~ exact.saveMembrane("exact");
        const auto ipol = exact.measureError();
        std::cerr << "Ipol " << ipol << std::endl;

        file << h;
        for(const auto& e : err)
          file  << "\t" << e;
        for(const auto& e : ipol)
          file  << "\t" << e;
        file << std::endl;
      }
      file.close();
    }
    
    
    static void convergenceTestCurveInexact( )
    {
      typedef CurveConvergenceTestProblem2 Problem;
      std::ofstream file;
      file.open("convergence_curve_inexact.txt");
      file << std::setprecision(15);

      Problem exact;
      exact.solve(2.*M_PI/128);
      exact.saveMembrane("./iterates/curveInexactMembraneFineSolution");
      
      DUNE_THROW(Dune::Exception, "Stop it.");

      for(int i = 16; i <= 63; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        try{
          double h = 1./i;

          Problem problem;
          problem.solve(h);
          if(i < 100)
            problem.saveMembrane("./iterates/curveInexactMembrane_0" + std::to_string(i));
          else
            problem.saveMembrane("./iterates/curveInexactMembrane_" + std::to_string(i));

          const auto err = exact.measureError(problem);
          std::cerr << "Error " << err << std::endl;

          file << h << "\t" << err << std::endl;
        }
        catch(...)
        {
          std::cerr << "Failed." << std::endl;
        }
      }
      file.close();
    }
    
    
    static void convergenceTestCurveInexact2( )
    {
      typedef CurveConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("convergence_curve_inexact2.txt");
      file << std::setprecision(15);

      const int nMax = 128;

      Problem exact;
      exact.solve(1./nMax);
      exact.saveMembrane("./iterates/curveInexactMembraneFineSolution2");

      for(int i = 16; i < nMax; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        try{
          double h = 1./i;

          Problem problem;
          problem.solve(h);
          if(i < 100)
            problem.saveMembrane("./iterates/curveInexact2Membrane_0" + std::to_string(i));
          else
            problem.saveMembrane("./iterates/curveInexact2Membrane_" + std::to_string(i));

          const auto err = exact.measureError(problem);
          std::cerr << "Error " << err << std::endl;

          file << h;
          for(const auto& e : err)
            file  << "\t" << e;
          file << std::endl;
        }
        catch(...)
        {
          std::cerr << "Failed." << std::endl;
        }
      }
      file.close();
    }
    
    
    static void convergenceTestPointExact( )
    {
      typedef AnnulusPointConvergenceTestProblem Problem;
      std::ofstream file;
      file.open("convergence_point_exact.txt");
      file << std::setprecision(15);
      for(int i = 16; i <= 512; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i;

        Problem problem;
        problem.solve(h);
        if(i < 100)
          problem.saveMembrane("./iterates/pointExactMembrane_0" + std::to_string(i));
        else
          problem.saveMembrane("./iterates/pointExactMembrane_" + std::to_string(i));
        const auto err = problem.measureError();
        std::cerr << "Error " << err << std::endl;

        Problem exact;
        exact.solveExactly(h);
        //~ exact.saveMembrane("exact");
        const auto ipol = exact.measureError();
        std::cerr << "Ipol " << ipol << std::endl;

        file << h;
        for(const auto& e : err)
          file  << "\t" << e;
        for(const auto& e : ipol)
          file  << "\t" << e;
        file << std::endl;
      }
      file.close();
    }
    
  
    // Purpose is to compare derivatives with finite differences along a trajectory.
    // One ellipsoidal particle in Monge gauge, moving around the center.
    // Later: Make it two particles.
    // Later: Convergence in dependence of h.
    static void derivativeTestCurve()
    {
      const double h = 1./32;
      
      typedef DerivativeTestCurveProblem Problem;
      for(int dir = 0; dir < 6; ++dir)
      {
        std::ofstream file;
        file.open("derivative_curve_" + std::to_string(dir) + ".txt");
        file << std::setprecision(15);
        
        int iMax = 50;
        double stepSize = 1./1000;
        if(dir == 3 || dir == 4)
          stepSize  = 20*M_PI/180 * 1./iMax;
        else if(dir == 5)
          stepSize  = 0.5*M_PI/iMax;
        
        for(int i = -iMax; i <= iMax; ++i)
        //~ for(int i = -85; i <= -84; ++i)
        //~ for(int i = 0; i <= 0; ++i)
        {
          std::cerr << "Iteration i = " << i << std::endl;
          
          typename Problem::Position position(1);
          position[0] *= 0;
          position[0][dir] = i*stepSize;

          Problem problem;
          const auto energy = problem.solve(h,position);
          const auto id = i+iMax;
          if(id < 10)
            problem.saveMembrane("./iterates/derivativeTestCurve_" + std::to_string(dir) + "_00" + std::to_string(id));
          else if(id < 100)
            problem.saveMembrane("./iterates/derivativeTestCurve_" + std::to_string(dir) + "_0" + std::to_string(id));
          else
            problem.saveMembrane("./iterates/derivativeTestCurve_" + std::to_string(dir) + "_" + std::to_string(id));

          //~ std::cerr << position[0] << " " << energy << " " << problem.derivative(dir) << std::endl;
          file << position[0] << " " << energy << " " << problem.derivative(dir) << std::endl;
        }
        //~ DUNE_THROW(Dune::Exception, "Wait a moment!");
        file.close();
      }
    }
    
    
    static void derivativeConvergenceTestCurve()
    {
      typedef DerivativeConvergenceTestCurveProblem Problem;

      const int nMin = 32;
      const int nMax = 128;
      const auto nParticles = Problem::nParticles;

      std::ofstream file;
      file.open("derivative_convergence_curve.txt");
      file << std::setprecision(15);

      const auto&& printLine = [&file](const double h, const auto grad, const auto errors, const auto norm_error)
      {
        file << h << " ";
        for(int i = 0; i < grad.size(); ++i)
          for(int j = 0; j < grad[i].size(); ++j)
            file << grad[i][j] << " ";
            
        for(int i = 0; i < errors.size(); ++i)
          for(int j = 0; j < errors[i].size(); ++j)
            file << errors[i][j] << " ";
            
        file << norm_error << std::endl;
      };

      const double delta_t = 1e-5;
      typename Problem::Position dif_quot_gradient(nParticles);
      {
        Problem problem;
        dif_quot_gradient = problem.differenceQuotients(1./nMax);
        Debug::dprintVector(dif_quot_gradient, "Difference quotient gradient");
      
        auto dummy = dif_quot_gradient;
        dummy *= 0;
        printLine(0, dif_quot_gradient, dummy, 0);
      }

      for(int n = nMin; n <= nMax; ++n)
      {
        std::cerr << "n=" << n << "<=" << nMax << std::endl;
        Problem problem;

        const double h = 1./n;
        problem.solve(h);
        const auto gradient = problem.gradient();

        auto errors = dif_quot_gradient;
        errors -= gradient;

        const double norm_error = errors.two_norm();

        printLine(h, gradient, errors, norm_error);
      }
      
      file.close();
    }
    
    
    static void derivativeConvergenceTestCurve2()
    {
      typedef DerivativeConvergenceTestCurveProblem2 Problem;

      const int nMin = 16;
      const int nMax = 128;
      const auto nParticles = Problem::nParticles;

      std::ofstream file;
      file.open("derivative_convergence_curve2.txt");
      file << std::setprecision(15);

      const auto&& printLine = [&file](const double h, const auto grad)
      {
        file << h;
        for(int i = 0; i < grad.size(); ++i)
          for(int j = 0; j < grad[i].size(); ++j)
            file << " " << grad[i][j];
        file << std::endl;
      };

      /*const double delta_t = 1e-5;
      typename Problem::Position dif_quot_gradient(nParticles);
      {
        Problem problem;
        dif_quot_gradient = problem.differenceQuotients(1./nMax);
        Debug::dprintVector(dif_quot_gradient, "Difference quotient gradient");
      
        auto dummy = dif_quot_gradient;
        dummy *= 0;
        printLine(0, dif_quot_gradient, dummy, 0);
      }*/

      for(int n = nMin; n <= nMax; ++n)
      {
        std::cerr << "n=" << n << "<=" << nMax << std::endl;
        Problem problem;

        const double h = 1./n;
        problem.solve(h);
        const auto gradient = problem.gradient();

        printLine(h, gradient);
      }
      
      file.close();
    }
    
    
    static void convergenceTestPointInexact( )
    {
      typedef PointConvergenceTestProblem2 Problem;
      std::ofstream file;
      file.open("convergence_point_inexact.txt");
      file << std::setprecision(15);

      const int nMax = 128;
      
      const auto&& h_ = [](const int n)
      {
        return 400./n;
      };

      Problem exact;
      exact.solve(h_(nMax));
      exact.saveMembrane("./iterates/pointInexactMembraneFineSolution");

      for(int i = 16; i < nMax; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        try{
          double h = h_(i);

          Problem problem;
          auto energy = problem.solve(h);
          if(i < 100)
            problem.saveMembrane("./iterates/pointInexactMembrane_0" + std::to_string(i));
          else
            problem.saveMembrane("./iterates/pointInexactMembrane_" + std::to_string(i));

          const auto err = exact.measureError(problem);
          std::cerr << "Error " << err << std::endl;

          file << h;
          for(const auto& e : err)
            file  << "\t" << e;
          file << "\t" << energy;
          file << std::endl;
        }
        catch(...)
        {
          std::cerr << "Failed." << std::endl;
        }
      }
      file.close();
    }
    
    
    static void convergenceTestPointInexact2( )
    {
      typedef PointConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("convergence_point_inexact2.txt");
      file << std::setprecision(15);

      const int nMax = 128;
      
      const auto&& h_ = [](const int n)
      {
        return 4./n;
      };

      Problem exact;
      exact.solve(h_(nMax));
      exact.saveMembrane("./iterates/pointInexact2MembraneFineSolution");

      for(int i = 16; i < nMax; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        try{
          double h = h_(i);

          Problem problem;
          problem.solve(h);
          if(i < 100)
            problem.saveMembrane("./iterates/pointInexactMembrane2_0" + std::to_string(i));
          else
            problem.saveMembrane("./iterates/pointInexactMembrane2_" + std::to_string(i));

          const auto err = exact.measureError(problem);
          std::cerr << "Error " << err << std::endl;

          file << h;
          for(const auto& e : err)
            file  << "\t" << e;
          file << std::endl;
        }
        catch(...)
        {
          std::cerr << "Failed." << std::endl;
        }
      }
      file.close();
    }
    

    // Purpose is to compare derivatives with finite differences along a trajectory.
    // FCHo2 particle in Monge gauge, moving around the center.
    static void derivativeTestPoint()
    {
      const double h = 1./32;
      
      typedef DerivativeTestPointProblem Problem;
      for(int dir = 0; dir < 6; ++dir)
      {
        std::ofstream file;
        file.open("derivative_point_" + std::to_string(dir) + ".txt");
        file << std::setprecision(15);
        
        int iMax = 50;
        double stepSize = 1./1000;
        if(dir == 3 || dir == 4)
          stepSize  = 20*M_PI/180 * 1./iMax;
        else if(dir == 5)
          stepSize  = 0.5*M_PI/iMax;
        
        for(int i = -iMax; i <= iMax; ++i)
        {
          std::cerr << "Iteration i = " << i << std::endl;
          
          typename Problem::Position position(1);
          position[0] *= 0;
          position[0][dir] = i*stepSize;

          Problem problem;
          const auto energy = problem.solve(h,position);

          const auto id = i+iMax;
          if(id < 10)
            problem.saveMembrane("./iterates/derivativeTestPoint_" + std::to_string(dir) + "_00" + std::to_string(id));
          else if(id < 100)
            problem.saveMembrane("./iterates/derivativeTestPoint_" + std::to_string(dir) + "_0" + std::to_string(id));
          else
            problem.saveMembrane("./iterates/derivativeTestPoint_" + std::to_string(dir) + "_" + std::to_string(id));

          //~ std::cerr << position[0] << " " << energy << " " << problem.derivative(dir) << std::endl;
          file << position[0] << " " << energy << " " << problem.derivative(dir) << std::endl;
        }
        file.close();
      }
    }
    
    
    static void derivativeConvergenceTestPoint()
    {
      typedef DerivativeConvergenceTestPointProblem Problem;

      const int nMin = 8;
      const int nMax = 256;

      std::ofstream file;
      file.open("derivative_convergence_point.txt");
      file << std::setprecision(15);

      const auto&& printLine = [&file](const double h, const auto grad)
      {
        file << h;
        for(int i = 0; i < grad.size(); ++i)
          for(int j = 0; j < grad[i].size(); ++j)
            file << " " << grad[i][j];
        file << std::endl;
      };

      for(int n = nMin; n <= nMax; ++n)
      {
        std::cerr << "n=" << n << "<=" << nMax << std::endl;
        Problem problem;

        const double h = 8./n;
        problem.solve(h);
        const auto gradient = problem.gradient();

        printLine(h, gradient);
      }
      
      file.close();
    }
    
    
    static void eggCartonTest()
    {
      // General configuration.
      const double c0     = 0.01;
      const double tau    = 0.5e-2;
      const int nMaxSteps = 1000;
      
      // Set up base problem.
      typedef EggCartonProblem Problem;
      Problem problem;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        return (componentId < 3) ? 1.0 : 1./(2*M_PI*problem.particleRadius(componentId-3));
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      const auto callback = [&file, &problem, &writeVector](const int i, const double t, const auto& x_i, const auto& grad_i, const auto& noise_i)
      {
        std::cerr << "Hit callback " << i << " at time " << t << std::endl;
        
        // Update info in file.
        file << i << " " << t << " ";
        writeVector(x_i);
        file << problem.energy() << " ";
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;
        
        // Save iterate.
        problem.saveMembrane("./iterates/eggCarton_iterate_" + std::string(ceil(log10(nMaxSteps+0.5))-ceil(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
      };
      
      // Run reflective Euler-Maruyama scheme.
      GradientMethods::reflectiveEulerMaruyama(f, problem.initialPosition(), c0, f.lowerBound(), f.upperBound(), tau, nMaxSteps, callback);
    }
    
    
    static void eggCartonTest2()
    {
      // General configuration.
      const double c0     = 0.01;
      const double tau    = 1e-3;
      const int nMaxSteps = 1e4;
      
      // Set up base problem.
      typedef EggCartonPeriodicProblem Problem;
      Problem problem;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        return (componentId < 3) ? 1.0 : 1./(20*M_PI*problem.particleRadius(componentId-3));
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve_periodic.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      const auto callback = [&file, &problem, &writeVector](const int i, const double t, const auto& x_i, const auto& grad_i, const auto& noise_i)
      {
        std::cerr << "Hit callback " << i << " at time " << t << std::endl;
        
        // Update info in file.
        file << i << " " << t << " ";
        writeVector(x_i);
        file << problem.energy() << " ";
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;
        
        Debug::dprintColumnVector(x_i, "x_i");
        Debug::dprintColumnVector(grad_i, "grad_i");
        Debug::dprintColumnVector(noise_i, "noise_i");
        Debug::dprint(problem.energy(), "new energy");

        // Save iterate.
        problem.saveMembrane("./iterates/eggCartonPeriodic_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
      };

      // Run reflective Euler-Maruyama scheme.
      GradientMethods::reflectiveEulerMaruyama(f, problem.initialPosition(), c0, f.lowerBound(), f.upperBound(), tau, nMaxSteps, callback, 10.);
    }
    
    
    static void eggCartonTest2_Backup()
    {
      // General configuration.
      const double c0     = 0.01;
      const double tau    = 1e-3;
      const int nMaxSteps = 1e4;
      
      // Set up base problem.
      typedef EggCartonPeriodicProblem Problem;
      Problem problem;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        return (componentId < 3) ? 1.0 : 1./(20*M_PI*problem.particleRadius(componentId-3));
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve_periodic_continuation.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      const auto callback = [&file, &problem, &writeVector](const int i, const double t, const auto& x_i, const auto& grad_i, const auto& noise_i)
      {
        std::cerr << "Hit callback " << i << " at time " << t << std::endl;
        
        // Update info in file.
        file << i << " " << t << " ";
        writeVector(x_i);
        file << problem.energy() << " ";
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;
        
        Debug::dprintColumnVector(x_i, "x_i");
        Debug::dprintColumnVector(grad_i, "grad_i");
        Debug::dprintColumnVector(noise_i, "noise_i");
        Debug::dprint(problem.energy(), "new energy");

        // Save iterate.
        problem.saveMembrane("./iterates/eggCartonPeriodic_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
      };

      // Run reflective Euler-Maruyama scheme.
      int skipId = 1827;
      auto x0 = problem.initialPosition();
      x0 = { {-1.53347, -1.3833, -0.0288142, 0.0759911, 0.112919, 0.0884073},
      {-0.323466, -1.33869, 0.00595041, 0.255006, -0.00441225, 0.00276427},
      {1.05312, -1.40908, -0.0393455, -0.0666763, -0.32618, 0.0568286},
      {-1.51469, -0.0532244, -0.019887, -0.0584527, 0.00805575, -0.236832},
      {0.0981998, -0.0653874, -0, 0.204564, 0.105335, 0.163116},
      {1.20966, 0.00299264, -0.0120336, 0.108002, -0.495452, -0.0574472},
      {-1.30894, 1.1743, -0.00632475, 0.0309873, -0.0921188, -0.326722},
      {-0.144862, 1.35572, -0.00187424, -0.240159, -0.201851, -0.0792326},
      {1.27134, 1.2119, -0.0106139, -0.0622968, -0.202672, 0.0570694} };
      
      GradientMethods::reflectiveEulerMaruyama(f, x0, c0, f.lowerBound(), f.upperBound(), tau, nMaxSteps, callback, skipId, 10.);
    }


    static void eggCartonTest3()
    {
      // General configuration.
      const int nMaxSteps = 1e4;
      
      // Set up base problem.
      typedef EggCartonPeriodicProblem Problem;
      Problem problem;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        if(componentId == 2)
          return 0.1;
        else if(componentId >= 3)
          return 1./(20*M_PI*problem.particleRadius(componentId-3));
        return 1.0;
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve_periodic_3.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      const auto callback = [&file, &problem, &writeVector](const int i, const int k, const auto& fx_i, const auto& fx_ik, const auto& norm_grad, const auto& x_i, const auto& x_ik, const auto& grad_i, const auto& noise_i, const bool accepted)
      {
        std::cerr << "Hit callback " << i << " at step length iteration " << k << std::endl;
        
        // Update info in file.
        file << i << " " << k << " " << fx_i << " " << fx_ik << " " << norm_grad << " ";
        writeVector(x_i);
        writeVector(x_ik);
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;
        
        Debug::dprintColumnVector(x_i, "x_i");
        Debug::dprintColumnVector(grad_i, "grad_i");
        Debug::dprintColumnVector(noise_i, "noise_i");
        Debug::dprint(problem.energy(), "xk energy");

        // Save iterate.
        if(accepted)
        {
          problem.saveMembrane("./iterates/eggCartonPeriodic3_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
          problem.saveMembrane("./iterates/eggCartonPeriodic3_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
        }
      };

      // Run perturbed projected gradient scheme.
      //~ GradientMethods::perturbedProjectedGradient(f, problem.initialPosition8(), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10.);
      GradientMethods::perturbedProjectedGradient(f, f.applyInverseRescaling(problem.initialPosition()), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10., 0);
    }
    
    static void eggCartonTest3_checkCandidates()
    {      
      // Set up base problem.
      typedef EggCartonPeriodicProblem Problem;
      Problem problem;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        return 1.0;
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve_periodic_3_check_candidates.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      const auto callback = [&file, &problem, &writeVector](const int i, const double r, const auto& fx, const auto& x)
      {
        std::cerr << "Hit callback " << i << " at distance " << r << std::endl;
        
        // Update info in file.
        file << i << " " << r << " " << fx << " ";
        writeVector(x);
        file << std::endl;
        
        Debug::dprintColumnVector(x, "x");
        Debug::dprint(problem.energy(), "x energy");

        // Save iterate.
        const int nMaxSteps = 1000;
        problem.saveMembrane("./iterates/eggCartonPeriodic3_check_candidate_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
        problem.saveMembrane("./iterates/eggCartonPeriodic3_check_candidate_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
      };

      double r = 0.6;
      auto x = problem.initialPosition();
      for(int i = 0; r <= 1.5; ++i, r+=0.05)
      {
        for(int j = 0; j < x.size(); ++j)
        {
          x[j]  *= 0;
          x[j][0] = r*((j%3)-1);
          x[j][1] = r*((j/3)-1);
        }

        const auto fx = f.evaluate(x);
        callback(i, r, fx, x);
      }
    }    
    
    
    static void eggCartonTest4()
    {
      // General configuration.
      const int nMaxSteps = 301;
      const int nParticles = 6;
      
      // Set up base problem.
      typedef EggCartonSaddlePeriodicProblem Problem;
      Problem problem;
      problem.fShift = true;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        if(componentId == 2)
          return 0.1;
        else if(componentId == 5)
          //~ return 1./(2*M_PI*problem.particleRadius(componentId-3));
          return 5.;
        else if(componentId >= 3)
          return 1./(20*M_PI*problem.particleRadius(componentId-3));
        return 1.0;
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve_periodic_4.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      int counter = 0;
      const auto callback = [&file, &problem, &writeVector, &counter](const int i, const int k, const auto& fx_i, const auto& fx_ik, const auto& norm_grad, const auto& x_i, const auto& x_ik, const auto& grad_i, const auto& noise_i, const bool accepted)
      {
        std::cerr << "Hit callback " << i << " at step length iteration " << k << std::endl;
        
        // Update info in file.
        file << i << " " << k << " " << fx_i << " " << fx_ik << " " << norm_grad << " ";
        writeVector(x_i);
        writeVector(x_ik);
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;
        
        Debug::dprintColumnVector(x_i, "x_i");
        Debug::dprintColumnVector(grad_i, "grad_i");
        Debug::dprintColumnVector(noise_i, "noise_i");
        Debug::dprint(problem.energy(), "xk energy");

        // Save iterate.
        if(accepted)
        {
          problem.saveMembrane("./iterates/eggCartonPeriodic4_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
          problem.saveMembrane("./iterates/eggCartonPeriodic4_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
        }
        problem.saveMembrane("./iterates/eggCartonPeriodic4_candidate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(counter++), 0);
      };
      
      
      //~ auto x = problem.initialPosition(6);
      
      //~ try{
        //~ x = {{-1.0000072667691, -1.92349949823937, -0.359159522591107, 0.051302449036851, 0.0214270187939432, -1.62659753178939},
          //~ {-2.74495091921166, -1.47317776946858, 0.128211722757625, -0.00175102499239695, 0.0252166064295969, -2.07550896930764},
          //~ {3.7044688109769, -0.494056977121534, 0.248308013222987, 0.0385644769120725, -0.068338254783218, -3.35773713593549},
          //~ {-1.40433987325169, -0.0823958331559178, -0.237061930336415, 0.0774773175426962, -0.0563457004354865, 1.55009299208697},
          //~ {0, 0, 0, 0, 0, 0},
          //~ {1.33860128562926, 0.423331551115663, -0.12646595081872, 0.0339645610891181, -0.0700589825706385, 0.341590595617907} };
        //~ x.resize(1);
        //~ std::cerr << "f_0 = " << f.evaluate(x) << std::endl;
        //~ problem.saveMembrane("./iterates/f0", 0);
      //~ }catch(...){}
      
      //~ try{
        //~ x = {{-1.0000072667691, -1.92349949823937, -0.359159522591107, 0.051302449036851, 0.0214270187939432, 0},
          //~ {-2.74495091921166, -1.47317776946858, 0.128211722757625, -0.00175102499239695, 0.0252166064295969, -2.07550896930764},
          //~ {3.7044688109769, -0.494056977121534, 0.248308013222987, 0.0385644769120725, -0.068338254783218, -3.35773713593549},
          //~ {-1.40433987325169, -0.0823958331559178, -0.237061930336415, 0.0774773175426962, -0.0563457004354865, 1.55009299208697},
          //~ {0, 0, 0, 0, 0, 0},
          //~ {1.33860128562926, 0.423331551115663, -0.12646595081872, 0.0339645610891181, -0.0700589825706385, 0.341590595617907} };
        //~ x.resize(1);
        //~ std::cerr << "f_1 = " << f.evaluate(x) << std::endl;
        //~ problem.saveMembrane("./iterates/f1", 0);
      //~ }catch(...){}
      

      //~ x[0] *= 0;
      //~ x[0][0] = -1;
      //~ x[0][1] = -1;
      //~ x[0][2] = 0;
      //~ x[0][5] = 1*90.*M_PI/180.;
      //~ std::cerr << "f_4 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
      
      //~ try{
        //~ x[0] *= 0;
        //~ x[0][0] = -1.10959759283548;
        //~ x[0][1] = -1.69891952316403;
        //~ x[0][2] = 0;
        //~ x[0][5] = 0*90.*M_PI/180.;
        //~ std::cerr << "f_0 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
        //~ problem.saveMembrane("./iterates/f0", 0);
      //~ }catch(...){}
      
      //~ try{
        //~ x[0] *= 0;
        //~ x[0][0] = -1.16258340921593;
        //~ x[0][1] = -1.77679416271061; 
        //~ x[0][2] = 0;
        //~ x[0][5] = 0*90.*M_PI/180.;
        //~ std::cerr << "f_0 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
        //~ problem.saveMembrane("./iterates/f1", 0);
      //~ }catch(...){}
      
      //~ try{
        //~ x[0] *= 0;
        //~ x[0][0] = -1.10959759283548;
        //~ x[0][1] = -1.69891952316403;
        //~ x[0][2] = 0;
        //~ x[0][5] = 1*90.*M_PI/180.;
        //~ std::cerr << "f_0 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
        //~ problem.saveMembrane("./iterates/f2", 0);
      //~ }catch(...){}
      
      //~ try{
        //~ x[0] *= 0;
        //~ x[0][0] = -1.16258340921593;
        //~ x[0][1] = -1.77679416271061; 
        //~ x[0][2] = 0;
        //~ x[0][5] = 1*90.*M_PI/180.;
        //~ std::cerr << "f_0 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
        //~ problem.saveMembrane("./iterates/f3", 0);
      //~ }catch(...){}
      
      //~ x[0] *= 0;
      //~ x[0][0] = 0;
      //~ x[0][1] = 0;
      //~ x[0][2] = 0;
      //~ x[0][5] = 0*90.*M_PI/180.;
      //~ std::cerr << "f_2 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
      
      //~ x[0] *= 0;
      //~ x[0][0] = 0;
      //~ x[0][1] = 0;
      //~ x[0][2] = 0;
      //~ x[0][5] = 1*90.*M_PI/180.;
      //~ std::cerr << "f_3 = " << f.evaluate(f.applyInverseRescaling(x)) << std::endl;
      
      //~ DUNE_THROW(Dune::Exception, "Stop! Hammer time.");

      // Run perturbed projected gradient scheme.
      //~ GradientMethods::perturbedProjectedGradient(f, problem.initialPosition8(), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10.);
      GradientMethods::perturbedProjectedGradient(f, f.applyInverseRescaling(problem.initialPosition(nParticles)), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10., 1e-3);
    }

    static void eggCartonTest4_checkCandidates()
    {      
      // Set up base problem.
      typedef EggCartonSaddlePeriodicProblem Problem;
      Problem problem;
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        return 1.0;
      };
      
      // Define optimization functional.
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 1./32);
      
      // Set callback function.
      std::ofstream file;
      file.open("egg_carton_curve_periodic_4_check_candidates.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      const auto callback = [&file, &problem, &writeVector](const int i, const double r, const auto& fx, const auto& x)
      {
        std::cerr << "Hit callback " << i << " at distance " << r << std::endl;
        
        // Update info in file.
        file << i << " " << r << " " << fx << " ";
        writeVector(x);
        file << std::endl;
        
        Debug::dprintColumnVector(x, "x");
        Debug::dprint(problem.energy(), "x energy");

        // Save iterate.
        const int nMaxSteps = 1000;
        problem.saveMembrane("./iterates/eggCartonPeriodic4_check_candidate_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
        problem.saveMembrane("./iterates/eggCartonPeriodic4_check_candidate_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
      };

      double r = 0.6;
      auto x = problem.initialPosition();
      for(int i = 0; r <= 1.5; ++i, r+=0.05)
      {
        for(int j = 0; j < x.size(); ++j)
        {
          x[j]  *= 0;
          x[j][0] = r*((j%3)-1);
          x[j][1] = r*((j/3)-1);
        }

        const auto fx = f.evaluate(x);
        callback(i, r, fx, x);
      }
    }
    
    
    static void tubeInteractionTest(const double slope = 1.0, const double angle = 90.0)
    {
      // General configuration.
      const int nMaxSteps = 1e4;
      
      // Set up base problem.
      typedef TubeInteractionProblem Problem;
      Problem problem(slope, angle);
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        return 1.0;
      };
      
      // Define optimization functional.
      //~ RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 2.*M_PI/256);
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, 2.*M_PI/128);
      
      // Set callback function.
      std::ofstream file;
      file.open("tube_interaction.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };
      
      int counter = 0;
      const auto callback = [&file, &problem, &writeVector, &counter](const int i, const int k, const auto& fx_i, const auto& fx_ik, const auto& norm_grad, const auto& x_i, const auto& x_ik, const auto& grad_i, const auto& noise_i, const bool accepted)
      {
        std::cerr << "Hit callback " << i << " at step length iteration " << k << std::endl;
        
        // Update info in file.
        file << i << " " << k << " " << fx_i << " " << fx_ik << " " << norm_grad << " ";
        writeVector(x_i);
        writeVector(x_ik);
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;
        
        Debug::dprintColumnVector(x_i, "x_i");
        Debug::dprintColumnVector(grad_i, "grad_i");
        Debug::dprintColumnVector(noise_i, "noise_i");
        Debug::dprint(problem.energy(), "xk energy");

        // Save iterate.
        if(accepted)
        {
          problem.saveMembrane("./iterates/tube_interaction_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 2);
          problem.saveMembrane("./iterates/tube_interaction_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
          problem.saveCrossSection("./iterates/cross_section_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i));
        }
        problem.saveMembrane("./iterates/tube_interaction_candidate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(counter++), 0);
      };

      // Run perturbed projected gradient scheme.
      //~ GradientMethods::perturbedProjectedGradient(f, problem.initialPosition8(), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10.);
      GradientMethods::perturbedProjectedGradient(f, f.applyInverseRescaling(problem.initialPosition()), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10., 1e-2);
    }



    static void checkFiniteDifferenceConvergence()
    {
      typedef DerivativeConvergenceTestCurveProblem2 Problem;

      const int nMax = 128;
      const auto nParticles = Problem::nParticles;

      std::ofstream file;
      file.open("finite_difference_convergence.txt");

      const auto&& printLine = [&file](const int exp, const auto grad)
      {
        file << std::setprecision(15);
        file << exp;
        for(int i = 0; i < grad.size(); ++i)
          for(int j = 0; j < grad[i].size(); ++j)
            file << " " << grad[i][j];

        file << std::endl;
      };

      for(int exp = 1; exp <= 10; ++exp)
      {
        const double delta_t = std::pow(10,-exp);

        typename Problem::Position dif_quot_gradient(nParticles);
        Problem problem;
        dif_quot_gradient = problem.differenceQuotients(1./nMax, delta_t);
        Debug::dprintVector(dif_quot_gradient, "Difference quotient gradient");

        printLine(exp, dif_quot_gradient);
      }

      file.close();
    }


    static void checkFiniteDifferenceConvergenceExt()
    {
      typedef DerivativeConvergenceTestCurveProblem2 Problem;

      const auto nParticles = Problem::nParticles;

      std::ofstream file;
      file.open("finite_difference_convergence_ext.txt");

      const auto&& printLine = [&file](const int n, const int exp, const auto grad)
      {
        file << std::setprecision(15);
        file << n;
        file << " " << exp;
        for(int i = 0; i < grad.size(); ++i)
          for(int j = 0; j < grad[i].size(); ++j)
            file << " " << grad[i][j];

        file << std::endl;
      };

      const int n0 = 32;
      for(int i = 0; i <= 8*2; ++i)
      {
        const int nMax = std::round(n0 * std::pow(2.,0.5*i));
        for(int exp = 4; exp <= 4; ++exp)
        {
          const double delta_t = std::pow(10,-exp);

          typename Problem::Position dif_quot_gradient(nParticles);
          Problem problem;
          dif_quot_gradient = problem.differenceQuotients(1./nMax, delta_t);
          Debug::dprintVector(dif_quot_gradient, "Difference quotient gradient");

          printLine(nMax, exp, dif_quot_gradient);
        }
      }

      file.close();
    }


    static void FCHo2Test(const int configurationId = 0, const double repulsion = 0)
    {
      // General configuration.
      const int nMaxSteps = 1e4;
      
      // Set up base problem.
      //~ typedef FCHo2PeriodicProblem Problem;
      //~ Problem problem(6+(configurationId==2), repulsion);
      typedef FCHo2PeriodicProblem Problem;
      Problem problem(6-(configurationId==2), repulsion);
      
      // Define rescaling.
      const auto&& rescaling = [&problem](const int particleId, const int componentId)
      {
        if(componentId < 2)
          return 100.0;
        if(componentId == 2)
          return 10.;
        else if(componentId == 3 || componentId == 4)
          return 100./(20*M_PI*problem.particleRadius(componentId-3));
        else if(componentId == 5)
          return 1000./(20*M_PI*problem.particleRadius(componentId-3));
        return 1.0;
      };
      
      // Define optimization functional.
      const double h = Problem::length/32;
      RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);
      
      // Set callback function.
      std::ofstream file;
      file.open("FCHo2_" + std::to_string(configurationId) + "_" + std::to_string(repulsion) + ".txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << vec[i][j] << " ";
      };

      int counter = 0;
      const auto callback = [&file, &problem, &writeVector, &configurationId, &repulsion, &counter](const int i, const int k, const auto& fx_i, const auto& fx_ik, const auto& norm_grad, const auto& x_i, const auto& x_ik, const auto& grad_i, const auto& noise_i, const bool accepted)
      {
        std::cerr << "Hit callback " << i << " at step length iteration " << k << std::endl;

        // Update info in file.
        file << i << " " << k << " " << fx_i << " " << fx_ik << " " << norm_grad << " ";
        writeVector(x_i);
        writeVector(x_ik);
        writeVector(grad_i);
        writeVector(noise_i);
        file << std::endl;

        Debug::dprintColumnVector(x_i, "x_i");
        Debug::dprintColumnVector(grad_i, "grad_i");
        Debug::dprintColumnVector(noise_i, "noise_i");
        Debug::dprint(problem.energy(), "xk energy");

        // Save iterate.
        if(accepted)
        {
          problem.saveMembrane("./iterates/FCHo2_" + std::to_string(configurationId) + "_" + std::to_string(repulsion) + "_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
          problem.saveMembrane("./iterates/FCHo2_" + std::to_string(configurationId) + "_" + std::to_string(repulsion) + "_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 1);
        }
        problem.saveMembrane("./iterates/FCHo2_candidate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(counter++), 0);
      };
      
      
      // Set projection function.
      const auto& lb = problem.lowerBound();
      const auto& ub = problem.upperBound();
      FCHo2Projection<typename std::decay<decltype(problem.initialPosition())>::type,typename FCHo2ParticleRaw::ParticleCoordinates,typename Problem::Transformation>
        P(FCHo2ParticleRaw::rawMinimalPositions, problem.getTransformation(), lb, ub, 20);
      const auto&& projection = [&P, &f](const auto& y)
      {
        // Rescale y.
        const auto&& y_ = f.applyRescaling(y);
        
        // Compute projection and un-scale.
        const auto&& x  = P.evaluate(y_, y_);
        return f.applyInverseRescaling(x);
      };

      //~ projection(f.applyInverseRescaling(problem.initialPosition()));
      //~ std::cerr << "Done with test." << std::endl;
      //~ DUNE_THROW(Dune::Exception, "Stop.");

      // Run perturbed projected gradient scheme.
      //~ GradientMethods::perturbedProjectedGradient(f, problem.initialPosition8(), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0, 10.);
      //~ GradientMethods::perturbedProjectedGradient(f, f.applyInverseRescaling(problem.initialPosition(configurationId)), projection, nMaxSteps, callback, 0, 10., 1e-3);
      GradientMethods::fRethrow = true;
      if(repulsion == 0. && false)
        GradientMethods::transitioningGradient(f, f.applyInverseRescaling(problem.initialPosition(configurationId)), projection, 99, nMaxSteps, callback, 0.00, 10., 1e-3);
      else
        GradientMethods::perturbedProjectedGradient(f, f.applyInverseRescaling(problem.initialPosition(configurationId)), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0*0.05, 0., 1e-3);
        //~ GradientMethods::transitioningGradient(f, f.applyInverseRescaling(problem.initialPosition(configurationId)), f.lowerBound(), f.upperBound(), 99, nMaxSteps, callback, 0.00, 1., 1e-3);
    }


    static void gradientsCurveExact( )
    {
      typedef AnnulusCurveConvergenceTestProblem Problem;
      std::ofstream file;
      file.open("gradients_curve_exact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };

      for(int i = 16; i <= 256; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i;

        Problem problem;
        problem.solve(h);
        
        const auto&& gradient = problem.gradient();

        file << h;
        writeVector(gradient);
        file << std::endl;
      }
      file.close();
    }


    static void gradientsCurveInexact( )
    {
      typedef CurveConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("gradients_curve_inexact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };

      for(int i = 16; i <= 256; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i;

        Problem problem;
        problem.solve(h);
        
        const auto&& gradient = problem.gradient();

        file << h;
        writeVector(gradient);
        file << std::endl;
      }
      file.close();
    }


    static void gradientsPointExact( )
    {
      typedef AnnulusPointConvergenceTestProblem Problem;
      std::ofstream file;
      file.open("gradients_point_exact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };

      for(int i = 16; i <= 256; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i;

        Problem problem;
        problem.solve(h);
        
        const auto&& gradient = problem.gradient();

        file << h;
        writeVector(gradient);
        file << std::endl;
      }
      file.close();
    }


    static void gradientsPointInexact( )
    {
      typedef PointConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("gradients_point_inexact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };

      for(int i = 16; i <= 256; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i*4;

        Problem problem;
        problem.solve(h);
        
        const auto&& gradient = problem.gradient();

        file << h;
        writeVector(gradient);
        file << std::endl;
      }
      file.close();
    }


    /*static void differenceQuotientsCurveInexact( )
    {
      typedef CurveConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("difference_quotients_curve_inexact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };
      //~ const double h = 2./256;
      const double h = 2./400; const bool fBreak = false;
      int nParticles = 0;
      typename Problem::Model::Position x;
      {
        Problem problem;
        x = problem.initialPosition();
        nParticles = x.size();
      }

      //~ for(int pow = 1; pow <= 10; ++pow)
      for(int pow = 3; pow <= 3; ++pow)
      {
        std::cerr << "Iteration pow = " << pow << std::endl;
        std::cerr << std::setprecision(15);
        const double t = std::pow(10, -pow);

        file << pow;
        for(int particleId = 0; particleId < nParticles; ++particleId)
        {
          for(int dir = 0; dir < x[particleId].size(); ++dir)
          {
            try{
              double fxp;
              auto x_p = x;
              x_p[particleId][dir]  += t;
              {
                Problem problem;
                fxp = problem.solve(h, x_p);
              }
              
              double fxm;
              auto x_m = x;
              x_m[particleId][dir]  -= t;
              {
                Problem problem;
                fxm = problem.solve(h, x_m);
              }
              std::cerr << "Values for " << particleId << "," << dir << ": " << fxp << "; " << fxm << "; " << t << " yields " << (fxp-fxm)/(2*t) << std::endl;
              file << " " << (fxp-fxm)/(2*t);
            }catch(...){
              file << " " << std::numeric_limits<double>::quiet_NaN();
            }
            if(fBreak)
              break;
          }
          if(fBreak)
            break;
        }
        file << std::endl;
      }
      file.close();
    }*/
    
    static void differenceQuotientsCurveInexact( )
    {
      typedef CurveConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("difference_quotients_curve_inexact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };
      //~ const double h = 2./256;
      //~ const double h = 2./400; const bool fBreak = false;
      int nParticles = 0;
      typename Problem::Model::Position x;
      {
        Problem problem;
        x = problem.initialPosition();
        nParticles = x.size();
      }

      for(int i = 16; i <= 256; ++i)
      {
        std::cerr << std::setprecision(15);
        const double h = 2./i;
        const double h_ = std::pow(h, 1);
        std::cerr << "Iteration i = " << i << " with h = " << h << std::endl;

        file << h;
        for(int particleId = 0; particleId < nParticles; ++particleId)
        {
          for(int dir = 0; dir < x[particleId].size(); ++dir)
          {
            const double t = (dir != 5) ? 2*h_ : 10*h_;
            try{
              double fxp;
              auto x_p = x;
              x_p[particleId][dir]  += t;
              {
                Problem problem;
                fxp = problem.solve(h, x_p);
              }
              
              double fxm;
              auto x_m = x;
              x_m[particleId][dir]  -= t;
              {
                Problem problem;
                fxm = problem.solve(h, x_m);
              }
              std::cerr << "Values for " << particleId << "," << dir << ": " << fxp << "; " << fxm << "; " << t << " yields " << (fxp-fxm)/(2*t) << std::endl;
              file << " " << (fxp-fxm)/(2*t);
            }catch(...){
              file << " " << std::numeric_limits<double>::quiet_NaN();
            }
          }
        }
        file << std::endl;
      }
      file.close();
    }


    static void differenceQuotientsPointInexact( )
    {
      typedef PointConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("difference_quotients_point_inexact.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };
      //~ const double h = 2./256;
      //~ const double h = 2./400; const bool fBreak = false;
      int nParticles = 0;
      typename Problem::Model::Position x;
      {
        Problem problem;
        x = problem.initialPosition();
        nParticles = x.size();
      }

      for(int i = 16; i <= 256; ++i)
      {
        std::cerr << std::setprecision(15);
        const double h = 8./i;
        const double h_ = std::pow(h, 1);
        std::cerr << "Iteration i = " << i << " with h = " << h << std::endl;

        file << h;
        for(int particleId = 0; particleId < nParticles; ++particleId)
        {
          for(int dir = 0; dir < x[particleId].size(); ++dir)
          {
            const double t = (dir != 5) ? 2*h_ : 10*h_;
            try{
              double fxp;
              auto x_p = x;
              x_p[particleId][dir]  += t;
              {
                Problem problem;
                fxp = problem.solve(h, x_p);
              }
              
              double fxm;
              auto x_m = x;
              x_m[particleId][dir]  -= t;
              {
                Problem problem;
                fxm = problem.solve(h, x_m);
              }
              std::cerr << "Values for " << particleId << "," << dir << ": " << fxp << "; " << fxm << "; " << t << " yields " << (fxp-fxm)/(2*t) << std::endl;
              file << " " << (fxp-fxm)/(2*t);
            }catch(...){
              file << " " << std::numeric_limits<double>::quiet_NaN();
            }
          }
        }
        file << std::endl;
      }
      file.close();
    }
    
    
    
    
    
    // TEMPORARY
    static void gradientsCurveInexactTmp( )
    {
      typedef CurveConvergenceTestProblem3 Problem;
      std::ofstream file;
      file.open("gradients_curve_inexact_tmp.txt");
      file << std::setprecision(15);

      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };

      for(int i = 128; i <= 128; ++i)
      {
        std::cerr << "Iteration i = " << i << std::endl;
        double h = 2./i;

        Problem problem;
        problem.solve(h);
        
        const auto&& gradient = problem.gradient();

        file << h;
        writeVector(gradient);
        file << std::endl;
        
        Debug::dprintVector(gradient, "gradient");
      }
      
      
      int nParticles = 0;
      typename Problem::Model::Position x;
      {
        Problem problem;
        x = problem.initialPosition();
        nParticles = x.size();
      }
      
      const double h = 2./128;
      const double t = 2*h;
      
      try{
        int particleId = 0;
        int dir = 4;

        double fxp;
        auto x_p = x;
        x_p[particleId][dir]  += t;
        {
          Problem problem;
          fxp = problem.solve(h, x_p);
        }
        
        double fxm;
        auto x_m = x;
        x_m[particleId][dir]  -= t;
        {
          Problem problem;
          fxm = problem.solve(h, x_m);
        }
        std::cerr << "Values for " << particleId << "," << dir << ": " << fxp << "; " << fxm << "; " << t << " yields " << (fxp-fxm)/(2*t) << std::endl;
      }catch(...){
        // Do nuffin.
      }
    }
  
    static void freeEnergySlope( )
    {
      typedef AnnulusCurveConvergenceTestProblem Problem;
      std::ofstream file;
      file.open("free_energy_slope.txt");
      file << std::setprecision(15);
      const auto writeVector = [&file](const auto vec)
      {
        for(int i = 0; i < vec.size(); ++i)
          for(int j = 0; j < vec[i].size(); ++j)
            file << " " << vec[i][j];
      };
      
      const int nMax = 8;
      const double h = 2./std::pow(2,nMax-1);
      const double particleRadius = 0.1;
      const double slope0 = 0.1;
      const double slope1 = 1.0; // not necessary due to linearity of energy in slope (if height is 0)
      
      const double L = 1 - particleRadius;
      
      // Sample from the interval of interest in a hierarchical manner.
      for(int n = 0; n <= nMax; ++n)
      {
        std::cerr << "Level n = " << n << std::endl;
        
        const double step = L*std::pow(0.5,n);
        for(double r = (n == 0) ? 0 : step; r < L; r += 2*step)
        {
          try{
            std::cerr << "At r = " << r << std::endl;
            Problem problem(particleRadius, 0, slope0);
            const auto E0 = problem.solve(h,r);
            const auto G = problem.gradient();
            file << r << " " << E0;
            writeVector(G);
            file << std::endl;
          }catch(...){
          }
        }
      }
      file.close();
    }
    
    
    // Save the solutions of the problems that are relevant in the dissertation. (id 96)
    // This entails curveExact, curveExact2, pointExact and pointInexact2
    static void saveSolutions()
    {
      {
        typedef AnnulusCurveConvergenceTestProblem Problem;
        Problem problem;
        problem.solveExactly(2./512);
        problem.saveMembrane("./iterates/curveExactMembrane_solution", 2);
      }
      
      {
        typedef CurveConvergenceTestProblem3 Problem;
        Problem exact;
        exact.solve(1./128);
        exact.saveMembrane("./iterates/curveInexactMembrane_solution", 3);
      }

      {
        typedef AnnulusPointConvergenceTestProblem Problem;
        Problem problem;
        problem.solveExactly(2./512);
        problem.saveMembrane("./iterates/pointExactMembrane_solution", 2);
      }
      
      {
        typedef PointConvergenceTestProblem3 Problem;
        Problem exact;
        exact.solve(4./128);
        exact.saveMembrane("./iterates/pointInexactMembrane_solution", 3);
      }
    }
};

#endif
