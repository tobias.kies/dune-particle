#ifndef DUNE_EGG_CARTON_PROBLEM
#define DUNE_EGG_CARTON_PROBLEM

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/surface/planarRectangularSurface.hh>

#include <dune/particle/particle/ellipsoidalParticle.hh>
#include <dune/particle/integrand/mongeGaugeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/particle/derivative/derivativeVectorField_MongeGaugeSpecialization.hh>

class EggCartonProblem
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef PlanarRectangularSurface<> Surface;
    typedef EllipsoidalParticle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef MongeGaugeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;

    static double ellipse_a; // For init values see below, please. (I had linking problems and did not know why so I applied a workaround.)
    static double ellipse_b;
    static constexpr double slope     = 1;
    static constexpr double length    = 2;

    EggCartonProblem()
      : u(0), surface(length), particle(ellipse_a, ellipse_b, 0, slope), transformation()
    {
    }
    
    auto initialPosition() const
    {
      Position x(2);
      
      x[0]  = {-1, 1, 0, 0, 0, 0};
      x[1]  = {1.1, -.9, 0, 0, 0, 0};
      
      return x;
    }
    
    auto upperBound() const
    {
      auto up = initialPosition();
      for(int i = 0; i < up.size(); ++i)
      {
        up[i][0] = length;
        up[i][1] = length;
        up[i][2] = std::numeric_limits<double>::infinity();
        up[i][3] = 15*M_PI/180;
        up[i][4] = 15*M_PI/180;
        up[i][5] = std::numeric_limits<double>::infinity();
      }
      return up;
    }
    
    auto lowerBound() const
    {
      auto lo = upperBound();
      lo *= -1;
      return lo;
    }
    
    double particleRadius(const int axis) const
    {
      if(axis == 2)
        return std::max(ellipse_a, ellipse_b);
        
      if(axis == 1)
        return ellipse_a;
        
      return ellipse_b; // axis == 0
    }
    

    auto solve(const double h, const Model::Position& position)
    {
      lastPosition = position;
      
      // Delete grid (if there is one).
      pGrid.reset();

      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      surface.updateConstraints(*pBasis);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      energy_u = 0.5*(u*Au);
      Debug::dprint("Got energy", energy_u);
      return energy_u;
    }
    
    auto energy() const
    {
      return energy_u;
    }

    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      typedef DerivativeVectorField_MongeGaugeSpecialization<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      const auto dif = derivative.evaluateGradient(lastPosition);
      Debug::dprint("Evaluated gradient.");

      return dif;
    }

    void saveMembrane(const std::string& fileName = "membrane")
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), 0);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }
    
    auto& solver() const
    {
      return *pMembraneSolver;
    }


  protected:

    Dune::ParameterTree params;

          Surface         surface;
    const Particle        particle;
    const Transformation transformation;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Position lastPosition;
    Vector u;
    double energy_u;
};

double EggCartonProblem::ellipse_a = 1./3;
double EggCartonProblem::ellipse_b = EggCartonProblem::ellipse_a;

#endif
