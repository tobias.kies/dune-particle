#ifndef DUNE_TUBE_INTERACTION_PROBLEM
#define DUNE_TUBE_INTERACTION_PROBLEM

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/surface/tubularSurface.hh>
#include <dune/particle/particle/ellipsoidalTubeParticle.hh>
#include <dune/particle/integrand/tubeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/particle/derivative/derivativeVectorField.hh>

#warning No periodicity is set at all here.

class TubeInteractionProblem
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef TubularSurface<> Surface;
    //~ typedef EllipsoidalTubeParticle<> Particle;
    typedef EllipsoidalParticle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef TubeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;

    static constexpr double tubeLength        = 6;
    const double tubeRadius                   = 1;
    const            double ellipse_a;
    const            double ellipse_b;
    static constexpr double ellipse_height    = 0.0;
    const            double ellipse_slope;
    const            double separationAngle;
    static constexpr double particleCapRadius = 0.5;

    TubeInteractionProblem(const double ellipse_slope = 1.0, const double separationAngle = 90, const double radius = 0.2)
      //~ : u(0), surface(tubeLength, tubeRadius), particle(ellipse_a, ellipse_b, particleCapRadius), transformation()
      : ellipse_a(radius), ellipse_b(radius), ellipse_slope(ellipse_slope), separationAngle(separationAngle), u(0), surface(tubeLength, tubeRadius), particle(ellipse_a, ellipse_b, ellipse_height, ellipse_slope), transformation()
    {
    }

    auto initialPosition() const
    {
      const int nParticles = 2;
      Position position(nParticles);
      
      //~ const double alpha  = 90*M_PI/180;
      const double alpha  = separationAngle*M_PI/180;
      const double elev1  = tubeRadius;
      const double elev2  = tubeRadius;
      
      position[0] = {tubeLength/2, 0, elev1, 0, 0, 0}; // corresponds to alpha = 0
      if(nParticles > 1)
        position[1] = {tubeLength/2, elev2*sin(alpha), elev2*cos(alpha), (2*M_PI-alpha), 0, 0};
      
      return position;
    }
    
    auto upperBound() const
    {
      auto up = initialPosition();
      for(int i = 0; i < up.size(); ++i)
      {
        up[i][0] = tubeLength-2*std::max(ellipse_a,ellipse_b);
        up[i][0] = tubeLength/2.;
        up[i][1] = std::numeric_limits<double>::infinity();
        up[i][2] = std::numeric_limits<double>::infinity();
        
        up[i][3] = std::numeric_limits<double>::infinity();
        up[i][4] = std::numeric_limits<double>::infinity();
        up[i][5] = std::numeric_limits<double>::infinity();
      }
      return up;
    }
    
    auto lowerBound() const
    {
      auto lo = upperBound();
      lo *= -1;
      for(int i = 0; i < lo.size(); ++i)
        //~ lo[i][0]  = 2*std::max(ellipse_a,ellipse_b);
        lo[i][0]  = tubeLength/2.;
      return lo;
    }
    
    double particleRadius(const int axis) const
    {
      if(axis == 2)
        return std::max(ellipse_a, ellipse_b);
        
      if(axis == 1)
        return ellipse_a;
        
      return ellipse_b; // axis == 0
    }
    

    auto solve(const double h, const Model::Position& position)
    {
      lastPosition = position;
      
      // Delete grid (if there is one).
      pGrid.reset();

      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      //~ pModel->setPeriodicity(true);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      surface.updateConstraints(*pBasis);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,tubeRadius,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      energy_u = 0.5*(u*Au);
      Debug::dprint("Got energy", energy_u);
      return energy_u;
    }
    
    auto energy() const
    {
      return energy_u;
    }

    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,tubeRadius,f);

      // Compute derivative.
      typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      const auto dif = derivative.evaluateGradient(lastPosition);
      Debug::dprint("Evaluated gradient.");

      return dif;
    }

    void saveMembrane(const std::string& fileName = "membrane", const int nRefinement = 3)
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), nRefinement);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    void saveCrossSection(const std::string& fileName)
    {
      std::ofstream file;
      file.open(fileName + ".txt");
      file << std::setprecision(15);
      
      // Write position info. (Yes, this is kind of dirty.)
      const auto& position = lastPosition;
      if(position.size() > 0)
        for(int i = 0; i < position[0].size(); ++i)
        {
          for(int j = 0; j < position.size(); ++j)
            file << position[j][i] << " ";
          file << std::endl;
        }
      
      // Write actual cross section info.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      BasisMultiDifferentiableGridFunction<COBasis,Vector,Dune::FieldVector<size_t,2>> f(coBasis, u);
      
      const int N = 360*10;
      const double h = 2*M_PI/N;
      for(int i = 0; i < N; ++i)
        file << i*h << " " << f.evaluate({3., i*h}) << std::endl;
      
      file.close();
    }
    
    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }
    
    auto& solver() const
    {
      return *pMembraneSolver;
    }


  protected:

    Dune::ParameterTree params;

    Surface        surface;
    Particle       particle;
    Transformation transformation;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Position lastPosition;
    Vector u;
    double energy_u;
};
#endif
