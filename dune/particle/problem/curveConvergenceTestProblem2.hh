#ifndef DUNE_CURVE_CONVERGENCE_TEST_PROBLEM_2_HH
#define DUNE_CURVE_CONVERGENCE_TEST_PROBLEM_2_HH

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/transformation/otherSpatialTransformation.hh>
#include <dune/particle/surface/tubularSurface.hh>
#include <dune/particle/particle/ellipsoidalTubeParticle.hh>
#include <dune/particle/integrand/tubeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>


class CurveConvergenceTestProblem2
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef TubularSurface<> Surface;
    typedef EllipsoidalTubeParticle<> Particle;
    typedef OtherSpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef TubeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;

    static constexpr double tubeLength      = 6;
    static constexpr double tubeRadius      = 1; // Need to adapt integrand further below before changing this! Had linking problems.
    static constexpr double ellipse_a       = 3./7;
    static constexpr double ellipse_b       = 1.5/7;
    static constexpr double ellipse_height  = 0.1;
    //~ static constexpr double ellipse_slope   = 1;
    static constexpr double particleRadius  = 0.6;

    CurveConvergenceTestProblem2()
      : u(0),
        surface(tubeLength, tubeRadius),
        particle(ellipse_a, ellipse_b, particleRadius),
        transformation()
    {
      assert(ellipse_a < particleRadius);
      assert(ellipse_b < particleRadius);
    }

    auto solve(const double h)
    {
      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      typename Model::Position position(3);
      const double alpha = 30*M_PI/180;
      const double elev  = tubeRadius+0*ellipse_height;
      const double elev2  = tubeRadius-1*ellipse_height;
      position[0] = {1.5, sin(-alpha)*elev2, cos(-alpha)*elev2, -alpha, 0,  45*M_PI/180};
      position[1] = {3.0, 0, elev, 0, 0,  90*M_PI/180};
      position[2] = {4.5, sin(-alpha)*elev2, cos(-alpha)*elev2, -alpha, 0,  135*M_PI/180};
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      surface.updateConstraints(*pBasis);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,1,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      Solver membraneSolver(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = membraneSolver.solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      const auto energy = 0.5*(u*Au);
      Debug::dprint("Got energy", energy);
      return energy;
    }

    void saveMembrane(const std::string& fileName = "membrane")
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), 2);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }


    // Approach here: Use a high order quadrature rule on the coarse
    // grid and apply it against the exact solution function.
    template<class OtherProblem>
    auto measureError(const OtherProblem& other) const
    {
      DUNE_THROW(Dune::Exception, "Please check first if the error measurement routines from TestProblem3 would be more reasonable." );
      double error = 0;

      const auto& basis       = *pBasis;
      const auto& domainInfo  = *pDomainInfo;

      const auto& o_basis     = other.basis();
      typedef typename std::decay<decltype(o_basis)>::type OBasis;
      const auto& o_u         = other.membrane();

      // Set up functions.
      BasisMultiDifferentiableGridFunction<Basis,Vector,Dune::FieldVector<size_t,2>> func_u(basis, u);
      BasisMultiDifferentiableGridFunction<OBasis,Vector,Dune::FieldVector<size_t,2>> func_ex(o_basis, o_u);

      // Iterate over all elements.
      for(const auto& e : elements(basis.getGridView()))
      {
        // Get quadrature rule.
        const bool isInside   = domainInfo.isInside(e);
        const bool isOutside  = domainInfo.isOutside(e);

        if(isOutside)
          continue;

        const auto tFE = basis.getLocalFiniteElement(e);
        QuadratureRuleKey quadKey(tFE);
        quadKey.setOrder(tFE.localBasis().order()*2 + 10);
        auto quadratureRule = (isInside) ? QuadratureRuleCache<double,2>::rule(quadKey) : LocalParameterizationQuadrature<double>::rule(quadKey, e, domainInfo);

        for(const auto& quadPoint : quadratureRule)
        {
          // Update error term.
          const auto& xLocal = quadPoint.position();
          const auto xGlobal = e.geometry().global(xLocal);
          const auto dif   = func_u.evaluateLaplacianLocal(e,xLocal) - func_ex.evaluate(xGlobal,{2,0}) - func_ex.evaluate(xGlobal,{0,2});
          error += dif*dif*quadPoint.weight()*e.geometry().integrationElement(xLocal);
        }
      }

      return sqrt(error);
    }


  protected:

    Dune::ParameterTree params;

          Surface         surface;
    const Particle        particle;
    const Transformation transformation;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;

    Vector u;
};
#endif
