#ifndef DUNE_FCHO2_PERIODIC_PROBLEM
#define DUNE_FCHO2_PERIODIC_PROBLEM

#include <dune/particle/problem/genericIncludes.hh>

#include <dune/particle/surface/planarRectangularPeriodicSurface.hh>

#include <dune/particle/particle/FCHo2Particle.hh>
#include <dune/particle/integrand/mongeGaugeIntegrand.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>

#include <dune/particle/derivative/derivativeVectorField_MongeGaugeSpecialization.hh>

class FCHo2PeriodicProblem
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef PlanarRectangularPeriodicSurface<> Surface;
    typedef FCHo2Particle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef MongeGaugeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;
    
    typedef Dune::FieldVector<double,2> QuadPoint;
    typedef std::vector<QuadPoint> QuadPoints;

    static constexpr double length    = 500;
    
    const bool fRefineGrid = true;
    //~ const int nMaxRefinementMarks = 1e3;
    const int nMaxGridSize = 20000;
    const double refinementDistance = 10;
    const bool fMakeRepulsionFinite = true;

    FCHo2PeriodicProblem(const int nParticles = 6, const double repulsionConstant = 0.)
      : u(0), surface(length), particle(), transformation(), nParticles(nParticles), repulsionConstant(repulsionConstant)
    {
      //~ assert(nParticles <= 6);
      
      particle.makePeriodic(surface);
      transformation.makePeriodic(surface);
      
      initQuadrature();
    }

    auto initialPosition(const int id = 0) const
    {
      Position x_;
      
      const auto DEG = M_PI/180.;;
      
      switch(id)
      {
        case 1:
          x_  = {
            {   0,    0, 0, 0, 0,  0 },
            {   0,  100, 0, 0, 0,  0 },
            {   0, -100, 0, 0, 0,  0 },
            {   0,  200, 0, 0, 0,  0 },
            {   0, -200, 0, 0, 0,  0 },
            {   0,  300, 0, 0, 0,  0 }
          };
          break;
          
        case 2:
          //~ x_  = {
            //~ {  -300,  10, 0, 0, 0, 110*DEG },
            //~ {  -200, -30, 0, 0, 0,  80*DEG },
            //~ {  -100,  20, 0, 0, 0, 100*DEG },
            //~ {     0,   0, 0, 0, 0,  90*DEG },
            //~ {   100,  30, 0, 0, 0,  70*DEG },
            //~ {   200, -10, 0, 0, 0, 110*DEG },
            //~ {   300, -20, 0, 0, 0,  80*DEG }
          //~ };
          x_  = {
            {  -300,  10, 0, 0, 0, 110*DEG },
            {  -150,  20, 0, 0, 0, 100*DEG },
            {     0,   0, 0, 0, 0,  90*DEG },
            {   150,  30, 0, 0, 0,  70*DEG },
            {   300, -20, 0, 0, 0,  80*DEG }
          };
          break;
          
        default:
          x_  = {
            {   0,    0, 0, 0, 0,  0   },
            {   0,  200, 0, 0, 0,  1.6 },
            { 120, -150, 0, 0, 0, -0.75},
            {-220,  140, 0, 0, 0, -0.5 },
            { 240,   80, 0, 0, 0,  0   },
            {-160, -150, 0, 0, 0,  0.8 }
          };
          //~ x_  = {
            //~ {   0,    0, 0, 0, 0,  0 },
            //~ {   0,  100, 0, 0, 0,  0 }
          //~ };
          //~ x_  = {
            //~ {   0,    0, 0, 0, 0,  0 },
            //~ {   390,  100, 0, 0, 0,  0 }
          //~ };
          break;
        
      }
      
      Position x(std::min(nParticles,(int)x_.size()));
      for(int i = 0; i < std::min(nParticles,(int)x_.size()); ++i)
        x[i]  = x_[i];
      
      return x;
    }
    
    auto upperBound() const
    {
      auto up = initialPosition();
      
      for(int i = 0; i < up.size(); ++i)
      {
        up[i][0] = std::numeric_limits<double>::max();
        up[i][1] = std::numeric_limits<double>::max();
        up[i][2] = 0.5;
        
        up[i][3] = 0*10*M_PI/180;
        up[i][4] = 0*10*M_PI/180;
        up[i][5] = std::numeric_limits<double>::max();
      }
        
      // Fix the position and x1-x2-rotation of one particle for uniqueness reasons.
      // We choose the central one at id i==0. (at at id==4 for configurationId==2)
      //~ up[0+4*(nParticles==7)][0] = 0;
      //~ up[0+4*(nParticles==7)][1] = 0;
      //~ up[0+4*(nParticles==7)][2] = 0;
      //~ up[0+4*(nParticles==7)][5] = 0;

      Debug::dprintVector(up, "up");
      return up;
    }
    
    auto lowerBound() const
    {
      auto lo = upperBound();
      lo *= -1;
      Debug::dprintVector(lo, "lo");
      return lo;
    }
    
    double particleRadius(const int axis) const
    {
      // Inspired by the dimensions of the FCHo2 particle.
      if(axis == 2)
        return std::max(85, 8);
        
      if(axis == 1)
        return 85;
        
      return 8; // axis == 0
    }
    

    auto solve(const double h, const Model::Position& position)
    {
      lastPosition = position;
      
      if(!isFeasible(position))
      {
        energy_u = std::numeric_limits<double>::infinity();
        return energy_u;
      }
      
      // Delete grid (if there is one).
      pGrid.reset();

      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      pModel->setPeriodicity(true);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);
      if(fRefineGrid)
        gridRefine(*pGrid, position);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      ConstraintsMachine::updateConstraints(*pBasis, surface, model.transformedParameterizedPoints().first);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();
      Debug::dprint("Solved.");

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      energy_u = 0.5*(u*Au);
      auto rep = repulsionEnergy(position);
      Debug::dprint("Partial energies:", energy_u, "and", rep);
      energy_u += rep;
      Debug::dprint("Got energy", energy_u);
      return energy_u;
    }
    
    auto energy() const
    {
      return energy_u;
    }

    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      typedef DerivativeVectorField_MongeGaugeSpecialization<DomainInfo,Model,Solver> VectorField;
      //~ typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      auto dif = derivative.evaluateGradient(lastPosition);
      Debug::dprint("Got membrane gradient.");
      Debug::dprintVector(dif, "Membrane gradient");
      const auto gradRep = repulsionGradient(lastPosition);
      Debug::dprint("Got repulsion gradient.");
      Debug::dprintVector(gradRep, "Repulsion gradient");
      dif += gradRep;
      Debug::dprint("Evaluated gradient.");

      return dif;
    }

    void saveMembrane(const std::string& fileName, const int nRefinement = 3)
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), nRefinement);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }
    
    auto& solver() const
    {
      return *pMembraneSolver;
    }
    
    const auto& getTransformation() const
    {
      return transformation;
    }


  protected:

    Dune::ParameterTree params;

    Surface        surface;
    Particle       particle;
    Transformation transformation;
    const double   repulsionConstant;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Position lastPosition;
    Vector u;
    double energy_u;

    const int nParticles;
    
    
    
    bool isFeasible(const Position& pos) const
    {
      /*// Config.
      const auto& rawAtoms  = FCHo2ParticleRaw::rawFullPositions;
      const int nParticles  = pos.size();
      const int nAtoms      = rawAtoms.size();
      const double minDist  = 2*500./64;
      
      const auto&& dist = [](const auto& x, const auto& y)
      {
        auto z = x;
        z -= y;
        return z.two_norm();
      };

      // Get transformed particle points.
      std::vector<FCHo2ParticleRaw::ParticleCoordinates> transformedAtoms(pos.size());
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtoms[i].resize(nAtoms);
        for(int k = 0; k < nAtoms; ++k)
          transformedAtoms[i][k]  = transformation.apply(pos[i], rawAtoms[k]);
      }
     
      
      // Compare all particles with each other.
      // Punish it when two particles come too close to each other. 
      for(int i = 0; i < nParticles; ++i)
      {
        for(int j = 0; j < i; ++j)
        {
          // Compare distances of all particles.
          for(int k = 0; k < nAtoms; ++k)
            for(int l = 0; l < nAtoms; ++l)
              if(dist(transformedAtoms[i][k], transformedAtoms[j][l]) < minDist)
                return false;
        }
      }*/
      
      return true;
    }
    
    // Repulsion energy at a given position.
    // The parameter only restricts evaluation to a single particle (if only >=0);
    // this is mostly used for the lazy finite difference implementation.
    // This implementation makes only sense of the Transformation is an orthogonal
    // transformation and if no rotation around the x1 or x2 axis is present.
    double repulsionEnergy(const Model::Position& position, int only = -1) const
    {
      // Transform quadrature points. Works essentially like transforming atoms.
      const auto&& transformedQuadPoints = transformQuadPoints(position);
      
      // Evaluate interaction integrals.
      double res = 0;
      for(int i = 0; i < position.size(); ++i)
      {
        for(int j = 0; j < i; ++j)
        {
          // Skip iteration if parameter only is set and only \notin {i,j}.
          if(only >= 0 && i != only && j != only)
            continue;
            
          // Evaluate smoothed integral.
          res += interactionIntegral(transformedQuadPoints[i], transformedQuadPoints[j]);
        }
      }
      return res*repulsionConstant;
    }
    
    double interactionIntegral(const QuadPoints& X, const QuadPoints& Y) const
    {
      // int f(|X-Y|) * g(X_0) * g(Y_0)
      // f(r) = 1/(1+r^2)
      
      // Define distance function.
      auto length0 = length;
      const auto&& distance2 = [&length0](const auto& x, const auto& y)
      {
        auto z = x;
        z -= y;

        // Apply periodicity.
        for(int i = 0; i < 2; ++i)
          z[i] = (fmod(z[i]+length,2*length)-length) + (z[i] < -length)*(2*length);
        
        return z.two_norm2();
      };
      
      // Evaluate actual integral.
      double res = 0;
      for(int i = 0; i < quadWeights.size(); ++i)
      {
        //~ std::cerr << "\r" << i << "/" << quadWeights.size();
        for(int j = 0; j < quadWeights.size(); ++j)
          res += quadWeights[i] * quadWeights[j] / (100 + distance2(X[i],Y[j]));
      }
      //~ std::cerr << std::endl;
      
      return res;
    }
    
    Model::Position repulsionGradient(const Model::Position& position) const
    {
      // Lazy finite difference implementation.
      auto gradient = position; gradient *= 0;

      const double t = 0.5e-4;
      for(int i = 0; i < position.size(); ++i)
      {
        for(int j = 0; j < position[i].size(); ++j)
        {
          auto pp = position;
          pp[i][j] += t;
          
          auto pm = position;
          pm[i][j] -= t;
          
          gradient[i][j] = (repulsionEnergy(pp,i) - repulsionEnergy(pm,i))/(2*t);
        }
      }

      return gradient;
    }
    



    double repulsionEnergyClassic(const Model::Position& position) const
    {
      double energy = 0;
      
      const FCHo2ParticleRaw::ParticleCoordinates& atoms = FCHo2ParticleRaw::rawMinimalPositions;
      const int nAtoms = atoms.size();
      const auto&& transformedAtoms = transformAtoms(position, atoms);
      
      for(int i = 0; i < nParticles; ++i)
      {
        for(int j = 0; j < i; ++j)
        {
          for(int k = 0; k < nAtoms; ++k)
          {
            for(int l = 0; l < nAtoms; ++l)
            {
              auto z = transformedAtoms[i][k];
              z -= transformedAtoms[j][l];
              energy += 1./(fMakeRepulsionFinite+z.two_norm2());
            }
          }
        }
      }
      
      return energy * repulsionConstant;
    }
    
    Model::Position repulsionGradientClassic(const Model::Position& position) const
    {
      auto gradient = position;
      gradient *= 0;
      
      /// DEBUG: Difference quotient solution.
      const double t = 0.5e-4;
      for(int i = 0; i < gradient.size(); ++i)
      {
        for(int j = 0; j < gradient[i].size(); ++j)
        {
          auto pp = position;
          pp[i][j] += t;
          
          auto pm = position;
          pm[i][j] -= t;
          
          //~ Debug::dprintVector(position, "position");
          //~ Debug::dprintVector(pp, "pp");
          //~ Debug::dprintVector(pm, "pm");
          //~ Debug::dprint("energies", repulsionEnergy(pp), "and", repulsionEnergy(pm), "for ids", i, j, "yielding", (repulsionEnergy(pp)-repulsionEnergy(pm))/(2*t) );
          gradient[i][j] = (repulsionEnergy(pp)-repulsionEnergy(pm))/(2*t);
        }
      }
      ///
      
      /*const FCHo2ParticleRaw::ParticleCoordinates& atoms = FCHo2ParticleRaw::rawMinimalPositions;
      const int nAtoms = atoms.size();
      const auto&& transformedAtoms = transformAtoms(position, atoms);
      const auto&& transformedAtomDerivatives = transformAtomDerivatives(position, atoms);

      assert(position.size() > 0);
      const int nParticleDOFs = position[0].size();
      
      for(int i = 0; i < nParticles; ++i)
      {
        for(int j = 0; j < nParticles; ++j)
        {
          if(i == j)
            continue;

          for(int k = 0; k < nAtoms; ++k)
          {
            for(int l = 0; l < nAtoms; ++l)
            {
              auto z = transformedAtoms[i][k];
              z -= transformedAtoms[j][l];
              const auto z_norm2 = z.two_norm2();
              
              for(int d = 0; d < nParticleDOFs; ++ d)
                gradient[i][d] += -2*(z*transformedAtomDerivatives[i][k][d])/(z_norm2*z_norm2);
            }
          }
        }
      }*/
      //~ gradient *= repulsionConstant;

      return gradient;
    }



    std::vector<FCHo2ParticleRaw::ParticleCoordinates> transformAtoms(const Model::Position& position, const FCHo2ParticleRaw::ParticleCoordinates& atoms) const
    {
      const int nAtoms = atoms.size();
      std::vector<FCHo2ParticleRaw::ParticleCoordinates> transformedAtoms(nParticles);
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtoms[i].resize(nAtoms);
        for(int k = 0; k < nAtoms; ++k)
        {
          transformedAtoms[i][k] = transformation.apply(position[i], atoms[k]);
          for(int l = 0; l < 2; ++l)
            transformedAtoms[i][k][l] = (fmod(transformedAtoms[i][k][l]+length,2*length)-length)
                                          + (transformedAtoms[i][k][l] < -length)*(2*length);
            
        }
      }
      return transformedAtoms;
    }
    
    std::vector<std::vector<std::vector<typename FCHo2ParticleRaw::ParticleCoordinates::value_type>>> transformAtomDerivatives(const Model::Position& position, const FCHo2ParticleRaw::ParticleCoordinates& atoms) const
    {      
      typedef typename FCHo2ParticleRaw::ParticleCoordinates::value_type Point;
      const int nAtoms = atoms.size();
      assert(position.size() > 0);
      const int nParticleDOFs = position[0].size();

      std::vector<std::vector<std::vector<Point>>> transformedAtomDerivatives(nParticles);
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtomDerivatives[i].resize(nAtoms);
        for(int j = 0; j < nAtoms; ++j)
        {
          transformedAtomDerivatives[i][j].resize(nParticleDOFs);
          for(int k = 0; k < nParticleDOFs; ++k)
          {
            transformedAtomDerivatives[i][j][k] = transformation.apply(position[i], atoms[j], {0,k});
          }
        }
      }
      
      return transformedAtomDerivatives;
    }
    
    
    void gridRefine(Grid& grid, const Model::Position& p) const
    {
      //~ DUNE_THROW(Dune::Exception, "Grid refinement might not work yet as intended because it does not lead to a conforming basis. This is maybe because either the ConformingBasis itself is erroneous or because the incorporation of point value constraints is not yet compatible with the constraints from the ConformingBasis. To-do: Check results for plausibility and fix errors if necessary.");
      Debug::dprint("Number of elements before marking:", grid.size(0));
      
      // Get constraint positions in dependence of p.
      // TODO: Make this less hard coded.
      const auto& residuals = FCHo2ParticleRaw::residualPositions;
      std::vector<Dune::FieldVector<double,2>> atoms;
      for(int i = 0; i < p.size(); ++i)
      {
        for(const auto& residual : residuals)
        {
          const auto&& q = transformation.apply(p[i], residual);
          atoms.push_back({
            (fmod(q[0]+length,2*length)-length) + (q[0] < -length)*(2*length),
            (fmod(q[1]+length,2*length)-length) + (q[1] < -length)*(2*length)
          });
        }
      }
      
      // Mark relevant grid elements until threshold is reached.
      int markCounter = 0;
      //~ while(markCounter < nMaxRefinementMarks)
      while( grid.size(0) < nMaxGridSize)
      {
        // Choose candidates.
        for(const auto& e : elements(grid.leafGridView()))
        {
          // Mark element if it is close enough to one of the points.
          for(const auto& x : atoms)
          {
            auto z = e.geometry().center();
            z -= x;
            // Apply periodicity.
            for(int i = 0; i < 2; ++i)
              z[i] = (fmod(z[i]+length,2*length)-length) + (z[i] < -length)*(2*length);

            if(z.two_norm() < refinementDistance)
            {
              ++markCounter;
              grid.mark(1,e);
            }
          }
        }
      
        // Apply marking.
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
        
        // For simplicity (with respect to periodic boundary conditions):
        // Also make sure that all boundary elements are uniformly refined.
        BoundaryPatch<typename std::decay<decltype(grid.leafGridView())>::type> boundaryPatch(grid.leafGridView(), true);
        
        int maxLevel = 0;
        for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
          maxLevel = std::max(it->inside().level(), maxLevel);
        
        bool fUniform = false;
        while(fUniform == false)
        {
          fUniform = true;
          for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
          {
            const auto& e = it->inside();
            if(e.level() < maxLevel)
            {
              fUniform = false;
              ++markCounter;
              grid.mark(1,e);
            }
          }
      
          // Apply marking.
          if(!fUniform)
          {
            grid.preAdapt();
            grid.adapt();
            grid.postAdapt();
          }
        }
        
        // Ensure that refinement is not too steep.
        markCounter += gridClose(grid);
        
        Debug::dprint("Intermediate grid size:", grid.size(0));
      }

      Debug::dprint("Number of elements after marking:", grid.size(0));
    }
    
    int gridClose(Grid& grid) const
    {
      // Mark all elements that have more than two elements on one of their edges.
      const int conformityLevel = 1;
      int markCounter = 0;
      bool fClosable = true;

      while(fClosable)
      {
        fClosable = false;
        const auto gridView = grid.leafGridView();
        // Iterate over all elements...
        for( const auto& e : elements(gridView) )
        {
          // Refine if too many finer elements are around.
          const auto iitEnd = gridView.iend(e);
          size_t finerCounter = 0;
          for( auto iit = gridView.ibegin(e); iit != iitEnd; iit++ )
          {
            // Skip if there is no neighbor on edge.
            if( !iit->neighbor() )
              continue;

            // Increase counter if level of neighbor is finter.
            if( iit->outside().level() > e.level() )
              finerCounter++;
          }
          
          if( finerCounter > 2 )
          {
            ++markCounter;
            fClosable = true;
            grid.mark(1, e);
            continue;
          }
          
          Dune::BitSetVector<1> checked( grid.size(0), false );
          fClosable |= gridCheckNeighbors( grid, e, e, conformityLevel, checked );
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
      }
      
      return markCounter;
    }
    
    template<class Element>
    bool gridCheckNeighbors(Grid& grid, const Element& e, const Element& origin, const size_t depth, Dune::BitSetVector<1>& checked) const
    {
      // Skip if element has been checked already; else check it.
      const auto gridView = grid.leafGridView();
      const auto elementIndex = gridView.indexSet().index(e);
      if( checked[elementIndex].any() )
        return false;
      checked[elementIndex] = true;

      // Iterate over all neighbors of the current element e.
      const auto&& g    = e.geometry();
      const auto iitEnd = gridView.iend(e);
      for( auto iit = gridView.ibegin(e); iit != iitEnd; iit++ )
      {
        // Skip if there is no neighbor on edge.
        if( !iit->neighbor() )
          continue;

        // Mark origin if current element is on a level that is too high.
        if( iit->outside().level() > origin.level()+1 )
        {
          grid.mark(1,origin);
          return true;
        }
        
        // Else: Check the current element's neighbors, if we did not yet reach depth=0.
        if( depth > 0 )
          if( gridCheckNeighbors( grid, iit->outside(), origin, depth-1, checked ) )
            return true;
      }
      
      return false;
    }
    
    
    /// Quadrature logic.    
    QuadPoints quadPoints;
    std::vector<double> quadWeights;
    
    void initQuadrature()
    {
      // Get reference quadrature rules in each direction.
      // (Could by the way also use the DUNE functionalities for that but right now I do not remember up to which order ise possible to use wihtout crashing it. Rule as generated by R's gauss.quad.)
      // R -> X=gauss.quad(n); for(x in X$nodes) cat(sprintf("%.15f, ",x)); cat('\n')
      // R -> X=gauss.quad(n); for(x in X$weights) cat(sprintf("%.15f, ",x)); cat('\n')
      std::vector<double> pointsX  = {-0.998866404420071, -0.994031969432090, -0.985354084048006, -0.972864385106692, -0.956610955242807, -0.936656618944878, -0.913078556655792, -0.885967979523613, -0.855429769429946, -0.821582070859336, -0.784555832900399, -0.744494302226068, -0.701552468706822, -0.655896465685439, -0.607702927184950, -0.557158304514650, -0.504458144907464, -0.449806334974038, -0.393414311897565, -0.335500245419437, -0.276288193779532, -0.216007236876042, -0.154890589998146, -0.093174701560086, -0.031098338327189, 0.031098338327189, 0.093174701560086, 0.154890589998146, 0.216007236876042, 0.276288193779532, 0.335500245419437, 0.393414311897565, 0.449806334974039, 0.504458144907464, 0.557158304514650, 0.607702927184950, 0.655896465685439, 0.701552468706822, 0.744494302226069, 0.784555832900399, 0.821582070859336, 0.855429769429946, 0.885967979523613, 0.913078556655792, 0.936656618944878, 0.956610955242808, 0.972864385106692, 0.985354084048006, 0.994031969432090, 0.998866404420071, };
      std::vector<double> weightsX = {0.002908622553155, 0.006759799195745, 0.010590548383651, 0.014380822761486, 0.018115560713489, 0.021780243170125, 0.025360673570012, 0.028842993580535, 0.032213728223577, 0.035459835615146, 0.038568756612588, 0.041528463090148, 0.044327504338803, 0.046955051303948, 0.049400938449467, 0.051655703069581, 0.053710621888996, 0.055557744806212, 0.057189925647729, 0.058600849813222, 0.059785058704265, 0.060737970841771, 0.061455899590317, 0.061936067420683, 0.062176616655347, 0.062176616655347, 0.061936067420683, 0.061455899590316, 0.060737970841770, 0.059785058704265, 0.058600849813224, 0.057189925647729, 0.055557744806213, 0.053710621888996, 0.051655703069582, 0.049400938449467, 0.046955051303950, 0.044327504338803, 0.041528463090148, 0.038568756612587, 0.035459835615147, 0.032213728223578, 0.028842993580535, 0.025360673570011, 0.021780243170125, 0.018115560713490, 0.014380822761486, 0.010590548383651, 0.006759799195746, 0.002908622553155, };
      std::vector<double> pointsY  = {-0.993128599185095, -0.963971927277914, -0.912234428251326, -0.839116971822219, -0.746331906460151, -0.636053680726515, -0.510867001950827, -0.373706088715420, -0.227785851141645, -0.076526521133498, 0.076526521133497, 0.227785851141645, 0.373706088715420, 0.510867001950827, 0.636053680726515, 0.746331906460151, 0.839116971822219, 0.912234428251326, 0.963971927277914, 0.993128599185095, };
      std::vector<double> weightsY = {0.017614007139152, 0.040601429800387, 0.062672048334109, 0.083276741576705, 0.101930119817241, 0.118194531961518, 0.131688638449177, 0.142096109318382, 0.149172986472604, 0.152753387130726, 0.152753387130726, 0.149172986472604, 0.142096109318382, 0.131688638449177, 0.118194531961518, 0.101930119817241, 0.083276741576705, 0.062672048334109, 0.040601429800387, 0.017614007139152, };
      const double lengthX = 250;
      const double lengthY = 100;
      
      
      // g0(r) = (r^2/eps^2-1)^2/eps^2*c if r^2 <= eps^2 else 0
      const auto g = [](const QuadPoint& x)
      {
        const double eps2 = std::pow(30,2);
        const FCHo2ParticleRaw::ParticleCoordinates& points = FCHo2ParticleRaw::rawMinimalPositions;
        double res = 0;
        for(const auto& point : points)
        {
          double r2 = (x[0] - point[0])*(x[0] - point[0]) + (x[1] - point[1])*(x[1] - point[1]);
          if(r2 < eps2)
            res += (r2/eps2-1)*(r2/eps2-1);
        }
        return res * (3./M_PI/eps2);
      };
      
      // Join them to one rule and apply an appropriate rescaling.
      // Update the weights furthermore by the smoothed dirac functions g
      quadPoints.resize(0);
      quadPoints.reserve(pointsX.size()*pointsY.size());
      quadWeights.resize(0);
      quadWeights.reserve(pointsX.size()*pointsY.size());
      for(int j = 0; j < pointsY.size(); ++j)
      {
        for(int i = 0; i < pointsX.size(); ++i)
        {
          quadPoints.push_back({pointsX[i]*lengthX/2., pointsY[j]*lengthY/2.});
          quadWeights.push_back(weightsX[i]*lengthX/2.*weightsY[j]*lengthY/2.*g(quadPoints.back()));
        }
      }
    }
    
    std::vector<QuadPoints> transformQuadPoints(const Model::Position& p) const
    {
      std::vector<QuadPoints> transformedPoints(p.size());
      for(int i = 0; i < p.size(); ++i)
      {
        transformedPoints[i].resize(quadPoints.size());
        for(int j = 0; j < quadPoints.size(); ++j)
        {
          const auto&& z = transformation.apply(p[i], {quadPoints[j][0], quadPoints[j][1], 0.});
          transformedPoints[i][j] = {z[0],z[1]};
        }
      }
      
      return transformedPoints;
    }
};






/*
class FCHo2PeriodicProblemTmp
{
  public:

    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    typedef PlanarRectangularPeriodicSurface<> Surface;
    typedef FCHo2Particle<> Particle;
    typedef SpatialTransformation<> Transformation;
    typedef ParameterizedMembraneParticleProblem<Surface,Particle,Transformation> Model;
    typedef typename Model::Position Position;

    typedef typename Dune::UGGrid<2> Grid;
    typedef typename Grid::LeafGridView GridView;
    typedef HermiteKNodalBasis<GridView,2> NonconformingBasis;
    typedef ConformingKBasis<NonconformingBasis> Basis;

    typedef typename Model::ConstraintFunctions ConstraintFunctions;
    typedef typename Model::LevelSetFunctions LevelSetFunctions;
    typedef DomainInformation<Basis,LevelSetFunctions> DomainInfo;
    typedef ConstraintFunction<ConstraintFunctions,DomainInfo> Constraints;
    typedef MongeGaugeIntegrand<Constraints> Integrand;
    typedef MembraneAssembler<Integrand,DomainInfo> Assembler;
    typedef MembraneSolver<Model,Basis> Solver;
    
    static constexpr double length    = 200;
    
    const bool fRefineGrid = true;
    //~ const int nMaxRefinementMarks = 1e3;
    const int nMaxGridSize = 30000;
    const double refinementDistance = 10;
    const bool fMakeRepulsionFinite = true;

    FCHo2PeriodicProblemTmp(const int nParticles = 2, const double repulsionConstant = 0.)
      : u(0), surface(length), particle(), transformation(), nParticles(nParticles), repulsionConstant(repulsionConstant)
    {
      //~ assert(nParticles <= 6);
      
      particle.makePeriodic(surface);
      transformation.makePeriodic(surface);
    }

    auto initialPosition(const int id = 0) const
    {
      Position x_;
      
      const auto DEG = M_PI/180.;;
      
      switch(id)
      {
        case 0:
          x_  = {
            {   0,    0, 0, 0, 0,  0 },
            {   0,  100, 0, 0, 0,  0 }
          };
          break;
          
        default:
          DUNE_THROW(Dune::Exception, "Initial position not supported.");
          break;
        
      }
      
      Position x(std::min(nParticles,(int)x_.size()));
      for(int i = 0; i < std::min(nParticles,(int)x_.size()); ++i)
        x[i]  = x_[i];
      
      return x;
    }
    
    auto upperBound() const
    {
      auto up = initialPosition();
      
      for(int i = 0; i < up.size(); ++i)
      {
        up[i][0] = std::numeric_limits<double>::max();
        up[i][1] = std::numeric_limits<double>::max();
        up[i][2] = 0.5;
        
        up[i][3] = 10*M_PI/180;
        up[i][4] = 10*M_PI/180;
        up[i][5] = std::numeric_limits<double>::max();
      }

      Debug::dprintVector(up, "up");
      return up;
    }
    
    auto lowerBound() const
    {
      auto lo = upperBound();
      lo *= -1;
      Debug::dprintVector(lo, "lo");
      return lo;
    }
    
    double particleRadius(const int axis) const
    {
      // Inspired by the dimensions of the FCHo2 particle.
      if(axis == 2)
        return std::max(85, 8);
        
      if(axis == 1)
        return 85;
        
      return 8; // axis == 0
    }
    

    auto solve(const double h, const Model::Position& position)
    {
      lastPosition = position;
      
      if(!isFeasible(position))
      {
        energy_u = std::numeric_limits<double>::infinity();
        return energy_u;
      }
      
      // Delete grid (if there is one).
      pGrid.reset();

      // Reset quadrature caches.
      LocalParameterizationQuadratureCache<double>::clear();
      CurveQuadratureCache<double,2>::clear();

      // Full model.
      pModel = std::make_shared<Model>(surface, particle, transformation);
      pModel->setNumberOfParticles(position.size());
      pModel->setPosition(position);
      pModel->setPeriodicity(true);
      const auto& model = *pModel;

      // Set up grid.
      pGrid = MembraneGrid::template makeGrid(model, h);
      if(fRefineGrid)
        gridRefine(*pGrid, position);

      // Set up basis.
      pNonconformingBasis = std::make_shared<NonconformingBasis>(pGrid->leafGridView());
      pBasis = std::make_shared<Basis>(*pNonconformingBasis);
      ConstraintsMachine::updateConstraints(*pBasis, surface, model.transformedParameterizedPoints().first);
      const auto& basis = *pBasis;

      // Collect geometric information.
      levelSetFunctions = model.transformedLevelSets();
      pDomainInfo = std::make_shared<DomainInfo>(basis, levelSetFunctions, 1);
      const auto& domainInfo = *pDomainInfo;
      Debug::dprint("DomainInfo constructed.");

      // Set integrand.
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Assemble system.
      Assembler assembler(integrand, domainInfo);
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      Debug::dprint("Assembled.");

      // Solve system.
      pMembraneSolver = std::make_shared<Solver>(assembler.systemMatrix(), assembler.rightHandSide(), model, basis, constraintPoints);
      u = pMembraneSolver->solve();

      // Compute energy.
      Vector Au(u.size());
      assembler.volumeMatrix().mv(u,Au);
      energy_u = 0.5*(u*Au);
      auto rep = repulsionEnergy(position);
      Debug::dprint("Partial energies:", energy_u, "and", rep);
      energy_u += rep;
      Debug::dprint("Got energy", energy_u);
      return energy_u;
    }
    
    auto energy() const
    {
      return energy_u;
    }

    auto gradient() const
    {
      // Preparations.
      const auto& model = *pModel;
      const auto&& constraintFunctions = model.transformedConstraintFunctions();
      const auto& domainInfo = *pDomainInfo;
      Constraints f(constraintFunctions, domainInfo);
      Integrand integrand(1,0,f);

      // Compute derivative.
      typedef DerivativeVectorField_MongeGaugeSpecialization<DomainInfo,Model,Solver> VectorField;
      //~ typedef DerivativeVectorField<DomainInfo,Model,Solver> VectorField;
      VectorField V(domainInfo, model, *pMembraneSolver);

      typedef ParticleDerivative<Integrand,VectorField,DomainInfo,Vector> Derivative;
      Derivative derivative(integrand, V, domainInfo, u);

      Debug::dprint("Evaluating gradient.");
      auto dif = derivative.evaluateGradient(lastPosition);
      dif += repulsionGradient(lastPosition);
      Debug::dprint("Evaluated gradient.");

      return dif;
    }

    void saveMembrane(const std::string& fileName, const int nRefinement = 3)
    {
      // Visualize.
      const auto& model = *pModel;
      const auto& domainInfo = *pDomainInfo;
            auto& basis = *pBasis;

      typedef Dune::CutOffBasis<Basis,typename Model::Function,DomainInfo> COBasis;
      COBasis coBasis(basis, domainInfo);
      const auto&& levelSetFunctions = model.transformedLevelSets();
      for(const auto& levelSetFunction : levelSetFunctions)
        coBasis.addInterface(levelSetFunction);

      typedef ParametricPerturbedSurface<Surface,COBasis,Vector> PerturbedSurface;
      PerturbedSurface perturbedSurface(surface, coBasis, u);
      typedef Dune::VTKFunction<typename COBasis::GridView> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<PerturbedSurface>(perturbedSurface);

      typedef VTKBasisGridFunction<COBasis,Vector> VTKGridFunction;
      std::shared_ptr<VTKGridFunction> vtkGridFunction = std::make_shared<VTKGridFunction>(coBasis, u, "elevation");

      Dune::SubsamplingVTKWriter<typename COBasis::GridView> writer(basis.getGridView(), nRefinement);
      writer.addVertexData(vtkFunction);
      writer.addVertexData(vtkGridFunction);

      writer.write(fileName, Dune::VTK::OutputType::base64);
      Debug::dprint("Wrote surface.");
    }

    const auto& membrane() const
    {
      return u;
    }

    const auto& basis() const
    {
      return *pBasis;
    }
    
    auto& solver() const
    {
      return *pMembraneSolver;
    }
    
    const auto& getTransformation() const
    {
      return transformation;
    }


  protected:

    Dune::ParameterTree params;

    Surface        surface;
    Particle       particle;
    Transformation transformation;
    const double   repulsionConstant;

    LevelSetFunctions levelSetFunctions;

    std::shared_ptr<Grid>               pGrid;
    std::shared_ptr<NonconformingBasis> pNonconformingBasis;
    std::shared_ptr<Basis>              pBasis;
    std::shared_ptr<DomainInfo>         pDomainInfo;
    std::shared_ptr<Model>              pModel;
    std::shared_ptr<Solver>             pMembraneSolver;

    Position lastPosition;
    Vector u;
    double energy_u;

    const int nParticles;
    
    
    
    bool isFeasible(const Position& pos) const
    {
      /* // Config.
      const auto& rawAtoms  = FCHo2ParticleRaw::rawFullPositions;
      const int nParticles  = pos.size();
      const int nAtoms      = rawAtoms.size();
      const double minDist  = 2*500./64;
      
      const auto&& dist = [](const auto& x, const auto& y)
      {
        auto z = x;
        z -= y;
        return z.two_norm();
      };

      // Get transformed particle points.
      std::vector<FCHo2ParticleRaw::ParticleCoordinates> transformedAtoms(pos.size());
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtoms[i].resize(nAtoms);
        for(int k = 0; k < nAtoms; ++k)
          transformedAtoms[i][k]  = transformation.apply(pos[i], rawAtoms[k]);
      }
     
      
      // Compare all particles with each other.
      // Punish it when two particles come too close to each other. 
      for(int i = 0; i < nParticles; ++i)
      {
        for(int j = 0; j < i; ++j)
        {
          // Compare distances of all particles.
          for(int k = 0; k < nAtoms; ++k)
            for(int l = 0; l < nAtoms; ++l)
              if(dist(transformedAtoms[i][k], transformedAtoms[j][l]) < minDist)
                return false;
        }
      }/
      
      return true;
    }

    double repulsionEnergy(const Model::Position& position) const
    {
      double energy = 0;
      
      const FCHo2ParticleRaw::ParticleCoordinates& atoms = FCHo2ParticleRaw::rawMinimalPositions;
      const int nAtoms = atoms.size();
      const auto&& transformedAtoms = transformAtoms(position, atoms);
      
      for(int i = 0; i < nParticles; ++i)
      {
        for(int j = 0; j < i; ++j)
        {
          for(int k = 0; k < nAtoms; ++k)
          {
            for(int l = 0; l < nAtoms; ++l)
            {
              auto z = transformedAtoms[i][k];
              z -= transformedAtoms[j][l];
              energy += 1./(fMakeRepulsionFinite+z.two_norm2());
            }
          }
        }
      }
      
      return energy * repulsionConstant;
    }
    
    Model::Position repulsionGradient(const Model::Position& position) const
    {
      auto gradient = position;
      gradient *= 0;
      
      /// DEBUG: Difference quotient solution.
      const double t = 0.5e-4;
      for(int i = 0; i < gradient.size(); ++i)
      {
        for(int j = 0; j < gradient[i].size(); ++j)
        {
          auto pp = position;
          pp[i][j] += t;
          
          auto pm = position;
          pm[i][j] -= t;
          
          //~ Debug::dprintVector(position, "position");
          //~ Debug::dprintVector(pp, "pp");
          //~ Debug::dprintVector(pm, "pm");
          //~ Debug::dprint("energies", repulsionEnergy(pp), "and", repulsionEnergy(pm), "for ids", i, j, "yielding", (repulsionEnergy(pp)-repulsionEnergy(pm))/(2*t) );
          gradient[i][j] = (repulsionEnergy(pp)-repulsionEnergy(pm))/(2*t);
        }
      }
      ///
      
      /*const FCHo2ParticleRaw::ParticleCoordinates& atoms = FCHo2ParticleRaw::rawMinimalPositions;
      const int nAtoms = atoms.size();
      const auto&& transformedAtoms = transformAtoms(position, atoms);
      const auto&& transformedAtomDerivatives = transformAtomDerivatives(position, atoms);

      assert(position.size() > 0);
      const int nParticleDOFs = position[0].size();
      
      for(int i = 0; i < nParticles; ++i)
      {
        for(int j = 0; j < nParticles; ++j)
        {
          if(i == j)
            continue;

          for(int k = 0; k < nAtoms; ++k)
          {
            for(int l = 0; l < nAtoms; ++l)
            {
              auto z = transformedAtoms[i][k];
              z -= transformedAtoms[j][l];
              const auto z_norm2 = z.two_norm2();
              
              for(int d = 0; d < nParticleDOFs; ++ d)
                gradient[i][d] += -2*(z*transformedAtomDerivatives[i][k][d])/(z_norm2*z_norm2);
            }
          }
        }
      }/
      
      gradient *= repulsionConstant;
      return gradient;
    }
    
    std::vector<FCHo2ParticleRaw::ParticleCoordinates> transformAtoms(const Model::Position& position, const FCHo2ParticleRaw::ParticleCoordinates& atoms) const
    {
      const int nAtoms = atoms.size();
      std::vector<FCHo2ParticleRaw::ParticleCoordinates> transformedAtoms(nParticles);
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtoms[i].resize(nAtoms);
        for(int k = 0; k < nAtoms; ++k)
        {
          transformedAtoms[i][k] = transformation.apply(position[i], atoms[k]);
          for(int l = 0; l < 2; ++l)
            transformedAtoms[i][k][l] = (fmod(transformedAtoms[i][k][l]+length,2*length)-length)
                                          + (transformedAtoms[i][k][l] < -length)*(2*length);
            
        }
      }
      return transformedAtoms;
    }
    
    std::vector<std::vector<std::vector<typename FCHo2ParticleRaw::ParticleCoordinates::value_type>>> transformAtomDerivatives(const Model::Position& position, const FCHo2ParticleRaw::ParticleCoordinates& atoms) const
    {      
      typedef typename FCHo2ParticleRaw::ParticleCoordinates::value_type Point;
      const int nAtoms = atoms.size();
      assert(position.size() > 0);
      const int nParticleDOFs = position[0].size();

      std::vector<std::vector<std::vector<Point>>> transformedAtomDerivatives(nParticles);
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtomDerivatives[i].resize(nAtoms);
        for(int j = 0; j < nAtoms; ++j)
        {
          transformedAtomDerivatives[i][j].resize(nParticleDOFs);
          for(int k = 0; k < nParticleDOFs; ++k)
          {
            transformedAtomDerivatives[i][j][k] = transformation.apply(position[i], atoms[j], {0,k});
          }
        }
      }
      
      return transformedAtomDerivatives;
    }
    
    
    void gridRefine(Grid& grid, const Model::Position& p) const
    {
      //~ DUNE_THROW(Dune::Exception, "Grid refinement might not work yet as intended because it does not lead to a conforming basis. This is maybe because either the ConformingBasis itself is erroneous or because the incorporation of point value constraints is not yet compatible with the constraints from the ConformingBasis. To-do: Check results for plausibility and fix errors if necessary.");
      Debug::dprint("Number of elements before marking:", grid.size(0));
      
      // Get constraint positions in dependence of p.
      // TODO: Make this less hard coded.
      const auto& residuals = FCHo2ParticleRaw::residualPositions;
      std::vector<Dune::FieldVector<double,2>> atoms;
      for(int i = 0; i < p.size(); ++i)
      {
        for(const auto& residual : residuals)
        {
          const auto&& q = transformation.apply(p[i], residual);
          atoms.push_back({
            (fmod(q[0]+length,2*length)-length) + (q[0] < -length)*(2*length),
            (fmod(q[1]+length,2*length)-length) + (q[1] < -length)*(2*length)
          });
        }
      }
      
      // Mark relevant grid elements until threshold is reached.
      int markCounter = 0;
      //~ while(markCounter < nMaxRefinementMarks)
      while( grid.size(0) < nMaxGridSize)
      {
        // Choose candidates.
        for(const auto& e : elements(grid.leafGridView()))
        {
          // Mark element if it is close enough to one of the points.
          for(const auto& x : atoms)
          {
            auto z = e.geometry().center();
            z -= x;
            if(z.two_norm() < refinementDistance)
            {
              ++markCounter;
              grid.mark(1,e);
            }
          }
        }
      
        // Apply marking.
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
        
        // For simplicity (with respect to periodic boundary conditions):
        // Also make sure that all boundary elements are uniformly refined.
        BoundaryPatch<typename std::decay<decltype(grid.leafGridView())>::type> boundaryPatch(grid.leafGridView(), true);
        
        int maxLevel = 0;
        for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
          maxLevel = std::max(it->inside().level(), maxLevel);
        
        bool fUniform = false;
        while(fUniform == false)
        {
          fUniform = true;
          for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
          {
            const auto& e = it->inside();
            if(e.level() < maxLevel)
            {
              fUniform = false;
              ++markCounter;
              grid.mark(1,e);
            }
          }
      
          // Apply marking.
          if(!fUniform)
          {
            grid.preAdapt();
            grid.adapt();
            grid.postAdapt();
          }
        }
        
        // Ensure that refinement is not too steep.
        markCounter += gridClose(grid);
        
        Debug::dprint("Intermediate grid size:", grid.size(0));
      }

      Debug::dprint("Number of elements after marking:", grid.size(0));
    }
    
    int gridClose(Grid& grid) const
    {
      // Mark all elements that have more than two elements on one of their edges.
      const int conformityLevel = 1;
      int markCounter = 0;
      bool fClosable = true;

      while(fClosable)
      {
        fClosable = false;
        const auto gridView = grid.leafGridView();
        // Iterate over all elements...
        for( const auto& e : elements(gridView) )
        {
          // Refine if too many finer elements are around.
          const auto iitEnd = gridView.iend(e);
          size_t finerCounter = 0;
          for( auto iit = gridView.ibegin(e); iit != iitEnd; iit++ )
          {
            // Skip if there is no neighbor on edge.
            if( !iit->neighbor() )
              continue;

            // Increase counter if level of neighbor is finter.
            if( iit->outside().level() > e.level() )
              finerCounter++;
          }
          
          if( finerCounter > 2 )
          {
            ++markCounter;
            fClosable = true;
            grid.mark(1, e);
            continue;
          }
          
          Dune::BitSetVector<1> checked( grid.size(0), false );
          fClosable |= gridCheckNeighbors( grid, e, e, conformityLevel, checked );
        }
        grid.preAdapt();
        grid.adapt();
        grid.postAdapt();
      }
      
      return markCounter;
    }
    
    template<class Element>
    bool gridCheckNeighbors(Grid& grid, const Element& e, const Element& origin, const size_t depth, Dune::BitSetVector<1>& checked) const
    {
      // Skip if element has been checked already; else check it.
      const auto gridView = grid.leafGridView();
      const auto elementIndex = gridView.indexSet().index(e);
      if( checked[elementIndex].any() )
        return false;
      checked[elementIndex] = true;

      // Iterate over all neighbors of the current element e.
      const auto&& g    = e.geometry();
      const auto iitEnd = gridView.iend(e);
      for( auto iit = gridView.ibegin(e); iit != iitEnd; iit++ )
      {
        // Skip if there is no neighbor on edge.
        if( !iit->neighbor() )
          continue;

        // Mark origin if current element is on a level that is too high.
        if( iit->outside().level() > origin.level()+1 )
        {
          grid.mark(1,origin);
          return true;
        }
        
        // Else: Check the current element's neighbors, if we did not yet reach depth=0.
        if( depth > 0 )
          if( gridCheckNeighbors( grid, iit->outside(), origin, depth-1, checked ) )
            return true;
      }
      
      return false;
    }
};

*/
#endif
