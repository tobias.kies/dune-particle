#ifndef DUNE_FUNCTIONS_HELPER_HH
#define DUNE_FUNCTIONS_HELPER_HH

#include <dune/particle/functions/funcpolz.hh>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

/**
    This class provides some standard level sets for particles.
    And also just some other convenient functions.
*/
class FunctionsHelper
{
    private:
        
    
        FunctionsHelper()
        {}


    public:

        template<class float_x, size_t dim> using DomainType = Dune::FieldVector<float_x,dim>;
        template<class float_x> using RangeType = Dune::FieldVector<float_x,1>;
        template<size_t dim> using OrderType = Dune::FieldVector<size_t,dim>;
        template<class float_x, size_t dim> using Polynomial = FuncPolZ<DomainType<float_x,dim>,RangeType<float_x>,OrderType<dim>>;
        template<class float_x, size_t dim> using PT = typename Polynomial<float_x,dim>::PolTuple;
        typedef double float_x;
        
        // The following is not very pretty and a mere result of tiredness.
        template<class float_x, size_t dim> using Function = Dune::VirtualMultiDifferentiableFunction<DomainType<float_x,dim>,RangeType<float_x>,OrderType<dim>>;
        
        //template<class float_x = double>
        static auto ellipse( const float_x a, const float_x b )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            return Polynomial<float_x,dim>( { 
                std::make_pair(         1, P({0,0}) ),
                std::make_pair( -1./(a*a), P({2,0}) ),
                std::make_pair( -1./(b*b), P({0,2}) ),
             } );
        }
        
        static auto almostSquare( float_x r )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            const size_t n = 10;
            const float_x rn = std::pow(r,n);
            
            return Polynomial<float_x,dim>( { 
                std::make_pair(      1, P({0,0}) ),
                std::make_pair( -1./rn, P({n,0}) ),
                std::make_pair( -1./rn, P({0,n}) ),
             } );
        }
        
        //template<class float_x = double>
        static auto ellipseInverse( const float_x a, const float_x b )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            return Polynomial<float_x,dim>( { 
                std::make_pair(        -1, P({0,0}) ),
                std::make_pair( +1./(a*a), P({2,0}) ),
                std::make_pair( +1./(b*b), P({0,2}) ),
             } );
        }
    
        //template<class float_x = double>
        static auto circle( const float_x r = 1 )
        {
            return ellipse( r, r );
        }
    
        //template<class float_x = double>
        static auto circleInverse( const float_x r = 1 )
        {
            return ellipseInverse( r, r );
        }
        
        //template<class float_x = double>
        static auto constant( const float_x c = 1 )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            return Polynomial<float_x,dim>( {std::make_pair(c, P({0,0}))} );
        }
        
        //template<class float_x = double>
        static auto linear( const float_x a, const float_x b, const float_x c )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            return Polynomial<float_x,dim>( { 
                std::make_pair( a, P({0,0}) ),
                std::make_pair( b, P({1,0}) ),
                std::make_pair( c, P({0,1}) ),
             } );
        }
        
        //template<class float_x = double>
        static auto quadratic( const float_x a, const float_x b, const float_x c, const float_x d, const float_x e, const float_x f )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            return Polynomial<float_x,dim>( { 
                std::make_pair( a, P({0,0}) ),
                std::make_pair( b, P({1,0}) ),
                std::make_pair( c, P({0,1}) ),
                std::make_pair( d, P({2,0}) ),
                std::make_pair( e, P({1,1}) ),
                std::make_pair( f, P({0,2}) ),
             } );
        }
        
        //template<class float_x = double>
        static auto peanut( const float_x r = 1 )
        {
            const size_t dim = 2;
            typedef PT<float_x,dim> P;

            return Polynomial<float_x,dim>( { 
                std::make_pair( + 1./20,        P({0,0}) ),
                std::make_pair( +19./(20*r*r),  P({2,0}) ),
                std::make_pair( - 1./(r*r*r*r), P({4,0}) ),
                std::make_pair( -19./(20*r*r),  P({0,2}) ),
                std::make_pair( - 1./(r*r*r*r), P({0,4}) ),
                std::make_pair( - 2./(r*r*r*r), P({2,2}) )
            } );
        }
        
        
        static auto octagon( const float_x r0 = 1 )
        {
            // Based on equation x² + y² = 4 - c * (x-y)^k * (x+y)^k * x^k * y^k
            // Which corresponds to
            //  4 - x^2 - y^2 - c*x^4 * y^12 + 4*c*x^6*y^10 - 6*c*x^8*y^8 + 4*c*x^10*y^6 - c*x^12 * y^4
            const float_x c = 0.05;
            const size_t k = 4;
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            const float_x r = r0/2, r2 = r*r, r16 = std::pow(r2,8), cr = c/r16;
            
            return Polynomial<float_x,dim>( {
                std::make_pair(     4, P({ 0, 0}) ),
                std::make_pair( -1/r2, P({ 2, 0}) ),
                std::make_pair( -1/r2, P({ 0, 2}) ),
                std::make_pair(   -cr, P({ 4,12}) ),
                std::make_pair(  4*cr, P({ 6,10}) ),
                std::make_pair( -6*cr, P({ 8, 8}) ),
                std::make_pair(  4*cr, P({10, 6}) ),
                std::make_pair(   -cr, P({12, 4}) )
            } );
        }
        
        
        static auto octallipse( const float_x r0 = 1 )
        {
            // Based on equation x² + y² = 4 - c * (x-y)^k * (x+y)^k * x^k * y^k
            // Which corresponds to
            //  4 - x^2 - y^2 - c*x^4 * y^12 + 4*c*x^6*y^10 - 6*c*x^8*y^8 + 4*c*x^10*y^6 - c*x^12 * y^4
            const float_x c = 0.2;
            const size_t k = 4;
            const size_t dim = 2;
            typedef PT<float_x,dim> P;
            
            const float_x r = r0/2, r2 = r*r, r16 = std::pow(r2,8), cr = c/r16;
            
            const float_x ex = 2, ex2 = ex*ex, ex4 = ex2*ex2, ex6 = ex4*ex2, ex8 = ex4*ex4, ex10 = ex4*ex6, ex12 = ex6*ex6;
            
            return Polynomial<float_x,dim>( {
                std::make_pair(     4     , P({ 0, 0}) ),
                std::make_pair( -1/r2     , P({ 2, 0}) ),
                std::make_pair( -1/r2* ex2, P({ 0, 2}) ),
                std::make_pair(   -cr*ex12, P({ 4,12}) ),
                std::make_pair(  4*cr*ex10, P({ 6,10}) ),
                std::make_pair( -6*cr* ex8, P({ 8, 8}) ),
                std::make_pair(  4*cr* ex6, P({10, 6}) ),
                std::make_pair(   -cr* ex4, P({12, 4}) )
            } );
        }
};
#endif
