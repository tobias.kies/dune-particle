#ifndef DUNE_MISC_HH
#define DUNE_MISC_HH

#include <dune/istl/matrixindexset.hh>

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>

class Misc
{
  public:
  
    template<class Matrix>
    static auto joinSparseMatrices(const Matrix& A, const Matrix& B)
    {      
      assert(A.N() == B.N() && A.M() == B.M());
      
      Dune::MatrixIndexSet indices(A.N(), A.M());
      indices.import(A);
      indices.import(B);
      
      Matrix C;
      indices.exportIdx(C);
      C *= 0;
      C += A;
      C += B;
      
      return C;
    }
  
    // I know -- this is not pretty.
    template<class Matrix>
    static auto joinSparseMatrices(const Matrix& A, const Matrix& B, const Matrix& C)
    {      
      assert(A.N() == B.N() && A.M() == B.M());
      assert(A.N() == C.N() && A.M() == C.M());
      
      Dune::MatrixIndexSet indices(A.N(), A.M());
      indices.import(A);
      indices.import(B);
      indices.import(C);
      
      Matrix D;
      indices.exportIdx(D);
      D *= 0;
      D += A;
      D += B;
      D += C;
      
      return D;
    }
    
    
    template<class BitVector>
    static auto joinDOFs(const BitVector& A, const BitVector& B)
    {
      assert(A.size() == B.size());

      BitVector C(A.size());
      for(int i = 0; i < A.size(); ++i)
        C[i]  = (A[i].any() || B[i].any());
        
      return C;
    }
    
    // Incorproate DOFs into system.
    // Also makes sure that there are no fully zero rows.
    template<class Matrix, class Vector, class Basis, class DOFS>
    static auto incorporateDOFs(const Matrix& A, Vector& b, const Basis& basis, const DOFS& dofs)
    {
      assert(A.N() == A.M());
      assert(A.N() == dofs.size());
      
      
      const auto&& isZeroRow = [](const auto& row)
      {
        for(auto it = row.begin(); it != row.end(); ++it)
          if(std::abs(*it) > std::numeric_limits<double>::epsilon() * 1024)
            return false;
        return true;
      };
      
      
      Dune::MatrixIndexSet indices(A.N(), A.M());
      indices.import(A);
      for(int i = 0; i < A.N(); ++i)
        if(isZeroRow(A[i]) || dofs[i].any())
          indices.add(i,i);
        
      Matrix B;
      indices.exportIdx(B);
      B += A;
      
      for(int i = 0; i < A.N(); ++i)
      {
        if(isZeroRow(A[i]))
          B[i][i] = 1;

        if(dofs[i].none())
          continue;
          
        //~ b[i]  = 0;
        
        for(auto it = B[i].begin(); it != B[i].end(); ++it)
        {
          *it = 0;
          //~ B[it.index()][i]  = 0;
        }
        B[i][i] = 1;
      }
      
      return B;
    }
    
    template<class Matrix, class Basis, class DOFS>
    static auto incorporateDOFs(const Matrix& A, const Basis& basis, const DOFS& dofs)
    {
      Dune::BlockVector<Dune::FieldVector<double,1>> b(A.N(),0);
      return incorporateDOFs(A,b,basis,dofs);
    }
    
    
    template<class Matrix>
    static auto regularizeDiagonal(const Matrix& A)
    {
      Dune::MatrixIndexSet indices(A.N(), A.M());
      indices.import(A);
      for(int i = 0; i < A.N(); ++i)
        indices.add(i,i);
      
      Matrix B;
      indices.exportIdx(B);
      B += A;
      
      for(int i = 0; i < std::min(A.N(), A.M()); ++i)
        if(B[i][i] == 0)
          B[i][i] = 1;

      return B;
    }
    
    
    /*
    template<class Basis, class Points>
    static auto mapPointsToDOFs(const Basis& basis, const Points& points)
    {
      std::map<int,int> map;
      const double threshold = std::numeric_limits<double>::epsilon() * 1024;
      
      const auto&& distance = [](const auto& a, const auto& b)
      {
        assert(a.size() == b.size());
        double c = 0;
        for(int i = 0; i < a.size(); ++i)
          c += (a[i]-b[i])*(a[i]-b[i]);
        return sqrt(c);
      };
      
      for(int i = 0; i < points.size(); ++i)
      {
        const auto& point = points[i];
        map[i]  = -1;
        
        // Iterate over all elements.
        for(const auto& e : elements(basis.getGridView()))
        {
          const auto& fe      = basis.getLocalFiniteElement(e);
          const auto& coeffs  = fe.localCoefficients();
          
          // Iterate over all coefficients.
          for(int j = 0; j < coeffs.size(); ++j)
          {
            const auto& key = coeffs.localKey(j);
            if(key.index() != 0 || key.codim() != Points::value_type::dimension)
              continue;
              
            // Get corner associated to this key:
            const auto& corner = e.geometry().corner(key.subEntity());
            
            if(distance(corner,point) < threshold)
            {
              map[i]  = basis.index(e,j);
              goto endOfPointsLoop;
            }
          }
        }
        
endOfPointsLoop:
        if( map[i] == -1 )
          DUNE_THROW(Dune::Exception, "Could not map point to dof.");
      }
      
      return map;
    }*/
    
    
    template<class Basis, class Vector>
    static void writeDiscreteFunction( const typename Basis::GridView& gridView, const Basis& basis, const Vector& u, const std::string& filename, size_t nSubsampling )
    {
      Dune::SubsamplingVTKWriter<typename Basis::GridView> writer( gridView, nSubsampling );
      typedef VTKBasisGridFunction<Basis, Vector> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<VTKFunction>( basis, u, "u(x,y)" );
      writer.addVertexData(vtkFunction);
      writer.write( filename, Dune::VTK::OutputType::base64 );
    }

    template<class Basis, class Vector>
    static auto writeDiscreteFunction( const Basis& basis, const Vector& u, const std::string& filename, size_t nSubsampling )
    {
      return writeDiscreteFunction( basis.getGridView(), basis, u, filename, nSubsampling );
    }

    template<class GridFunction>
    static auto writeDiscreteFunction( const GridFunction& uh, const std::string& filename, size_t nSubsampling )
    {
      const auto& vector      = uh.coefficientVector();
      const auto& basis       = uh.basis();
      const auto& gridView    = basis.getGridView();

      return writeDiscreteFunction( gridView, basis, vector, filename, nSubsampling );
    }
};
#endif
