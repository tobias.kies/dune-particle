#ifndef DUNE_BISECTION_HH
#define DUNE_BISECTION_HH

class Bisection
{
    public:
    
        template<class Function, class Point, class float_x = double>
        static auto bisectionMethod( const Function& f, const Point& a0, const Point& b0, const float_x tol = std::numeric_limits<float_x>::epsilon()*128, const size_t maxiter = 100 )
        {
            auto a          = a0;
            auto b          = b0;
            const auto&& ba = b-a;
            auto d          = ba.two_norm();
            float_x twok    = 0.5;
            const auto&& fa = f(a);
            const auto&& fb = f(b);
            const auto eps  = std::numeric_limits<typename std::decay<decltype(fa)>::type>::epsilon() * 32;
            
            if( std::abs(fa) < eps )
                return a;
                
            if( std::abs(fb) < eps )
                return b;
            
            if( fa*fb > 0 )
                DUNE_THROW( Dune::Exception, "Invalid starting points in bisectionMethod()." );
            
            auto x      = a;           
            x.axpy( 0.5, ba ); 
            for( size_t k = 0; k < maxiter; ++k )
            {
                if( d*twok < tol )
                    break;
                
                auto&& fx   = f(x);
                if( std::abs(fx) < eps )
                    break;

                twok    *= 0.5;
                if( fx*fa > 0 )
                {
                    std::swap(a,x);
                    x.axpy( 3*twok, ba );
                }
                else
                {
                    std::swap(b,x);
                    x.axpy( -3*twok, ba );
                }
            }
            
            return x;
        }
};
#endif
