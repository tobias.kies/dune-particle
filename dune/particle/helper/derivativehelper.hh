#ifndef DUNE_DERIVATIVE_HELPER_HH
#define DUNE_DERIVATIVE_HELPER_HH

#include <dune/istl/bvector.hh>
#include <dune/particle/helper/multiindexhelper.hh>

/**
    This class helps transforming commonly used derivatives
    \todo Adapt this to the new interface / replace the helper wherever it occurs with the new interface.
*/
template<class JIT>
class DerivativeHelper
{
    public:
        
        DerivativeHelper( const JIT& jacobianInverseTransposed_ ) : jacobianInverseTransposed(jacobianInverseTransposed_), dim(std::decay<JIT>::type::rows)
        {}
        
        template<class LocalBasis, class LocalCoordinate>
        auto functionValues( const LocalBasis& localBasis, const LocalCoordinate& xLocal ) const
        {
            assert( dim == LocalCoordinate::dimension );
            
            std::vector<Dune::FieldVector<typename LocalCoordinate::field_type,1>> functionValues(localBasis.size());
            localBasis.evaluateFunction(xLocal, functionValues);
            
            return functionValues;
        }
        
        template<class LocalBasis, class LocalCoordinate>
        auto gradients( const LocalBasis& localBasis, const LocalCoordinate& xLocal ) const
        {
            assert( dim == LocalCoordinate::dimension );
            Dune::BlockVector<Dune::FieldVector<typename LocalCoordinate::field_type,LocalCoordinate::dimension>> transformedGradients(localBasis.size());

            std::vector<typename LocalBasis::Traits::JacobianType> referenceGradients(localBasis.size());
            localBasis.evaluateJacobian(xLocal, referenceGradients);
            for( size_t i = 0; i < localBasis.size(); ++i )
              jacobianInverseTransposed.mv(referenceGradients[i][0], transformedGradients[i]);

            return transformedGradients;
        }
        
        // Currently only implemented in two dimensions. Also could be done more elegantly, I suppose.
        template<class LocalBasis, class LocalCoordinate>
        auto gradientsOfLaplacians( const LocalBasis& localBasis, const LocalCoordinate& xLocal ) const
        {
          assert( LocalCoordinate::dimension == 2 );
          const size_t dimworld = LocalCoordinate::dimension;
          const auto& D = jacobianInverseTransposed;
          typedef typename LocalCoordinate::field_type float_x;
          Dune::BlockVector<Dune::FieldVector<typename LocalCoordinate::field_type,LocalCoordinate::dimension>> transformedGradientsOfLaplacians(localBasis.size());

          for( size_t i = 0; i < localBasis.size(); ++i )
          {
            transformedGradientsOfLaplacians[i] = 0;
            for( size_t k = 0; k < std::pow( dimworld, 3 ); ++k )
            {
              auto&& longDif = MultiIndexHelper::toLongMultiIndex( k, dimworld, 3 );

              auto&& dif = MultiIndexHelper::toShortMultiIndex<typename LocalBasis::OrderType>( longDif );
              auto&& derivative = localBasis.evaluate( xLocal, i, dif );

              float_x factor_0 = D[0][longDif[0]] * D[0][longDif[1]] * D[0][longDif[2]]
                       + D[0][longDif[0]] * D[1][longDif[1]] * D[1][longDif[2]];
              float_x factor_1 = D[0][longDif[0]] * D[0][longDif[1]] * D[1][longDif[2]]
                       + D[1][longDif[0]] * D[1][longDif[1]] * D[1][longDif[2]];

              transformedGradientsOfLaplacians[i][0]	+= derivative * factor_0;
              transformedGradientsOfLaplacians[i][1]	+= derivative * factor_1;
            }
          }
            
          return transformedGradientsOfLaplacians;
        }
        
        template<class LocalBasis, class LocalCoordinate>
        auto hessians( const LocalBasis& localBasis, const LocalCoordinate& xLocal ) const
        {
          assert( dim == LocalCoordinate::dimension );
          typename std::decay<JIT>::type jacobianInverse;
          for( size_t i = 0; i < dim; ++i )
            for( size_t j = 0; j < dim; ++j )
              jacobianInverse[i][j]	= jacobianInverseTransposed[j][i];

          std::vector<Dune::FieldMatrix<typename LocalCoordinate::field_type,LocalCoordinate::dimension,LocalCoordinate::dimension>> hessians_(localBasis.size());
          for( size_t i = 0; i < localBasis.size(); ++i )
          {
            typename LocalBasis::OrderType directions(0);
            for( size_t k = 0; k < dim; ++k )
            { 
              directions[k]++;
              for( size_t l = 0; l <= k; ++l )
              {
                directions[l]++;
                hessians_[i][k][l]	= localBasis.evaluate( xLocal, i, directions );
                hessians_[i][l][k]	= hessians_[i][k][l];
                directions[l]--;
              }
              directions[k]--;
            }
            hessians_[i].rightmultiply( jacobianInverse );
            hessians_[i].leftmultiply( jacobianInverseTransposed );
          }

          return hessians_;
        }
        
        template<class LocalBasis, class LocalCoordinate>
        auto thirdDerivatives( const LocalBasis& localBasis, const LocalCoordinate& xLocal) const
        {
          assert( LocalCoordinate::dimension == 2 );
          const size_t dimworld = LocalCoordinate::dimension;
          const auto& D = jacobianInverseTransposed;

          typedef typename LocalCoordinate::field_type float_x;
          typedef std::array<std::array<std::array<float_x,LocalCoordinate::dimension>,LocalCoordinate::dimension>,LocalCoordinate::dimension> ThirdDerivative;
          std::vector<ThirdDerivative> thirdDerivatives(localBasis.size());
          
          for(int n = 0; n < localBasis.size(); ++n)
          {
            for(int k = 0; k < std::pow(dimworld, 3); ++k)
            {
              auto&& K          = MultiIndexHelper::toLongMultiIndex(k, dimworld, 3);
              auto&& dif        = MultiIndexHelper::toShortMultiIndex<typename LocalBasis::OrderType>(K);
              auto&& derivative = localBasis.evaluate(xLocal, n, dif);
              
              for(int i = 0; i < std::pow(dimworld, 3); ++i)
              {                
                auto&& I = MultiIndexHelper::toLongMultiIndex(i, dimworld, 3);
                
                if(k == 0)
                  thirdDerivatives[n][I[0]][I[1]][I[2]] = 0;

                const auto factor = D[I[0]][K[0]] * D[I[1]][K[1]] * D[I[2]][K[2]];
                thirdDerivatives[n][I[0]][I[1]][I[2]] += factor * derivative;
              }
            }
          }
          
          return thirdDerivatives;
          
          
          /*assert( dim == LocalCoordinate::dimension );
          typename std::decay<JIT>::type jacobianInverse;
          for( size_t i = 0; i < dim; ++i )
            for( size_t j = 0; j < dim; ++j )
              jacobianInverse[i][j]	= jacobianInverseTransposed[j][i];

          typedef typename LocalCoordinate::field_type float_x;
          typedef std::array<std::array<std::array<float_x,LocalCoordinate::dimension>,LocalCoordinate::dimension>,LocalCoordinate::dimension> ThirdDerivative;
          std::vector<ThirdDerivative> thirdDerivatives(localBasis.size());
          for( int i = 0; i < localBasis.size(); ++i )
          {
            typename LocalBasis::OrderType directions(0);
            for( int k = 0; k < LocalCoordinate::dimension; ++k )
            {
              directions[k]++;
              for( int l = 0; l <= k; ++l )
              {
                directions[l]++;
                for( int m = 0; m <= l; ++m )
                {
                  const auto val = localBasis.evaluate(xLocal, i, directions);
                  thirdDerivatives[i][k][l][m]  = val;
                  thirdDerivatives[i][k][m][l]  = val; 
                  thirdDerivatives[i][l][k][m]  = val; 
                  thirdDerivatives[i][l][m][k]  = val; 
                  thirdDerivatives[i][m][k][l]  = val; 
                  thirdDerivatives[i][m][l][k]  = val; 
                }
                directions[l]--;
              }
              directions[k]--;
            }
          }
          
          const auto&& applyJacobian = [&jacobianInverse](const auto& D3, const int k, const int l, const int m )
          {
            float_x val = 0;
            
            for(int a = 0; a < LocalCoordinate::dimension; ++a)
              for(int b = 0; b < LocalCoordinate::dimension; ++b)
                for(int c = 0; c < LocalCoordinate::dimension; ++c)
                {
                  float_x factor = jacobianInverse[a][k] * jacobianInverse[b][l] * jacobianInverse[c][m];
                  val += D3[a][b][c] * factor;
                }
            return val;
          };
          
          std::vector<ThirdDerivative> transformedThirdDerivatives(localBasis.size());
          for( int i = 0; i < localBasis.size(); ++i )
            for( int k = 0; k < LocalCoordinate::dimension; ++k )
              for( int l = 0; l <= k; ++l )
                for( int m = 0; m <= l; ++m )
                {
                  float_x val = applyJacobian(thirdDerivatives[i], k, l, m);
                  transformedThirdDerivatives[i][k][l][m]  = val;
                  transformedThirdDerivatives[i][k][m][l]  = val; 
                  transformedThirdDerivatives[i][l][k][m]  = val; 
                  transformedThirdDerivatives[i][l][m][k]  = val; 
                  transformedThirdDerivatives[i][m][k][l]  = val; 
                  transformedThirdDerivatives[i][m][l][k]  = val;
                }

          return transformedThirdDerivatives;*/
        }
        
        template<class LocalBasis, class LocalCoordinate>
        auto laplacians( const LocalBasis& localBasis, const LocalCoordinate& xLocal ) const
        {
            assert( dim == LocalCoordinate::dimension );
            const auto& transformedHessians = hessians( localBasis, xLocal );
            Dune::BlockVector<Dune::FieldVector<typename LocalCoordinate::field_type,1>> transformedLaplacians(localBasis.size());
            for( size_t i = 0; i < localBasis.size(); ++i )
            {
                transformedLaplacians[i] = 0;
                for( size_t j = 0; j < dim; ++j )
                    transformedLaplacians[i]    += transformedHessians[i][j][j];
            }
            
            return transformedLaplacians;
        }
        
        template<class LocalBasis, class LocalCoordinate, class OrderType>
        auto partialDerivative( const LocalBasis& localBasis, const LocalCoordinate& xLocal, const OrderType& d, size_t localBasisId ) const
        {
            typename LocalBasis::Traits::RangeFieldType partialDerivativeValue(0);
            const auto& D = jacobianInverseTransposed;
            
            // Determine differentiation order and reformat differentiation order parameter.
            const size_t difOrder   = MultiIndexHelper::shortMultiIndexOrder(d);
            const auto alpha        = MultiIndexHelper::toLongMultiIndex(d);
            
            // Iterate over all multi-indices in {1,\dots,dim}^difOrder
            const size_t maxI = std::pow( (size_t) d.size(), difOrder );
            for( size_t i = 0; i < maxI; ++i )
            {
                const auto beta = MultiIndexHelper::toLongMultiIndex( i, d.size(), difOrder );
                const auto betaShort = MultiIndexHelper::template toShortMultiIndex<OrderType>( beta );
                
                // Compute coefficient.
                double c = 1;
                for( size_t j = 0; j < difOrder; ++j )
                    c   *= D[alpha[j]][beta[j]];
                    
                // Skip if constant is trivial.
                if( c == 0 )
                    continue;
                
                // Evaluate \partial^\beta f in xLocal.
                const auto fval = localBasis.evaluate( xLocal, localBasisId, betaShort );
                    
                // Update res.
                partialDerivativeValue   += c*fval;
            }
            
            return partialDerivativeValue;
        }
        
        template<class LocalBasis, class LocalCoordinate, class OrderType>
        auto partialDerivatives( const LocalBasis& localBasis, const LocalCoordinate& xLocal, const OrderType& d ) const
        {
            assert( dim == LocalCoordinate::dimension );
            std::vector<typename LocalBasis::Traits::RangeFieldType> partialDerivativeValues(localBasis.size());
            
            for( size_t localBasisId = 0; localBasisId < localBasis.size(); ++localBasisId )
                partialDerivativeValues[localBasisId]   = partialDerivative(localBasis, xLocal, d, localBasisId);
            
            return partialDerivativeValues;
        }


    private:
        
        const JIT& jacobianInverseTransposed;
        const size_t dim;
};
#endif
