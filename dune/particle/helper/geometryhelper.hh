#ifndef DUNE_GEOMETRY_HELPER_HH
#define DUNE_GEOMETRY_HELPER_HH

#include <dune/particle/geometries/geometries.hh>

/**
    This class helps with different geometry tasks/
*/
class GeometryHelper
{
    private:

        GeometryHelper()
        {}


    public:

        /**
         * Compute the distance between two rectangles.
         */
        template<class float_x>
        static auto distance( const Geometries::RealRectangle<float_x>& rec1, const Geometries::RealRectangle<float_x>& rec2 )
        {
            float_x dist = 0;

            // Zero distance if they intersect.
            if( intersect(rec1,rec2) )
                return dist;

            // Else we check for each corner of rec2 how far it is from rec1.
            dist = std::numeric_limits<float_x>::infinity();
            typedef typename Geometries::RealRectangle<float_x>::Vector Vector;
            std::array<Vector,8> corner;
            corner[0] = rec1.bottomLeft();
            corner[1] = rec1.bottomRight();
            corner[2] = rec1.upperRight();
            corner[3] = rec1.upperLeft();
            corner[4] = rec2.bottomLeft();
            corner[5] = rec2.bottomRight();
            corner[6] = rec2.upperRight();
            corner[7] = rec2.upperLeft();

            for( size_t i = 0; i < 4; ++i )
            {
                // Choose segment in rec1.
                const auto seg = corner[(i==3) ? 0 : i+1] - corner[i];
                const auto norm_seg = seg.two_norm2();

                // Choose points in rec2.
                for( size_t j = 0; j < 4; ++j )
                {
                    // Compute projection and create distance vector.
                    auto d = corner[4+j] - corner[i];
                    float_x t = std::max( 0., std::min( 1., d*seg / norm_seg ) );
                    d.axpy( t, seg );

                    // Update dist.
                    dist = std::min( dist, d.two_norm() );
                }
            }

            return dist;
        }

        /**
         * Compute the intersections of a polygon with an ellipse.
         */
        template<class float_x>
        static auto intersections( const Geometries::Ellipse<float_x>& ellipse, const Geometries::Polygon<float_x>& polygon )
        {
            typedef typename Geometries::Polygon<float_x>::Vector Point;
            typename std::vector<Point> intersectionPoints;

            // Iterate over all line segements of the polygon and add the line intersections to the list.
            for( size_t i = 0; i < polygon.size() - 1; ++i )
            {
                Geometries::LineSegment<float_x> segment( polygon[i+1]-polygon[i], polygon[i] );
                const auto& lineIntersectionPoints = intersections( ellipse, segment );
                intersectionPoints.insert( intersectionPoints.end(), lineIntersectionPoints.begin(), lineIntersectionPoints.end() );
            }

            // Remove duplicate intersection points (occurs when a corner is an intersection point).
            sortPointsByCenter( intersectionPoints, Point({0,0}) );
            makePointsUnique( intersectionPoints );

            // Return result.
            return intersectionPoints;
        }

        template<class float_x>
        static auto intersections( const Geometries::Polygon<float_x>& polygon, const Geometries::Ellipse<float_x>& ellipse )
        {
            return intersections( ellipse, polygon );
        }


        /**
         * Compute the intersections of an ellipse and a line segment.
         */
         template<class float_x>
         static auto intersections( const Geometries::Ellipse<float_x>& ellipse, const Geometries::LineSegment<float_x>& lineSegment )
         {
            // Compute intersections by interpreting the line segment as a full line.
            auto&& intersectionPoints = intersections( ellipse, (const Geometries::Line<float_x>&) lineSegment );

            // Remove all intersections that lie outside the segment.
            // Note: It would be more efficient to copy the intersection algorithm from the ellipse-line intersection here and to just drop the corresponding t values directly.
            intersectionPoints.erase(
                std::remove_if( intersectionPoints.begin(),
                                intersectionPoints.end(),
                                [lineSegment]( const typename std::decay<decltype(intersectionPoints)>::type::value_type& point )
                                {
                                    float_x t = ( (point - lineSegment.shift()) * lineSegment.direction() ) / ( lineSegment.direction() * lineSegment.direction() );
                                    return ( t < 0 || t > 1 );
                                } ),
                intersectionPoints.end()
            );

            return intersectionPoints;
         }

         template<class float_x>
         static auto intersections( const Geometries::LineSegment<float_x>& lineSegment, const Geometries::Ellipse<float_x>& ellipse )
         {
             return intersections( ellipse, lineSegment );
         }


        /**
         * Compute the intersections of an ellipse and a line.
         */
        template<class float_x>
        static auto intersections( const Geometries::Ellipse<float_x>& ellipse, const Geometries::Line<float_x>& line )
        {
            // For the calculations in this code: compare ellipseLineIntersection.m

            const auto&
                center = ellipse.center(),
                direction = line.direction(),
                shift = line.shift();

            const float_x
                a = ellipse.a(),
                b = ellipse.b(),
                phi = ellipse.rotation(),
                cx = center[0],
                cy = center[1],
                px = direction[0],
                py = direction[1],
                qx = shift[0],
                qy = shift[1];

            const float_x
                t3 = cos(phi),
                t4 = cy-qy,
                t5 = sin(phi),
                t6 = cx-qx,
                t9 = t3*t6,
                t10 = t4*t5,
                t2 = t9-t10,
                t12 = t3*t4,
                t13 = t5*t6,
                t7 = t12+t13,
                t8 = 1.0/(a*a),
                t11 = 1.0/(b*b),
                t14 = px*t3,
                t15 = t14-py*t5,
                t16 = py*t3,
                t17 = px*t5,
                t18 = t16+t17;


            // The intersection points are given by the points on the line
            //  p*t + q
            // where t solves
            //  a0 + a1*t + a2*t^2 = 0
            const float_x
                a0 = (t2*t2)*t8+(t7*t7)*t11-1.0,
                a1 = t2*t8*t15*-2.0-t7*t11*t18*2.0,
                a2 = t8*(t15*t15)+t11*(t18*t18),
                disc = a1*a1 - 4*a2*a0;


            // Compute the solutions using the quadratic solution formula.
            std::vector<float_x> t;
            if( disc > 0 )
            {
                t.push_back( (-a1 + std::sqrt(disc)) / (2*a2) );
                t.push_back( (-a1 - std::sqrt(disc)) / (2*a2) );
            }
            else if( std::abs(disc) < std::numeric_limits<float_x>::epsilon() * 512 )
            {
                t.push_back( -a1 / (2*a2) );
            }


            // Convert the scalar t to points and return those.
            std::vector<typename Geometries::Line<float_x>::Vector> intersectionPoints;
            for( const auto& tval : t )
            {
                auto dummy = direction;
                dummy   *= tval;
                intersectionPoints.push_back( dummy + shift );
            }

            return intersectionPoints;
        }

        template<class float_x>
        static auto intersections( const Geometries::Line<float_x>& line, const Geometries::Ellipse<float_x>& ellipse )
        {
            return intersections( ellipse, line );
        }


        /**
         * Computes whether two (real) rectangles intersect.
         */
        template<class float_x>
        static auto intersect( const Geometries::RealRectangle<float_x>& rec1, const Geometries::RealRectangle<float_x>& rec2 )
        {
            typedef typename Geometries::RealRectangle<float_x>::Vector Vector;
            std::array<Vector,8> corner;

            corner[0] = rec1.bottomLeft();
            corner[1] = rec1.bottomRight();
            corner[2] = rec1.upperRight();
            corner[3] = rec1.upperLeft();
            corner[4] = rec2.bottomLeft();
            corner[5] = rec2.bottomRight();
            corner[6] = rec2.upperRight();
            corner[7] = rec2.upperLeft();

            // Check if one of the corners of one rectangle is contained in the other.
            for( size_t i = 0; i < 7; i+=4 )
            {
                const auto ab = corner[i+1] - corner[i+0];
                const auto ad = corner[i+3] - corner[i+0];
                const auto cb = corner[i+1] - corner[i+2];
                const auto cd = corner[i+3] - corner[i+1];
                const size_t shift = (i==0) ? 4 : 0;
                for( size_t j = 0; j < 4; ++j )
                {
                    const auto xa = corner[shift+j] - corner[i+0];
                    const auto xc = corner[shift+j] - corner[i+2];
                    if( ab * xa >= 0 && ad * xa >= 0 && cb * xc >= 0 && cd * xc >= 0 )
                        return true;
                }
            }

            // Nothing happened.
            return false;
        }


        /**
         * Remove duplicates from an array of points.
         * Note: For this it is required that the points are sorted first!
         */
        template<class Points>
        static void makePointsUnique( Points& points )
        {
            typedef typename Points::value_type Point;

            points.erase(
                unique(
                    points.begin(),
                    points.end(),
                    [](const Point& a, const Point& b){
                        Point c(a-b);
                        return ( c.infinity_norm() < std::numeric_limits<typename Point::value_type>::epsilon() * 512 );
                    } ),
                   points.end()
            );
        }


        /**
         * Remove points that are not inside the specified element.
         * Currently only 2D rectangles are supported.
         */
        template<class Element, class Points>
        static void removePointsOutsideElement( const Element& element, Points& points )
        {
            const auto& geometryType = element.type();
            if( !(geometryType.isCube() && geometryType.dim() == 2) )
                DUNE_THROW( Dune::NotImplemented, "The function removePointsOutsideElement() currently does not support the specified element type." );

            double min_x = std::numeric_limits<double>::infinity(),
                    max_x = -min_x,
                    min_y = min_x,
                    max_y = -min_y;

            const auto&& geometry = element.geometry();
            for( size_t i = 0; i < geometry.corners(); ++i )
            {
                const auto&& corner = geometry.corner(i);
                min_x   = std::min( min_x, corner[0] );
                max_x   = std::max( max_x, corner[0] );
                min_y   = std::min( min_y, corner[1] );
                max_y   = std::max( max_y, corner[1] );
            }

            points.erase(
                std::remove_if( points.begin(),
                                points.end(),
                                [min_x,max_x,min_y,max_y]( const typename Points::value_type& point )
                                {
                                    return ( min_x > point[0] || max_x < point[0]
                                                || min_y > point[1] || max_y < point[1] );
                                } ),
                points.end()
            );
        }


        /**
         * Sort points in 2D with respect to a given center using atan2.
         */
        template<class Points, class Point>
        static void sortPointsByCenter( Points& points, const Point& center )
        {
            std::sort( points.begin(), points.end(),
                        [center]( const Point& a, const Point& b ){
                            return ( std::atan2( a[1]-center[1], a[0]-center[0] ) < std::atan2( b[1]-center[1], b[0]-center[0] ) );
                        } );
        }

        /**
         *  Sort points with respect to an element's center.
         */
        template<class Points, class Element>
        static void sortPointsByCenterOfElement( Points& points, const Element& element )
        {
            const auto& geometry = element.geometry();
            const auto& center = geometry.center();
            sortPointsByCenter( points, center );
        }


        /**
         * Transform points from an element to its reference element.
         * Returns a vector containing the local points.
         */
        template<class Element, class Points>
        static auto transformOntoReferenceElement( const Element& element, Points& points )
        {
            std::vector<typename std::decay<decltype( element.geometry().local(points[0]) )>::type> localPoints( points.size() );

            for( size_t i = 0; i < points.size(); ++i ) // one day we maybe can replace this by a python map-style loop
                localPoints[i] = element.geometry().local(points[i]);

            return localPoints;
        }
};
#endif
