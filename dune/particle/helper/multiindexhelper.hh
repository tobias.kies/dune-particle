#ifndef DUNE_MULTI_INDEX_HELPER_HH
#define DUNE_MULTI_INDEX_HELPER_HH
namespace MultiIndexHelper
{
    // Base here is shifted for convenience in the above parts.
    // Explanation: Translate an integer idx to a multi-index out such that out <= base.
    template<class T>
    inline void toMultiIndex( size_t idx, const T& base, T& out ) //const
    {
        for( size_t i = 0; i < base.size(); ++i )
        {
            out[i]	= idx % (base[i]+1);
            idx		/= (base[i]+1);
        }
    }

    template<class T>
    inline T toMultiIndex( size_t idx, const T& base ) //const
    {
        T out;
        toMultiIndex( idx, base, out );
        return out;
    }

    template<class T>
    inline std::vector<size_t> toLongMultiIndex( const T& shortMultiIndex ) //const
    {
        std::vector<size_t> longMultiIndex;
        for( size_t i = 0; i < shortMultiIndex.size(); ++i )
            for( size_t j = 0; j < shortMultiIndex[i]; ++j )
                longMultiIndex.push_back(i);
        return longMultiIndex;
    }

    template<class OrderType, class T>
    inline OrderType toShortMultiIndex( const T& longMultiIndex ) //const
    {
        OrderType shortMultiIndex(0);
        for( size_t i = 0; i < longMultiIndex.size(); ++i )
            shortMultiIndex[longMultiIndex[i]]++;
        return shortMultiIndex;
    }

    // Translates an index to a long multi index with given order in dim dimensions.
    inline std::vector<size_t> toLongMultiIndex( size_t idx, size_t dim, size_t order ) //const
    {
        std::vector<size_t> longMultiIndex(order);
        for( size_t i = 0; i < order; ++i )
        {
            longMultiIndex[i]	= idx % dim;
            idx	/= dim;
        }
        return longMultiIndex;
    }

    // Computes the multi-index factorial (for short representation)
    // Note: Not most efficient implementation.
    template<class T>
    inline size_t shortMultiIndexFactorial( const T& shortMultiIndex )
    {
        size_t fac = 1;
        for( size_t i = 0; i < shortMultiIndex.size(); ++i )
            for( size_t j = shortMultiIndex[i]; j > 1 ; --j )
                fac	*= j;
        return fac;
    }

    // Compute n choose k in multi index sense.
    // Note: Also not quite efficient.
    template<class T>
    inline size_t shortMultiIndexChoose( const T& alpha, const T& beta )
    {
        return shortMultiIndexFactorial( alpha ) / shortMultiIndexFactorial( beta ) / shortMultiIndexFactorial( alpha - beta );
    }

    template<class T>
    inline size_t shortMultiIndexOrder( const T& alpha )
    {
        size_t order = 0;
        for( size_t i = 0; i < alpha.size(); ++i )
            order	+= alpha[i];
        return order;
    }
}
#endif
