#ifndef DUNE_BOUNDING_BOX_HH
#define DUNE_BOUNDING_BOX_HH

#include <math.h>

// Describe a bounding box in 2D.

class BoundingBox
{
  public:
  
    typedef Dune::FieldVector<double,2> Point;
    
    BoundingBox(const Point& lower_, const Point& upper_)
    : lower_(lower_), upper_(upper_)
    {}
    
    BoundingBox( )
    {}
    
    
    const auto& lower( ) const
    {
      return lower_;
    }
    
    const auto& upper( ) const
    {
      return upper_;
    }
    
    void setLower(const Point& lower__)
    {
      lower_  = lower__;
    }
    
    void setUpper(const Point& upper__)
    {
      upper_  = upper__;
    }


  private:
  
    Point lower_;
    Point upper_;
};

#endif
