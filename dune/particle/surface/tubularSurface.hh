#ifndef DUNE_TUBULAR_SURFACE_HH
#define DUNE_TUBULAR_SURFACE_HH

#include <math.h>
#include <dune/particle/surface/boundingBox.hh>

#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>
#include <dune/particle/helper/functionshelper.hh>

#include <dune/common/bitsetvector.hh>
#include "dune/fufem/boundarypatch.hh"
#include <dune/fufem/functiontools/boundarydofs.hh>

#include <dune/particle/basis/conforming/conformingkbasis.hh>


// Describes a centered rectangle in 2D.
template<class float_x = double>
class TubularSurface
{
  public:
  
    typedef Dune::FieldVector<float_x,1> Point1D;
    typedef Dune::FieldVector<float_x,2> Point2D;
    typedef Dune::FieldVector<float_x,3> Point3D;
    
    typedef typename FunctionsHelper::Polynomial<float_x,2> LevelSetFunction;
    typedef typename FunctionsHelper::Polynomial<float_x,2> ConstraintFunction;
    
    static const int dim = 2;

    TubularSurface(const double length, const double radius)
      : r({length, 2*M_PI}), radius(radius), levelSetFunction_(FunctionsHelper::constant(-1)), constraintFunction_(FunctionsHelper::constant(0)), parameterizationFunction_(radius), parameterizedProjectionDistanceFunction_(radius)
    {
      if( !(r[0] >= 0 && r[1] >= 0) )
        DUNE_THROW( Dune::Exception, "Assertion r[0] > 0 && r[1] > 0 failed. Got r[0]=" + std::to_string(r[0]) + ", r[1]=" + std::to_string(r[1]) );
      
      auto mr = r;
      mr  *= 0;
      boundingBox_.setLower(mr);
      boundingBox_.setUpper(r);
      
      parameterizedProjectionDistanceFunction_.set_r(r);
    }
    
    const auto& boundingBox( ) const
    {
      return boundingBox_;
    }
    
    const auto& periodicBox( ) const
    {
      auto periodicBox_ = boundingBox_;
      periodicBox_.setLower( {std::numeric_limits<double>::infinity(), boundingBox_.lower()[1]} );
      periodicBox_.setUpper( {std::numeric_limits<double>::infinity(), boundingBox_.upper()[1]} );
      // In this case the bounding box is identical with the periodic box.
      return boundingBox_;
    }
    
    auto parameterizedProjection(const Point3D& x) const
    {
      return parameterizedProjectionDistanceFunction_.evaluate(x).first;
    }
    
    auto parameterizedDistance(const Point3D& x) const
    {
      return parameterizedProjectionDistanceFunction_.evaluate(x).second;
    }

    const auto& levelSetFunction( ) const
    {
      return levelSetFunction_;
    }

    const auto& constraintFunction( ) const
    {
      return constraintFunction_;
    }
    
    const auto& parameterizationFunction( ) const
    {
      return parameterizationFunction_;
    }
    
    const auto& parameterizedNormal( ) const
    {
      return parameterizedNormal_;
    }
    
    const auto& parameterizedProjectionDistanceFunction( ) const
    {
      return parameterizedProjectionDistanceFunction_;
    }
    
    
    template<class Basis>
    const auto ignoreDOFs(const Basis& basis) const
    {
      const auto L = r[0];
      
      const auto&& condition = [&L](const auto& intersection, const auto& element, const auto& localCoefficients, const int id)
      {
        const auto& g = intersection.geometry();
        const int n   = g.corners();
        for(int i = 0; i < n; ++i )
        {
          const auto&& corner = g.corner(i);
          if( feq(corner[0],0) || feq(corner[0],L) )
            return true;
        }
        return false;
      };


      Dune::BitSetVector<1> dirichletDofs;
      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
      constructConditonalBoundaryDOFs(boundaryPatch, basis, dirichletDofs, condition);
      return dirichletDofs;
    }


    template<class Basis>
    void updateConstraints(Basis& basis) const
    {
      const auto N = basis.size();
      
      // Determine periodic degrees of freedom.
      auto&& periodicDOFs = determinePeriodicDOFs(basis);
      //~ Debug::dprint("Determined periodic DOFs");
      
      // Determine buddies.
      const auto&& buddy        = determineBuddies(basis, periodicDOFs);
      //~ Debug::dprint("Determined buddies");
      
      // Get constraints.
      auto& transformation      = basis.dofConstraints();
      auto& isConstrained       = transformation.isConstrained();
      auto& linearCombinations  = transformation.interpolation();
      
      // Error check: We can not yet handle constraining DOFs that are constrained already.
      for(int i = 0; i < N; ++i)
        if(isConstrained[i].any() && periodicDOFs[i].any())
          DUNE_THROW(Dune::NotImplemented, "Adding periodic BCs is not implemented for constrained DOFs on the periodic boundary.");
      
      // Set constraints.
      for(int i = 0; i < N; ++i)
      {        
        isConstrained[i]  = periodicDOFs[i].any();
        if(isConstrained[i].none())
          continue;
          
        const auto& buddyId = buddy.at(i);
          
        auto& linearCombination = linearCombinations[i];
        linearCombination.push_back(
          DOFConstraintsK::LinearFactor(buddyId, 1)
        );
        
        // Make sure that we do not restrict the partner DOF.
        periodicDOFs[buddyId] = false;
      }
      
      return;
    }
    
    auto minDistance() const
    {
      return -0.9*radius;
    }
    

  private:
  
    const typename BoundingBox::Point r;
    const double radius;
    BoundingBox boundingBox_;

    class ParameterizationFunction : public Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,Dune::FieldVector<size_t,2>>
    {
      public:

        typedef Dune::FieldVector<size_t,2> OrderType;
      
        typedef Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,OrderType> BaseType;
        
        typedef typename BaseType::RangeType RangeType;
        
        using BaseType::evaluate;
      
        ParameterizationFunction(const double radius)
          : radius(radius)
        {}
        
        RangeType evaluate(const Point2D& x, const OrderType& d) const
        {
          Point3D res(0);
          const auto order = d[0] + d[1];
          
          switch(order)
          {
            case 0:
            {
              res[0]  = x[0];
              res[1]  = radius*cos(x[1]);
              res[2]  = radius*sin(x[1]);
              break;
            }
              
            case 1:
            {
              if(d[0] == 1)
                res[0]  = 1;
              else
              {
                res[1]  = -radius*sin(x[1]);
                res[2]  =  radius*cos(x[1]);
              }
              break;
            }
              
            default:
              DUNE_THROW(Dune::NotImplemented, "Not implemented.");
              break;
          }
          
          return res;
        }
        
      private:
        const double radius;
    };

    class ParameterizedProjectionDistanceFunction
      : public Dune::VirtualMultiDifferentiableFunction<Point3D,std::pair<Point2D,Point1D>,Dune::FieldVector<size_t,3>>
    {
      public:
      
        typedef Point3D DomainType;
        typedef std::pair<Point2D,Point1D> RangeType;
        //~ typedef std::pair<Dune::FieldVector<size_t,2>,Dune::FieldVector<size_t,1>> OrderType;
        typedef Dune::FieldVector<size_t,3> OrderType;
      
        typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;
        
        using BaseType::evaluate;
        
        ParameterizedProjectionDistanceFunction(const double radius)
          : radius(radius)
        {}
        
        void set_r(const typename BoundingBox::Point& r_)
        {
          r = r_;
        }
        
        RangeType evaluate(const DomainType& x, const OrderType& d) const
        {
          RangeType res;
          const auto order = d[0] + d[1] + d[2];
          
          switch(order)
          {
            case 0:
              if( x[0] < 0 && x[0] > r[0] )
                DUNE_THROW(Dune::Exception, "Point outside feasible range.");
              else
                res.first[0]  = x[0];
              res.first[1]  = atan2(x[2], x[1]);
              if(res.first[1] < 0)
                res.first[1]  += 2*M_PI;
              res.second    = sqrt(x[1]*x[1] + x[2]*x[2]) - radius;
              break;
              
            case 1:
              if(d[0] == 1)
                res.first[0] = 1;
              else if(d[1] == 1){
                res.first[1]  = -x[2]/(x[1]*x[1] + x[2]*x[2]);
                res.second    =  x[1]/sqrt(x[1]*x[1] + x[2]*x[2]);
              }else if(d[2] == 1){
                res.first[1]  =  x[1]/(x[1]*x[1] + x[2]*x[2]);
                res.second    =  x[2]/sqrt(x[1]*x[1] + x[2]*x[2]);
              }
              break;
              
            default:
              DUNE_THROW(Dune::NotImplemented, "Not implemented.");
              break;
          }
          
          return res;
        }
        
      private:
        typename BoundingBox::Point r;
        const double radius;
    };
    
    
    class ParameterizedNormal : public Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,Dune::FieldVector<size_t,2>>
    {
      public:
        
        typedef Dune::FieldVector<size_t,2> OrderType;
      
        typedef Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,OrderType> BaseType;
        
        typedef typename BaseType::RangeType RangeType;

        using BaseType::evaluate;
        
        RangeType evaluate(const Point2D& x, const OrderType& d) const
        {
          Point3D res(0);
          const auto order = d[0] + d[1];
          
          switch(order)
          {
            case 0:
            {
              res[1]  = cos(x[1]);
              res[2]  = sin(x[1]);
              break;
            }
            
            case 1:
            {
              if(d[1] == 1)
              {
                res[1]  = -sin(x[1]);
                res[2]  =  cos(x[1]);
              }
              // else: d[0] == 1 and we need to do nothing.
              break;
            }
              
            default:
              DUNE_THROW(Dune::NotImplemented, "Not implemented: Order " + std::to_string(order));
              break;
          }
          
          return res;
        }
    };
    
    const ParameterizationFunction parameterizationFunction_;
          ParameterizedProjectionDistanceFunction parameterizedProjectionDistanceFunction_;
    const ParameterizedNormal parameterizedNormal_;
    const LevelSetFunction levelSetFunction_;
    const ConstraintFunction constraintFunction_;
    
    
    // Some helper functions:

    // Modification of the "constructBoundaryDofs" in fufem/functiontools/boundarydofs.hh
    // To-do: Replace the class conditional by the appropriate function object.
    template <class GridView, class Basis, class Conditional, int blocksize>
    static void constructConditonalBoundaryDOFs(
        const BoundaryPatch<GridView>& boundaryPatch,
        const Basis& basis,
        Dune::BitSetVector<blocksize>& boundaryDofs,
        const Conditional& conditional)
    {
      boundaryDofs.resize(basis.size());
      boundaryDofs.unsetAll();

      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& inside = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(inside).localCoefficients();

        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          unsigned int entity = localCoefficients.localKey(i).subEntity();
          unsigned int codim  = localCoefficients.localKey(i).codim();

          if(it.containsInsideSubentity(entity, codim))
            if(conditional(*it, inside, localCoefficients, i))
              boundaryDofs[basis.index(inside, i)] = true;
        }
      }
    }    
    
    static auto feq(double a, double b)
    {
      return ( std::abs(a-b) < 128*std::numeric_limits<double>::epsilon() );
    }

    template<class Basis>
    auto determinePeriodicDOFs(const Basis& basis) const
    {
      const auto&& condition = [](const auto& intersection, const auto& element, const auto& localCoefficients, const int id)
      {
        const auto& g = intersection.geometry();
        const int n   = g.corners();
        for(int i = 0; i < n; ++i )
        {
          const auto&& corner = g.corner(i);
          if( !feq(corner[1],0) && !feq(corner[1],2*M_PI) )
            return false;
        }
        return true;
      };


      Dune::BitSetVector<1> periodicDofs;
      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
      constructConditonalBoundaryDOFs(boundaryPatch, basis, periodicDofs, condition);
      return periodicDofs;
    }
    
    /*template<class Basis, class BP, class SubEntity>
    static auto findBuddy(const int idx0, const Basis& basis, const Dune::BitSetVector<1>& periodicDOFs, const BP& boundaryPatch, const SubEntity& subEntity0, const int subIndex0)
    {
      const auto&& corner0 = subEntity0.geometry().corner(0);

      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& element = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(element).localCoefficients();

        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          const auto idx = basis.index(element, i);
          if(periodicDOFs[idx].none() || idx == idx0)
            continue;

          const auto entity = localCoefficients.localKey(i).subEntity();
          const auto codim  = localCoefficients.localKey(i).codim();
          const auto index  = localCoefficients.localKey(i).index();
          
          if(index != subIndex0)
            continue;
          
          if( codim != dim )
            DUNE_THROW(Dune::Exception, "Only point-based finite elements are supported.");
          
          const auto&& subEntity  = element.template subEntity<dim>(entity);
          const auto&& corner = subEntity.geometry().corner(0);
          if(feq(corner[0],corner0[0]))
            return idx;
        }
      }
      
      DUNE_THROW(Dune::Exception, "Buddy not found.");
      return -1;
    }*/
    
    template<class Basis>
    static auto determineBuddies(const Basis& basis, Dune::BitSetVector<1> periodicDOFs)
    {
      std::map<int,int> buddy;
      
      int counter = 0;
      int nTotal = 0;
      for(int i = 0; i < periodicDOFs.size(); ++i)
        if(periodicDOFs[i].any())
          ++nTotal;
      
      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
      
      std::map<std::pair<double,size_t>,std::vector<int>> indexMap;
      
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& element = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(element).localCoefficients();

        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          const auto idx = basis.index(element, i);
          //~ if(periodicDOFs[idx].none() || buddy.count(idx) > 0)
          if(periodicDOFs[idx].none())
            continue;
            
          //~ std::cerr << "dealing with index " << counter++ << "/" << nTotal;
          
          const auto entity = localCoefficients.localKey(i).subEntity();
          const auto codim  = localCoefficients.localKey(i).codim();
          const auto index  = localCoefficients.localKey(i).index();
          
          if( codim != dim )
            DUNE_THROW(Dune::Exception, "Only point-based finite elements are supported.");

          const auto&& subEntity  = element.template subEntity<dim>(entity);
          const auto&& corner = subEntity.geometry().corner(0);
          indexMap[std::make_pair(corner[0],index)].push_back(idx);
          //~ std::cerr << " adding " << idx << " to " << corner[0] << " leading to size " << indexMap[std::make_pair(corner[0],index)].size() << std::endl;
          /*const auto buddyId = findBuddy(idx, basis, periodicDOFs, boundaryPatch, subEntity, index);
          buddy[idx]      = buddyId;
          buddy[buddyId]  = idx;*/
          
          // Avoid revisiting this.
          periodicDOFs[idx] = false;
        }
      }
      
      for(const auto& ids : indexMap)
      {
        if(ids.second.size() != 2){
          Debug::dprintVector(ids.second, ids.first.first, ids.first.second);
          DUNE_THROW(Dune::Exception, "Buddy finding failed, got size " + std::to_string(ids.second.size()));
        }
        buddy[ids.second[0]]  = ids.second[1];
        buddy[ids.second[1]]  = ids.second[0];
      }
      
      return buddy;
    }
};

#endif
