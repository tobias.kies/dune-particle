#ifndef DUNE_PLANAR_RECTANGULAR_PERIODIC_SURFACE_HH
#define DUNE_PLANAR_RECTANGULAR_PERIODIC_SURFACE_HH

#include <math.h>
#include <dune/particle/surface/boundingBox.hh>

#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>
#include <dune/particle/helper/functionshelper.hh>

#include <dune/common/bitsetvector.hh>
#include "dune/fufem/boundarypatch.hh"
#include <dune/fufem/functiontools/boundarydofs.hh>


// Describes a centered rectangle in 2D; uses periodic boundary conditions (instead of Dirichlet ones). Also projection is realized in a periodic way.
template<class float_x = double>
class PlanarRectangularPeriodicSurface
{
  public:

    typedef Dune::FieldVector<float_x,1> Point1D;
    typedef Dune::FieldVector<float_x,2> Point2D;
    typedef Dune::FieldVector<float_x,3> Point3D;

    typedef typename FunctionsHelper::Polynomial<float_x,2> LevelSetFunction;
    typedef typename FunctionsHelper::Polynomial<float_x,2> ConstraintFunction;
    
    static const int dim = 2;

    PlanarRectangularPeriodicSurface(const double a = 1, const double b = std::numeric_limits<double>::quiet_NaN())
      : r({a, std::isnan(b) ? a : b }), levelSetFunction_(FunctionsHelper::constant(-1)), constraintFunction_(FunctionsHelper::constant(0))
    {
      if( !(r[0] >= 0 && r[1] >= 0) )
        DUNE_THROW( Dune::Exception, "Assertion r[0] > 0 && r[1] > 0 failed. Got r[0]=" + std::to_string(r[0]) + ", r[1]=" + std::to_string(r[1]) );

      auto mr = r;
      mr  *= -1;
      boundingBox_.setLower(mr);
      boundingBox_.setUpper(r);

      parameterizedProjectionDistanceFunction_.set_r(r);
    }

    const auto& boundingBox( ) const
    {
      return boundingBox_;
    }
    
    const auto& periodicBox( ) const
    {
      // In this case the bounding box is identical with the periodic box.
      return boundingBox_;
    }

    auto parameterizedProjection(const Point3D& x) const
    {
      return parameterizedProjectionDistanceFunction_.evaluate(x).first;
    }

    auto parameterizedDistance(const Point3D& x) const
    {
      return parameterizedProjectionDistanceFunction_.evaluate(x).second;
    }

    const auto& levelSetFunction( ) const
    {
      return levelSetFunction_;
    }

    const auto& constraintFunction( ) const
    {
      return constraintFunction_;
    }

    const auto& parameterizationFunction( ) const
    {
      return parameterizationFunction_;
    }

    const auto& parameterizedNormal( ) const
    {
      return parameterizedNormal_;
    }

    const auto& parameterizedProjectionDistanceFunction( ) const
    {
      return parameterizedProjectionDistanceFunction_;
    }


    template<class Basis>
    const auto ignoreDOFs(const Basis& basis) const
    {
      Dune::BitSetVector<1> dirichletDofs(basis.size(),false);
      return dirichletDofs;
    }


    template<class Basis>
    void updateConstraints(Basis& basis) const
    {
      #if NO_PERIODIC_CONSTRAINTS
        return;
      #endif
      const auto N = basis.size();

      // Determine periodic degrees of freedom.
      auto&& periodicDOFs = determinePeriodicDOFs(basis);
      //~ Debug::dprint("Determined periodic DOFs");

      // Determine buddies.
      const auto&& buddies      = determineBuddies(basis, periodicDOFs);
      //~ Debug::dprint("Determined buddies");

      // Get constraints.
      auto& transformation      = basis.dofConstraints();
      auto& isConstrained       = transformation.isConstrained();
      auto& linearCombinations  = transformation.interpolation();

      // Error check: We can not yet handle constraining DOFs that are constrained already.
      #warning Is it still true that periodic BCs and constrained DOFs may not collide?
      for(int i = 0; i < N; ++i)
        if(isConstrained[i].any() && periodicDOFs[i].any())
          DUNE_THROW(Dune::NotImplemented, "Adding periodic BCs is not implemented for constrained DOFs on the periodic boundary.");

      // Set constraints.
      //~ for(int i = 0; i < N; ++i)
        //~ isConstrained[i]  = false;
      for(int i = 0; i < N; ++i)
      {
        if(periodicDOFs[i].none())
          continue;

        for(const auto& buddyId : buddies.at(i))
        {
          isConstrained[buddyId]  = true;
          auto& linearCombination = linearCombinations[buddyId];
          linearCombination.push_back(
            DOFConstraintsK::LinearFactor(i, 1)
          );

          // Make sure that we do not restrict the partner DOF.
          periodicDOFs[buddyId] = false;
        }
      }
      return;
    }

    auto minDistance() const
    {
      return -std::numeric_limits<double>::infinity();
    }

    const typename BoundingBox::Point r;

  private:

    BoundingBox boundingBox_;

    class ParameterizationFunction : public Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,Dune::FieldVector<size_t,2>>
    {
      public:

        typedef Dune::FieldVector<size_t,2> OrderType;

        typedef Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,OrderType> BaseType;

        typedef typename BaseType::RangeType RangeType;

        using BaseType::evaluate;

        RangeType evaluate(const Point2D& x, const OrderType& d) const
        {
          Point3D res;
          res = 0;
          const auto order = d[0] + d[1];

          switch(order)
          {
            case 0:
              res[0]  = x[0];
              res[1]  = x[1];
              break;

            case 1:
              if(d[0] == 1)
                res[0]  = 1;
              else
                res[1]  = 1;
              break;

            default:
              DUNE_THROW(Dune::NotImplemented, "Not implemented.");
              break;
          }

          return res;
        }
    };

    class ParameterizedProjectionDistanceFunction
      : public Dune::VirtualMultiDifferentiableFunction<Point3D,std::pair<Point2D,Point1D>,Dune::FieldVector<size_t,3>>
    {
      public:

        typedef Point3D DomainType;
        typedef std::pair<Point2D,Point1D> RangeType;
        //~ typedef std::pair<Dune::FieldVector<size_t,2>,Dune::FieldVector<size_t,1>> OrderType;
        typedef Dune::FieldVector<size_t,3> OrderType;

        typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;

        using BaseType::evaluate;

        typename BoundingBox::Point r;

        void set_r(const typename BoundingBox::Point& r_)
        {
          r = r_;
        }

        RangeType evaluate(const DomainType& x, const OrderType& d) const
        {
          RangeType res;
          const auto order = d[0] + d[1] + d[2];

          switch(order)
          {
            case 0:
              for(int i = 0; i < DomainType::dimension-1; ++i)
                res.first[i] = (fmod(x[i]+r[i],2*r[i])-r[i]) + (x[i]<-r[i])*(2*r[i]); // Attention: x[i]==r[i] will be mapped onto -r[i] now!
              res.second  = x[2];
              break;

            case 1:
              if(d[0] == 1)
                res.first[0]  = 1;
              else if(d[1] == 1)
                res.first[1]  = 1;
              else if(d[2] == 1)
                res.second  = 1;
              break;

            default:
              // Do nothing: Higher derivatives are always zero.
              break;
          }

          return res;
        }
    };


    class ParameterizedNormal : public Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,Dune::FieldVector<size_t,2>>
    {
      public:

        typedef Dune::FieldVector<size_t,2> OrderType;

        typedef Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,OrderType> BaseType;

        typedef typename BaseType::RangeType RangeType;

        using BaseType::evaluate;

        RangeType evaluate(const Point2D& x, const OrderType& d) const
        {
          Point3D res(0);
          const auto order = d[0] + d[1];

          switch(order)
          {
            case 0:
              res[2]  = 1;
              break;

            default:
              // Do nothing -- higher derivative of normal is always zero.
              break;
          }

          return res;
        }
    };


    // Some helper functions:

    // Modification of the "constructBoundaryDofs" in fufem/functiontools/boundarydofs.hh
    // To-do: Replace the class conditional by the appropriate function object.
    template <class GridView, class Basis, class Conditional, int blocksize>
    static void constructConditonalBoundaryDOFs(
        const BoundaryPatch<GridView>& boundaryPatch,
        const Basis& basis,
        Dune::BitSetVector<blocksize>& boundaryDofs,
        const Conditional& conditional)
    {
      boundaryDofs.resize(basis.size());
      boundaryDofs.unsetAll();

      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& inside = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(inside).localCoefficients();

        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          unsigned int entity = localCoefficients.localKey(i).subEntity();
          unsigned int codim  = localCoefficients.localKey(i).codim();

          if(it.containsInsideSubentity(entity, codim))
            if(conditional(*it, inside, localCoefficients, i))
              boundaryDofs[basis.index(inside, i)] = true;
        }
      }
    }

    static auto feq(double a, double b)
    {
      return ( std::abs(a-b) < 128*1024*std::numeric_limits<double>::epsilon() );
    }
    
    template<class Basis>
    auto determinePeriodicDOFs(const Basis& basis) const
    {
      const auto&& condition = [&](const auto& intersection, const auto& element, const auto& localCoefficients, const int id)
      {
        const auto& g = intersection.geometry();
        const int n   = g.corners();
        for(int i = 0; i < n; ++i )
        {
          const auto&& corner = g.corner(i);
          if( !feq(abs(corner[0]),r[0]) && !feq(abs(corner[1]),r[1]) )
            return false;
        }
        return true;
      };


      Dune::BitSetVector<1> periodicDofs;
      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
      constructConditonalBoundaryDOFs(boundaryPatch, basis, periodicDofs, condition);
      return periodicDofs;
    }
    
    // This routine needs to be coded properly.
    template<class Basis>
    auto determineBuddies(const Basis& basis, Dune::BitSetVector<1> periodicDOFs) const
    {
      std::map<int,std::vector<int>> buddies;

      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);

      //       (corner,localId)  |->  vector of boundary indices with same corner x coordinate (or y coordinate) 
      //~ std::map<std::pair<double,size_t>,std::vector<int>> indexMapX, indexMapY;
      //       (coarsified corner coordinate,localId)  |->  vector of boundary indices with same corner x coordinate (or y coordinate) 
      std::map<std::pair<long int,size_t>,std::vector<int>> indexMapX, indexMapY;

      int nPre = 0;
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        const auto& g = it->inside().geometry();
        for(int i = 0; i < g.corners(); ++i)
          for(int j = 0; j < 2; ++j)
            if(std::abs(g.corner(i)[j]) > 1e-15)
              nPre = std::max( nPre, (int) std::ceil(std::log10(std::abs(g.corner(i)[j]))) );
      }
      const int nMult = 9 - nPre;
      const auto&& coarseCoord = [&nMult](const double& x)
      {
        long int coarse = x * std::pow(10,nMult);
        return coarse;
      };
      Debug::dprint("Buddy finding uses nMult =", nMult);
      
      // Iterate over all boundary interfaces.
      for(auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it)
      {
        // Get inside element and local coefficients.
        const auto& element = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(element).localCoefficients();

        // For each local basis function (identified by a local coefficient)...
        for(int i = 0; i < localCoefficients.size(); ++i)
        {
          // ... skip if the basis function is not periodic. (E.g. because it is not really on the boundary.)
          const auto idx = basis.index(element, i);
          if(periodicDOFs[idx].none())
            continue;
            
          // Skip of basis index is constrained.
          if(basis.isConstrained(idx))
            continue;

          // Extract geometric information.
          const auto entity = localCoefficients.localKey(i).subEntity();
          const auto codim  = localCoefficients.localKey(i).codim();
          const auto index  = localCoefficients.localKey(i).index();
          
          if( codim != dim )
            DUNE_THROW(Dune::Exception, "Only point-based finite elements are supported.");

          const auto&& subEntity  = element.template subEntity<dim>(entity);
          const auto&& corner = subEntity.geometry().corner(0);
          
          // Check if corner is on boundary and which periodic boundary it belongs to.
          if(feq(abs(corner[0]),r[0]))
            //~ indexMapX[std::make_pair(corner[1],index)].push_back(idx);
            indexMapX[std::make_pair(coarseCoord(corner[1]),index)].push_back(idx);
          if(feq(abs(corner[1]),r[1]))
            //~ indexMapY[std::make_pair(corner[0],index)].push_back(idx);
            indexMapY[std::make_pair(coarseCoord(corner[0]),index)].push_back(idx);
          
          // Avoid revisiting this basis function.
          periodicDOFs[idx] = false;
        }
      }
      
      
      for(const auto& ids : indexMapX)
      {
        if(ids.second.size() != 2){
          Debug::dprintVector(ids.second, ids.first.first, ids.first.second);
          #if DISABLE_FBUDDY_ERROR
            std::cerr << "XBuddy finding failed, got size " + std::to_string(ids.second.size()) << std::endl;
            buddies[ids.second[0]].push_back(ids.second[0]);
            //~ continue;
          #else
            std::cerr << std::setprecision(15);
            for(const auto& ids2 : indexMapX)
              if(ids2.first.second == ids.first.second)
                std::cerr << ids2.first.first << " ";
            std::cerr << std::endl;
            DUNE_THROW(Dune::Exception, "XBuddy finding failed, got size " + std::to_string(ids.second.size()) + " for " + std::to_string(ids.first.first) + ", " + std::to_string(ids.first.second));
          #endif
        }
        else
        {
          buddies[ids.second[0]].push_back(ids.second[1]);
          buddies[ids.second[1]].push_back(ids.second[0]);
        }
      }
      
      for(const auto& ids : indexMapY)
      {
        if(ids.second.size() != 2){
          Debug::dprintVector(ids.second, ids.first.first, ids.first.second);
          #if DISABLE_FBUDDY_ERROR
            std::cerr << "YBuddy finding failed, got size " + std::to_string(ids.second.size()) << std::endl;
            //~ continue;
            buddies[ids.second[0]].push_back(ids.second[0]);
          #else
            std::cerr << std::setprecision(15);
            for(const auto& ids2 : indexMapY)
              if(ids2.first.second == ids.first.second)
                std::cerr << ids2.first.first << " ";
            std::cerr << std::endl;
            DUNE_THROW(Dune::Exception, "YBuddy finding failed, got size " + std::to_string(ids.second.size()) + " for " + std::to_string(ids.first.first) + ", " + std::to_string(ids.first.second));
          #endif
        }
        else
        {
          buddies[ids.second[0]].push_back(ids.second[1]);
          buddies[ids.second[1]].push_back(ids.second[0]);
        }
      }
      
      // To-do: Need to join buddies. We may use that the network has at most depth 2. (Which may not even be a restriction if those iterators may be updated during iteration; that I do not know.)
      
      std::map<int,std::vector<int>> buddyUpdates;
      for(auto& rel : buddies)
        for(const auto& buddyId : rel.second)
          for(const auto& buddyId2 : buddies.at(buddyId))
            if(buddyId2 != rel.first
              && std::find(rel.second.begin(), rel.second.end(), buddyId2) == rel.second.end()
              && std::find(buddyUpdates[rel.first].begin(), buddyUpdates[rel.first].end(), buddyId2) == buddyUpdates[rel.first].end()
              )
                buddyUpdates[rel.first].push_back(buddyId2);
      for(const auto& rel : buddyUpdates)
        for(const auto& buddyId : rel.second)
          buddies.at(rel.first).push_back(buddyId);
      
      return buddies;
    }

    const ParameterizationFunction parameterizationFunction_;
          ParameterizedProjectionDistanceFunction parameterizedProjectionDistanceFunction_;
    const ParameterizedNormal parameterizedNormal_;
    const LevelSetFunction levelSetFunction_;
    const ConstraintFunction constraintFunction_;
};

#endif
