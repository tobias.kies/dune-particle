#ifndef DUNE_PLANAR_RECTANGULAR_SURFACE_HH
#define DUNE_PLANAR_RECTANGULAR_SURFACE_HH

#include <math.h>
#include <dune/particle/surface/boundingBox.hh>

#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>
#include <dune/particle/helper/functionshelper.hh>

#include <dune/common/bitsetvector.hh>
#include "dune/fufem/boundarypatch.hh"
#include <dune/fufem/functiontools/boundarydofs.hh>


// Describes a centered rectangle in 2D.
template<class float_x = double>
class PlanarRectangularSurface
{
  public:
  
    typedef Dune::FieldVector<float_x,1> Point1D;
    typedef Dune::FieldVector<float_x,2> Point2D;
    typedef Dune::FieldVector<float_x,3> Point3D;
    
    typedef typename FunctionsHelper::Polynomial<float_x,2> LevelSetFunction;
    typedef typename FunctionsHelper::Polynomial<float_x,2> ConstraintFunction;

    PlanarRectangularSurface(const double a = 1, const double b = std::numeric_limits<double>::quiet_NaN())
      : r({a, std::isnan(b) ? a : b }), levelSetFunction_(FunctionsHelper::constant(-1)), constraintFunction_(FunctionsHelper::constant(0))
    {
      if( !(r[0] >= 0 && r[1] >= 0) )
        DUNE_THROW( Dune::Exception, "Assertion r[0] > 0 && r[1] > 0 failed. Got r[0]=" + std::to_string(r[0]) + ", r[1]=" + std::to_string(r[1]) );
      
      auto mr = r;
      mr  *= -1;
      boundingBox_.setLower(mr);
      boundingBox_.setUpper(r);
      
      parameterizedProjectionDistanceFunction_.set_r(r);
    }
    
    const auto& boundingBox( ) const
    {
      return boundingBox_;
    }
    
    const auto& periodicBox( ) const
    {
      DUNE_THROW(Dune::Exception, "Periodicity is not supported here.");
      return boundingBox_;
    }
    
    auto parameterizedProjection(const Point3D& x) const
    {
      return parameterizedProjectionDistanceFunction_.evaluate(x).first;
    }
    
    auto parameterizedDistance(const Point3D& x) const
    {
      return parameterizedProjectionDistanceFunction_.evaluate(x).second;
    }

    const auto& levelSetFunction( ) const
    {
      return levelSetFunction_;
    }

    const auto& constraintFunction( ) const
    {
      return constraintFunction_;
    }
    
    const auto& parameterizationFunction( ) const
    {
      return parameterizationFunction_;
    }
    
    const auto& parameterizedNormal( ) const
    {
      return parameterizedNormal_;
    }
    
    const auto& parameterizedProjectionDistanceFunction( ) const
    {
      return parameterizedProjectionDistanceFunction_;
    }
    
    
    template<class Basis>
    const auto ignoreDOFs(const Basis& basis) const
    {
      // Boundary dofs are ignore dofs.
      Dune::BitSetVector<1> dofs(basis.size());
      
      BoundaryPatch<typename std::decay<decltype(basis.getGridView())>::type> boundaryPatch(basis.getGridView(), true);
      constructBoundaryDofs(boundaryPatch, basis, dofs);
      
      return dofs;
    }


    template<class Basis>
    void updateConstraints(Basis& basis) const
    {
      return;
    }
    
    auto minDistance() const
    {
      return -std::numeric_limits<double>::infinity();
    }

  private:
  
    const typename BoundingBox::Point r;
    BoundingBox boundingBox_;

    class ParameterizationFunction : public Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,Dune::FieldVector<size_t,2>>
    {
      public:
      
        typedef Dune::FieldVector<size_t,2> OrderType;
      
        typedef Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,OrderType> BaseType;
        
        typedef typename BaseType::RangeType RangeType;
        
        using BaseType::evaluate;
        
        RangeType evaluate(const Point2D& x, const OrderType& d) const
        {
          Point3D res;
          res = 0;
          const auto order = d[0] + d[1];
          
          switch(order)
          {
            case 0:
              res[0]  = x[0];
              res[1]  = x[1];
              break;
              
            case 1:
              if(d[0] == 1)
                res[0]  = 1;
              else
                res[1]  = 1;
              break;
              
            default:
              DUNE_THROW(Dune::NotImplemented, "Not implemented.");
              break;
          }
          
          return res;
        }
    };

    class ParameterizedProjectionDistanceFunction
      : public Dune::VirtualMultiDifferentiableFunction<Point3D,std::pair<Point2D,Point1D>,Dune::FieldVector<size_t,3>>
    {
      public:
      
        typedef Point3D DomainType;
        typedef std::pair<Point2D,Point1D> RangeType;
        //~ typedef std::pair<Dune::FieldVector<size_t,2>,Dune::FieldVector<size_t,1>> OrderType;
        typedef Dune::FieldVector<size_t,3> OrderType;
      
        typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;
        
        using BaseType::evaluate;
        
        typename BoundingBox::Point r;
        
        void set_r(const typename BoundingBox::Point& r_)
        {
          r = r_;
        }
        
        RangeType evaluate(const DomainType& x, const OrderType& d) const
        {
          RangeType res;
          const auto order = d[0] + d[1] + d[2];
          
          switch(order)
          {
            case 0:
              for(int i = 0; i < DomainType::dimension; ++i)
                if( x[i] < -r[i] && x[i] > r[i] )
                  DUNE_THROW(Dune::Exception, "Point outside feasible range.");
                else
                  res.first[i]  = x[i];
              res.second  = x[2];
              break;
              
            case 1:
              if(d[0] == 1)
                res.first[0]  = 1;
              else if(d[1] == 1)
                res.first[1]  = 1;
              else if(d[2] == 1)
                res.second  = 1;
              break;
              
            default:
              // Do nothing: Higher derivatives are always zero.
              break;
          }
          
          return res;
        }
    };
    
    
    class ParameterizedNormal : public Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,Dune::FieldVector<size_t,2>>
    {
      public:
        
        typedef Dune::FieldVector<size_t,2> OrderType;
      
        typedef Dune::VirtualMultiDifferentiableFunction<Point2D,Point3D,OrderType> BaseType;
        
        typedef typename BaseType::RangeType RangeType;

        using BaseType::evaluate;
        
        RangeType evaluate(const Point2D& x, const OrderType& d) const
        {
          Point3D res(0);
          const auto order = d[0] + d[1];
          
          switch(order)
          {
            case 0:
              res[2]  = 1;
              break;
              
            default:
              // Do nothing -- higher derivative of normal is always zero.
              break;
          }
          
          return res;
        }
    };
    
    const ParameterizationFunction parameterizationFunction_;
          ParameterizedProjectionDistanceFunction parameterizedProjectionDistanceFunction_;
    const ParameterizedNormal parameterizedNormal_;
    const LevelSetFunction levelSetFunction_;
    const ConstraintFunction constraintFunction_;
};

#endif
