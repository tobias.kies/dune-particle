#ifndef DUNE_MEMBRANE_GRID_HH
#define DUNE_MEMBRANE_GRID_HH

#include <dune/grid/uggrid.hh>
#include <dune/grid/utility/structuredgridfactory.hh>
#include <dune/grid/utility/tensorgridfactory.hh>

#include <dune/particle/particle/particleTypes.hh>

// Functions from MembraneGrid are responsible for creating grids for
// given problems. Grids are determined by the concrete problem as well
// as a desired reference width.
// We always use UG-grid.
class MembraneGrid
{
  public:
    static const size_t dim = 2;
    typedef Dune::UGGrid<dim> Grid;
      
    // For point value constraints: Create a point-fitted grid.
    // ATTENTION: The algorithm below is not a very good solution because
    // the resulting grids become arbitrarily fine as two points come
    // arbitrarily close to each other.
    // Even worse: It is enough if only two coordinates come arbitrarily
    // close.
    template<class MembraneParticleProblem>
    static auto makeGridContainingPoints( const MembraneParticleProblem& mpp, const double& maxWidth )
    {    
      DUNE_THROW(Dune::Exception, "Deprecated.");
      // Some (rather arbitrary) configuration parameter: Degeneracy limit.
      const double maxDegeneracy  = 5;

      // Get bounding box.
      const auto& boundingBox = mpp.boundingBox();
      
      // Extract a list of transformed points.
      const auto& points      = mpp.transformedParameterizedPoints().first;
      const auto& lower       = boundingBox.lower();
      const auto& upper       = boundingBox.upper();
      
      // Set up a first grid (described by two vectors).
      const int n = points.size();
      std::array<std::vector<double>,dim> X;
      for( int d = 0; d < dim; ++ d )
      {
        X[d].resize(n+2);
        X[d][0] = lower[d];
        for( int i = 0; i < n; ++i )
          X[d][i+1] = points[i][d];
        X[d][n+1] = upper[d];
        std::sort(X[d].begin(), X[d].end());
        auto last = std::unique( X[d].begin(), X[d].end(),
          [](const double a, const double b){ return ( std::abs(a-b) < std::numeric_limits<double>::epsilon()*1024*1024 ); }
        );
        X[d].erase(last, X[d].end());
      }
      
      // Refine all elements that are too large.
      refineOversizedElements(X, maxWidth);
      
      // Refine all elements that are too deformed.
      refineDegenerateElements(X, maxDegeneracy);
      
      // Create and return the actual grid.
      Dune::TensorGridFactory<Grid> factory;
      for( int d = 0; d < dim; ++d )
      {
        factory.setStart(d, lower[d]);
        factory[d].reserve(n+2);
        for( int i = 1; i < X[d].size(); ++i )
          factory[d].push_back(X[d][i]);

        auto last = std::unique( factory[d].begin(), factory[d].end(),
          [](const double a, const double b){ return ( std::abs(a-b) < std::numeric_limits<double>::epsilon()*1024*1024 ); }
        );
        factory[d].erase(last, factory[d].end());
          
        Debug::dprintVector(factory[d], "X", d);
      }
      
      auto gridPtr = factory.createGrid();
      gridPtr->setClosureType(Grid::ClosureType::NONE);
      gridPtr->setRefinementType(Grid::RefinementType::COPY);
      
      return gridPtr;
    }
    
    
    // For curve constraints: Just create a uniform grid.
    // To-do: Rewrite this piece such that it allows to use a hierarchical
    // structure.
    template<class MembraneParticleProblem>
    static auto makeGrid( const MembraneParticleProblem& mpp, const double& maxWidth )
    {
      // Get bounding box.
      const auto& boundingBox = mpp.boundingBox();
      const auto& lower       = boundingBox.lower();
      const auto& upper       = boundingBox.upper();
      
      // Determine necessary number of elements.
      std::array<unsigned int,dim> arrElements;
      for( int i = 0; i < dim; ++i )
        arrElements[i]  = std::ceil( (upper[i]-lower[i])/maxWidth );
      Debug::dprintVector(arrElements, "nElementsPerAxis given maxWidth", maxWidth);
      
      // Create and return the grid.
      auto gridPtr = Dune::StructuredGridFactory<Grid>::createCubeGrid(lower, upper, arrElements);
      gridPtr->setClosureType(Grid::ClosureType::NONE);
      gridPtr->setRefinementType(Grid::RefinementType::COPY);

      return gridPtr;
    }

    template<class MembraneParticleProblem>
    static auto makeGrid( const MembraneParticleProblem& mpp, const std::array<unsigned int,dim>& arrElements )
    {
      // Get bounding box.
      const auto& boundingBox = mpp.boundingBox();
      const auto& lower       = boundingBox.lower();
      const auto& upper       = boundingBox.upper();
      
      Debug::dprintVector(arrElements, "nElementsPerAxis");
      
      // Create and return the grid.
      auto gridPtr = Dune::StructuredGridFactory<Grid>::createCubeGrid(lower, upper, arrElements);
      gridPtr->setClosureType(Grid::ClosureType::NONE);
      gridPtr->setRefinementType(Grid::RefinementType::COPY);

      return gridPtr;
    }
  
  
  private:
    // Function to refine a tensor grid until its elements are small enough.
    static void refineOversizedElements(std::array<std::vector<double>,dim>& X, const double maxWidth)
    {
      for( int d = 0; d < dim; ++d )
      {
        auto& Xd = X[d];
        for( int i = 0; i < Xd.size()-1; ++i )
        {
          if( Xd[i+1]-Xd[i] > maxWidth )
          {
            Xd.insert(Xd.begin()+(i+1), 0.5*(Xd[i]+Xd[i+1]));
            --i;
          }
        }
      }
    }
    
    
    // Function to refine a tensor grid until its elements are no longer too degenerate.
    // Probably could be generalized to higher dimensions by the use of multi-indices.
    static void refineDegenerateElements(std::array<std::vector<double>,dim>& X, const double maxDegeneracy)
    {
      static_assert(dim == 2);
      if(maxDegeneracy < 2)
        DUNE_THROW( Dune::Exception, "Degeneracy parameter must be at least 2." );
        
      const int nMaxIterations = 1e3;
      bool flag = true;
      for( int n = 0; n < nMaxIterations && flag; ++n )
      {
        flag = false;
        
        for( int i = 0; i < X[0].size()-1; ++i )
        {
          for( int j = 0; j < X[1].size()-1; ++j )
          {
            const double lx = X[0][i+1]-X[0][i];
            const double ly = X[1][j+1]-X[1][j];
            const double ratio = lx/ly;
            
            if( ratio > maxDegeneracy )
            {
              flag  = true;
              X[0].insert(X[0].begin()+(i+1), 0.5*(X[0][i]+X[0][i+1]));
              --i;
            }
            else if( 1./ratio > maxDegeneracy )
            {
              flag = true;
              X[1].insert(X[1].begin()+(j+1), 0.5*(X[1][j]+X[1][j+1]));
              --j;
            }
          }
        }
        
        
        if( n == nMaxIterations )
          DUNE_THROW( Dune::Exception, "Refinement algorithm exceeded maximum number of iterations." );
      }
    }
};

#endif
