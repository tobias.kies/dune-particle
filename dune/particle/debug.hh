#ifndef DUNE_DEBUG_METHODS_HH
#define DUNE_DEBUG_METHODS_HH

#define MAX_WRITE_SIZE 5000

#include <fstream>
#include <iomanip>

class Debug
{
  
  public:
  
    static std::string timeNowStr()
    {
      time_t rawtime;
      struct tm * timeinfo;
      char buffer[80];

      time (&rawtime);
      timeinfo = localtime(&rawtime);

      strftime(buffer,80,"%Y-%m-%d %H:%M:%S",timeinfo);
      std::string str(buffer);

      return str;
    }
      
    static void die( const std::string& msg = "Die." )
    {
      DUNE_THROW( Dune::Exception, msg );
    }

    static void dprint()
    {
      std::cerr << "[" << timeNowStr() << "] " << std::endl;
    }

    template<class ... Args>
    static void dprint( const Args& ... args )
    {
      std::cerr << "[" << timeNowStr() << "] ";
      dbgPrint(args...);
    }

    template<class DenseMatrix, class ... Args>
    static void dprintDMatrix( const DenseMatrix& A, const Args& ... args )
    {
      dprint( args..., "(size", A.N(), "by", A.M(), "matrix)" );
      for( size_t i = 0; i < A.N(); ++i )
      {
        for( size_t j = 0; j < A.M(); ++j )
          std::cerr << A[i][j] << " ";
        std::cerr << std::endl;
      }
      std::cerr << std::endl;
    }

    template<class SparseMatrix, class ... Args>
    static void dprintSMatrix( const SparseMatrix& A, const Args& ... args )
    {
      dprint( args..., "(size", A.N(), "by", A.M(), "matrix)" );
      for( size_t i = 0; i < A.N(); ++i )
      {
        for( size_t j = 0; j < A.M(); ++j )
          if( A.exists(i,j) )
            std::cerr << A[i][j] << " ";
          else
            std::cerr << (typename SparseMatrix::field_type) 0 << " ";
        std::cerr << std::endl;
      }
      std::cerr << std::endl;
    }

    template<class SparseMatrix, class ... Args>
    static void dprintSMatrixCompact( const SparseMatrix& A, const Args& ... args )
    {
      dprint( args..., "(size", A.N(), "by", A.M(), "matrix)" );
      for( size_t i = 0; i < A.N(); ++i )
      {
        for( size_t j = 0; j < A.M(); ++j )
          if( A.exists(i,j) )
            std::cerr << A[i][j] << " ";
      }
      std::cerr << std::endl;
    }

    template<class SparseMatrix>
    static void dfprintSMatrix( const std::string& filename, const SparseMatrix& A )
    {
      if( A.N() > MAX_WRITE_SIZE || A.M() > MAX_WRITE_SIZE )
      {
        dprint( "NOT writing matrix to file", filename, "(size", A.N(), "by", A.M(), "matrix)" );
        return;
      }
      
      dprint( "Write matrix to file", filename, "(size", A.N(), "by", A.M(), "matrix)" );
      
      std::ofstream hFile;
      hFile.open(filename);
      
      for( size_t i = 0; i < A.N(); ++i )
      {
        for( size_t j = 0; j < A.M(); ++j )
          if( A.exists(i,j) )
            hFile << std::setprecision(15) << A[i][j] << " ";
          else
            hFile << (typename SparseMatrix::field_type) 0 << " ";
        hFile << std::endl;
      }
      hFile << std::endl;
      
      hFile.close();
    }

    template<class Vector>
    static void dfprintVector( const std::string& filename, const Vector& v )
    {
      if( v.size() > MAX_WRITE_SIZE )
      {
        dprint( "NOT writing vector to file", filename, "(size", v.size(), "vector)" );
        return;
      }
      dprint( "Writing vector to file", filename, "(size", v.size(), "vector)" );
      std::ofstream hFile;
      hFile.open(filename);
      
      for( size_t i = 0; i < v.size(); ++i )
        hFile << std::setprecision(15) << v[i] << " ";
      
      hFile.close();
    }

    template<class Vector>
    static void dfprintVectorAny( const std::string& filename, const Vector& v )
    {
      if( v.size() > MAX_WRITE_SIZE )
      {
        dprint( "NOT writing vector to file", filename, "(size", v.size(), "vector)" );
        return;
      }
      dprint( "Writing vector to file", filename, "(size", v.size(), "vector)" );
      std::ofstream hFile;
      hFile.open(filename);
      
      for( size_t i = 0; i < v.size(); ++i )
        hFile << v[i].any() << " ";
      
      hFile.close();
    }

    template<class V, class ... Args>
    static void dprintVector( const V& v, const Args& ... args )
    {
      dprint( args..., "(size", v.size(), "vector)" );
      for( const auto& vi : v )
        std::cerr << vi << " ";
      std::cerr << std::endl;
    }

    template<class V, class ... Args>
    static void dprintColumnVector( const V& v, const Args& ... args )
    {
      dprint( args..., "(size", v.size(), "vector)" );
      for( const auto& vi : v )
        std::cerr << vi << std::endl;
      std::cerr << std::endl;
    }

    template<class V, class ... Args>
    static void dprintVectorAny( const V& v, const Args& ... args )
    {
      dprint( args..., "(size", v.size(), "vector)" );
      for( size_t i = 0; i < v.size(); ++i )
        std::cerr << v[i].any() << " ";
      std::cerr << std::endl;
    }

    static void dbgPrint( )
    {
    }

    template<class T>
    static void dbgPrint( const T& t )
    {
      std::cerr << t << std::endl;
    }

    template<class T, class ... Args>
    static void dbgPrint( const T& t, const Args& ... args)
    {
      std::cerr << t << " ";
      dbgPrint(args...);
    }
    
    // Tells (virtual) memory consumption in kB. From http://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
    static int memory( )
    {
        FILE* file = fopen("/proc/self/status", "r");
        int result = -1;
        char line[128];

        while (fgets(line, 128, file) != NULL){
            if (strncmp(line, "VmSize:", 7) == 0){
                int i = strlen(line);
                const char* p = line;
                while (*p <'0' || *p > '9') p++;
                line[i-3] = '\0';
                i = atoi(p);
                result = i;
                break;
            }
        }
        fclose(file);
        return result;
    }
};

#endif
