#ifndef DUNE_CONSTRAINTS_MACHINE
#define DUNE_CONSTRAINTS_MACHINE

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixmatrix.hh>
#include <dune/particle/basis/point/pointnodalbasis.hh>
#include <dune/particle/assembler/pointevaluationassembler.hh>

namespace BlockStructureMethods
{
  // Here we export some typedefs related to block indexing.
  typedef std::vector<size_t> Indices;
  typedef std::pair<Indices,Indices> CartesianProduct;
  typedef std::vector<CartesianProduct> Blocks;

  // This method determines if two containers have an overlap.
  // It is from
  //  https://stackoverflow.com/questions/29419922/fastest-way-to-find-out-if-two-sets-overlap
  template<class Lhs, class Rhs>
  bool miscHasOverlap(Lhs const& lhs, Rhs const& rhs)
  {
    if (lhs.empty() || rhs.empty()) return false;
    if (lhs.size() * 100 < rhs.size()) {
      for (auto&& x:lhs)
        if (rhs.find(x)!=rhs.end())
          return true;
      return false;
    }
    if (rhs.size() * 100 < lhs.size()) {
      for(auto&& x:rhs)
        if (lhs.find(x)!=lhs.end())
          return true;
      return false;
    }

    using std::begin; using std::end;

    auto lit = begin(lhs);
    auto lend = end(lhs);

    auto rit = begin(rhs);
    auto rend = end(rhs);

    while( lit != lend && rit != rend ) {
      if (*lit < *rit) {
        lit = lhs.lower_bound(*rit);
        continue;
      }
      if (*rit < *lit) {
        rit = rhs.lower_bound(*lit);
        continue;
      }
      return true;
    }
    return false;
  }


  // This method detects a block structure within a sparse matrix A.
  // The algorithm used in here certainly is not the most efficient.
  // But it is simple, which is nice too.
  template<class SparseMatrix>
  auto detectBlockStructure(SparseMatrix& A)
  {
    typedef std::set<size_t> SetIndices;
    typedef std::pair<SetIndices,SetIndices> SetCartesianProduct;
    typedef std::list<SetCartesianProduct> SetBlocks;

    SetBlocks blocks;

    // First step: Determine basic index sets for each row of A.
    for( size_t rowId = 0; rowId < A.N(); ++rowId )
    {
      SetIndices colIndices;
      for( auto it = A[rowId].begin(); it != A[rowId].end(); ++it )
        colIndices.insert( it.index() ); // inefficient because we always insert at the end?

      SetIndices rowIndices;
      rowIndices.insert(rowId);

      if( colIndices.size() > 0 )
        blocks.push_back( std::make_pair( rowIndices, colIndices ) );
    }

    // Second step: Merge indices that have overlap.
    for( auto it = blocks.begin(); it != blocks.end(); ++it )
    {
      bool  fMerged = true;
      while( fMerged )
      {
        fMerged = false;
        auto it2 = it; ++it2;
        for( ; it2 != blocks.end(); )
        {
          // If has overlap: Merge the lists and delete the second one.
          if( miscHasOverlap( it->second, it2->second ) )
          {
            fMerged = true;
            it->first .insert( it2->first .begin(), it2->first .end() );
            it->second.insert( it2->second.begin(), it2->second.end() );
            it2 = blocks.erase(it2);
          }
          // else increment as usual.
          else
          {
            ++it2;
          }
        }
      }
    }

    // Convert the blocks to a vector-based structure.
    Blocks vectorBlocks;
    for( const auto& block : blocks )
    {
      const auto& rowIndices = block.first;
      const auto& colIndices = block.second;

      Indices vectorRowIndices;
      Indices vectorColIndices;

      vectorRowIndices.reserve(rowIndices.size());
      vectorColIndices.reserve(colIndices.size());

      for( const auto& rowId : rowIndices )
        vectorRowIndices.push_back(rowId);

      for( const auto& colId : colIndices )
        vectorColIndices.push_back(colId);

      vectorBlocks.push_back( std::make_pair( vectorRowIndices, vectorColIndices ) );
    }

    // Finally: Return the result.
    return vectorBlocks;
  }
  
  
  // Extract a block from a sparse matrix and return the block as dense
  // matrix.
  template<class SparseMatrix>
  auto extractBlock(const SparseMatrix& A, const CartesianProduct& block)
  {
    typedef Dune::Matrix<Dune::FieldMatrix<double,1,1>> DenseMatrix;
    const auto n = block.first.size();
    const auto m = block.second.size();
    
    DenseMatrix B(n,m);
    for(int i = 0; i < n; ++i)
      for(int j = 0; j < m; ++j)
        if(A.exists(block.first[i],block.second[j]))
          B[i][j] = A[block.first[i]][block.second[j]];
    return B;
  }
  
  
  // Create a full (sparse) quadratic matrix from a given set of blocks.
  // Specialty here: Blocks that are not included are set to the identity.
  // You see, that this function is rather specialized to the constraints class below.
  template<class Blocks>
  auto createBlockMatrix(const int n, const Blocks& blocks)
  {
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>> SparseMatrix;
    SparseMatrix A;
    
    // Collect all indices, including the diagonal.
    Dune::MatrixIndexSet indices(n,n);

    for(int i = 0; i < n; ++i)
      indices.add(i,i);

    for(int blockId = 0; blockId < blocks.size(); ++blockId)
    {
      const auto& blockIndices = blocks[blockId].first;
      const auto& rowIndices = blockIndices.first;
      const auto& colIndices = blockIndices.first;
      
      for(int i = 0; i < rowIndices.size(); ++i)
        for(int j = 0; j < colIndices.size(); ++j)
          indices.add(rowIndices[i],colIndices[j]);
    }


    // Export indices and set identity part.  
    indices.exportIdx(A);
    for(int i = 0; i < n; ++i)
      A[i][i] = 1;


    // Populate A with the block matrices as well.
    for(int blockId = 0; blockId < blocks.size(); ++blockId)
    {
      const auto& blockIndices = blocks[blockId].first;
      const auto& rowIndices = blockIndices.first;
      const auto& colIndices = blockIndices.first;
      const auto& blockMatrix = blocks[blockId].second;
      
      for(int i = 0; i < rowIndices.size(); ++i)
        for(int j = 0; j < colIndices.size(); ++j)
          A[rowIndices[i]][colIndices[j]] = blockMatrix[i][j];
    }
    
    return A;
  }
};


namespace QRMethods
{
  typedef Dune::Matrix<Dune::FieldMatrix<double,1,1>> DenseMatrix;
  typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;
  struct QRPair
  {
    DenseMatrix Q;
    DenseMatrix R;
  };
  
  QRPair qrDecomposition(const DenseMatrix& A)
  {
    
    QRPair QR;
    auto& Q = QR.Q;
    auto& R = QR.R;
    const auto n = A.N();
    const auto m = A.M();
    assert(m<=n);
    
    // Initialize Q and R
    Q.setSize(n,n);
    for(int i = 0; i < n; ++i)
      Q[i][i] = 1;
    R = A;
    
    // Lambdas used during QR method.
    const auto&& alpha = [&R](const int colId)
    {
      double a = 0;
      for(int i = colId; i < R.N(); ++i)
        a += R[i][colId]*R[i][colId];
      return sqrt(a);
    };
    
    const auto&& houseHolderVector = [&R](const int colId, const double alpha)
    {
      // Note: The equality checks here could also be replaced by the significantly more stable abs(.)<c*eps statements.
      // Here their purpose is just to make sure that the program does not crash terribly.
      Vector v(R.N(),0);
      
      if(alpha == 0)
      {
        // In that case we could just set v=0 and work with the identity matrix for this iteration.
        // But it should really not happen here, so we throw an error for good measure.
        // (Again, because of the equality check this will not ever happen anyway, until it is replaced by a "suitable" check.)
        DUNE_THROW(Dune::Exception, "QR input A definitely has invalid rank.");
      }

      if(R[colId][colId] == -alpha) // In that case the column is already a unit vector.
      {
        v[colId]  = 1;
      }
      else
      {
        for(int i = colId; i < R.N(); ++i)
          v[i]  = R[i][colId];
        v[colId]  += alpha;
        v /= v.two_norm();
      }
      
      return v;
    };
    
    const auto&& updateR = [&R](const int colId, const double alpha, const Vector& v)
    {
      // Set first column to multiple of unit vector.
      R[colId][colId] = -alpha;
      for(int i = colId+1; i < R.N(); ++i)
        R[i][colId] = 0;
        
      // Done if this was the last column.
      if(colId+1 == R.M())
        return;
        
      // Update remaining rectangular part to R-2*v*v^T*R
      // First: Compute v^T*R (1-by-n_ times n_-by-m_)
      Vector vTR(R.M()-(colId+1),0);
      for(int j = 0; j < vTR.size(); ++j)
        for(int i = colId; i < R.N(); ++i)
          vTR[j]  += v[i]*R[i][colId+j+1];
          
      // Second: Subtract 2*v*vTR from R.
      for(int i = colId; i < R.N(); ++i)
        for(int j = 0; j < vTR.size(); ++j)
          R[i][colId+j+1] -= 2 * v[i] * vTR[j];
          
      return;
    };
    
    const auto&& updateQ = [&Q](const int colId, const Vector& v)
    {
      // Based on the formula Q <- Q*(I-2*v*v^T) = Q - 2 * Qv * v^T
      
      // First: Compute Qv.
      Vector Qv(Q.N(),0);
      for(int i = 0; i < Q.N(); ++i)
        for(int j = colId; j < Q.M(); ++j)
          Qv[i] += Q[i][j] * v[j];
          
      // Second: perform the update.
      for(int i = 0; i < Q.N(); ++i)
        for(int j = colId; j < Q.M(); ++j)
          Q[i][j] -= 2*Qv[i]*v[j];
          
      return;
    };

    // Actual QR (Householder) method: Iterate over columns.
    for(int k = 0; k < m; ++k)
    {
      // Compute alpha.
      const auto a = alpha(k);
      
      // Compute v.
      const auto&& v = houseHolderVector(k, a);
      
      // Update A (which means R).
      updateR(k, a, v);
      
      // Update Q.
      updateQ(k, v);
    }

    return QR;
  }
};


class ConstraintsMachine
{
  public:

    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>> SparseMatrix;
    typedef Dune::Matrix<Dune::FieldMatrix<double,1,1>> DenseMatrix;
    typedef std::vector<typename QRMethods::QRPair> QRBlocks;
    typedef std::vector<DenseMatrix> RInvBlocks;
    typedef std::vector<std::pair<typename BlockStructureMethods::CartesianProduct,DenseMatrix>> DBlocks;
  
    template<class Basis, class Surface, class ConstraintPoints>
    static void updateConstraints(Basis& basis, const Surface& surface, const ConstraintPoints& points)
    {
      // First incorporate the surface constraints.
      surface.updateConstraints(basis);
      
      // Next assemble the evaluation matrix and transpose it.
      const auto&& E  = assembleEvaluationMatrix(basis, points);
      const auto&& Et = transpose(E);
      
      // Extract blocks. This entails that we get index sets which map
      // "local" to global DOFs.
      auto&& Et_blocks = BlockStructureMethods::detectBlockStructure(Et);
      
      // Apply a QR decomposition to each E_i^T.
      // In future: The QR blocks may use a permutation of the initial
      // basis ordering in order to have a basis "closer to the identity".
      // Hence, that information might be added at some point.
      auto&& QR_blocks = computeQRBlocks(Et, Et_blocks);
      
      // Invert the regular block in R.
      auto&& Rinv_blocks = computeRInverses(QR_blocks);
      
      // Combine Q and the inverse of R to a new basis representation.
      // The result must carry both the actual local matrix and the
      // degrees of freedom in order to allow reconstruction fo the full
      // block matrix.
      const auto&& D_blocks = formulateBasisBlocks(Et_blocks, QR_blocks, Rinv_blocks);
      const auto&& D = BlockStructureMethods::createBlockMatrix(basis.size(), D_blocks);
      
      // Possible test: It should hold "E*D = (I, 0)".
      //~ TODO
      
      // Extract constraint matrix C from basis.
      const auto&& C = extractConstraintMatrix(basis);
      
      // Compute new constraint matrix C_ = C*D
      SparseMatrix C_;
      Dune::matMultMat(C_, C, D);
      
      /*/// DEBUG --- works only if DOFs do not overlap.
      auto& dofConstraints = basis.dofConstraints();
      auto& interpolation = dofConstraints.interpolation();
      auto& isConstrained = dofConstraints.isConstrained();
      const auto& blocks = D_blocks;
      for(int blockId = 0; blockId < blocks.size(); ++blockId)
      {
        const auto& blockIndices = blocks[blockId].first;
        const auto& rowIndices = blockIndices.first;
        const auto& colIndices = blockIndices.first;
        const auto& blockMatrix = blocks[blockId].second;
        
        for(int i = 0; i < rowIndices.size(); ++i)
        {
          if(isConstrained[rowIndices[i]] == true)
            DUNE_THROW(Dune::Exception, "Debugging alternative in the point constraints implementation does not work here.");
          isConstrained[rowIndices[i]] = true;
          for(int j = 0; j < colIndices.size(); ++j)
            interpolation[rowIndices[i]].push_back(DOFConstraints::LinearFactor(colIndices[j],blockMatrix[i][j]));
        }
      }
      return;
      ///*/
      
      
      // Incorporate new constraint matrix into basis.
      setConstraintMatrix(basis, C_);

      return;
    }
    
    
    // Yes, this is not the most efficient way to go.
    // Do I care after midnight? -- No.
    template<class Basis, class ConstraintPoints>
    static auto mapPointsToDOFs(const Basis& basis, const ConstraintPoints& points)
    {
      const auto&& E  = assembleEvaluationMatrix(basis, points);
      
      std::map<int,int> map;
      for(int i = 0; i < E.N(); ++i)
      {
        int idx = -1;
        for(auto it = E[i].begin(); it != E[i].end(); ++it)
        {
          if(std::abs(*it-1) < std::numeric_limits<double>::epsilon()*1024*1024)
          {
            idx = it.index();
            break;
          }
        }
        
        if(idx == -1)
        {
          for(auto it = E[i].begin(); it != E[i].end(); ++it)
            std::cerr << "(i," << it.index() << ") -> " << *it << std::endl;
          DUNE_THROW(Dune::Exception, "Could not map point to dof.");
        }
          
        map[i]  = idx;
      }
      
      return map;
    }
    
  
  private:
  
    template<class Basis, class ConstraintPoints>
    static auto assembleEvaluationMatrix(const Basis& basis, const ConstraintPoints& points)
    {
      // Extract some relevant quantities.
      typedef typename Basis::GridView GridView;
      typedef typename GridView::Grid Grid;
      const auto& gridView = basis.getGridView();
      
      // Set up point basis.
      typedef PointNodalBasis<GridView,typename ConstraintPoints::value_type> PointBasis;
      PointBasis pointBasis(gridView, points);
      
      // Set up assembler.
      typedef typename PointBasis::LocalFiniteElement TrialLFE;
      typedef typename Basis::LocalFiniteElement AnsatzLFE;
      Assembler<PointBasis,Basis> assembler(pointBasis, basis);
      PointEvaluationAssembler<Grid,TrialLFE,AnsatzLFE> pointEvaluationAssembler;
      
      // Do actual assembling and return the result.
      SparseMatrix E;
      assembler.assembleOperator(pointEvaluationAssembler, E);
      return E;
    }

    
    static auto transpose(const SparseMatrix& A)
    {
      SparseMatrix B;
      
      // Compute index set.
      Dune::MatrixIndexSet index(A.M(),A.N());
      for(int i = 0; i < A.N(); ++i)
        for(auto it = A[i].begin(); it != A[i].end(); ++it)
          index.add(it.index(),i);
      
      // Populate matrix.
      index.exportIdx(B);
      for(int i = 0; i < A.N(); ++i)
        for(auto it = A[i].begin(); it != A[i].end(); ++it)
          B[it.index()][i]  = *it;
      
      // Return.
      return B;
    }
    

    static auto computeQRBlocks(const SparseMatrix& A, const typename BlockStructureMethods::Blocks& blocks)
    {
      QRBlocks qrBlocks;
      
      // Iterate over all blocks.
      for(const auto& block : blocks)
      {
        // Extract block.
        const auto&& M = BlockStructureMethods::extractBlock(A,block);
        
        // Apply QR method to the transpose.
        qrBlocks.push_back(QRMethods::qrDecomposition(M));
      }
      
      return qrBlocks;
    }
    
    
    static auto computeRInverses(const QRBlocks& QR_blocks)
    {
      RInvBlocks RInv_blocks;

      for(const auto& QR_block : QR_blocks)
      {
        const auto& R = QR_block.R;
        assert(R.N() >= R.M());
        const auto n = R.M();
        
        // Invert R: Just do a couple of back-solves.
        // Do not care too much about efficiency.
        DenseMatrix S(n,n);
        for(int j = 0; j < n; ++j)
        {
          for(int i = n-1; i >= 0; --i)
          {
            S[i][j] = (i==j);
            for(int k = i+1; k < n; ++k)
              S[i][j] -= R[i][k]*S[k][j];
            S[i][j] /= R[i][i];
          }
        }          

        RInv_blocks.push_back(S);
      }
      
      return RInv_blocks;
    }
    
    
    static auto formulateBasisBlocks(const typename BlockStructureMethods::Blocks& id_blocks, const QRBlocks& QR_blocks, const RInvBlocks& RInv_blocks)
    {
      DBlocks D_blocks;
      
      for(int blockId = 0; blockId < QR_blocks.size(); ++blockId)
      {
        auto D = QR_blocks[blockId].Q;
        const auto& S = RInv_blocks[blockId];
        const auto n = S.N();
        
        // Update first n columns of D by multiplying them with S^T from the right.
        for(int j = 0; j < n; ++j)
        {
          for(int i = 0; i < D.N(); ++i)
          {
            double Dij = 0;
            for(int k = j; k < n; ++k)
              Dij += D[i][k] * S[j][k];
            D[i][j] = Dij;
          }
        }
        
        // Also extract the corresponding DOF ids.
        const auto& id_block = id_blocks[blockId];
        typename BlockStructureMethods::CartesianProduct blockIds = std::make_pair(id_block.first,id_block.first);
        
        D_blocks.push_back(std::make_pair(blockIds,D));
      }
      
      return D_blocks;
    }
    
    
    template<class Basis>
    static auto extractConstraintMatrix(const Basis& basis)
    {
      const auto n = basis.size();
      SparseMatrix C;
      
      // Collect indices.
      Dune::MatrixIndexSet indices(n,n);
      for(int i = 0; i < n; ++i)
      {
        if(!basis.isConstrained(i))
        {
          indices.add(i,i);
          continue;
        }
          
        const auto& constraints = basis.constraints(i);
        for(int j = 0; j < constraints.size(); ++j)
          indices.add(i,constraints[j].index);
      }
      
      // Set up C.
      indices.exportIdx(C);
      for(int i = 0; i < n; ++i)
      {
        if(!basis.isConstrained(i))
        {
          C[i][i] = 1;
          continue;
        }
          
        const auto& constraints = basis.constraints(i);
        for(int j = 0; j < constraints.size(); ++j)
          C[i][constraints[j].index] = constraints[j].factor;
      }
      
      return C;
    }
    
    
    template<class Basis>
    static void setConstraintMatrix(Basis& basis, const SparseMatrix& C)
    {
      const auto n = basis.size();
      auto& dofConstraints = basis.dofConstraints();
      auto& interpolation = dofConstraints.interpolation();
      auto& isConstrained = dofConstraints.isConstrained();
      
      const auto eps = std::numeric_limits<double>::epsilon()*1024;
      
      // Reset basis constraints.
      for(int i = 0; i < n; ++i)
        isConstrained[i] = false;
      interpolation.clear();
      interpolation.resize(n);

      // Set new basis constrains.
      for(int i = 0; i < n; ++i)
      {
        bool fConstrained = false;
        for(auto it = C[i].begin(); it != C[i].end(); ++it)
        {
          if( (it.index() != i && std::abs(*it) > eps
            || std::abs(*it-1) > eps) )
          {
            fConstrained = true;
            break;
          }
        }
        if(!fConstrained)
          continue;

        isConstrained[i]  = true;
        auto& linearCombination = interpolation[i];
        for(auto it = C[i].begin(); it != C[i].end(); ++it)
          if(std::abs(*it) > eps)
            linearCombination.push_back(DOFConstraints::LinearFactor(it.index(),*it));
      }
    }
};

#endif
