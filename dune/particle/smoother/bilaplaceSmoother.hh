#ifndef DUNE_BILAPLACE_SMOOTHER_HH
#define DUNE_BILAPLACE_SMOOTHER_HH

#include <dune/particle/assembler/bilaplaceAssembler.hh>
#include <dune/particle/helper/misc.hh>
#include <dune/istl/superlu.hh>

// Make a function smoother by solving a Dirichlet Bi-Laplace problem.

template<class Basis, class T = Dune::FieldMatrix<double,1,1>, class S = Dune::FieldVector<double,1>>
class BilaplaceSmoother
{
  public:
  
    typedef Dune::BCRSMatrix<T> Matrix;
    typedef Dune::BitSetVector<1> Ignore;
  
    BilaplaceSmoother(const Basis& basis, const Ignore& ignore)
      : basis(basis), ignore(ignore)
    {
      assembleSystem();
    }
    
    template<class Vector>
    void apply(Vector& b) const
    {      
      const auto nComponents = Vector::value_type::dimension;
      
      const bool fVerbose = false;
      Dune::SuperLU<Matrix> solverSuperLU(systemMatrix, fVerbose);

      for(int i = 0; i < nComponents; ++i)
      {
        // Extract vector;
        Dune::BlockVector<S> rhs(b.size(),0);
        for(int j = 0; j < b.size(); ++j)
          rhs[j]  = b[j][i];
        
        int c = 0;
        for(int j = 0; j < b.size(); ++j)
          c += (rhs[j] != 0);
        std::cerr << "got c = " << c << std::endl;
        
        // Solve the laplace system.
        Dune::BlockVector<S> u(systemMatrix.M(), 0);
        Dune::InverseOperatorResult res;
        solverSuperLU.apply(u, rhs, res);
      
        // Overwrite vector.
        for(int j = 0; j < u.size(); ++j)
          b[j][i] = u[j];
      }
    }
    
  protected:
  
    const Basis& basis;
    const Ignore& ignore;
    Matrix systemMatrix;
    
    void assembleSystem()
    {
      typedef typename Basis::GridType Grid;
      typedef typename Basis::LocalFiniteElement LFE;
      
      BilaplaceAssembler<Grid,LFE,LFE> operatorAssembler;
      Matrix systemMatrix_;

      Assembler<Basis,Basis> assembler(basis, basis);
      assembler.assembleOperator(operatorAssembler, systemMatrix_);
      
      Dune::BlockVector<S> u(systemMatrix_.M(), 0);
      systemMatrix = Misc::incorporateDOFs(systemMatrix_, u, basis, ignore);
    }
};

#endif
