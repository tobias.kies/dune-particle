#ifndef DUNE_MATRIX_SMOOTHER_HH
#define DUNE_MATRIX_SMOOTHER_HH

#include <dune/istl/superlu.hh>

// Make a function smoother by solving a Dirichlet Bi-Laplace problem.

template<class Matrix, class Model, class Basis>
class MatrixSmoother
{
  public:
  
    static int c;
  
    typedef typename std::vector<typename Model::ParameterizedPoints> PointConstraints;
  
    MatrixSmoother(const Matrix& matrix, const Model& model, const Basis& basis)
      : matrix(matrix), model(model), basis(basis)
    {
      assert(Matrix::block_type::rows == Matrix::block_type::cols);
    }

    template<class Vector>
    void apply(Vector& b, const PointConstraints& pointConstraints) const
    {      
      const auto nComponents = Vector::value_type::dimension;
      assert(pointConstraints.size() == nComponents);
      
      const bool fVerbose = false;
      Dune::SuperLU<Matrix> solverSuperLU(matrix, fVerbose);

      for(int i = 0; i < nComponents; ++i)
      {
        // Extract vector;
        Dune::BlockVector<Dune::FieldVector<double,Matrix::block_type::rows>> rhs(b.size(),0);
        for(int j = 0; j < b.size(); ++j)
          rhs[j]  = b[j][i];

        // Solve the system.
        MembraneSolver<Model,Basis> solver(matrix, rhs, model, basis, pointConstraints[i]);
        Dune::BlockVector<Dune::FieldVector<double,Matrix::block_type::rows>> u(basis.size(),0);
        double pointNorm = 0;
        for(int k = 0; k < pointConstraints[i].second.size(); ++k)
          pointNorm = max(pointNorm, std::abs(pointConstraints[i].second[k]));
        if(rhs.infinity_norm() > 1e-10 || pointNorm > 1e-10)
          u = solver.solve();
      
        // Overwrite vector.
        for(int j = 0; j < u.size(); ++j)
          b[j][i] = u[j];
      }
    }

    template<class Vector, class Solver>
    void apply(Vector& b, const PointConstraints& pointConstraints, Solver& solver) const
    {      
      const auto nComponents = Vector::value_type::dimension;
      assert(pointConstraints.size() == nComponents);
      
      const bool fVerbose = false;
      Dune::SuperLU<Matrix> solverSuperLU(matrix, fVerbose);

      for(int i = 0; i < nComponents; ++i)
      {
        // Extract vector;
        Dune::BlockVector<Dune::FieldVector<double,Matrix::block_type::rows>> rhs(b.size(),0);
        for(int j = 0; j < b.size(); ++j)
          rhs[j]  = b[j][i];

        // Solve the system.
        solver.setRhs(rhs);
        solver.setConstraintPointsRhs(pointConstraints[i].second);
        Dune::BlockVector<Dune::FieldVector<double,Matrix::block_type::rows>> u(basis.size(),0);
        double pointNorm = 0;
        for(int k = 0; k < pointConstraints[i].second.size(); ++k)
          pointNorm = std::max(pointNorm, std::abs(pointConstraints[i].second[k]));
        auto rhsNorm = rhs.infinity_norm();
        if(std::isnan(rhsNorm) || std::isnan(pointNorm))
          DUNE_THROW(Dune::Exception, "Right hand side/point constraint contains NaN-values for component " + std::to_string(i));
        if(rhsNorm > 1e-13 || pointNorm > 1e-13)
          u = solver.solve();
      
        // Overwrite vector.
        for(int j = 0; j < u.size(); ++j)
          b[j][i] = u[j];
          
        //~ Misc::writeDiscreteFunction(basis, u, "vectorField_" + std::to_string(c++), 1);
      }
    }


  protected:
  
    const Matrix& matrix;
    const Model& model;
    const Basis& basis;
};

template<class Matrix, class Model, class Basis>
int MatrixSmoother<Matrix,Model,Basis>::c = 0;

#endif
