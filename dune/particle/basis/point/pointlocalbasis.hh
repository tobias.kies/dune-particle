// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_POINT_LOCALBASIS_HH
#define DUNE_LOCALFUNCTIONS_POINT_LOCALBASIS_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/power.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/common/localfiniteelementtraits.hh>



namespace Dune
{
    template<class Points, class D, size_t d, class R = double>
    class PointLocalBasis
    {

    public:

        typedef Dune::FieldVector<size_t,d> OrderType;
        typedef LocalBasisTraits<D,d,Dune::FieldVector<D,d>,R,1,Dune::FieldVector<R,1>,Dune::FieldMatrix<R,1,d> > Traits;

        PointLocalBasis( const Points& points_ ) : points(points_)
        {}

        PointLocalBasis( const PointLocalBasis<Points,D,d,R>& other ) = default;

        //! \brief number of shape functions
        unsigned int size() const
        {
            return points.size();
        }
        
        
        //! \brief returns the i-th point of the current element
        auto centerOfMass( size_t i ) const
        {
            return points[i].point;
        }


        //! \brief Polynomial order of the shape functions (in 1D)
        unsigned int order() const
        {
            DUNE_THROW( Dune::RangeError, "Can not compute order of non-polynomial basis." );
            return 0;
        }


        //! \brief Evaluates the dif derivative of the i-th d-dimensional basis function at x.
        inline R evaluate( const typename Traits::DomainType& x, size_t i, const OrderType& dif ) const
        {
            DUNE_THROW( Dune::NotImplemented, "Can not perform evaluation yet." );
            return 0;
        }


        inline void evaluate( const typename Traits::DomainType& x, size_t i, const OrderType& dif, R& y ) const
        {
            y    = evaluate(x, i, dif);
        }


        /** \brief Evaluate all shape functions at a given position.
         *  \param in position where to evaluate
         *  \param out return value
         */
        inline void evaluateFunction (const typename Traits::DomainType& in,
                                        std::vector<typename Traits::RangeType>& out) const
        {
            const OrderType dif(0);

            // Resize output vector to being able to hold the results.
            out.resize(size());

            // Evaluate each function at given position.
            for( size_t i = 0; i < size(); ++i )
                out[i]    = evaluate( in, i, dif );
        }


        /** \brief Evaluate Jacobian of all shape functions
         *  \param in position where to evaluate
         *  \param out return value
         */
        inline void
        evaluateJacobian (const typename Traits::DomainType& in,
                            std::vector<typename Traits::JacobianType>& out) const
        {
            OrderType dif(0);

            // Resize output matrix to being able to hold the results.
            out.resize(size());

            // Evaluate each function at given position.
            // Loop over all base functions.
            for( size_t i = 0; i < size(); ++i )    // What is the bigger time penalty? The unfavorable index order (i.e. the right index in the outer loop) or reformatting dif in every iteration?
            {
                for( size_t j = 0; j < d; ++j )
                {
                    dif[j]          = 1;
                    out[i][0][j]    = evaluate( in, i, dif );
                    dif[j]          = 0;
                }
            }
        }


    protected:

        const Points& points;

    };
}

#endif
