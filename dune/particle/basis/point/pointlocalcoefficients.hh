// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_POINT_LOCALCOEFFICIENTS_HH
#define DUNE_LOCALFUNCTIONS_POINT_LOCALCOEFFICIENTS_HH

#include <array>
#include <cassert>
#include <vector>

#include <dune/common/power.hh>

#include <dune/localfunctions/common/localkey.hh>

namespace Dune
{
    /** \brief Attaches a shape function to an entity
     *
     * \tparam d Dimension of the reference cube
     */
    template<size_t d>
    class PointLocalCoefficients {

    public:

        //! \brief Default constructor
        PointLocalCoefficients() 
        {
        }

        //! number of coefficients
        std::size_t size () const
        {
            DUNE_THROW( Dune::NotImplemented, "Attaching shape functions do entities does not make sense for PointBasis." );
            return 0;
        }

        //! get i'th index
        const LocalKey& localKey (std::size_t i) const
        {
            DUNE_THROW( Dune::NotImplemented, "Attaching shape functions do entities does not make sense for PointBasis." );
            return li[i];
        }

    private:
        std::vector<LocalKey> li;
    };

}

#endif
