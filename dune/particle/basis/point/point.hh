// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_POINT_LOCALFINITEELEMENT_HH
#define DUNE_LOCALFUNCTIONS_POINT_LOCALFINITEELEMENT_HH

#include <dune/particle/basis/point/pointlocalinterpolation.hh>
#include <dune/particle/basis/point/pointlocalbasis.hh>
#include <dune/particle/basis/point/pointlocalcoefficients.hh>

namespace Dune
{
    template<class Points, class D, class R, size_t d>
    class PointLocalFiniteElement {

        typedef PointLocalBasis<Points, D,d,R> LocalBasis;
        typedef PointLocalCoefficients<d> LocalCoefficients;
        typedef PointLocalInterpolation<d,LocalBasis> LocalInterpolation;

    public:

        /** \todo Please doc me !
         */
        typedef LocalFiniteElementTraits<LocalBasis,LocalCoefficients,LocalInterpolation> Traits;

        /** \todo Please doc me !
         */
        PointLocalFiniteElement( const Points& points ) :
            basis(points)
        {
            gt.makeCube(d);
        }

        /** \todo Please doc me !
         */
        const typename Traits::LocalBasisType& localBasis () const
        {
            return basis;
        }

        /** \todo Please doc me !
         */
        const typename Traits::LocalCoefficientsType& localCoefficients () const
        {
            return coefficients;
        }

        /** \todo Please doc me !
         */
        const typename Traits::LocalInterpolationType& localInterpolation () const
        {
            return interpolation;
        }

        /** \brief Number of shape functions in this finite element */
        unsigned int size () const
        {
            return basis.size();
        }

        /** \todo Please doc me !
         */
        GeometryType type () const
        {
            return gt;
        }

        PointLocalFiniteElement* clone() const
        {
            DUNE_THROW( Dune::NotImplemented, "Did not yet implement proper clone function here." );
            return new PointLocalFiniteElement(*this);
        }

    private:

        const LocalBasis basis;
        const LocalCoefficients coefficients;
        LocalInterpolation interpolation;
        GeometryType gt;

    };

}

#endif
