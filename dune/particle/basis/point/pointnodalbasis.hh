#ifndef POINT_NODALBASIS_HH
#define POINT_NODALBASIS_HH


/**
   @file
   @brief TODO

   @author
 */

#include <dune/geometry/type.hh>

#include <dune/particle/basis/point/point.hh>
#include <dune/common/power.hh>
#include <dune/fufem/functionspacebases/functionspacebasis.hh>
#include <dune/grid/utility/hierarchicsearch.hh>


template<class Point>
struct GlobalPoint
{
    const Point& point;
    const size_t globalIndex;
};

template <class GV, class Point, class RT = double>
class PointNodalBasis :
    public FunctionSpaceBasis<
        GV,
        RT,
        typename Dune::template PointLocalFiniteElement<
            std::vector<GlobalPoint<Point>>,
            typename GV::Grid::ctype,
            RT,
            GV::Grid::dimension
        >
    >
{
    protected:
        
        typedef std::vector<Point> Points;
        typedef std::vector<GlobalPoint<Point>> GlobalPoints;

        typedef typename Dune::template PointLocalFiniteElement<GlobalPoints,typename GV::Grid::ctype, RT, GV::Grid::dimension> LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;
        using Base::dim;
        using Base::gridview_;

    public:

        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;

        typedef typename Base::GridView::IndexSet IndexSet;
        typedef typename Base::GridView::IndexSet::IndexType IndexType;

        typedef typename GridView::Grid Grid;

        PointNodalBasis(const GridView& gridview, const Points& points_) :
            Base(gridview),
            points(points_)
        {
            findContainingElements();

            initializeLocalFiniteElements();
        }

        size_t size() const
        {
            return points.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            // We create the local finite element dynamically by lookup techniques.
            const size_t elementId = gridview_.indexSet().index(e);
            return localFiniteElements[elementId];
            //~ return LocalFiniteElement( pointsOfElementId[elementId] );
        }

        // Return the global id of a basis function from an element
        // and a local id.
        int index(const Element& e, const int i) const
        {
            const size_t elementId = gridview_.indexSet().index(e);
            if( i >= pointsOfElementId[elementId].size() )
                DUNE_THROW( Dune::RangeError, "Local index is out of range." );
            return pointsOfElementId[elementId][i].globalIndex;
        }

        void update( const GridView& gridview )
        {
            //Base::update();
            Base::gridview_ = gridview;
            findContainingElements();
            return;
        }

        // This method creates the necessary data structures to associate each point to an element.
        void findContainingElements()
        {
            //~ elementIdOfPoint.clear();
            //~ elementIdOfPoint.resize( points.size(), -1 );
            
            pointsOfElementId.clear();
            pointsOfElementId.resize( gridview_.size(0) );
            
            Dune::HierarchicSearch<Grid, GridView> hsearch(gridview_.grid(), gridview_);
            for( size_t pointId = 0; pointId < points.size(); ++pointId )
            {
                const auto& point       = points[pointId];
                const auto&& e          = hsearch.findEntity(point);
                const size_t elementId  = gridview_.indexSet().index(e);
                
                //~ elementIdOfPoint[pointId]   = elementId;
                pointsOfElementId[elementId].push_back( {point, pointId} );
            }
        }
        
        void initializeLocalFiniteElements( )
        {
            localFiniteElements.reserve(gridview_.size(0));
            for( size_t i = 0; i < gridview_.size(0); ++i )
                localFiniteElements.push_back( LocalFiniteElement( pointsOfElementId[i] ) );            
        }

    protected:
        const Points& points;
        //~ std::vector<int> elementIdOfPoint;
        std::vector<GlobalPoints> pointsOfElementId;
        std::vector<LocalFiniteElement> localFiniteElements;
};

#endif

