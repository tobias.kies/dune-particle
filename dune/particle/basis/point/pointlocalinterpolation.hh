// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_POINT_LOCALINTERPOLATION_HH
#define DUNE_LOCALFUNCTIONS_POINT_LOCALINTERPOLATION_HH

#include <dune/common/fvector.hh>
#include <dune/common/power.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/common/localfiniteelementtraits.hh>


namespace Dune
{
    /** \todo Please doc me! */
    template<size_t d, class LB>
    class PointLocalInterpolation
    {

    public:

        //! \brief Local interpolation of a function
        template<typename F, typename C>
        void interpolate (const F& f, std::vector<C>& out) const
        {
            DUNE_THROW( Dune::NotImplemented, "Local interpolation does not work for PointBasis as long as we do not specify some element-dependent parameters." );
        }
  };

}


#endif
