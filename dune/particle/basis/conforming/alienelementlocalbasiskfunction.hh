#ifndef ALIEN_ELEMENT_LOCALBASIS_K_FUNCTION_HH
#define ALIEN_ELEMENT_LOCALBASIS_K_FUNCTION_HH

/**
   @file alienelementlocalbasisfunction.hh
   @brief See alienelementlocalbasisfunction.hh for details.
   
   Had to copy a lot of the code because members were private instead of protected and I did not want to alter code in the fufem branch.
 */

#include <dune/fufem/functions/alienelementlocalbasisfunction.hh>
#include <dune/particle/helper/derivativehelper.hh>

template <class Entity, class FE, class Base
    =typename Dune::Function<
        const typename FE::Traits::LocalBasisType::Traits::DomainType&,
        typename FE::Traits::LocalBasisType::Traits::RangeType&> >
class AlienElementLocalBasisKFunction :
    //public AlienElementLocalBasisFunction<Entity,FE,Base>
    public CachedComponentWrapper<AlienElementLocalBasisKFunction<Entity, FE, Base>,
    typename std::vector<typename FE::Traits::LocalBasisType::Traits::RangeType>,
    Base>
{
    public:
    
        typedef typename std::vector<typename FE::Traits::LocalBasisType::Traits::RangeType> AllRangeType;
        typedef typename FE::Traits::LocalBasisType LocalBasis;
        typedef typename FE::Traits::LocalBasisType::Traits::DomainType DomainType;
        typedef typename FE::Traits::LocalBasisType::Traits::RangeType RangeType;
        typedef typename FE::Traits::LocalBasisType::OrderType OrderType;


    private:
    
        typedef AlienElementLocalBasisKFunction<Entity, FE, Base> MyType;
        typedef CachedComponentWrapper<MyType, AllRangeType, Base> BaseClass;


    public:

        /** \brief Construct function from entities and local basis.
         *
         * \param entity Entity where the function should be evaluated. Use local coordinates w.r.t. this entity.
         * \param alienEntity Entity the local basis lives on.
         * \param localBasis Local basis on alienEntity
         */
        AlienElementLocalBasisKFunction(const Entity& entity, const Entity& alienEntity, const LocalBasis& localBasis, const int i=0) :
            BaseClass(i),
            entity_(entity),
            alienEntity_(alienEntity),
            localBasis_(localBasis)
        {}


        /** \brief Evaluate all local basis functions
         *
         * \param x Local coordinates with respect to entity given in constructor.
         * \param yy The result vector containing all values is stored here.
         */
        void evaluateAll(const DomainType& x, std::vector<RangeType>& yy) const
        {
            const Dune::ReferenceElement<double, Entity::dimension>& alienRefElement
                = Dune::ReferenceElements<double, Entity::dimension>::general(alienEntity_.type());

            const DomainType globalPosition = entity_.geometry().global(x);
            const DomainType alienLocalPosition = alienEntity_.geometry().local(globalPosition);

            if (alienRefElement.checkInside(alienLocalPosition))
                localBasis_.evaluateFunction(alienLocalPosition, yy);
            else
            {
                yy.resize(localBasis_.size());
                for(size_t j=0; j<localBasis_.size(); ++j)
                    yy[j] = 0;
            }
        }
        
        void evaluate(const DomainType& x, const OrderType& dif, RangeType& y ) const
        {
            const Dune::ReferenceElement<double, Entity::dimension>& alienRefElement
                = Dune::ReferenceElements<double, Entity::dimension>::general(alienEntity_.type());

            const DomainType globalPosition = entity_.geometry().global(x);
            const DomainType alienLocalPosition = alienEntity_.geometry().local(globalPosition);

            // Note: If we evaluate a local basis function's derivative we need to scale it appropriately in order to get the global derivative.
            if(alienRefElement.checkInside(alienLocalPosition))
            {
                const auto& jacobianInverseTransposed   = alienEntity_.geometry().jacobianInverseTransposed(alienLocalPosition);
                DerivativeHelper<typename std::decay<decltype(jacobianInverseTransposed)>::type> derivativeHelper(jacobianInverseTransposed);
                y   = derivativeHelper.partialDerivative( localBasis_, alienLocalPosition, dif, BaseClass::i_ );                
            }
            else
                y = 0;
        }


    private:
    
        const Entity& entity_;
        const Entity& alienEntity_;
        const LocalBasis& localBasis_;
};

#endif

