#ifndef CONFORMING_K_BASIS_HH
#define CONFORMING_K_BASIS_HH

#include <dune/fufem/functionspacebases/conformingbasis.hh>
#include <dune/particle/basis/conforming/dofconstraintsk.hh>

/** \brief Generic class for conforming global basis
 *
 * Extends the existing conforming basis to the multi-differentiable setting (which is kind of unnecessary).
 * Also it turns out that the conforming basis works even in the C^k sense, at least for the Hermite-k finite elements.
 */
template <class NonconformingBasis>
class ConformingKBasis
    //: public ConformingBasis<NonconformingBasis>
    : public FunctionSpaceBasis<
        typename NonconformingBasis::GridView,
        typename NonconformingBasis::ReturnType,
        typename NonconformingBasis::LocalFiniteElement>

{
    protected:
        typedef typename NonconformingBasis::GridView GV;
        typedef typename NonconformingBasis::ReturnType RT;
        typedef typename NonconformingBasis::LocalFiniteElement LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::BitVector BitVector;
        typedef typename Base::LinearCombination LinearCombination;
        typedef typename GridView::Traits::Grid GridType;

        /**
         * \brief Setup conforming global basis
         *
         * This constructor will take some time since the interpolation values
         * for the conforming basis need to be build.
         *
         * \param ncBasis Global basis of underlying nonconforming function space
         */
        ConformingKBasis(/*const*/ NonconformingBasis& ncBasis) :
            Base(ncBasis.getGridView()),
            ncBasis_(ncBasis)
        {
            dofConstraints_ = new DOFConstraintsK;
            dofConstraintsAllocated_ = true;
            dofConstraints_->setupFromBasis<NonconformingBasis>(ncBasis_);
        }

        /**
         * \brief Setup conforming global basis
         *
         * Using this constructor you can provide a custom DOFConstrains object.
         * This needs to be set up before using the basis and after grid modification manually.
         *
         * \param ncBasis Global basis of underlying nonconforming function space
         * \param dofConstraints Set of interpolation values for the conforming subspace basis
         */
        ConformingKBasis(/*const*/ NonconformingBasis& ncBasis, const DOFConstraintsK& dofConstraints) :
            Base(ncBasis.getGridView()),
            ncBasis_(ncBasis),
            dofConstraints_(const_cast<DOFConstraintsK*>(&dofConstraints)),
            dofConstraintsAllocated_(false)
        {}

        /**
         * \brief Deep copy
         */
        ConformingKBasis(const ConformingKBasis& from) :
            Base(from.getGridView()),
            ncBasis_(from.ncBasis_)
        {
            dofConstraintsAllocated_ = from.dofConstraintsAllocated_;
            if (dofConstraintsAllocated_)
                dofConstraints_ = new DOFConstraintsK(*(from.dofConstraints_));
            else
                dofConstraints_ = from.dofConstraints_;
        }

        virtual ~ConformingKBasis()
        {
            if (dofConstraintsAllocated_)
                delete dofConstraints_;
        }

        using Base::update;

        void update(const GridView& gridview)
        {
            Base::update(gridview);
            ncBasis_.update(gridview);
            if (dofConstraintsAllocated_)
                dofConstraints_->setupFromBasis<NonconformingBasis>(ncBasis_);
        }

        size_t size() const
        {
            return ncBasis_.size();
        }

        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const // this version may be critial if the loal finite elements from the ncBasis_ are temporary
        //~ const LocalFiniteElement getLocalFiniteElement(const Element& e) const // this version may be inefficient due to copying
        {
            return ncBasis_.getLocalFiniteElement(e);
        }

        int index(const Element& e, const int i) const
        {
            return ncBasis_.index(e, i);
        }

        bool isConstrained(const int index) const
        {
            return dofConstraints_->isConstrained()[index][0];
        }

        const LinearCombination& constraints(const int index) const
        {
            return dofConstraints_->interpolation()[index];
        }

        const BitVector& isConstrained() const
        {
            return dofConstraints_->isConstrained();
        }
        
        DOFConstraintsK& dofConstraints()
        {
          return *dofConstraints_;
        }

    protected:
        /*const*/ NonconformingBasis& ncBasis_;
        DOFConstraintsK* dofConstraints_;
        bool dofConstraintsAllocated_;
};

#endif

