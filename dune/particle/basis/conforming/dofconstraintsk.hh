#ifndef DOF_CONSTRAINTS_K_HH
#define DOF_CONSTRAINTS_K_HH

#include <cmath>
#include <vector>
#include <map>

#include <dune/common/bitsetvector.hh>
#include <dune/common/fvector.hh>

#include <dune/localfunctions/common/virtualinterface.hh>


#include <dune/fufem/functionspacebases/dofconstraints.hh>
#include <dune/particle/basis/conforming/alienelementlocalbasiskfunction.hh>

class DOFConstraintsK :
    public DOFConstraints
{
    public:

        /*
         *  \todo please doc me
         */
        template<class B>
        void setupFromBasis(const B& basis)
        {
            typedef typename B::GridView::template Codim<0>::Entity Element;
            typedef typename B::GridView::template Codim<0>::Iterator ElementIterator;
            typedef typename B::GridView::IntersectionIterator NeighborIterator;

            typedef typename B::LocalFiniteElement FE;
            typedef typename FE::Traits::LocalInterpolationType LocalInterpolationType;
            typedef typename FE::Traits::LocalBasisType LocalBasisType;

            typedef std::vector< std::map<int, double> > InterpolationMap;

            const typename B::GridView gridview = basis.getGridView();

            resize(basis.size());

            InterpolationMap interpolationMap(basis.size());

            ElementIterator it = gridview.template begin<0>();
            ElementIterator end = gridview.template end<0>();
            for(; it != end; ++it)
            {
                const auto& element = *it;
                int level = element.level();
                const LocalBasisType& localBasis = basis.getLocalFiniteElement(element).localBasis();
                const LocalInterpolationType& localInterpolation = basis.getLocalFiniteElement(element).localInterpolation();

                // check if all dofs on element are already constrained
                bool allDofsConstrained = true;
                for(size_t i=0; i<localBasis.size(); ++i)
                    allDofsConstrained = allDofsConstrained and isConstrained_[basis.index(element, i)][0];

                if (allDofsConstrained)
                    continue;

                NeighborIterator nIt = gridview.ibegin(element);
                NeighborIterator nEnd = gridview.iend(element);
                for(; nIt != nEnd; ++nIt)
                {
                    if (not(nIt->neighbor()))
                        continue;

                    const Element& neighbor = nIt->outside();

                    if (not(neighbor.level()<level))
                        continue;

                    const LocalBasisType& nLocalBasis = basis.getLocalFiniteElement(neighbor).localBasis();

                    typedef typename Dune::LocalFiniteElementFunctionBase<FE>::type FunctionBaseClass;

                    AlienElementLocalBasisKFunction<Element, FE, FunctionBaseClass> f(element, neighbor, nLocalBasis);

                    std::vector<double> interpolationValues(localBasis.size());

                    Dune::BitSetVector<1> localDofIsConstrained(localBasis.size(), false);

                    for(size_t j=0; j<nLocalBasis.size(); ++j)
                    {
                        int jIndex = basis.index(neighbor, j);

                        f.setIndex(j);
                        localInterpolation.interpolate(f, interpolationValues);

                        for(size_t i=0; i<localBasis.size(); ++i)
                        {
                            int iIndex = basis.index(element, i);

                            // we need to skip this if dof is already constrained from another element
                            // i.e. isConstrained_ is already set
                            // otherwise we might get interpolation weights from multiple level neighbors
                            // resulting in doubled interpolation weights after resolving the dependencies
                            if (isConstrained_[iIndex][0])
                                continue;

                            if (iIndex != jIndex)
                            {
                                if (std::abs(interpolationValues[i]) > 1e-5)
                                {
                                    interpolationMap[iIndex][jIndex] = interpolationValues[i];
                                    localDofIsConstrained[i][0] = true;
                                }
                            }
                        }
                    }
                    for(size_t i=0; i<localBasis.size(); ++i)
                    {
                        if (localDofIsConstrained[i][0])
                            isConstrained_[basis.index(element, i)] = true;
                    }
                }
            }

            // copy linear combinations
            for(size_t i=0; i<interpolationMap.size(); ++i)
            {
                InterpolationMap::value_type::const_iterator it = interpolationMap[i].begin();
                InterpolationMap::value_type::const_iterator end = interpolationMap[i].end();
                for(; it != end; ++it)
                    interpolation_[i].push_back(LinearFactor((*it).first,(*it).second));
            }

            resolveDependencies();
#ifndef NDEBUG
            check();
#endif
        }
        
        // Removed check for "sum of coefficients = 1".
        bool check() const
        {
            typedef LinearCombination::const_iterator ConstIt;
            bool r = true;

            int size = isConstrained_.size();
            for (int i=0; i<size; ++i)
            {
                if (isConstrained_[i][0])
                {
                    double sum = 0.0;
                    bool errorForDof = false;

                    ConstIt it = interpolation_[i].begin();
                    ConstIt end = interpolation_[i].end();
                    for(; it!=end; ++it)
                    {
                        sum += it->factor;
                        if (isConstrained_[it->index][0])
                        {
                            std::cout << "Error: interpolation for constrained DOF " << i << " depends on constrained DOF " << it->index << std::endl;
                            errorForDof = true;
                        }
                    }
                    r = r and not(errorForDof);
                    if (errorForDof)
                        report(i);
                }
            }
            return r;
        }
};


#endif
