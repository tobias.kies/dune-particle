// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALINTERPOLATION_HH
#define DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALINTERPOLATION_HH

#include <dune/common/fvector.hh>
#include <dune/common/power.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/common/localfiniteelementtraits.hh>


namespace Dune
{
  /** \todo Please doc me! */
  template<size_t d, size_t k, class LB>
  class HermiteKLocalInterpolation
  {

    enum { funcsPerNode = Dune::template StaticPower<k,d>::power,
           funcsTotal = Dune::template StaticPower<2,d>::power * funcsPerNode };

  public:

    // Examples of numbering to coordinate translation of the corners in low dimensions:
    // 1D:
    //  0 - 1
    // 2D:
    //  0,0 - 1,0 - 0,1 - 1,1
    // 3D:
    //   0,0,0 - 1,0,0 - 0,1,0 - 1,1,0 - 0,0,1 - 1,0,1 - 0,1,1 - 1,1,1
    //
    // Generally corner coordinates can be seen in a binary way.
    //  You get the i-th coordinate of a corner numbered c by (with i = 0..d-1)
    //  (c & ( 1 << i )) >> i

    //! \brief Local interpolation of a function
    template<typename F, typename C>
    void interpolate (const F& f, std::vector<C>& out) const
    {
      typename LB::Traits::DomainType x;
      typename LB::Traits::RangeType y;

      out.resize(funcsTotal);

      // Generate differentiation vector.
      typename F::OrderType dif;
      
      for( size_t i = 0; i < funcsTotal; ++i )
      {
        // The identification of corners follows the concept in
        // e.g. bfslocalcoefficients.hh.
        // Should one consider making the localcoefficients-class
        // a parameter of this class?

        // Set index.
        int index  = i % funcsPerNode;

        // New corner?
        if( index == 0 )
        {
          // Set subEntity
          int subEntity  = i/funcsPerNode;

          // Set coordinates
          for( size_t j = 0; j < d; ++j )
            x[j]  = ( subEntity & ( 1 << j )) >> j;
        }
        
        // Set up differentiation vector.
        size_t index_base_k  = index;  // Will be used to derive the j-th digit of the variable index from the left in the k-base.
        for( size_t j = 0; j < dif.size(); ++j )
        {
          dif[j]  = index_base_k % k;
          index_base_k  /= k;
        }

        // Evaluate function.
        f.evaluate(x,dif,y);
        
        out[i]  = y;
      }

    }
  };

}


#endif
