// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALFINITEELEMENT_HH
#define DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALFINITEELEMENT_HH

#include <dune/particle/basis/hermitek/hermiteklocalinterpolation.hh>
#include <dune/particle/basis/hermitek/hermiteklocalbasis.hh>
#include <dune/particle/basis/hermitek/hermiteklocalcoefficients.hh>

namespace Dune
{
    /** \brief Hermite Elements in arbitrary dimensions.
     *  \note Elements are restricted to equidistant quadrilateral meshes.
     *             One could try to extend this, however, by keeping track of possible different orientations.
     *  
     * \tparam D type used for domain coordinates
     * \tparam R type used for function values
     * \tparam d dimension of the reference element
     * \tparam k degree of the finite element (s.t. it will be in C^(k-1) afterwards)
     */
    template<class D, class R, size_t d, size_t k>
    class HermiteKLocalFiniteElement {

        typedef HermiteKLocalBasis<D,d,k,R> LocalBasis;
        typedef HermiteKLocalCoefficients<d,k> LocalCoefficients;
        typedef HermiteKLocalInterpolation<d,k,LocalBasis> LocalInterpolation;
        typedef typename LocalBasis::Traits::RangeType::field_type float_x;
        
    public:

        typedef Dune::FieldVector<float_x,d> HType;
        typedef typename LocalBasis::PType PType;

        /** \todo Please doc me !
         */
        typedef LocalFiniteElementTraits<LocalBasis,LocalCoefficients,LocalInterpolation> Traits;

        /** \todo Please doc me !
         */
        HermiteKLocalFiniteElement ( const HType& grid_h, const PType& sigma ) : basis(grid_h, sigma), grid_h_(grid_h), sigma_(sigma)
        {
            gt.makeCube(d);
        }

        HermiteKLocalFiniteElement ( const float_x grid_h, const PType& sigma ) : basis(grid_h, sigma), grid_h_(grid_h), sigma_(sigma)
        {
            gt.makeCube(d);
        }

        /** \todo Please doc me !
         */
        const typename Traits::LocalBasisType& localBasis () const
        {
            return basis;
        }

        /** \todo Please doc me !
         */
        const typename Traits::LocalCoefficientsType& localCoefficients () const
        {
            return coefficients;
        }

        /** \todo Please doc me !
         */
        const typename Traits::LocalInterpolationType& localInterpolation () const
        {
            return interpolation;
        }

        /** \brief Number of shape functions in this finite element */
        unsigned int size () const
        {
            return basis.size();
        }

        /** \todo Please doc me !
         */
        GeometryType type () const
        {
            return gt;
        }

        HermiteKLocalFiniteElement* clone () const
        {
            //DUNE_THROW( Dune::NotImplemented, "Did not yet implement proper clone function here." );
            //~ return new HermiteKLocalFiniteElement(*this);
            return new HermiteKLocalFiniteElement(grid_h_, sigma_);
        }
        
        // (Re-)initializes the basis when the grid is updated.
        void initialize( const HType& grid_h, const PType& sigma ) const
        {
            basis.initialize(grid_h, sigma);
        }

    private:
    
        const LocalBasis basis;
        LocalCoefficients coefficients;
        LocalInterpolation interpolation;
        GeometryType gt;
        const HType grid_h_;
        const PType sigma_;
        
    };

}

#endif
