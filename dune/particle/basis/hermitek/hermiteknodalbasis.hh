#ifndef HERMITE_K_NODALBASIS_HH
#define HERMITE_K_NODALBASIS_HH


/**
   @file
   @brief TODO

   @author
 */

#include <dune/geometry/type.hh>

#include <dune/particle/basis/hermitek/hermitek.hh>
#include <dune/common/power.hh>
#include <dune/fufem/functionspacebases/functionspacebasis.hh>



template <class GV, size_t k, class RT = double>
class HermiteKNodalBasis :
    public FunctionSpaceBasis<GV, RT, 
        typename Dune::template HermiteKLocalFiniteElement<typename GV::Grid::ctype, RT, GV::Grid::dimension, k> >
{
    enum { funcsPerNode = Dune::template StaticPower<k,GV::Grid::dimension>::power,
            funcsTotal = Dune::template StaticPower<2,GV::Grid::dimension>::power * funcsPerNode };


    private:
    
        //~ HermiteKNodalBasis( const HermiteKNodalBasis<GV,k,RT>& other ) = delete;


    protected:
        typedef typename Dune::template HermiteKLocalFiniteElement<typename GV::Grid::ctype, RT, GV::Grid::dimension, k> LFE;
        typedef FunctionSpaceBasis<GV, RT, LFE> Base;
        typedef typename Base::Element Element;
        using Base::dim;
        using Base::gridview_;


    public:
        typedef typename Base::GridView GridView;
        typedef typename Base::ReturnType ReturnType;
        typedef typename Base::LocalFiniteElement LocalFiniteElement;
        typedef typename Base::LinearCombination LinearCombination;
        
        typedef typename Base::GridView::IndexSet IndexSet;
        typedef typename Base::GridView::IndexSet::IndexType IndexType;
        
        typedef typename GridView::Traits::Grid GridType;

        /// ToDo: Extract grid size and give it to the local finite element.
        HermiteKNodalBasis(const GridView& gridview) :
            Base(gridview)//,
            //localFE_(1./std::pow(gridview.size(0),1./dim))
        {
            setFiniteElements();
        }

        size_t size() const
        {
        // Return number of nodes (of the whole grid!) times funcsPerNode.
            return gridview_.indexSet().size(dim) * funcsPerNode;
        }

        /// To-Do: Make getLocalFiniteElement more efficient.
        // Option 1: Create initially one big map: elementId |-> LocalFiniteElement
        // Option 2: Work with a FE-Factory, i.e. a map: "h sizes" |-> LocalFiniteElement
        const LocalFiniteElement& getLocalFiniteElement(const Element& e) const
        {
            const auto& elementIndex = gridview_.indexSet().index(e);
            return *localFEs_.at(elementIndex);
        }
        //~ const LocalFiniteElement getLocalFiniteElement(const Element& e) const
        //~ {
            //~ Dune::FieldVector<RT,dim> h;
            //~ auto g = e.geometry();
            //~ auto DJ = g.jacobianTransposed( g.local( g.corner(0) ) );
            //~ // Throw an error if there are off-diagonal entries. (Works for UG grid, YaspGrid may behave weirdly.)
            //~ #ifdef DUNE_GRID_YASPGRID_HH // To-Do: More elegant way to check if current gridView is based on YaspGrid?
                //~ #error It is not possible to use HermiteK-Finite Elements with YaspGrid unless the jacobianTransposed type gets adapted properly.
            //~ #endif
            //~ for( size_t i = 0; i < h.size(); ++i )
                //~ for( size_t j = 0; j < h.size(); ++j )
                    //~ if( i != j && std::abs(DJ[i][j]) > std::numeric_limits<double>::epsilon()*16 )
                        //~ DUNE_THROW( Dune::Exception, "HermiteKNodalBasis only handles diagonal Jacobians in the grid so far." );
            //~ for( size_t i = 0; i < h.size(); ++i )
                //~ h[i]    = DJ[i][i];
            //~ return LocalFiniteElement(h);
        //~ }

        // Return the global id of a basis function from an element
        // and the local id.
        int index(const Element& e, const int i) const
        {
            size_t corner_id = gridview_.indexSet().subIndex(e, getLocalFiniteElement(e).localCoefficients().localKey(i).subEntity(), dim);
            return corner_id * funcsPerNode + getLocalFiniteElement(e).localCoefficients().localKey(i).index();
        }
        
        void update( const GridView& gridview )
        {
            Base::update(gridview);
            //Base::gridview_ = gridview;
            setFiniteElements();
            return;
        }
        
        /**
         * 
                #warning We assume that Jacobian is constant over the whole reference element. Also, for now, we can only deal with diagonal matrices in DJ.
                #warning Check for diagonality of Jacobian is only performed correctly for UG-Grid. This is due to the fact that different grid managers implement the Jacobian differently. Thus, YaspGrid would trigger an error here.
         */
        void setFiniteElements()
        {
            //~ #warning Basically disabled the localFEs vector.
            //~ return;
            
            localFEs_.clear();
            //localFEs_.resize(0);
            //localFEs_.resize( gridview_.size(0) );
            Dune::FieldVector<RT,dim> h;
            
            auto itEnd = gridview_.template end<0>();
            for( auto&& it = gridview_.template begin<0>(); it != itEnd; ++it )
            {
                // ToDo: Initialize h. How do we get the right nodes?
                auto g = it->geometry();
                auto DJ = g.jacobianTransposed( g.local( g.corner(0) ) );
                // Throw an error if there are off-diagonal entries. (Works for UG grid, YaspGrid may behave weirdly.)
                #ifdef DUNE_GRID_YASPGRID_HH // To-Do: More elegant way to check if current gridView is based on YaspGrid?
                if (std::is_same<GridType,Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<typename GV::Grid::ctype,dim> > >::value)
                {
                    DUNE_THROW( Dune::NotImplemented, "It is not possible to use HermiteK-Finite Elements with YaspGrid unless the jacobianTransposed type gets adapted properly." );
                }
                #endif
                typename LocalFiniteElement::PType perm(-1);
                for( int i = 0; i < dim; ++i )
                {
                  for( int j = 0; j < dim; ++j )
                  {
                    if( std::abs(DJ[i][j]) > std::numeric_limits<double>::epsilon()*16 ) // Do not forget: DJ is the transposed Jacobian.
                    {
                      if( perm[i] != -1 )
                        DUNE_THROW( Dune::Exception, "HermiteKNodalBasis only handles permutations of diagonal Jacobians in the grid so far." );
                      perm[i] = j;
                      h[i]    = DJ[i][j];
                    }
                  } 
                }
                
                int eId = gridview_.indexSet().index(*it); // avoid trigger_entity_cast_warning
                localFEs_[eId] = std::make_shared<LocalFiniteElement>(h, perm);
            }
        }

    protected:
        // This could be extended to a container of LocalFiniteElement's if we'd want to treat different possible rotations in the mesh.
        // Compare for example with finite element cache in p1nodalbasis.hh.
        //~ const LocalFiniteElement localFE_;
        std::map<int,std::shared_ptr<LocalFiniteElement>> localFEs_;
};

#endif

