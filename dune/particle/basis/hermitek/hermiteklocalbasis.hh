// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALBASIS_HH
#define DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALBASIS_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/power.hh>

#include <dune/geometry/type.hh>

#include <dune/localfunctions/common/localbasis.hh>
#include <dune/localfunctions/common/localfiniteelementtraits.hh>



namespace Dune
{

    //! \brief Evaluates the l-th derivative of the i-th 1D basis function at x.
    template<class R, class D, size_t k>
    struct HermiteKLocalBasisEvaluator
    {
        static R evaluate1D( const D& x, size_t i, int l, const Dune::FieldMatrix<R,k,k>& hermiteCoefficients, const Dune::FieldVector<R,2*k>& factorial )
        {
            // Note that the i-th basis function is given by
            //     sum_{j=i}^{k-1} coeff(i,j) * x^j * (x-1)^k.
            // Thus the l-th derivative is given by
            //    sum_{j=i}^{k-1} sum_{m=0}^l coeff(i,j) * (l choose m) * j! / (j-l+m)! * x^(j-l+m) * k! / (k-m)! * (x-1)^(k-m)
            // where a/b! := 0 for b < 0 and a arbitrary (even a = inf).
            /// Note that the following implementation certainly is not the most efficient or stable!
            
            // If the order of the derivative is higher than the polynomial order, then we can directly return 0.
            if( l > 2*k-1 ) return 0;
            
            R ret = 0;
            std::array<R,k+1> xpow, x1pow;
            xpow[0]    = 1; x1pow[0] = 1;
            for( size_t j = 1; j < xpow.size(); ++j )
            {
                xpow[j]        = xpow[j-1] * x;
                x1pow[j]    = x1pow[j-1] * (x-1);
            }
            
            int mMax    = std::min( l, (int) k );
            for( int j = i; j < k; ++j )
            {
                for( int m = std::max( l-j, 0 ); m <= mMax; ++m )
                {
                    ret    += hermiteCoefficients[i][j] * factorial[l] * factorial[j] * factorial[k] / ( factorial[m] * factorial[l-m] * factorial[j-l+m] * factorial[k-m] ) * xpow[j-l+m] * x1pow[k-m]; // Note: For stability one should not do "full numerator divided by full denominator"; but the factors should stay small so here it is okay, I guess (also improves readability)
                }
            }
            
            return ret;
        }
    };

    template<class R, class D>
    struct HermiteKLocalBasisEvaluator<R,D,2>
    {
        static R evaluate1D( const D& x, size_t i, int l, const Dune::FieldMatrix<R,2,2>& hermiteCoefficients, const Dune::FieldVector<R,4>& factorial )
        {
            const D x2 = x*x;
            
            if( l > 3 ) return 0;
            switch( i )
            {
                case 1:
                    switch( l )
                    {
                        case 3:
                            return 6.0;
                            break;
                        
                        case 2:
                            return x*6.0-4.0;
                            break;
                        
                        case 1:
                            return x2*3.0-x*4.0+1.0;
                            break;
                        
                        default: // 0
                            return x2*-2.0+x+x2*x;
                            break;
                    }
                    break;
                    
                case 0:
                    switch( l )
                    {
                        case 3:
                            return 1.2E1;
                            break;
                        
                        case 2:
                            return x*1.2E1-6.0;
                            break;
                        
                        case 1:
                            return x2*6.0-x*6.0;
                            break;
                        
                        default: // 0
                            return x2*-3.0+x2*x*2.0+1.0;
                            break;
                    }
                    break;
                    
                default:
                    DUNE_THROW( Dune::Exception, "Something went wrong." );
                    break;
            }
            return 0;
        }
    };
    

    /**@ingroup LocalBasisImplementation
        \brief Hermite finite elements on quadrilateral.

        \tparam D Type to represent the field in the domain.
        \tparam R Type to represent the field in the range.
        \tparam d Dimension of the cube
        \tparam k Order of the Hermite interpolation (s.t. result is in C^(k-1))
        
        \note We assume uniform grids.
        \note We compute the hermite coefficients during run-time.
                Obviously one could try to deal with this at compile-time already.
        \note We could make evaluation a little more efficient if we would save (some information of) the last evaluation point.

        \nosubgrouping
    */
    
    template<class D, size_t d, size_t k, class R = double>
    class HermiteKLocalBasis
    {
    public:
        typedef Dune::FieldVector<size_t,d> OrderType;
        
    private:
        enum {  funcsPerNode = Dune::template StaticPower<k,d>::power,
                funcsTotal = Dune::template StaticPower<2,d>::power * funcsPerNode,
                polOrder = 2*k-1 };
        
        // Init computes the factorials and the hermite coefficients.
        void init( )
        {
            R fac = 1;
            for( size_t i = 0; i < factorial.size(); ++i, fac*=i )
                factorial[i]    = fac;

            Dune::FieldVector<R,k+1> hermiteVector(0);
            for( size_t i = 0; i < k; ++i )    // i-th basis function
            {
                // the right hand side is always of type (0...010...0)
                // with i+k zeros in the beginning and
                // with (1..10..0) as nodes.
                // Consequently, we only have to start computing the
                // coefficients in the (i+k)-st row and (i+1)-st column.
                // Just note in general that the Hermite scheme here
                // may look a little unusual.
                hermiteVector        *= 0;
                hermiteVector[0]    = 1./factorial[i];
                for( size_t j = i; j < k; ++j )
                {
                    for( size_t l = 1; l <= k; ++l )
                    {
                        hermiteVector[l] -= hermiteVector[l-1];
                    }
                    hermiteVector[0]    = 0;
                    hermiteCoefficients[i][j]    = hermiteVector[k];
                }
            }
        }
                
    public:
        
        typedef LocalBasisTraits<D,d,Dune::FieldVector<D,d>,R,1,Dune::FieldVector<R,1>,Dune::FieldMatrix<R,1,d> > Traits;
        typedef Dune::FieldVector<int,d> PType;
    
        //~ HermiteKLocalBasis( const R& grid_h )
        template<class HType>
        HermiteKLocalBasis( const HType& grid_h, const PType& sigma_ )
        {
            initialize( grid_h, sigma_ );
        }
        
        HermiteKLocalBasis( const HermiteKLocalBasis<D,d,k,R>& other ) = default;
        
        void initialize( const R& grid_h, const PType& sigma_ )
        {
            for( size_t i = 0; i < h.size(); ++i )
                h[i]    = grid_h;
            sigma = sigma_;
            init( );
        }
        
        void initialize( const Dune::FieldVector<R,d>& grid_h, const PType& sigma_ )
        {
            sigma = sigma_;
            for( size_t i = 0; i < h.size(); ++i )
                h[i]    = grid_h[i];
            init( );
        }
        
        
        //! \brief number of shape functions
        unsigned int size() const
        {
            return funcsTotal;
        }
        
        
        //! \brief Polynomial order of the shape functions (in 1D)
        unsigned int order () const
        {
            return 2*k-1;
        }        
        
        
        //! \brief Evaluates the dif derivative of the i-th d-dimensional basis function at x.
        inline R evaluate( const typename Traits::DomainType& x, size_t i, const OrderType& dif_ ) const
        {
            R    ret = 1;
            
            // Apply permutation to differential operator.
            OrderType dif;
            for( int j = 0; j < d; ++j )
              dif[sigma[j]]  = dif_[j];
            
            // Translate index i to its according tensor product, i.e.
            // determine the  necessary transformations on the x_j and
            // the 1D basis functions in use for the i-th basis function.
            //
            // We have
            //    i = cornerId * funcsPerNode + localId,
            //    cornerId \in \{0,\dots, 2^d-1\}
            //    localId \in \{0,\dots, k^d-1\}
            //    j-th bit of cornerId is j-th component of the associated coordinates
            //    localId = \sum_{j=0}^{d-1} hermiteId(j) * k^j.
            
            size_t  cornerId    = i/funcsPerNode,
                    localId     = i % funcsPerNode;
            int     hermiteId   = 0;
            D xj = 0;    // Will be modified depending on the corner.
            for( size_t j = 0; j < d; ++j )
            {
                // Extract relevant data.
                hermiteId    = localId % k;                
                xj = ( cornerId & 1 ) ? (1-x[j]) : x[j];
                
                
                // Note how functions get transformed to other corners:
                // f(x) -> (-1)^hermiteId * f(1-x)
                // differentiate --> (-1)^(hermiteId + dif[j]) * f^(dif[j])(1-x)
                // Evaluate 1D function.
                // Multiply by -1 if xj = 1-x[j] and (dif[j]+hermiteId) is odd.
                // Also multiply by h[j]^hermiteId in order to have correct
                // scaling for those basis functions which describe derivatives.
                //~ std::cout << ( (dif[j] ^ hermiteId ) & cornerId & 1) << " " << (int) ( (dif[j] ^ hermiteId ) & cornerId & 1) << std::endl;
                //~ std::cout << "h " << h[j] << " " << hermiteId << " " << std::pow( h[j], hermiteId ) << " " << ret << " " << (1-2*(int)( (dif[j] ^ hermiteId ) & cornerId & 1)) <<  std::endl;
                //~ std::cerr << "." << std::endl;
                //~ std::cerr << counter << std::endl; counter+=1;
                //~ std::cerr << dif[j] << std::endl;
                //~ std::cerr << hermiteId << std::endl;
                //~ std::cerr << (dif[j]^hermiteId) << std::endl;
                //~ std::cerr << cornerId << std::endl;
                //~ std::cerr << "j=" << j << std::endl;
                //~ std::cerr << factorial << std::endl;
                //~ std::cerr << hermiteCoefficients << std::endl;
                //~ std::cerr << h.size() << std::endl;
                //~ std::cerr << "h=" << h << std::endl;
                //~ std::cerr << "h[j]=" << h[j] << std::endl;
                //~ std::cerr << std::pow( h[j], hermiteId ) << std::endl;
                //~ std::cerr << HermiteKLocalBasisEvaluator<R,D,k>::evaluate1D( xj, hermiteId, dif[j], hermiteCoefficients, factorial ) << std::endl;
                ret    *=  (1-2*(int)( (dif[j] ^ hermiteId ) & cornerId & 1)) * HermiteKLocalBasisEvaluator<R,D,k>::evaluate1D( xj, hermiteId, dif[j], hermiteCoefficients, factorial ) * std::pow( h[j], hermiteId );
                // Note: We use std::pow( h[j], hermiteId ) instead of std::pow( h[j], hermiteId-(int) dif[j] ) because the scaling by dif[j] is usually done within the assembler. It maybe would make more sense to use gobal basis functions and thus use
                //~ ret    *=  (1-2*(int)( (dif[j] ^ hermiteId ) & cornerId & 1)) * evaluate1D( xj, hermiteId, dif[j] ) * std::pow( h[j], hermiteId-(int) dif[j] );
                // Then we also would not have to manually scale the derivatives in the BasisGridFunction.

                // Prepare next iteration.
                localId    /= k;
                cornerId >>= 1;
            }
            
            //~ std::cout << "2D: evaluated " << i << " at " << x[0] << " " << x[1] << " of order " << dif[0] << " " << dif[1] << " resulting in " << ret << std::endl;
            return ret;
        }
        
        
        inline void evaluate( const typename Traits::DomainType& x, size_t i, const OrderType& dif, R& y ) const
        {
            y    = evaluate(x, i, dif);
        }


        /** \brief Evaluate all shape functions at a given position.
         *  \param in position where to evaluate
         *  \param out return value
         */
        inline void evaluateFunction (const typename Traits::DomainType& in,
                                        std::vector<typename Traits::RangeType>& out) const
        {
            const OrderType dif(0);
            
            // Resize output vector to being able to hold the results.
            out.resize(size());            
            
            // Evaluate each function at given position.
            for( size_t i = 0; i < size(); ++i )
                out[i]    = evaluate( in, i, dif );
        }


        /** \brief Evaluate Jacobian of all shape functions
         *  \param in position where to evaluate
         *  \param out return value
         */
        inline void
        evaluateJacobian (const typename Traits::DomainType& in,
                            std::vector<typename Traits::JacobianType>& out) const
        {
            OrderType dif(0);
            
            // Resize output matrix to being able to hold the results.
            out.resize(size());
            
            // Evaluate each function at given position.
            // Loop over all base functions.
            for( size_t i = 0; i < funcsTotal; ++i )    // What is the bigger time penalty? The unfavorable index order (i.e. the right index in the outer loop) or reformatting dif in every iteration?
            {
                for( size_t j = 0; j < d; ++j )
                {
                    dif[j]            = 1;
                    out[i][0][j]      = evaluate( in, i, dif );
                    dif[j]            = 0;
                }
            }
        }
        
        
        R get_h( size_t id ) const
        {
            return h[id];
        }


    private:

        Dune::FieldMatrix<R,k,k> hermiteCoefficients;    // Hermite coefficients (i,j) of the i-th basis function.
        Dune::FieldVector<R,polOrder+1> factorial;        // Vector of factorials, factorial[i] = i! with 0! = 1. We take the data type as non-int to avoid complications in formulas.
        Dune::FieldVector<R,d> h;                        // Contains the stretching of the rectangle; i.e. this element is living on the cube h1 x h2 ... x hd.
        PType sigma;
    };   
}

#endif
