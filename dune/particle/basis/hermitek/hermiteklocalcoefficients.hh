// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALCOEFFICIENTS_HH
#define DUNE_LOCALFUNCTIONS_HERMITE_K_LOCALCOEFFICIENTS_HH

#include <array>
#include <cassert>
#include <vector>

#include <dune/common/power.hh>

#include <dune/localfunctions/common/localkey.hh>

namespace Dune
{
	/** \brief Attaches a shape function to an entity
	 *
	 * \tparam d Dimension of the reference cube
	 * \tparam k Degree of the hermite interpolation at each node.
	 */
	template<size_t d, size_t k>
	class HermiteKLocalCoefficients {
		
		enum {  funcsPerNode = Dune::template StaticPower<k,d>::power,					      // We have k^d degrees of freedom on each nodes.
            funcsTotal = funcsPerNode * Dune::template StaticPower<2,d>::power };	// We have 2^d nodes.

	public:
	
		//! \brief Default constructor
		HermiteKLocalCoefficients () : li(funcsTotal)
		{
			// LocalKey( subEntity, codimension, index );
			// - subEntity id is increased every k^d steps by one
			//	 since there are k^d basis functions on each node
			// - codimension is constant as we iterate over corners
			// - index is given by the number of the basis function
			//	 with respect to the given corner
			for( size_t i = 0; i < li.size(); i++ )
				li[i] = LocalKey( i/funcsPerNode, d, i % funcsPerNode );
		}

		//! number of coefficients
		std::size_t size () const
		{
			return funcsTotal;
		}

		//! get i'th index
		const LocalKey& localKey (std::size_t i) const
		{
			return li[i];
		}
		
	private:
		std::vector<LocalKey> li;
	};

}

#endif
