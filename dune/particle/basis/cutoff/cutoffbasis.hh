#ifndef CUT_OFF_BASIS_HH
#define CUT_OFF_BASIS_HH

/// ToDo: Just a modification of XBasis. Sould be cleaned up, I guess.

#include <dune/fufem/functionspacebases/functionspacebasis.hh>
#include <dune/localfunctions/common/localkey.hh>
#include <dune/geometry/referenceelements.hh>

// Note: In the following we require that pretty much all of the exported typedefs (and the used gridView) coincide for the StdBasis and for the EnrichmentBasis.
//     Also we do not yet remove any degrees of freedom from the standard basis. This may be added in future in order to increase efficiency.
// Note: We are not initializing the basis yet, i.e. we do not set up a map of CutTypes. This makes the code somewhat less efficient.
// Note: Evaluation and many other parts might change once local refinement (with hanging nodes) is added.

namespace Dune
{
    // Note: The notation there is such that d(x)<0  <==> x is interior (where the particles count as interior).
    // enum EnrichmentEvaluationType {DEFAULT, INTERIOR_ONLY, EXTERIOR_ONLY};

    enum CutType {
            ELEMENT_FULLY_INSIDE_DOMAIN,
            ELEMENT_FULLY_OUTSIDE_DOMAIN,
            ELEMENT_CUT
        };

    template<class Element, class B, class LevelSetFunctions, class DomainInfo>
    class CutOffBasisLocalBasis
    {
        public:
            typedef typename B::LocalFiniteElement::Traits::LocalBasisType LB;
            typedef typename LB::Traits Traits;
            typedef typename LB::OrderType OrderType;
        
        
            CutOffBasisLocalBasis( const Element& e, const B& stdBasis, const LevelSetFunctions& levelSetFunctions, const DomainInfo& domainInfo, const CutType cut ) :
                stdBasis_(stdBasis),
                e_(e),
                levelSetFunctions_(levelSetFunctions),
                domainInfo_(domainInfo),
                cut_(cut)
            {
            }


            //! \brief number of shape functions
            unsigned int size() const
            {
                return stdBasis_.getLocalFiniteElement(e_).localBasis().size();
            }
            
            //! \brief number of shape functions from the standard basis
            unsigned int standardSize() const
            {
                return stdBasis_.getLocalFiniteElement(e_).localBasis().size();
            }

            //! \brief Polynomial order of the shape functions
            unsigned int order () const
            {
                return stdBasis_.getLocalFiniteElement(e_).localBasis().order();
            }

            //! \brief Evaluates the dif-th derivative of the i-th function at x.
            mutable bool flag = true;
            inline typename Traits::RangeType evaluate( const typename Traits::DomainType& x, const size_t i, const OrderType& dif ) const
            {
                if( !fRestrict )
                    return stdBasis_.getLocalFiniteElement(e_).localBasis().evaluate( x, i, dif );

                auto&& xGlobal = e_.geometry().global(x);
                /*for( auto&& levelSetFunction = levelSetFunctions_.begin(); levelSetFunction != levelSetFunctions_.end(); levelSetFunction++ )
                {
                    /// ToDo: Use local evaluation here if it is supported.
                    if( (*levelSetFunction)->evaluate(xGlobal) > 0 )
                        return (fUseNAN) ? std::numeric_limits<typename Traits::RangeType::value_type>::quiet_NaN() : 0;
                }*/
                
                #warning Attention: Stuff possibly breaks after refining the grid without updating domain info.
                // For outside elements: Return NaN (or zero)
                if(domainInfo_.isOutside(e_))
                  return (fUseNAN) ? std::numeric_limits<typename Traits::RangeType::value_type>::quiet_NaN() : 0;
                
                // For inside elements: Return values.
                if(domainInfo_.isInside(e_))
                  return stdBasis_.getLocalFiniteElement(e_).localBasis().evaluate( x, i, dif );
                  
                // For boundary elements: Find the corresponding level set and check if it is inside.
                if( domainInfo_.levelSetFunction(e_).evaluate(xGlobal) > 0 )
                  return (fUseNAN) ? std::numeric_limits<typename Traits::RangeType::value_type>::quiet_NaN() : 0;
                
                /*for( size_t levelSetId = 0; levelSetId < levelSetFunctions_.size(); ++levelSetId )
                {
                    const auto& levelSetFunction = levelSetFunctions_[levelSetId];
                    /// ToDo: Use local evaluation here if it is supported.
                    if( levelSetFunction->evaluate(xGlobal) > 0 )
                        return (fUseNAN) ? std::numeric_limits<typename Traits::RangeType::value_type>::quiet_NaN() : 0;
                }*/
                
                return stdBasis_.getLocalFiniteElement(e_).localBasis().evaluate( x, i, dif );
            }
            
            inline void evaluate( const typename Traits::DomainType& x, size_t i, const OrderType& dif, typename Traits::RangeType& y ) const
            {
                y    = evaluate(x, i, dif);
            }


            /** \brief Evaluate all shape functions at a given position.
             *  \param in position where to evaluate
             *  \param out return value
             */
            inline void evaluateFunction (const typename Traits::DomainType& in,
                                            std::vector<typename Traits::RangeType>& out) const
            {
                OrderType dif(0);
                
                // Resize output vector to being able to hold the results.
                out.resize(size());            
                
                // Evaluate each function at given position.
                for( size_t i = 0; i < size(); ++i )
                    out[i]    = evaluate( in, i, dif );
            }


            /** \brief Evaluate Jacobian of all shape functions
             *  \param in position where to evaluate
             *  \param out return value
             */
            inline void
            evaluateJacobian (const typename Traits::DomainType& in,
                                std::vector<typename Traits::JacobianType>& out) const
            {
                OrderType dif(0);
                
                // Resize output matrix to being able to hold the results.
                out.resize(size());
                
                // Evaluate each function at given position.
                // Loop over all base functions.
                for( size_t i = 0; i < size(); ++i )    // What is the bigger time penalty? The unfavorable index order (i.e. the right index in the outer loop) or reformatting dif in every iteration?
                {
                    for( size_t j = 0; j < Traits::dimDomain; ++j )
                    {
                        dif[j]          = 1;
                        out[i][0][j]    = evaluate( in, i, dif );
                        dif[j]          = 0;
                    }
                }
            }
            
            bool enableRestriction( bool flag ) const
            {
                bool fOld = fRestrict;
                fRestrict = flag;
                return fOld;
            }
            
        private:
            const Element& e_;
            const B& stdBasis_;
            const LevelSetFunctions& levelSetFunctions_;
            const DomainInfo& domainInfo_;
            const CutType cut_;
            mutable bool fRestrict = true;
            mutable bool fUseNAN = true;
    };




    template<class LC>
    class CutOffBasisLocalCoefficients
    {
        public:
            //! \brief Default constructor
            CutOffBasisLocalCoefficients( const LC& stdLocalCoefficients ) :
                stdLocalCoefficients_(stdLocalCoefficients)
            {
            }

            //! number of coefficients
            std::size_t size () const
            {
                return stdLocalCoefficients_.size();
            }

            //! get i'th index
            const LocalKey localKey (std::size_t idx ) const
            {
                return stdLocalCoefficients_.localKey(idx);
            }
            
        private:
            const LC& stdLocalCoefficients_;
    };
     
     
     
    template<class LI>
    class CutOffBasisLocalInterpolation
    {
        public:
        
            CutOffBasisLocalInterpolation( const LI& stdLocalInterpolation ) :
                stdLocalInterpolation_(stdLocalInterpolation)
            {
            }
        
            //! \brief Local interpolation of a function
            template<typename F, typename C>
            void interpolate (const F& f, std::vector<C>& out) const
            {
                // Apply implementation from standard basis.
                stdLocalInterpolation_.interpolate(f,out);
            }

        private:
            const LI& stdLocalInterpolation_;
    };
     
     
     
    template<class B, class Element, class LevelSetFunctions, class DomainInfo>
    class CutOffBasisLocalFiniteElement
    {
        typedef typename B::LocalFiniteElement LFE;
        typedef CutOffBasisLocalBasis<Element,B,LevelSetFunctions,DomainInfo> LocalBasis;
        typedef CutOffBasisLocalCoefficients<typename LFE::Traits::LocalCoefficientsType> LocalCoefficients;
        typedef CutOffBasisLocalInterpolation<typename LFE::Traits::LocalInterpolationType> LocalInterpolation;
        
     public:
        typedef B StdBasis;
        typedef LFE StdLocalFiniteElement;
        typedef LocalFiniteElementTraits<LocalBasis,LocalCoefficients,LocalInterpolation> Traits;
     
        CutOffBasisLocalFiniteElement( const Element& e, const StdBasis& stdBasis, const LevelSetFunctions& levelSetFunctions, const DomainInfo& domainInfo, const CutType cut ) :
            stdLocalFiniteElement_(stdBasis.getLocalFiniteElement(e)),
            e_(e),
            basis(e, stdBasis, levelSetFunctions, domainInfo, cut),
            coefficients(stdLocalFiniteElement_.localCoefficients()),
            interpolation(stdLocalFiniteElement_.localInterpolation()),
            stdBasis_(&stdBasis)
        {
        }

        const typename Traits::LocalBasisType& localBasis () const
        {
            return basis;
        }

        const typename Traits::LocalCoefficientsType& localCoefficients () const
        {
            return coefficients;
        }

        const typename Traits::LocalInterpolationType& localInterpolation () const
        {
            return interpolation;
        }

        /** \brief Number of shape functions in this finite element */
        unsigned int size () const
        {
            return stdBasis_->getLocalFiniteElement(e_).size();
        }

        GeometryType type () const
        {
            return stdBasis_->getLocalFiniteElement(e_).type();
        }
        
        int getRefinement( ) const
        {
            return 0;
        }
        
    private:
        const StdLocalFiniteElement& stdLocalFiniteElement_;
        
        const Element& e_;
        
        const LocalBasis basis;
        const LocalCoefficients coefficients;
        const LocalInterpolation interpolation;
        
        const StdBasis* stdBasis_;
    };



     
    template <class StandardBasis, class LevelSetFunction, class DomainInfo>
    class CutOffBasis :
        public FunctionSpaceBasis<
            typename StandardBasis::GridView,
            typename StandardBasis::ReturnType,
            CutOffBasisLocalFiniteElement<
                StandardBasis,
                typename StandardBasis::GridView::Grid::template Codim<0>::Entity,
                std::vector<std::shared_ptr<LevelSetFunction>>,
                DomainInfo
            >
        >
    {
        protected:
            typedef std::shared_ptr<LevelSetFunction> LevelSetFunctionPtr;
            typedef std::vector<LevelSetFunctionPtr> LevelSetFunctions;
            typedef typename StandardBasis::GridView GV;
            typedef typename StandardBasis::ReturnType RT;
            typedef typename StandardBasis::LocalFiniteElement LFE;
            typedef FunctionSpaceBasis<
                typename StandardBasis::GridView,
                typename StandardBasis::ReturnType,
                CutOffBasisLocalFiniteElement<
                    StandardBasis,
                    typename StandardBasis::GridView::Grid::template Codim<0>::Entity,
                    LevelSetFunctions,
                    DomainInfo
                >
            > Base;
            typedef typename Base::Element Element;
            typedef Dune::BlockVector<typename LevelSetFunction::RangeType> VectorType;

        public:
            typedef typename Base::GridView GridView;
            typedef typename Base::ReturnType ReturnType;
            typedef typename Base::LocalFiniteElement LocalFiniteElement;
            typedef typename Base::BitVector BitVector;
            typedef typename Base::LinearCombination LinearCombination;

            /*template<class Dummy>
            CutOffBasis( StandardBasis& stdBasis, Dummy dummy ) :
                Base(stdBasis.getGridView()),
                stdBasis_(stdBasis)
            {
            }*/
            
            CutOffBasis( StandardBasis& stdBasis, const DomainInfo& domainInfo ) :
                Base(stdBasis.getGridView()),
                stdBasis_(stdBasis),
                domainInfo(domainInfo)
            {
            }

            size_t size() const
            {
                return stdBasis_.size();
            }

            size_t standardSize() const
            {
                return stdBasis_.size();
            }

            LocalFiniteElement getLocalFiniteElement(const Element& e) const
            {
                // Totally pointless. But for some reason it prevents a memory error?
                //~ {
                  //~ const auto& fe = stdBasis_.getLocalFiniteElement(e);
                  //~ const auto& localBasis = fe.localBasis();
                  //~ const auto value = localBasis.evaluate({0,0}, 0, {0,0});
                  //~ Debug::dprint( "GLFE Evaluated: ", value );
                //~ }
                LocalFiniteElement lfe( e, stdBasis_, levelSets_, domainInfo, computeCutTypeAt(e) );
                //~ {
                  //~ const auto& localBasis = lfe.localBasis();
                  //~ const auto value = localBasis.evaluate({0,0}, 0, {0,0});
                  //~ Debug::dprint( "GLFE2 Evaluated: ", value );
                //~ }
                //~ Debug::dprint( "returning" );
                return lfe;
            }

            int index(const Element& e, const int i) const
            {
                return stdBasis_.index(e, i);
            }

            bool isConstrained(const int index) const
            {
                if( index < stdBasis_.size() )
                    return stdBasis_.isConstrained(index);
                return false;
            }

            const LinearCombination& constraints(const int index) const
            {
                if( index < stdBasis_.size() )
                    return stdBasis_.constraints(index);
                return Base::dummyWeights_;
            }

            const BitVector isConstrained() const
            {
                auto constraints = stdBasis_.isConstrained();    // to-do: currently done with help of copy constructor... should be changed in future to be more efficient
                constraints.resize( size() );
                for( size_t i = stdBasis_.size(); i < size(); ++i )
                    constraints[i][0] = false;
                return constraints;
            }

            using Base::update;
            void update(const GridView& gridview)
            {
                Base::update(gridview);
                stdBasis_.update( gridview );
            }
            
            void addInterface( const LevelSetFunction& levelSetFunction )
            {
                levelSets_.push_back(std::make_shared<LevelSetFunction>(levelSetFunction));
            }

            void addInterface( const std::shared_ptr<LevelSetFunction>& ptrLevelSetFunction )
            {
                levelSets_.push_back(ptrLevelSetFunction);
            }

        protected:
        
            CutType computeCutTypeAt( const Element& element ) const
            {
                /// ToDo: What would be a reasonable mechanism to check this?
                return CutType::ELEMENT_CUT;
            }
        
            StandardBasis& stdBasis_;
            LevelSetFunctions levelSets_;
            //~ std::unordered_map<Element,bool> cut_;
            //~ bool fInitialized = false;
            size_t nZeroFunctions = 0;
            
            const DomainInfo& domainInfo;
    };

}

#endif

