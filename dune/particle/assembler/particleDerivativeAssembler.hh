#ifndef DUNE_PARTICLE_DERIVATIVE_ASSEMBLER_HH
#define DUNE_PARTICLE_DERIVATIVE_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/particle/quadrature/localparameterizationquadrature.hh>
#include <dune/particle/quadrature/curvequadrature.hh>
#include <dune/particle/helper/derivativehelper.hh>


// Technically is missing the stabilization part.
template<class Integrand, class DomainInfo, class Vector, class GridType, class TrialLocalFE, class T=Dune::FieldVector<double,3>>
class ParticleDerivativeAssembler : public LocalFunctionalAssembler<GridType, TrialLocalFE, T>
{
  private:

    typedef LocalFunctionalAssembler<GridType, TrialLocalFE, T> Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename T::value_type float_x;

    typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;


  public:

    typedef typename Base::Element Element;
    typedef typename Base::LocalVector LocalVector;

    ParticleDerivativeAssembler(const Integrand& integrand, const DomainInfo& domainInfo, const Vector& u)
    : rho(integrand), domainInfo(domainInfo), vec_u(u)
    {
        static_assert( dim == 2 && dim == dimworld, "Only supported for dim=2." );
    }

    void assemble(const Element& e, LocalVector& localVector, const TrialLocalFE& fe) const
    {
      const auto& basis = domainInfo.getBasis();
      const auto&& geometry   = e.geometry();
      const auto& localBasis  = fe.localBasis();
      localVector             = 0.0;
      
      // Skip outside elements.
      const bool isInside   = domainInfo.isInside(e);
      const bool isOutside  = domainInfo.isOutside(e);
      
      if(isOutside)
        return;
        
      // Get quadrature rule.
      // Because of the special structure of our basis functions we can get away with a lower order here.)
      // Technically not correct for boundary elements, however.
      Dune::QuadratureRule<double,2> quadratureRule;
      QuadratureRuleKey quadKeyFE(fe);
      QuadratureRuleKey quadKey = quadKeyFE.square();
      //~ #warning Technically using a too low quadKey for the evaluation of the derivative.
      quadKey.setOrder(localBasis.order()*3);
      //~ quadKey.setOrder(localBasis.order()*2); // Cheating a little here: The order is too low.
      if(!isInside) // element is on boundary
        quadratureRule  = LocalParameterizationQuadrature<double>::rule(quadKey, e, domainInfo);
      else
        quadratureRule  = QuadratureRuleCache<double,2>::rule(quadKey);

      // Iterate over all points and update the return value.
      for(const auto& quadPoint : quadratureRule)
      {
        // Get quadrature information.
        const auto& xLocal  = quadPoint.position();
        const auto& x       = geometry.global(xLocal); 
        const auto integrationFactor    = quadPoint.weight() * geometry.integrationElement(xLocal);
          
        // Pre-evaluate (and scale) all basis functions, including derivatives up to second order.
        const auto val0 = basisValues0(localBasis, e, xLocal);
        const auto val1 = basisValues1(localBasis, e, xLocal);
        const auto val2 = basisValues2(localBasis, e, xLocal);
                  
        // Get all derivatives of u.
        const auto u   = applyBasisValues(basis, e, vec_u, val0)[0];
        const auto Du  = applyBasisValues(basis, e, vec_u, val1)[0];
        const auto D2u = applyBasisValues(basis, e, vec_u, val2)[0];
        
        // Get all derivatives of rho.
        const auto rho_val  = rho.evaluateScalar(x, u, Du, D2u);
        const auto rho_x    = rho.evaluateScalar_x(x, u, Du, D2u);
        const auto rho_u    = rho.evaluateScalar_u(x, u, Du, D2u);
        const auto rho_z    = rho.evaluateScalar_z(x, u, Du, D2u);
        const auto rho_Z    = rho.evaluateScalar_Z(x, u, Du, D2u);
        
        // Update vector.
        for(int id = 0; id < localBasis.size(); ++id)
        {
          T vi(0);
          
          const auto& f   = val0[id];
          const auto& df  = val1[id];
          const auto& d2f = val2[id];
          
          for(int k = 0; k < 2; ++k)
          {
            vi[k] += rho_val * df[k];
            vi[k] += rho_x[k] * f;
            vi[k] -= rho_z[k] * (df[0]*Du[0] + df[1]*Du[1]);

            for(int i = 0; i < 2; ++i)
              for(int j = 0; j < 2; ++j)
                vi[k]  -= rho_Z[i][j] * (d2f[i][j] * Du[k] + df[i]*D2u[k][j] + D2u[i][k]*df[j]);
          }

          vi[2] += rho_u * f;
          vi[2] += rho_z * df;
          for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 2; ++j)
              vi[2] += rho_Z[i][j] * d2f[i][j];
          
          localVector[id].axpy(integrationFactor,vi);
        }
      }
      return;
    }


  private:

    const Integrand& rho;
    const DomainInfo& domainInfo;
    const Vector& vec_u;
    
    
    template<class Element, class LocalBasis, class LocalDomainType, class OrderType>
    static auto evaluatePartialDerivativesLocal(
      const LocalBasis& localBasis,
      const Element& e,
      const LocalDomainType& x,
      const OrderType& d)
    {
      typedef typename LocalBasis::Traits::RangeType LBRangeType;

      auto&& jacobianInverseTransposed = e.geometry().jacobianInverseTransposed(x);
      auto&& dLong = MultiIndexHelper::toLongMultiIndex(d);

      Dune::BlockVector<LBRangeType> y(localBasis.size());
      for (size_t i=0; i<localBasis.size(); ++i)
      {
        LBRangeType value(0);
    
        for( size_t k = 0; k < std::pow( d.size(), dLong.size() ); ++k )
        {
          auto&& betaLong = MultiIndexHelper::toLongMultiIndex( k, d.size(), dLong.size() );
          auto&& beta     = MultiIndexHelper::toShortMultiIndex<OrderType>( betaLong );
          LBRangeType transformationFactor(1);
          for( size_t l = 0; l < dLong.size(); ++l )
            transformationFactor    *= jacobianInverseTransposed[dLong[l]][betaLong[l]];
          if( std::abs(transformationFactor) > std::numeric_limits<LBRangeType>::epsilon()*32 )
            value    += transformationFactor * localBasis.evaluate(x, i, beta);
        }

        y[i]  = value;
      }
      
      return y;
    }
    
    template<size_t order, class LocalBasis, class Element, class X>
    auto basisValues(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      DUNE_THROW(Dune::NotImplemented, "Not implemented.");
    }
    
    template<class LocalBasis, class Element, class X>
    auto basisValues0(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      Dune::FieldVector<size_t,X::dimension> d(0);
      return evaluatePartialDerivativesLocal(localBasis, element, x, d);
    }
    
    template<class LocalBasis, class Element, class X>
    auto basisValues1(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      typedef typename LocalBasis::Traits::RangeType RT;
      Dune::BlockVector<Dune::FieldVector<double,X::dimension>> gradients(localBasis.size());
      
      for(int i = 0; i < X::dimension; ++i)
      {
        Dune::FieldVector<size_t,X::dimension> d(0);
        d[i]  = 1;

        const auto&& grad_i = evaluatePartialDerivativesLocal(localBasis, element, x, d);
        
        for(int k = 0; k < grad_i.size(); ++k)
          gradients[k][i] = grad_i[k];
      }

      return gradients;
    }
    
    template<class LocalBasis, class Element, class X>
    auto basisValues2(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      typedef typename LocalBasis::Traits::RangeType RT;
      Dune::BlockVector<Dune::FieldMatrix<double,X::dimension,X::dimension>> hessians(localBasis.size());
      
      for(int i = 0; i < X::dimension; ++i)
      {
        for(int j = 0; j <= i; ++j)
        {
          Dune::FieldVector<size_t,X::dimension> d(0);
          ++d[i];
          ++d[j];

          const auto&& hess_ij = evaluatePartialDerivativesLocal(localBasis, element, x, d);
          
          for(int k = 0; k < hess_ij.size(); ++k)
          {
            hessians[k][i][j] = hess_ij[k];
            hessians[k][j][i] = hess_ij[k];
          }
        }
      }

      return hessians;
    }


    template<class Basis, class Element, class Vec, class BC>
    auto applyBasisValues(const Basis& basis, const Element& e, const Vec& funcCoeffs, const BC& basisCoeffs) const
    {
      const auto& fe = basis.getLocalFiniteElement(e);
      const auto& localBasis = fe.localBasis();

      typedef typename std::decay<decltype(funcCoeffs[0])>::type FuncRT;
      typedef typename std::decay<decltype(basisCoeffs[0])>::type BasisRT;
      
      typedef Dune::FieldVector<BasisRT,FuncRT::dimension> RetType;
      RetType y;
      for(int k = 0; k < y.size(); ++k)
        y[k] *= 0;

      for(int localId = 0; localId < localBasis.size(); ++localId)
      {
        const auto index = basis.index(e,localId);
        if(basis.isConstrained(index))
        {
          const auto& constraints = basis.constraints(index);
          for (size_t w=0; w<constraints.size(); ++w)
            for(int k = 0; k < y.size(); ++k)
                y[k].axpy(constraints[w].factor * funcCoeffs[constraints[w].index][k], basisCoeffs[localId]);
        }
        else
          for(int k = 0; k < y.size(); ++k)
            y[k].axpy(funcCoeffs[index][k], basisCoeffs[localId]);
      }
      
      return y;
    }
};

#endif
