#ifndef DUNE_FICTITIOUS_NITSCHE_TRACE_ASSEMBLER_HH
#define DUNE_FICTITIOUS_NITSCHE_TRACE_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/particle/quadrature/localparameterizationquadrature.hh>
#include <dune/particle/quadrature/curvequadrature.hh>
#include <dune/particle/helper/derivativehelper.hh>


// Technically is missing the stabilization part.
template<class Integrand, class DomainInfo, class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1>>
class FictitiousNitscheTraceAssembler : public LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T>
{
  private:

    typedef LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T> Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename T::value_type float_x; 

    
  public:

    typedef typename Base::Element Element;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;

  
    FictitiousNitscheTraceAssembler( const Integrand& integrand, const DomainInfo& domainInfo )
    : integrand(integrand), domainInfo(domainInfo)
    {
        static_assert( dim == 2 && dim == dimworld, "Monge Gauge is only supported for dim=2." );
    }

    void indices( const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      isNonZero = !domainInfo.isOutside(element);
    }


    void assemble( const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      // Ensure that parameters are appropriate.
      assert(tFE.type() == element.type());
      assert(aFE.type() == element.type());
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "MongeGaugeDomainAssembler is only implemented for ansatz space=test space.");


      // Define and initialize variables.
      localMatrix = 0.0;

      // Skip outside-elements.
      if( domainInfo.isOutside(element) )
        return;

      if( domainInfo.isBoundary(element) )
        updateTracePart(element, localMatrix, tFE);
      
      return;
    }

  
  private:
  
    const Integrand& integrand;
    const DomainInfo& domainInfo;    
    
    void updateTracePart(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE) const
    {
      const bool isInside     = domainInfo.isInside(element);
      const auto& localBasis  = tFE.localBasis();

      int levelSetId = -1;
      for(int i = 0; i < domainInfo.numLevelSets(); ++i)
        if( domainInfo.isBoundary(element,i) )
        {
          levelSetId  = i;
          break;
        }
      if(levelSetId == -1)
        DUNE_THROW(Dune::Exception, "Unexpected error.");
      const auto& levelSetFunction = domainInfo.levelSetFunction(levelSetId);

      // Get an appropriate quadrature rule.
      #warning Still need to select a proper quadrature rule. Note that tensor based finite elements work differently than triangle-based finite elements.
      QuadratureRuleKey quadFE(tFE);
      QuadratureRuleKey quadKey   = quadFE.square();
      //~ const auto& quadratureRule  = CurveQuadratureCache<float_x,2>::rule(quadKey, element, domainInfo);
      const auto& quadratureRule  = CurveQuadrature<float_x>::template rule<2>(quadKey, element, domainInfo);

      // Loop over quadrature points.
      const auto&& geometry   = element.geometry();
      for( const auto& quadPoint : quadratureRule )
      {
        // Get quadrature point.
        const auto& xLocal    = quadPoint.position();
        const auto&& xGlobal  = geometry.global(xLocal); 
        
        // Evaluate necessary values.
        const auto& jacobianInverseTransposed   = geometry.jacobianInverseTransposed(xLocal);
        DerivativeHelper<decltype(jacobianInverseTransposed)> derivativeHelper( jacobianInverseTransposed );
        const auto&& u    = derivativeHelper.functionValues(localBasis,xLocal);
        const auto&& Du   = derivativeHelper.gradients(localBasis,xLocal);
        const auto&& D2u  = derivativeHelper.hessians(localBasis,xLocal);
        const auto&& D3u  = derivativeHelper.thirdDerivatives(localBasis,xLocal);
        
        // Get grid size.
        #warning Dirty way to get a measure of the element size. Requires shape regularity.
        const auto  h     = std::pow( geometry.integrationElement(xLocal), 1./dim );
            
        // Get normal.
        #warning We assume that only one level set function intersects element.
        typename std::decay<decltype(domainInfo.levelSetFunction(0).evaluateGradient(geometry.global(xLocal)))>::type normal;
        normal = levelSetFunction.evaluateGradient(xGlobal);
        normal /= normal.two_norm();

        // Compute trace part.
        const auto integrationFactor    = quadPoint.weight();
        {
          auto A = integrand.evaluateNitscheLocal(element, xLocal, normal, u, Du, D2u, D3u);
          A *= integrationFactor;
          localMatrix += A;
        }
        {
          auto A = integrand.evaluatePenaltyLocal(element, xLocal, h, normal, u, Du);
          A *= integrationFactor;
          localMatrix += A;
        }
      }
    }
};

#endif
