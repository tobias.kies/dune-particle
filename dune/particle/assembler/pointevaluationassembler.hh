#ifndef POINT_EVALUATION_ASSEMBLER_HH
#define POINT_EVALUATION_ASSEMBLER_HH


#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>

template<class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1>>
class PointEvaluationAssembler : public LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T>
{
    private:

        typedef LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T> Base;
        typedef typename TrialLocalFE::Traits::LocalBasisType::Traits::RangeType RangeType;


    public:

        typedef typename Base::Element Element;
        typedef typename Base::BoolMatrix BoolMatrix;
        typedef typename Base::LocalMatrix LocalMatrix;

        PointEvaluationAssembler( )
        {
        }


        void indices( const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
        {
            isNonZero = true;
        }


        void assemble( const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
        {
            // Ensure that parameters are appropriate.
            assert(tFE.type() == element.type());
            assert(aFE.type() == element.type());

            // Define and initialize variables.
            const size_t rows   = localMatrix.N();
            const size_t cols   = localMatrix.M();
            localMatrix = 0.0;

            const auto&& geometry           = element.geometry();
            const auto& testLocalBasis      = tFE.localBasis();
            const auto& ansatzLocalBasis    = aFE.localBasis();
            std::vector<RangeType> functionValues(ansatzLocalBasis.size());

            for( size_t pointId = 0; pointId < rows; ++pointId )
            {
                // Get point and transform it to local coordinates.
                const auto& xGlobal = testLocalBasis.centerOfMass(pointId);
                const auto& xLocal  = geometry.local(xGlobal);

                // Evaluate basis functions in this point.
                ansatzLocalBasis.evaluateFunction(xLocal, functionValues);

                // Update matrix.
                for( size_t colId = 0; colId < cols; ++colId )
                    localMatrix[pointId][colId] = functionValues[colId][0];
            }

            return;
        }
};


#endif

