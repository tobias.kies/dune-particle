#ifndef DUNE_FICTITIOUS_NITSCHE_VOLUME_ASSEMBLER_HH
#define DUNE_FICTITIOUS_NITSCHE_VOLUME_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/particle/quadrature/quadraturecaches.hh>
#include <dune/particle/helper/derivativehelper.hh>


// Technically is missing the stabilization part.
template<class Integrand, class DomainInfo, class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1>>
class FictitiousNitscheVolumeAssembler : public LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T>
{
  private:

    typedef LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T> Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename T::value_type float_x; 

    
  public:

    typedef typename Base::Element Element;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;

  
    FictitiousNitscheVolumeAssembler( const Integrand& integrand, const DomainInfo& domainInfo )
    : integrand(integrand), domainInfo(domainInfo)
    {
        static_assert( dim == 2 && dim == dimworld, "Monge Gauge is only supported for dim=2." );
    }

    void indices( const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      isNonZero = !domainInfo.isOutside(element);
    }


    void assemble( const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      // Ensure that parameters are appropriate.
      assert(tFE.type() == element.type());
      assert(aFE.type() == element.type());
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "MongeGaugeDomainAssembler is only implemented for ansatz space=test space.");


      // Define and initialize variables.
      localMatrix = 0.0;

      // Skip outside-elements.
      if( domainInfo.isOutside(element) )
        return;

      updateVolumePart(element, localMatrix, tFE);
      
      return;
    }

  
  private:
  
    const Integrand& integrand;
    const DomainInfo& domainInfo;
    
    
    void updateVolumePart(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE) const
    {
      const bool isInside     = domainInfo.isInside(element);
      const auto& localBasis  = tFE.localBasis();
      
      // Get an appropriate quadrature rule.
      #warning Still need to select a proper quadrature rule. Note that tensor based finite elements work differently than triangle-based finite elements.
      QuadratureRuleKey quadFE(tFE);
      QuadratureRuleKey quadKey   = quadFE.square();
      Dune::QuadratureRule<float_x,dim> quadratureRule;
      if( !isInside ) // element is on boundary and we do not want to compute the bulk operator
      {
        #warning We have the assumption that there is at most one level set function per element.
        //~ quadratureRule  = LocalParameterizationQuadratureCache<float_x>::rule(quadKey, element, domainInfo);
        quadratureRule  = LocalParameterizationQuadrature<float_x>::rule(quadKey, element, domainInfo);
      }
      else
      {
        #warning . (Because of the special structure of our basis functions we can get away with a lower order here.)
        quadKey.setOrder(localBasis.order()*2);
        quadratureRule  = QuadratureRuleCache<float_x, dim>::rule(quadKey);
      }


      // Loop over quadrature points.
      const auto&& geometry   = element.geometry();
      for( const auto& quadPoint : quadratureRule )
      {
        // Get quadrature point.
        const auto& xLocal  = quadPoint.position();
        
        // Evaluate necessary values.
        const auto& jacobianInverseTransposed   = geometry.jacobianInverseTransposed(xLocal);
        DerivativeHelper<decltype(jacobianInverseTransposed)> derivativeHelper( jacobianInverseTransposed );
        const auto&& u    = derivativeHelper.functionValues(localBasis,xLocal);
        const auto&& Du   = derivativeHelper.gradients(localBasis,xLocal);
        const auto&& D2u  = derivativeHelper.hessians(localBasis,xLocal);
        
        // Compute volume part.
        const auto integrationFactor    = quadPoint.weight() * geometry.integrationElement(xLocal);
        auto A = integrand.evaluateVolumeLocal(element, xLocal, u, Du, D2u);
        A *= integrationFactor;
        localMatrix += A;
      }
    }
};

#endif
