#ifndef DUNE_FICTITIOUS_NITSCHE_ASSEMBLER_HH
#define DUNE_FICTITIOUS_NITSCHE_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localoperatorassembler.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/particle/helper/derivativehelper.hh>


// Technically is missing the stabilization part.
template<class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1>>
class BilaplaceAssembler : public LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T>
{
  private:

    typedef LocalOperatorAssembler<GridType, TrialLocalFE, AnsatzLocalFE, T> Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename T::value_type float_x; 

    
  public:

    typedef typename Base::Element Element;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;

  
    BilaplaceAssembler( )
    {}

    void indices( const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      isNonZero = true;
    }


    void assemble( const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      // Ensure that parameters are appropriate.
      assert(tFE.type() == element.type());
      assert(aFE.type() == element.type());
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "MongeGaugeDomainAssembler is only implemented for ansatz space=test space.");


      // Define and initialize variables.
      localMatrix = 0.0;

      updateVolumePart(element, localMatrix, tFE);

      return;
    }

  
  private:

    void updateVolumePart(const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE) const
    {
      const auto& localBasis  = tFE.localBasis();
      const int n = localBasis.size();
      
      // Get an appropriate quadrature rule.
      #warning . (Because of the special structure of our basis functions we can get away with a lower order here.)
      QuadratureRuleKey quadKey(tFE);
      quadKey.setOrder(localBasis.order()*2);
      const auto& quadratureRule  = QuadratureRuleCache<float_x, dim>::rule(quadKey);


      // Loop over quadrature points.
      const auto&& geometry   = element.geometry();
      for( const auto& quadPoint : quadratureRule )
      {
        // Get quadrature point.
        const auto& xLocal  = quadPoint.position();
        
        // Evaluate necessary values.
        const auto& jacobianInverseTransposed   = geometry.jacobianInverseTransposed(xLocal);
        DerivativeHelper<decltype(jacobianInverseTransposed)> derivativeHelper( jacobianInverseTransposed );
        const auto&& delta_u = derivativeHelper.laplacians(localBasis, xLocal);
        
        // Compute volume part.
        const auto integrationFactor    = quadPoint.weight() * geometry.integrationElement(xLocal);
        for(int i = 0; i < n; ++i)
          for(int j = 0; j < n; ++j)
            localMatrix[i][j] += integrationFactor * delta_u[i] * delta_u[j];
      }
    }
};

#endif
