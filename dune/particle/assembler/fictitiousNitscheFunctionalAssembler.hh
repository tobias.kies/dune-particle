#ifndef DUNE_FICTITIOUS_NITSCHE_FUNCTIONAL_ASSEMBLER_HH
#define DUNE_FICTITIOUS_NITSCHE_FUNCTIONAL_ASSEMBLER_HH

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

#include <dune/particle/quadrature/localparameterizationquadrature.hh>
#include <dune/particle/quadrature/curvequadrature.hh>
#include <dune/particle/helper/derivativehelper.hh>


// Technically is missing the stabilization part.
template<class Integrand, class DomainInfo, class GridType, class TrialLocalFE, class T=Dune::FieldVector<double,1>>
class FictitiousNitscheFunctionalAssembler : public LocalFunctionalAssembler<GridType, TrialLocalFE, T>
{
  private:

    typedef LocalFunctionalAssembler<GridType, TrialLocalFE, T> Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename T::value_type float_x;

    typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;

    //~ typedef BasisMultiDifferentiableGridFunction<Basis, Dune::BlockVector<T>, Dune::FieldVector<size_t,dimworld>> GridFunction;


  public:

    typedef typename Base::Element Element;
    typedef typename Base::LocalVector LocalVector;

    typedef typename Dune::VirtualMultiDifferentiableFunction<GlobalCoordinate,T,Dune::FieldVector<size_t,dimworld>> Function;

    FictitiousNitscheFunctionalAssembler(const Integrand& integrand, const DomainInfo& domainInfo, const bool fTensorBasis = false, const int onlyLevelSetId = -1)
    : integrand(integrand), domainInfo(domainInfo), fTensorBasis(fTensorBasis), onlyLevelSetId(onlyLevelSetId)
    {
        static_assert( dim == 2 && dim == dimworld, "Only supported for dim=2." );
    }

    void assemble(const Element& element, LocalVector& localVector, const TrialLocalFE& tFE) const
    {
      const auto&& geometry   = element.geometry();
      const auto& localBasis  = tFE.localBasis();
      localVector             = 0.0;

      int levelSetId = -1;
      for(int i = 0; i < domainInfo.numLevelSets(); ++i)
        if( domainInfo.isBoundary(element,i) )
        {
          levelSetId  = i;
          break;
        }
      if(levelSetId == -1)
        return;
      if(onlyLevelSetId >= 0 && levelSetId != onlyLevelSetId)
        return;
        
      const auto& levelSetFunction = domainInfo.levelSetFunction(levelSetId);

      // Cast function to grid function.
      //~ const GridFunction* gf = dynamic_cast<const GridFunction*>(&f);
      //~ if( !(gf && gf->isDefinedOn(element)) )
        //~ DUNE_THROW( Dune::NotImplemented, "Only local evaluations are supported in MongeGaugeDerivativeAssembler." );

      // Get an appropriate quadrature rule.
      QuadratureRuleKey tFEquad(tFE);
      if( fTensorBasis ) // change order in case of tensor basis
        tFEquad.setOrder(localBasis.order());

      //~ const auto uFE = gf->basis().getLocalFiniteElement(element);
      //~ QuadratureRuleKey uFEQuadKey( uFE );
      //~ auto&& functionQuadKey = uFEQuadKey.derivative();
      //~ if( fTensorBasis )
        //~ functionQuadKey.setOrder( uFE.localBasis().order() );
      #warning Right now the functional assembler sets a rather arbitrary order.
      QuadratureRuleKey functionQuadKey(tFE);
      if( fTensorBasis ) // change order in case of tensor basis
        functionQuadKey.setOrder(localBasis.order());

      QuadratureRuleKey quadKey = tFEquad.product(functionQuadKey);
      //~ const auto& quadratureRule  = CurveQuadratureCache<float_x,2>::rule(quadKey, element, domainInfo);
      const auto& quadratureRule  = CurveQuadrature<float_x>::template rule<2>(quadKey, element, domainInfo);

      // Loop over quadrature points.
      for( const auto& quadPoint : quadratureRule )
      {
        // Get quadrature information.
        const auto& xLocal              = quadPoint.position();
        const auto&& xGlobal  = geometry.global(xLocal); 
        const auto integrationFactor    = quadPoint.weight();

        // Evaluate necessary values from the basis functions.
        const auto& jacobianInverseTransposed   = geometry.jacobianInverseTransposed(xLocal);
        DerivativeHelper<decltype(jacobianInverseTransposed)> derivativeHelper( jacobianInverseTransposed );
        const auto&& u    = derivativeHelper.functionValues(localBasis,xLocal);
        const auto&& Du   = derivativeHelper.gradients(localBasis,xLocal);
        const auto&& D2u  = derivativeHelper.hessians(localBasis,xLocal);
        const auto&& D3u  = derivativeHelper.thirdDerivatives(localBasis,xLocal);
        
        // Get grid size.
        #warning Dirty way to get a measure of the element size. Requires shape regularity.
        const auto  h     = std::pow( geometry.integrationElement(xLocal), 1./dim );
            
        // Get normal.
        #warning We assume that only one level set function intersects element.
        typename std::decay<decltype(domainInfo.levelSetFunction(0).evaluateGradient(xGlobal))>::type normal;
        normal = levelSetFunction.evaluateGradient(xGlobal);
        normal /= normal.two_norm();

        // Update vector.
        localVector.axpy(integrationFactor, integrand.evaluateNitscheFunctionalLocal(element, xLocal, normal, u, Du, D2u, D3u));
        localVector.axpy(integrationFactor, integrand.evaluatePenaltyFunctionalLocal(element, xLocal, h, normal, u, Du));
      }
      return;
    }


  private:

    const Integrand& integrand;
    const DomainInfo& domainInfo;
    const int onlyLevelSetId;

    const bool fTensorBasis;
};

#endif
