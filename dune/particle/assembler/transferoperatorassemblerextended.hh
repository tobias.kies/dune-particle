// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_ASSEMBLERS_TRANSFER_OPERATOR_ASSEMBLER_EXTENDED_HH
#define DUNE_FUFEM_ASSEMBLERS_TRANSFER_OPERATOR_ASSEMBLER_EXTENDED_HH

#include <dune/fufem/assemblers/transferoperatorassembler.hh>
#include <dune/particle/functions/basismultidifferentiablegridfunction.hh>
#include <dune/fufem/functiontools/basisinterpolator.hh>


template<class Basis, class LevelBasis>
class MultilevelBasisExtended
{
  private:

    typedef typename Basis::GridView::Traits::Grid GridType;
    static const int dim = GridType::dimension;

    typedef std::shared_ptr<LevelBasis> LevelBasisPtr;
    typedef std::vector<LevelBasisPtr> LevelBases;

    typedef typename GridType::Traits::GlobalIdSet::IdType IdType;


  public:

    typedef typename GridType::template Codim<0>::Entity Element;
    typedef typename LevelBasis::LocalFiniteElement LocalFiniteElement;


    MultilevelBasisExtended( const Basis& basis_ ) :
      basis_(basis_),
      grid(basis_.getGridView().grid())
    {
      // Prepare some containers.
      const size_t maxLevel = grid.maxLevel( );
      levelBases.resize(maxLevel+1);
      size_.resize(maxLevel+1);
      idToIndex.resize(maxLevel+1);

      // We create a level basis for each given grid level.
      for( size_t level = 0; level <= maxLevel; ++level )
      {
        levelBases[level]   = std::make_shared<LevelBasis>( grid.levelGridView(level) );
        size_[level]        = levelBases[level]->size();
      }

      // Now we initialize a mapping that maps for each level
      // entity ids (and multiplicities) to a global basis id
      // with respect to the level grid view (including entities
      // from coarser grids, if necessary).
      // This first starts by copying indices from the levelBases.
      // (Also see the original code for TransferOperatorAssembler
      // for a pretty picture and an explanation.)

      // So, for each level...
      for( size_t level = 0; level <= maxLevel; ++level )
      {
        const auto& levelBasis  = *levelBases[level];
        auto& entityToIndex   = idToIndex[level];

        // ... and on each element...
        for( const auto& e : elements(grid.levelGridView(level)) )
        {
          const auto& fe = getLocalFiniteElement(e);
          const auto& localBasis = fe.localBasis();

          // ... we look at all local degrees of freedom...
          for( size_t localBasisId = 0; localBasisId < localBasis.size(); ++localBasisId )
          {
            // ... and find the geometric entity that every
            // respective degree of freedom is attached to.
            // (Together with the multiplicity.)
            // This is just given by the localKey.
            const auto& localKey    = fe.localCoefficients().localKey(localBasisId);
            const auto& entityId    = grid.globalIdSet().subId( e, localKey.subEntity(), localKey.codim() );
            const auto& multiplicity  = localKey.index();

            // With this information it is now possible to
            // update the entity-to-index map.
            const auto& globalBasisId   = levelBasis.index(e, localBasisId);
            auto& multiplicityToIndex   = entityToIndex[entityId];
            multiplicityToIndex[multiplicity]   = globalBasisId;
          }
        }
      }

      // Then we extend the mappings by dummy indices.
      // This is done by looking at each entity of the leaf grid
      // that is associated to a degree of freedom,
      // determining what level it actually belongs to, and then
      // "extending" it artificially to all above levels.
      // So, for each leaf element...
      for( const auto& e : elements(grid.leafGridView()) )
      {
        const auto& fe = getLocalFiniteElement(e);
        const auto& localBasis = fe.localBasis();

        // ... and each leaf basis function...
        for( size_t localBasisId = 0; localBasisId < localBasis.size(); ++localBasisId )
        {
          // ... we determine the associated element id
          // and multiplicity.
          const auto& localKey      = fe.localCoefficients().localKey(localBasisId);
          const auto& entityId      = grid.globalIdSet().subId( e, localKey.subEntity(), localKey.codim() );
          const auto& multiplicity  = localKey.index();

          // We now loop through every level that contains the
          // current element but may not explicitly have it
          // in its level view...
          for( size_t level = e.level()+1; level <= maxLevel; ++level )
          {
            // ... and add a dummy index for the given entity
            // if it does not exist yet.
            auto& entityToIndex     = idToIndex[level];
            const auto& itMultToId  = entityToIndex.find(entityId);
            if( itMultToId != entityToIndex.end() )
              if( itMultToId->second.count(multiplicity) > 0 )
                continue;

            entityToIndex[entityId][multiplicity]   = size_[level];
            ++size_[level];
          }


          // We also can use this opportunity to populate the
          // other index mapping.
          const auto& leafBasisId   = basis_.index(e,localBasisId);
          maxlevelIndexToLeafIndex_[idToIndex[maxLevel].at(entityId).at(multiplicity)]  = leafBasisId;


          /* This part seems to be wrong. Maybe have a look at it later.
           *
          // Now we look in each coarser level...
          // Remark: There may be a more efficient way do the
          //     following by retrieving the level of the
          //     determined element directly.
          //
          // Also: While doing all of this we alsocompute a
          // map that allows us to map an index from the
          // maxLevel levelBasis to the corresponding index of
          // the leafBasis.
          for( size_t level = e.level()+1; level <= maxLevel; ++level )
          {
            auto& entityToIndex   = idToIndex[level];

            // And unless the given entity is present
            // together with the right multiplicity...
            const auto& itMultToId = entityToIndex.find(entityId);
            if( itMultToId != entityToIndex.end() )
            {
              const auto& itBasisIndex = itMultToId->second.find(multiplicity);
              if( itBasisIndex != itMultToId->second.end() )
              {
                // If we are on maxLevel we can use this
                // opportunity to update our other
                // index map.
                if( level == maxLevel )
                  maxlevelIndexToLeafIndex_[itBasisIndex->second]  = leafBasisId;
                continue;
              }
            }

            // ... we add a new index to the mapping by just
            // adding an artificial new id.
            // In that case we can again update our other
            // index map.
            entityToIndex[entityId][multiplicity]   = size_[level];
            maxlevelIndexToLeafIndex_[size_[level]] = leafBasisId;
            ++size_[level];
          }
          */
        }
      }
      
      
      // Debug output:
      /*std::cout << " === Transfer Operator MultiBasis === " << std::endl;
      std::cout << " - entityToIndex - " << std::endl;
      for( size_t level = 0; level < idToIndex.size(); ++level )
      {
        const auto& entityToIndex = idToIndex[level];
        std::cout << "Level " << level << ", size " << entityToIndex.size() << std::endl;
        for( const auto& it : entityToIndex )
          for( const auto& it2 : it.second )
            std::cout << "\t" << it.first << "\t" << it2.first << "\t" << it2.second << std::endl;
        std::cout << std::endl;
      }
      std::cout << std::endl;
      
      std::cout << " - maxlevelIndexToLeafIndex - " << std::endl;
      for( const auto& it : maxlevelIndexToLeafIndex_ )
        std::cout << "\t" << it.first << "\t" << it.second << std::endl;
      std::cout << std::endl;*/
    }


    const LocalFiniteElement& getLocalFiniteElement( const Element& e ) const
    {
      return levelBases[e.level()]->getLocalFiniteElement(e);
    }


    int size( const int level ) const
    {
      return size_[level];
    }


    size_t index( const Element& e, const size_t i, const size_t level ) const
    {
      // Get the local key describing the (local) geometric entity
      // associated to the local DOF i (including multiplicity).
      const auto& localKey = getLocalFiniteElement(e).localCoefficients().localKey(i);

      // Get the global id of this entity.
      const auto& entityId = grid.globalIdSet().subId( e, localKey.subEntity(), localKey.codim() );

      // Return the corresponding index.
      return idToIndex[level].at(entityId).at(localKey.index());
    }


    const LevelBasis& basis( const size_t level ) const
    {
      return *levelBases[level];
    }


    const auto& maxlevelIndexToLeafIndex( const size_t maxlevelIndex ) const
    {
      return maxlevelIndexToLeafIndex_.at(maxlevelIndex);
    }


    const auto& indexSet( ) const
    {
      return idToIndex;
    }

  private:

    const Basis& basis_;
    const GridType& grid;

    std::vector<size_t> size_;
    LevelBases levelBases;

    // The following represents a function that takes a level, and
    // an entity id and a multiplicity index and maps it to a basis
    // index.
    typedef typename std::map<size_t,size_t> MultiplicityToBasisIndex;
    typedef typename std::map<IdType,MultiplicityToBasisIndex> EntityIdToBasisIndex;
    typedef typename std::vector<EntityIdToBasisIndex> LevelToBasisIndex;
    LevelToBasisIndex idToIndex;

    // The following represents a map that maps a global basis index
    // of the maxLevel levelBasis to the corresponding global basis
    // index of the leafBasis.
    typedef typename std::map<size_t,size_t> MaxlevelIndexToLeafIndex;
    MaxlevelIndexToLeafIndex maxlevelIndexToLeafIndex_;

};


// This class extends the classical TransferOperatorAssembler for P1
// finite elements to arbitrary bases.
// This (as of now) comes at the cost of less computational efficiency.
template<class Basis, class LevelBasis>
class TransferOperatorAssemblerExtended
  //: public TransferOperatorAssembler<typename Basis::GridView::Traits::Grid>
{
  private:

    //typedef TransferOperatorAssembler<typename Basis::GridView::Traits::Grid> BaseClass;
    typedef typename Basis::GridView::Traits::Grid GridType;


  public:

    TransferOperatorAssemblerExtended( const Basis& basis ) :
      //BaseClass(basis.getGridView().grid()),
      basis(basis),
      grid(basis.getGridView().grid()),
      multilevelBasis(basis)
    {}


    // The following are literally the same parts as in the base class.
    // Is it somehow possible to "copy" the base class and replace
    // one method there completely, such that the other methods in
    // the base class are all just using that new method instead of
    // the old one?

    template <class TransferOperator>
    void assembleOperatorHierarchy(std::vector<TransferOperator>& T) const
    {
      std::vector <typename TransferOperator::TransferOperatorType*> M;

      int maxLevel = grid.maxLevel();

      M.resize(maxLevel);

      for (int i=0; i<maxLevel; ++i)
        M[i] = &(const_cast<typename TransferOperator::TransferOperatorType&>(T[i].getMatrix()));
      assembleMatrixHierarchy(M);
    }

    template <class TransferOperator, class RealTransferOperator>
    void assembleDerivedOperatorPointerHierarchy(std::vector<TransferOperator*>& T) const
    {
      typedef typename RealTransferOperator::TransferOperatorType TransferOperatorType;
      std::vector <TransferOperatorType*> M;

      int maxLevel = grid.maxLevel();

      M.resize(maxLevel);

      for (int i=0; i<maxLevel; ++i)
      {
        RealTransferOperator* t = dynamic_cast<RealTransferOperator*>(T[i]);
        M[i] = &(const_cast<TransferOperatorType&>(t->getMatrix()));
      }
      assembleMatrixHierarchy(M);
    }

    template <class TransferOperator>
    void assembleOperatorPointerHierarchy(std::vector<TransferOperator*>& T) const
    {
      std::vector <typename TransferOperator::TransferOperatorType*> M;

      int maxLevel = grid.maxLevel();

      M.resize(maxLevel);

      for (int i=0; i<maxLevel; ++i)
        M[i] = &(const_cast<typename TransferOperator::TransferOperatorType&>(T[i]->getMatrix()));
      assembleMatrixHierarchy(M);
    }

    /**
     * \brief  assemble hierarchy of transfer operators
     *
     * @param T std::vector of shared_ptr's to matrices for interpolation operators
     *
     * If the vector's size is smaller than grid.maxLevel(), then it is
     * filled up with newly allocated matrices.
     */
    template <class Matrix>
    void assembleMatrixHierarchy(std::vector<std::shared_ptr<Matrix> >& M) const
    {
      while(M.size() < uint(grid.maxLevel()))
        M.push_back(std::make_shared<Matrix>());

      std::vector <Matrix*> Mraw(M.size());
      for (size_t i=0; i<M.size(); ++i)
        Mraw[i] = M[i].get();

      assembleMatrixHierarchy(Mraw);
    }


    /**
     * \brief Assemble a hierarchy of transfer operators for the given basis.
     *
     * @param T std::vector of pointers to matrices for interpolation operators.
     *
     * The vector is required to have grid.maxLevel() entries pointing
     * to already allocated matrices.
     */
    // Note: So far this algorithm will fail if there are elements
    //     which get additional degrees of freedom after refinement.
    template<class Matrix>
    void assembleMatrixHierarchy( std::vector<Matrix*>& T ) const
    {
      // Typedefs for "TransformationHierarchy".
      // We basically use this as a "sparse" container for the
      // interpolation coefficients that we need to set up the
      // transfer operators.
      typedef std::map<int, double> LinearCombination;
      typedef std::vector<LinearCombination> BaseTransformation;
      typedef std::vector<BaseTransformation> TransformationHierarchy;

      // Other typedefs and variables that may be useful later on.
      typedef typename Basis::LocalFiniteElement::Traits::LocalBasisType::Traits::RangeType FERangeType;

      // Maximum grid level that is in use.
      const size_t maxLevel = grid.maxLevel();

      // The multilevel basis emulates a basis on each level of
      // the grid (including elements from lower levels, if there
      // is no element from a higher level to replace them).
      // NOTE: This line has been moved into the constructor in order
      //        to make the basis available to external routines.
      // MultilevelBasisExtended<Basis, LevelBasis> multilevelBasis(basis);

      // We set up transformationHierarchy, a container for all
      // the interpolation coefficients later on.
      TransformationHierarchy transformationHierarchy(maxLevel);
      for( size_t level = 0; level < maxLevel; ++level )
        transformationHierarchy[level].resize(multilevelBasis.size(level+1));

      // We loop from the coarsest to the finest level and always
      // compute the transfer operator's coefficients from the
      // current grid to the next finer grid.
      for( size_t level = 0; level < maxLevel; ++level )
      {
        // We loop over every element in order to determine the
        // coefficients locally.
        for( const auto& e : elements(grid.levelGridView(level)) )
        {
          const auto& coarseFE = multilevelBasis.getLocalFiniteElement(e);

          // If the current element is on leaf level, i.e. it
          // is not being divided any further on finer levels,
          // then we know that the transfer operator is the
          // identity for all DOFs emerging from this element
          // and for all levels that are at least as fine as
          // this one.
          if( e.isLeaf() )
          {
            for( size_t coarseLevel = level; coarseLevel < maxLevel; ++coarseLevel )
            {
              const size_t fineLevel = coarseLevel + 1;

              // (Note that we can assume that the localBasis stays constant, too.)
              for( size_t localBasisId = 0; localBasisId < coarseFE.localBasis().size(); ++localBasisId )
              {
                const size_t fineIndex  =
                 (fineLevel == maxLevel)
                 ? multilevelBasis.maxlevelIndexToLeafIndex( multilevelBasis.index(e, localBasisId, fineLevel) )
                 : multilevelBasis.index(e, localBasisId, fineLevel);
                const size_t coarseIndex  = multilevelBasis.index(e, localBasisId, coarseLevel);
                transformationHierarchy[coarseLevel][fineIndex][coarseIndex]  = 1.0;
              }
            }
          }
          // Else we need to compute the coefficients by
          // interpolating the coarse local basis functions
          // with respect to the fine local basis functions.
          else
          {
            const size_t coarseLevel  = level;
            const size_t fineLevel    = level+1;

            const auto& coarseBasis   = multilevelBasis.basis(coarseLevel);
            const auto& fineBasis     = multilevelBasis.basis(fineLevel);

            // We loop over all child elements to determine the relevant degrees of freedom.
            // We also save a list of those indices for efficient access later.
            Dune::BitSetVector<1> interpolatedDOFs( fineBasis.size(), false ); // Important here: Note that in general multilevelBasis.basis(fineLevel).size() != multilevelBasis.size(fineLevel) because the latter may also contain dummy indices.
            std::set<size_t> fineIndices; // We can not know its size a-priori, can we?
            for( const auto& fine_e : descendantElements(e, fineLevel) )
            {
              const auto& localBasis = multilevelBasis.getLocalFiniteElement(fine_e).localBasis();
              for( size_t localBasisId = 0; localBasisId < localBasis.size(); ++localBasisId )
              {
                const size_t fineIndex = fineBasis.index(fine_e, localBasisId);
                fineIndices.insert(fineIndex);
                interpolatedDOFs[fineIndex] = true;
              }
            }

            // We loop over all coarse local basis functions and interpolate them.
            const auto& coarseFE = multilevelBasis.getLocalFiniteElement(e);
            for( size_t localBasisId = 0; localBasisId < coarseFE.localBasis().size(); ++localBasisId )
            {
              // We define a function that represents our finite element function.
              // Remark: I am pretty sure there must be somewhere some wrapper
              //     that basically allows to evaluate a local basis function
              //     with respect to a finer grid. But where is it?
              //     Maybe there is even a version with caching?
              //     (Also it probably would have to be rewritten in order to
              //     fit the MultiDifferentiable interface.)
              // Remark: Note that the implementation here is rather inefficient.
              //     This is because we will perform many unnecessary steps by
              //     evaluating the same points multiple times and also because
              //     we do not define the coarse basis function as a grid
              //     function with respect to the fine grid, which makes the
              //     local interpolation less efficient.
              const size_t coarseIndex  = coarseBasis.index(e, localBasisId);
              typedef Dune::BlockVector<FERangeType> Vector;  // What is the correct type here in case we work with multiple components?
              typedef BasisMultiDifferentiableGridFunction<LevelBasis,Vector> F;
              Vector v( coarseBasis.size(), 0 ); // Important here: Note that in general multilevelBasis.basis(coarseLevel).size() != multilevelBasis.size(coarseLevel) because the latter may also contain dummy indices.
              v[coarseIndex]  = 1;
              F f( multilevelBasis.basis(coarseLevel), v );
              f.setCachedElement(e);


              // Perform the actual interpolation.
              // Remark: It would be more efficient to only do a local interpolation here.
              //     But it is also nice to hand that task to somewhere also (so that
              //     we do not have to think about component-wise interpolation, for
              //     example).
              // To-Do: Would be nicer if we could avoid iteration over un-needed elements completely.
              // TO-Do: And of course it would be really, really nice if we would store the function evaluations...
              Vector coeffs;
              // Replaced this call by a more optimized version Functions::interpolate( multilevelBasis.basis(fineLevel), coeffs, f, interpolatedDOFs );
              interpolate( multilevelBasis.basis(fineLevel), coeffs, f, interpolatedDOFs, descendantElements(e, fineLevel) );

              // Save the interpolation coefficients in the
              // appropriate part of the transfer hierarchy.
              for( const auto& fineIndex : fineIndices )
                if( std::abs(coeffs[fineIndex]) > 1e-10 )
                  if( fineLevel == maxLevel ) // As one specialty: We need to apply a permutation of the indices on the finest level.
                    transformationHierarchy[coarseLevel][multilevelBasis.maxlevelIndexToLeafIndex(fineIndex)][coarseIndex]  = coeffs[fineIndex];
                  else
                    transformationHierarchy[coarseLevel][fineIndex][coarseIndex]  = coeffs[fineIndex];
            }
          }
        }
      }


      // Now that we know all coefficients we can set up the
      // transfer operator matrices.
      for( size_t level = 0; level < maxLevel; ++level )
      {
        const size_t nCoarse  = multilevelBasis.size(level);
        const size_t nFine    = multilevelBasis.size(level+1);
        auto& transformation  = transformationHierarchy[level];
        auto& mat         = *T[level];

        std::cerr << "sizes on level " << level << " are " << nFine << " and " << nCoarse << std::endl;

        // First we determine and export the matrix occupation pattern.
        Dune::MatrixIndexSet indices(nFine, nCoarse);

        for( size_t rowId = 0; rowId < nFine; ++rowId )
          for( const auto& colIt : transformation[rowId] )
            indices.add( rowId, colIt.first );

        indices.exportIdx(mat);

        // Then we populate the matrix with the corresponding entries.
        mat *= 0;
        for( size_t rowId = 0; rowId < nFine; ++rowId )
          for( const auto& colIt : transformation[rowId] )
            for( size_t i = 0; i < Matrix::block_type::rows; ++i )
              mat[rowId][colIt.first][i][i]   = colIt.second;

        // We no longer need the current transformation.
        // Remark: But it would not be much longer around anyway.
        //     What is the real point behind clearing it then?
        transformation.clear();
      }
    }
    
    
    // Returns a reference to the multiLevelBasis in use.
    const auto& bases() const
    {
      return multilevelBasis;
    }


  private:
  
  
    // This is just a copy of the standard basis interpolator.
    // Only with the slight difference that we now can specify a
    // subset of elements that we want to traverse.
    // Maybe we could update the basisinterpolator to add this feature?
    template <class B, class RepresentationType, class F, class BitVectorType, class Elements>
    static void interpolate(const B& basis, RepresentationType& coeff, const F& f, const BitVectorType& interpolateDOFFlags, const Elements& elements )
    {
      typedef typename B::GridView GridView;
      typedef typename B::GridView::Traits::Grid Grid;
      
      typedef typename GridView::template Codim<0>::Iterator ElementIterator;
      typedef typename B::LocalFiniteElement LocalFiniteElement;

      typedef typename Dune::LocalFiniteElementFunctionBase<typename B::LocalFiniteElement>::type FunctionBaseClass;
      typedef LocalFunctionComponentWrapper<F, Grid, FunctionBaseClass> LocalWrapper;

      const GridView& gridview = basis.getGridView();

      if (coeff.size() != basis.size())
        coeff.resize(basis.size());

      typename Dune::BitSetVector<1> processed(basis.size(), false);
      for(size_t i=0; i<basis.size(); ++i)
      {
        if (interpolateDOFFlags[i].any())
        {
          if (basis.isConstrained(i))
          {
            coeff[i] = 0;
            processed[i][0] = true;
          }
        }
        else
          processed[i][0] = true;
      }

      std::vector<typename LocalWrapper::RangeType> interpolationValues;

      for( const auto& element : elements )
      {
        const LocalFiniteElement& fe = basis.getLocalFiniteElement(element);

        // check if all components have already been processed
        bool allProcessed = true;
        for(size_t i=0; i<fe.localBasis().size(); ++i)
          allProcessed = allProcessed and processed[basis.index(element, i)][0];

        if (not(allProcessed))
        {
          LocalWrapper fjLocal(f, element, 0);

          size_t lastComponent = Components<typename F::RangeType>::size(coeff[0])-1;
          for(size_t j=0; j<=lastComponent; ++j)
          {
            fjLocal.setIndex(j);
            fe.localInterpolation().interpolate(fjLocal, interpolationValues);

            for(size_t i=0; i<fe.localBasis().size(); ++i)
            {
              size_t index = basis.index(element, i);

              // check if DOF is unprocessed
              if (not(processed[index][0]))
              {
                Components<typename F::RangeType>::setComponent(coeff[index], j, interpolationValues[i]);
                if (j==lastComponent)
                  processed[index][0] = true;
              }
            }
          }
        }
      }
    }

    const Basis& basis;
    const GridType& grid;
    const MultilevelBasisExtended<Basis, LevelBasis> multilevelBasis;
};

#endif // DUNE_FUFEM_ASSEMBLERS_TRANSFER_OPERATOR_ASSEMBLER_EXTENDED_HH
