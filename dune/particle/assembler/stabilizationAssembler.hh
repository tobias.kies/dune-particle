#ifndef DUNE_STABILIZATION_ASSEMBLER_HH
#define DUNE_STABILIZATION_ASSEMBLER_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <dune/istl/matrix.hh>

#include <dune/fufem/assemblers/localassembler.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

template<class Basis, class DomainInfo, class GridType, class TrialLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1>>
class StabilizationAssembler : public LocalAssembler<GridType, TrialLocalFE, AnsatzLocalFE,T>
{
  private:
    typedef LocalAssembler < GridType, TrialLocalFE, AnsatzLocalFE ,T > Base;
    static const int dim    = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;
    typedef typename T::value_type float_x;
    typedef typename TrialLocalFE::Traits::LocalBasisType::OrderType OrderType;

  public:
    typedef typename Base::Element Element;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;

    StabilizationAssembler(const Basis& basis, const DomainInfo& domainInfo )
    : basis(basis), domainInfo(domainInfo)
    {
      static_assert( dim == 2 && dim == dimworld, "Only dim=2 is supported." );
    }

    void indices( const Element& element, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      DUNE_THROW( Dune::NotImplemented, "This assembler is only supposed to be called for intersections." );
    }

    void assemble( const Element& element, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      DUNE_THROW( Dune::NotImplemented, "This assembler is only supposed to be called for intersections." );
    }

    template<class Intersection>
    void indices( const Intersection& intersection, BoolMatrix& isNonZero, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      isNonZero = false;
    }

    template<class Intersection>
    void indices( const Intersection& intersection, BoolMatrix& isNonZero, const TrialLocalFE& tFEInside, const AnsatzLocalFE& aFEInside, const TrialLocalFE& tFEOutside, const AnsatzLocalFE& aFEOutside ) const
    {
      const auto&& elementInside  = intersection.inside();
      const auto&& elementOutside   = intersection.outside();
      isNonZero = ( domainInfo.isBoundary(elementInside) || domainInfo.isBoundary(elementOutside) );
    }

    template<class Intersection>
    void assemble( const Intersection& intersection, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE ) const
    {
      // We do not do anything on boundary faces.
      #warning Will have to update this once we allow for domains with periodic BCs.
      localMatrix = 0.0;
      return;
    }

    template<class Intersection>
    void assemble( const Intersection& intersection, LocalMatrix& localMatrix, const TrialLocalFE& tFE, const AnsatzLocalFE& aFE, const TrialLocalFE& tFEOutside, const AnsatzLocalFE& aFEOutside ) const
    {
      // Ensure that parameters are appropriate.
      const auto&& elementInside  = intersection.inside();
      const auto&& elementOutside   = intersection.outside();
      assert(tFE.type() == elementInside.type());
      assert(aFE.type() == elementInside.type());
      assert(tFEOutside.type() == elementOutside.type());
      assert(aFEOutside.type() == elementOutside.type());
      if (not Base::isSameFE(tFE, aFE) || not Base::isSameFE(tFEOutside, aFEOutside))
        DUNE_THROW(Dune::NotImplemented, "MongeGaugeStabilizationAssembler is only implemented for ansatz space=test space.");


      // Skip intersections that are not relevant.
      if( ! ( domainInfo.isBoundary(elementInside) || domainInfo.isBoundary(elementOutside) ) )
        return;

      if( domainInfo.isOutside(elementInside) || domainInfo.isOutside(elementOutside) )
        return;

      #warning Added heuristic for stabilization faces.
      if( !domainInfo.isStabilizationFace(intersection) )
        return;

      // Define and initialize variables.
      int rows  = localMatrix.N();
      int cols  = localMatrix.M();
      localMatrix = 0.0;

      // Get local bases.
      const auto& localBasisInside  = tFE.localBasis();
      const auto& localBasisOutside   = tFEOutside.localBasis();
      const size_t nBasisFunctions  = localBasisInside.size() + localBasisOutside.size();

      // Get geometries.
      const auto&& intersectionGeometry   = intersection.geometry();
      const auto&& geometryInside     = elementInside.geometry();
      const auto&& geometryOutside    = elementOutside.geometry();

      // Set maximal order of the derivatives.
      size_t maxOrder = std::max( localBasisInside.order(), localBasisOutside.order() );

      // Find associated indices, i.e. link indices from one element to another if they belong to the same global basis function.
      const auto& associatedIndices   = getAssociatedIndices( elementInside, localBasisInside, elementOutside, localBasisOutside );

      // Get quadrature rule on intersection.
      QuadratureRuleKey quadFE(tFE);
      quadFE.setOrder(maxOrder);
      quadFE.setGeometryType( Dune::GeometryType(elementInside.type().id(), dim-1) );
      QuadratureRuleKey quadKey   = quadFE.square();
      const auto& quadratureRule  = QuadratureRuleCache<float_x, dim-1>::rule(quadKey);

      // Loop over quadrature points.
      for( const auto& quadPoint : quadratureRule )
      {
        // Get quadrature point and translate it to an element coordinate.
        const auto& xFaceLocal  = quadPoint.position();
        auto&& xGlobal      = intersectionGeometry.global(xFaceLocal);

        // Collect inside information.
        auto&& xLocalInside   = geometryInside.local(xGlobal);
        const auto& jacobianInverseTransposedInside   = geometryInside.jacobianInverseTransposed(xLocalInside);
        DerivativeHelper<decltype(jacobianInverseTransposedInside)> derivativeHelperInside( jacobianInverseTransposedInside );

        // Collect outside information.
        auto&& xLocalOutside  = geometryOutside.local(xGlobal);
        const auto& jacobianInverseTransposedOutside   = geometryOutside.jacobianInverseTransposed(xLocalOutside);
        DerivativeHelper<decltype(jacobianInverseTransposedOutside)> derivativeHelperOutside( jacobianInverseTransposedOutside );

        // Set hF and hF_pow.
        const auto hF       = intersectionGeometry.integrationElement(xFaceLocal);
        auto prefactor      = hF;

        // Set integration factor.
        const auto integrationFactor  = quadPoint.weight() * hF;

        // Determine direction of normal.
        // Important: We make the assumption here that the normal will be a euclidean unit vector (up to sign).
        // Get the normal \nu_l and find l such that \nu = e_l.
        const auto&& normal   = intersection.unitOuterNormal(xFaceLocal);
        const auto l          = getNormalDirectionId(normal);

        // Loop over orders.
        for( size_t order = 0; order <= maxOrder; ++order )
        {
          // Evaluate partial derivatives.
          std::vector<float_x> jumps(nBasisFunctions,0);
          OrderType d(0);
          d[l]  = order;

          for( size_t sharedLocalBasisId = 0; sharedLocalBasisId < nBasisFunctions; ++sharedLocalBasisId )
          {
            const bool fInside          = (sharedLocalBasisId < localBasisInside.size());
            const size_t localBasisId   = fInside ? sharedLocalBasisId : sharedLocalBasisId-localBasisInside.size();
            const auto associatedIndex  = associatedIndices[sharedLocalBasisId];
            const float_x sgn           = fInside ? 1 : -1;

            const auto&& partialDerivative  =
              fInside
              ? derivativeHelperInside .partialDerivative( localBasisInside,  xLocalInside,  d, localBasisId )
              : derivativeHelperOutside.partialDerivative( localBasisOutside, xLocalOutside, d, localBasisId );
            jumps[sharedLocalBasisId]  += sgn * partialDerivative;

            if( associatedIndex >= 0 )
              jumps[associatedIndex] += sgn * partialDerivative;
          } // sharedLocalBasisId

          // Update local matrix.
          for( size_t i = 0; i < rows; ++i )
          {
            for( size_t j = i+1; j < cols; ++j )
            {
              auto A_ij = jumps[i] * jumps[j] * integrationFactor * prefactor;
              localMatrix[i][j]   += A_ij;
              localMatrix[j][i]   += A_ij;
            }
            localMatrix[i][i]   += jumps[i] * jumps[i] * integrationFactor * prefactor;
          }

          // Update hF_pow.
          prefactor  *= (hF*hF)/((order+1)*(order+1));
        } // order
      } // quadPoint

      return;
    }


  protected:

    template<class LocalBasis>
    auto getAssociatedIndices( const Element& innerElement, const LocalBasis& innerBasis, const Element& outerElement,const LocalBasis& outerBasis ) const
    {
      const auto& globalIndicesInside = globalIndices( innerElement, innerBasis );
      const auto& globalIndicesOutside = globalIndices( outerElement, outerBasis );

      std::vector<int> associatedIndices( globalIndicesInside.size() + globalIndicesOutside.size(), -1 );
      for( size_t i = 0; i < globalIndicesInside.size(); ++i )
      {
        const auto globalIndex = globalIndicesInside[i];
        for( size_t j = 0; j < globalIndicesOutside.size(); ++j )
        {
          if( globalIndex == globalIndicesOutside[j] )
          {
            associatedIndices[i]                = j + globalIndicesInside.size();
            associatedIndices[globalIndicesInside.size() + j]   = i;
            break; // exit loop over j
          }
        }
      }

      return associatedIndices;
    }

    template<class Normal>
    size_t getNormalDirectionId( const Normal& normal ) const
    {
      int l = -1;
      for( size_t idx = 0; idx < normal.size(); ++idx )
      {
        if( std::abs(normal[idx]) == 1 ) // May want to replace this by a less restrictive condition (i.e. abs(...) < eps)
        {
          l = idx;
          break;
        }
      }
      if( l < 0 )
        DUNE_THROW( Dune::NotImplemented, "MongeGaugeStabilizationAssembler requires that the faces align with the canonical Euclidean standard basis." );

      return l;
    }

    template<class LocalBasis>
    auto globalIndices( const Element& element, const LocalBasis& localBasis ) const
    {
      std::vector<size_t> g(localBasis.size(),0);
      for( size_t i = 0; i < g.size(); ++i )
        g[i]  = basis.index( element, i );
      return g;
    }


  private:

    const Basis& basis;
    const DomainInfo& domainInfo;
};

#endif
