#ifndef DUNE_MEMBRANE_ASSEMBLER_HH
#define DUNE_MEMBRANE_ASSEMBLER_HH

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/particle/assembler/fictitiousNitscheVolumeAssembler.hh>
#include <dune/particle/assembler/fictitiousNitscheTraceAssembler.hh>
#include <dune/particle/assembler/stabilizationAssembler.hh>
#include <dune/particle/assembler/fictitiousNitscheFunctionalAssembler.hh>

#include <dune/particle/helper/misc.hh>

template<class Integrand, class DomainInfo>
class MembraneAssembler
{
  public:

    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>> Matrix;
    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;
  
    MembraneAssembler(const Integrand& integrand, const DomainInfo& domainInfo)
    {
      assemble(integrand, domainInfo);
    }
    
    const auto& volumeMatrix() const
    {
      return volumeMatrix_;
    }
    
    const auto& systemMatrix() const
    {
      return systemMatrix_;
    }
    
    const auto& rightHandSide() const
    {
      return rightHandSide_;
    }


  protected:
  
    Matrix volumeMatrix_, traceMatrix_, stabilizationMatrix_, systemMatrix_;
    Vector rightHandSide_;
  
    void assemble(const Integrand& integrand, const DomainInfo& domainInfo)
    {
      const auto& basis = domainInfo.getBasis();
      typedef typename std::decay<decltype(basis)>::type Basis;
      typedef typename Basis::GridType Grid;
      typedef typename Basis::LocalFiniteElement LFE;

      typedef FictitiousNitscheVolumeAssembler<Integrand,DomainInfo,Grid,LFE,LFE> VolumeAssembler;
      typedef FictitiousNitscheTraceAssembler<Integrand,DomainInfo,Grid,LFE,LFE> TraceAssembler;
      typedef StabilizationAssembler<Basis,DomainInfo,Grid,LFE,LFE> StabAssembler;
      typedef FictitiousNitscheFunctionalAssembler<Integrand,DomainInfo,Grid,LFE> FunctionalAssembler;

      VolumeAssembler     volumeAssembler(integrand, domainInfo);
      TraceAssembler      traceAssembler(integrand, domainInfo);
      StabAssembler       stabilizationAssembler(basis, domainInfo);
      FunctionalAssembler functionalAssembler(integrand, domainInfo, true);

      Assembler<Basis,Basis> assembler(basis, basis);
      assembler.assembleOperator(volumeAssembler, volumeMatrix_);
      assembler.assembleOperator(traceAssembler, traceMatrix_);
      assembler.assembleIntersectionOperator(stabilizationAssembler, stabilizationMatrix_);
      assembler.assembleFunctional(functionalAssembler, rightHandSide_);

      systemMatrix_ = Misc::joinSparseMatrices(volumeMatrix_, traceMatrix_, stabilizationMatrix_);
    }
};

#endif
