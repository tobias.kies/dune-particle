#ifndef DUNE_OTHER_SPATIAL_TRANSFORMATION_HH
#define DUNE_OTHER_SPATIAL_TRANSFORMATION_HH

#include <math.h>
#include <utility>

#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

// Like SpatialTransformation but the rotations are applied in reverse order.
template<class float_x = double>
class OtherSpatialTransformation
 : public Dune::VirtualMultiDifferentiableFunction<
    std::pair<Dune::FieldVector<float_x,6>,Dune::FieldVector<float_x,3>>,
    Dune::FieldVector<float_x,3>,
    std::pair<Dune::FieldVector<size_t,6>,Dune::FieldVector<size_t,3>>
  >
{
  public:
  
    typedef Dune::FieldVector<float_x,6> Descriptor;
    typedef Dune::FieldVector<float_x,3> Point;
    typedef Dune::FieldVector<float_x,2> Point2D;

    typedef std::pair<Descriptor,Point> DomainType;
    typedef Point RangeType;
    
    typedef Dune::FieldVector<size_t,6> DescriptorOT;
    typedef Dune::FieldVector<size_t,3> PointOT;
    typedef std::pair<DescriptorOT,PointOT> OrderType;
    
    typedef Dune::VirtualMultiDifferentiableFunction<DomainType,RangeType,OrderType> BaseType;
    
    typedef std::array<int,2> Index;
    
    using BaseType::evaluate;
    
    RangeType evaluate(const DomainType& z, const OrderType& d) const
    {
      RangeType res;
      
      const auto order = orderOf(d);
      
      switch(order)
      {
        case 0:
          res = apply(z.first, z.second);
          break;
          
        case 1:
          res = apply(z.first, z.second, indexOf(d));
          break;

        default:
          // If the higher derivative is at least 2 in the "x"-part, then we simply may return 0.
          //~ int o = 0;
          //~ for(int i = 0; i < d.second.size(); ++i)
            //~ o += (d.second[i] != 0);
          int o = d.second.one_norm();
            
          if(o < 2)
            DUNE_THROW(Dune::NotImplemented, "Not implemented.");
          else
            res = 0;
          break;
      }
      
      return res;
    }

    static auto apply(Descriptor p, Point x, const Index& index = {-1,-1})
    {
      Point y(0);
      std::array<float_x,3> c = {cos(p[3]), cos(p[4]), cos(p[5])};
      std::array<float_x,3> s = {sin(p[3]), sin(p[4]), sin(p[5])};
      
      // Apply modifiers for derivative, if applicable.
      if(index[0] == 0)
      {
        if(index[1] < 3)
        {
          c = {0,0,0};
          s = {0,0,0};
          for(int i = 0; i < 3; ++i)
            p[i]  = (i == index[1]);
        }
        else
        {
          for(int i = 0; i < 3; ++i)
            p[i]  = 0;
          c[index[1]-3] = -sin(p[index[1]]);
          s[index[1]-3] =  cos(p[index[1]]);
        }
      }
      else if(index[0] == 1)
      {
        for(int i = 0; i < 3; ++i)
          x[i]  = (i == index[1]);
        for(int i = 0; i < 3; ++i)
          p[i]  = 0;
      }
      
      // Rotation.
      // Corresponds to R1*R2*R3 where R_i is the rotation around the x_i-axis.
      if(index[0] != 0 || index[1] >= 3)
      {
        y[0]  =                 (c[1]*c[2])*x[0] +                 (c[1]*s[2])*x[1] +      s[1]*x[2];
        y[1]  = (-c[2]*s[0]*s[1]-c[0]*s[2])*x[0] + ( c[0]*c[2]-s[0]*s[1]*s[2])*x[1] + c[1]*s[0]*x[2];
        y[2]  = (-c[0]*c[2]*s[1]+s[0]*s[2])*x[0] + (-c[0]*s[1]*s[2]-c[2]*s[0])*x[1] + c[0]*c[1]*x[2];
      }
      // else: Rotation vanishes.
      
      // Translation.
      for( int i = 0; i < 3; ++i )
        y[i]  = y[i] + p[i];
        
      return y;
    }
    

    static auto applyInverse(Descriptor p, Point x)
    {
      Point y(0);
      std::array<float_x,3> c = {cos(p[3]), cos(p[4]), cos(p[5])};
      std::array<float_x,3> s = {sin(p[3]), sin(p[4]), sin(p[5])};
      
      // First undo the translation.
      for( int i = 0; i < 3; ++i )
        x[i]  = x[i] - p[i];
        
      // Next apply the inverse rotation (which is the transpose of the original rotation).
        y[0]  = (c[1]*c[2])*x[0] + (-c[2]*s[0]*s[1]-c[0]*s[2])*x[1] + (-c[0]*c[2]*s[1]+s[0]*s[2])*x[2];
        y[1]  = (c[1]*s[2])*x[0] + ( c[0]*c[2]-s[0]*s[1]*s[2])*x[1] + (-c[0]*s[1]*s[2]-c[2]*s[0])*x[2];
        y[2]  =        s[1]*x[0] +                   c[1]*s[0]*x[1] +                   c[0]*c[1]*x[2];
      
      return y;
    }
    
    
    template<class Surface>
    auto applyPeriodicity(const Surface& surface, const Descriptor& p, const Point2D& x) const
    {
      Point2D y;
      const double c = cos(p[5]);
      const double s = sin(p[5]);
      
      // Apply x3-rotation.
      y = {c*y[0]-s*y[1], s*y[0]+c*y[1]};
      
      // Apply periodicity in box.
      const auto& periodicBox = surface.periodicBox();
      const auto& a = periodicBox.lower();
      const auto& b = periodicBox.upper();

      for(int i = 0; i < a.size(); ++i) // I guess this could generally be two- or three-dimensional.
        if(std::isfinite(a[i]) && std::isfinite(b[i]))
          y[i]  = (fmod(y[i]-a[i],b[i]-a[i])+a[i]) + (y[i]<a[i])*(b[i]-a[i]);
      
      // Undo x3-rotation.
      y = {c*y[0]+s*y[1], -s*y[0]+c*y[1]};
      
      return y;
    }

    
  private:
    
    static auto orderOf(const DomainType& d)
    {
      int order = 0;

      order += d.first.one_norm();
      order += d.second.one_norm();
        
      return order;
    }
    
    static auto indexOf(const DomainType& d)
    {
      if(orderOf(d) != 1)
        DUNE_THROW(Dune::Exception, "Invalid order.");
        
      Index index = {-2,-2};
      
      const auto& d1 = d.first;
      for(int i = 0; i < d1.size(); ++i)
        if(d1[i] != 0)
          index = {0,i};
      
      const auto& d2 = d.second;
      for(int i = 0; i < d2.size(); ++i)
        if(d2[i] != 0)
          index = {1,i};
      
      return index;
    }
};

#endif
