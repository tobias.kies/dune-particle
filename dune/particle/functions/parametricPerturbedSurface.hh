#ifndef PARAMETRIC_PERTURBED_SURFACE_HH
#define PARAMETRIC_PERTURBED_SURFACE_HH

#include <dune/grid/io/file/vtk/function.hh>
#include <dune/particle/functions/basismultidifferentiablegridfunction.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

template<class Surface, class Basis, class Vector>
class ParametricPerturbedSurface : public Dune::VTKFunction<typename Basis::GridView>
{
  public:
  
    typedef typename Basis::GridView GridView;
    typedef Dune::VTKFunction<GridView> Base;
    
    typedef typename Base::ctype ctype;
    typedef typename Base::Entity Entity;
    using Base::dim;
    
    typedef typename  Vector::value_type RangeType;
    
    //~ typedef BasisMultiDifferentiableGridFunction<Basis,Vector,Dune::FieldVector<size_t,dim>> F;
  
    ParametricPerturbedSurface(const Surface& surface, const Basis& basis, const Vector& u)
      //~ : surface(surface), func_u(basis,u)
      : surface(surface), basis(basis), u(u)
    {}


    int ncomps() const
    {
      return 3;
    }
    
    double evaluate(int comp, const Entity& e, const Dune::FieldVector<ctype,dim>& xi) const
    {
      auto xGlobal = e.geometry().global(xi);
      
      auto y = surface.parameterizationFunction().evaluate(xGlobal);
      //~ y.axpy(func_u.evaluateLocal(e,xi), surface.parameterizedNormal().evaluate(xGlobal));
      RangeType alpha;
      LocalEvaluator<Basis,Vector,RangeType>::evaluateLocal(basis, u, e, xi, alpha);
      y.axpy(alpha,surface.parameterizedNormal().evaluate(xGlobal));
      y -= xGlobal;
      
      for(int i = 0; i < y.size(); ++i)
        if(std::isnan(y[i]))
          return std::numeric_limits<double>::quiet_NaN();
      
      return y[comp];
    }
    
    std::string name() const
    {
      return "PerturbedSurface";
    }


  protected:
    
    const Surface&  surface;
    //~ const F         func_u;
    const Basis&    basis;
    const Vector&   u;
};

#endif
