// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef VIRTUAL_MULTI_DIFFERENTIABLE_GRID_FUNCTION_HH
#define VIRTUAL_MULTI_DIFFERENTIABLE_GRID_FUNCTION_HH


#include <dune/common/fvector.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/version.hh>

#include <dune/grid/utility/hierarchicsearch.hh>

//#include <dune/fufem/functions/virtualdifferentiablefunction.hh>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

#include <dune/geometry/referenceelements.hh>


/**
 *  \brief Virtual interface class for grid functions
 *
 *  A general grid function that might be defined only on parts of the underlying grid (isDefinedOn).
 *  If you have a grid function defined on a GridView derive from VirtualGridViewFunction instead.
 *
 *  \tparam GridType the underlying GridType
 *  \tparam RType the type of the range space
 *
 */
template <class GridType, class RType, class OT>
class VirtualMultiDifferentiableGridFunction :
  public VirtualGridFunction<GridType,RType>,
  public Dune::VirtualMultiDifferentiableFunction<typename GridType::template Codim<0>::Geometry::GlobalCoordinate, RType, OT>
{
  protected:

    typedef VirtualMultiDifferentiableGridFunction<GridType,RType,OT> ThisType;
    typedef VirtualGridFunction<GridType, RType> BaseType;
    typedef Dune::VirtualMultiDifferentiableFunction<typename GridType::template Codim<0>::Geometry::GlobalCoordinate, RType, OT> BaseType2;

    //! Element pointer type of underlying grid
    typedef typename GridType::template Codim<0>::Entity/*Pointer*/ ElementPointer;

  public:

    typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalDomainType;
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::DerivativeType DerivativeType;
	typedef OT OrderType;

    using BaseType::evaluate;
    using BaseType2::evaluate;

    //! The grid type
    typedef GridType Grid;

    //! Element type of underlying grid
    typedef typename Grid::template Codim<0>::Entity Element;

    /** \brief Constructor
     *
     *  \param grid the underlying grid
     */
    VirtualMultiDifferentiableGridFunction(const Grid& grid):
	  BaseType( grid ),
      grid_(&grid)
    {}

    /**
     * \brief function evaluation in global coordinates
     *
     * Default implementation using hierarchic search and evaluateLocal
     *
     * \warning may be VERY SLOW due to hierarchic search
     *
     * \param x Argument (global coordinates) for function evaluation
     * \param y Result of function evaluation.
     * \param d
     */
    virtual void evaluate(const DomainType& x, const OrderType& d, RangeType& y) const
    {
      GridFunctionDefinedInfo containsinfo(*this);
      Dune::HierarchicSearch<Grid, GridFunctionDefinedInfo> hsearch(*grid_, containsinfo);

#warning Should remove the caching and whatsoever at some point.
      Element e;
      if( fSetX && fSetE &&  x == cached_x )
      {
        e = cached_element;
      }
      else
      {
        // Try to find entity as a neighbor of the old one first.
        bool fFound = false;
        /*if( flag )
        {
            const auto& gridView = grid_->leafGridView();
            auto&& iit = gridView.ibegin(cached_element);
            const auto&& iitEnd = gridView.iend(cached_element);
            for( ; iit != iitEnd; iit++ )
            {
                if( !iit->neighbor() )
                    continue;

                auto&& entity = iit->outside();
                const auto &geo = entity.geometry();

                const auto&& local = geo.local( x );
                if( !Dune::ReferenceElements< double, Grid::dimension >::general( geo.type() ).checkInside( local ) )
                  continue;

                e   = entity;
                fFound = true;
                break;
            }
        }*/
        cached_x = x;
        fSetX = true;

        if( fSetE )
        {
            const auto& geometry    = cached_element.geometry();
            const auto& xLocal      = geometry.local(x);
            if( Dune::ReferenceElements<double, Grid::dimension>::general( geometry.type() ).checkInside( xLocal ) )
            {
                e               = cached_element;
                cached_x_local  = xLocal;
                fFound  = true;
                
            }
        }

        if( !fFound )
        {
            e = hsearch.findEntity(x);
            cached_element = e;
            fSetE = true;

            #if DUNE_VERSION_NEWER(DUNE_GRID,2,4)
            cached_x_local = e.geometry().local(x);
            #else
            cached_x_local = e->geometry().local(x);
            #endif
        }
      }

#if DUNE_VERSION_NEWER(DUNE_GRID,2,4)
      evaluateLocal(e, cached_x_local, d, y);
#else
      evaluateLocal(*e, cached_x_local, d, y);
#endif
    }


    /**
     * \brief Function evaluation in local coordinates.
     *
     * \param e Evaluate in local coordinates with respect to this element.
     * \param x Argument for function evaluation in local coordinates.
     * \param y Result of function evaluation.
     */
    virtual void evaluateLocal(const Element& e, const LocalDomainType& x, const OrderType& d, RangeType& y) const = 0;
    virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const = 0;


    /** \brief evaluation of the gradient
     * \param e Evaluate in local coordinates with respect to this element.
     * \param x Argument for function evaluation in local coordinates.
     */
    auto evaluateLocalGradient( const Element& e, const DomainType& x ) const
    {
        Dune::FieldVector<RangeType,DomainType::dimension> gradient;

        // Iterate over all \partial_{i}.
        OrderType d(0);
        for( size_t i = 0; i < x.size(); ++i )
        {
            d[i] = 1;
            evaluateLocal(e, x, d, gradient[i]);
            d[i] = 0;
        }

        return gradient;
    }

    /** \brief evaluation of the Hessian
     * \param e Evaluate in local coordinates with respect to this element.
     * \param x Argument for function evaluation in local coordinates.
     *  \note Could be made more efficient by exploiting symmetry.
     */
    auto evaluateLocalHessian( const Element& e, const DomainType& x ) const
    {
        Dune::FieldMatrix<RangeType,DomainType::dimension,DomainType::dimension> hessian;

        // Iterate over all \partial_{ij}.
        OrderType d(0);
        for( size_t i = 0; i < x.size(); ++i )
        {
            d[i]++;
            for( size_t j = 0; j < x.size(); ++j )
            {
                d[j]++;
                evaluateLocal(e, x, d, hessian[i][j]);
                d[j]--;
            }
            d[i]--;
        }

        return hessian;
    }


    /**
     * \brief Check whether local evaluation is possible
     *
     * \param e Return true if local evaluation is possible on this element.
     */
    virtual bool isDefinedOn(const Element& e) const = 0;

    //virtual ~VirtualMultiDifferentiableGridFunction() {}

    /**
     * \brief Export underlying grid
     */
    const Grid& grid() const
    {
      return *grid_;
    }


    /**
     * \brief Give a hint on at what element we should start looking for the hierarchic search.
     */
    void setCachedElement( const Element& e )
    {
        cached_element = e;
        fSetE = true;
    }

  protected:

    //! the underlying grid
    const Grid* grid_;

    /**
     *  \brief Wrapper class forwarding VirtualGridFunction::isDefinedOn(Element&) as contains(Element&)
     *
     *  Needed to plug into Dune::HierarchicSearch in default implementation of evaluate.
     */
    class GridFunctionDefinedInfo
    {
      private:
        const ThisType& gridFunction_;

      public:
        /**
         *  \brief constructor
         */
        GridFunctionDefinedInfo(const ThisType& gridFunction):
          gridFunction_(gridFunction)
        {}

        /** forwarding VirtualGridFunction::isDefinedOn(...) as contains(...)
         */
        bool contains(const Element& e) const
        {
          return gridFunction_.isDefinedOn(e);
        }
    };

    mutable DomainType cached_x;
    mutable DomainType cached_x_local;
    mutable Element cached_element;
    mutable bool fSetX = false;
    mutable bool fSetE = false;

}; // end of VirtualMultiDifferentiableGridFunction class





/**
 * \brief Virtual interface class for grid functions associated to grid views
 *
 */
template <class GridViewType, class RType, class OT>
class VirtualMultiDifferentiableGridViewFunction :
  public VirtualMultiDifferentiableGridFunction<typename GridViewType::Grid, RType, OT>
{
  protected:
    typedef VirtualMultiDifferentiableGridFunction<typename GridViewType::Grid, RType, OT> BaseType;
//    typedef VirtualGridViewFunction<GridViewType, RType> MyType;

    typedef typename GridViewType::template Codim<0>::Entity/*Pointer*/ ElementPointer;

  public:

    typedef typename BaseType::LocalDomainType LocalDomainType;
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::DerivativeType DerivativeType;
	typedef typename BaseType::OrderType OrderType;

    typedef GridViewType GridView;
    typedef typename BaseType::Grid Grid;

    //! Element type of underlying grid
    typedef typename Grid::template Codim<0>::Entity Element;

    /**
     *  \brief Constructor
     *
     *  We need to know the underlying grid view
     */
    VirtualMultiDifferentiableGridViewFunction(const GridView& gridView) :
      BaseType(gridView.grid()),
      gridView_(gridView)
    {}

    using BaseType::evaluate;

    /**
     * \brief function evaluation in global coordinates
     *
     * Default implementation using hierarchic search and evaluateLocal
     *
     * \warning may be VERY SLOW due to hierarchic search
     *
     * \param x Argument (global coordinates) for function evaluation
     * \param y Result of function evaluation.
     * \param d
     */
    virtual void evaluate(const DomainType& x, const OrderType& d, RangeType& y) const
    {
      Dune::HierarchicSearch<Grid, GridView> hsearch(gridView_.grid(), gridView_);
      Element e;
      if( x == cached_x && flag )
      {
        e = cached_element;
      }
      else
      {
        e = hsearch.findEntity(x);
        cached_x = x;
        cached_element = e;
        #if DUNE_VERSION_NEWER(DUNE_GRID,2,4)
            cached_x_local = e.geometry().local(x);
        #else
            cached_x_local = e->geometry().local(x);
        #endif
        flag = true;
      }
#if DUNE_VERSION_NEWER(DUNE_GRID,2,4)
      evaluateLocal(e, cached_x_local, d, y);
#else
      evaluateLocal(*e, cached_x_local, d, y);
#endif
    }

    virtual RangeType evaluate( const DomainType& x, const OrderType& d ) const
    {
        RangeType y(0);
        evaluate(x,d,y);
        return y;
    }


    /**
     * \brief Function evaluation in local coordinates.
     *
     * \param e Evaluate in local coordinates with respect to this element.
     * \param x Argument for function evaluation in local coordinates.
     * \param y Result of function evaluation.
     * \param d
     */
    virtual void evaluateLocal(const Element& e, const LocalDomainType& x, const OrderType& d, RangeType& y) const = 0;


    /**
     * \brief Check whether local evaluation is possible
     *
     * \param e Return true if local evaluation is possible on this element.
     */
    virtual bool isDefinedOn(const Element& e) const
    {
      #ifdef MULTIPLE_GRIDS
        return false;
      #endif
      return gridView_.contains(e);
    }

    virtual ~VirtualMultiDifferentiableGridViewFunction() {}

    /**
     * \brief Export underlying grid view
     */
    const GridView& gridView() const
    {
      return gridView_;
    }

  protected:

    const GridView gridView_;

    mutable DomainType cached_x;
    mutable DomainType cached_x_local;
    mutable Element cached_element;
    mutable bool flag = false;

}; // end of VirtualMultiDifferentiableGridViewFunction class



#endif
