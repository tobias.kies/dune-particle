// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef FUNCPOLZ_HH
#define FUNCPOLZ_HH

#include <math.h>

#include <vector>
#include <utility>
#include <array>

#include <dune/common/shared_ptr.hh>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

// Note that many computations could be done more efficiently by
// better computation of factorials and powers.
// Actually OrderType and polTuple could be the same...
template < class DT, class RT, class OT, int dimPol = DT::dimension, class float_x = double >
class FuncPolZ:
    public Dune::VirtualMultiDifferentiableFunction<DT,RT,OT>
{
    private:
        typedef Dune::VirtualMultiDifferentiableFunction<DT,RT,OT> BaseType;
        //const typename BaseType::RangeType c;
        
        // computes a!/b!
        RT fac( int a, int b ) const
        {
            RT res = 1;
            for( size_t i = b+1; i <= a; ++i )
                res    *= i;
            return res;
        }

    public:
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::OrderType OrderType;
        typedef typename std::array<int,dimPol> PolTuple;
        typedef std::vector<std::pair<RangeType,PolTuple>> CoeffType;
        
        using BaseType::evaluate;

        FuncPolZ( ) : z(0)
        {
        }
        
        FuncPolZ( const FuncPolZ& other ) : coeff(other.coeff), z(other.z)
        {
        }

        FuncPolZ( const CoeffType& coeff_ ) :
            coeff(coeff_), z(0)
        {
        }
        
        FuncPolZ( const CoeffType& coeff_, const DomainType& z_ ) :
            coeff(coeff_), z(z_)
        {
        }
        
        FuncPolZ( const RangeType& c ) :
            z(0)
        {
            PolTuple tuple;
            for( size_t i = 0; i < dimPol; ++i ) tuple[i] = 0;
            coeff.push_back( std::make_pair( c, tuple ) );
        }
        
        void setCoeffs( const CoeffType& coeff_ )
        {
            coeff.clear();
            coeff.insert( coeff.end(), coeff_.begin(), coeff_.end() );
        }
        
        void setCenter( const DomainType& z_ )
        {
            z    = z_;
        }
        
        RangeType evaluate( const DomainType& x, const OrderType& d ) const
        {
            RangeType res = 0;
            for( size_t i = 0; i < coeff.size(); ++i )
            {
                RangeType a = coeff[i].first;
                PolTuple deg = coeff[i].second;
                RangeType x_pow    = 1;
                for( size_t j = 0; j < std::min( (typename DomainType::size_type) dimPol,x.size()); ++j )
                {
                    if( d[j] <= deg[j] )
                    {
                        x_pow    *= std::pow( x[j]-z[j], deg[j]-d[j] ) * fac( deg[j], deg[j]-d[j] );
                    }
                    else
                    {
                        x_pow    = 0;
                        break;
                    }
                }
                res += a * x_pow;
            }
            return res;
        }
        
        #warning .
        const auto& innerFunction() const
        {
            return *this;
        }
        const auto& middleFunction() const
        {
            return *this;
        }
        const auto& outerFunction() const
        {
            return *this;
        }
        
    private:
        CoeffType coeff;
        DomainType z;

};

#endif
