#ifndef DUNE_PERIODIC_FUNCTION_HH
#define DUNE_PERIODIC_FUNCTION_HH

#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>
#include <dune/particle/surface/boundingBox.hh>

// Note that many computations could be done more efficiently by
// better computation of factorials and powers.
// Actually OrderType and polTuple could be the same...
template <class Function>
class PeriodicFunction :
    public Dune::VirtualMultiDifferentiableFunction<typename Function::DomainType,typename Function::RangeType,typename Function::OrderType>
{
    public:

      typedef Dune::VirtualMultiDifferentiableFunction<typename Function::DomainType,typename Function::RangeType,typename Function::OrderType> BaseType;
      typedef typename BaseType::DomainType DomainType;
      typedef typename BaseType::RangeType RangeType;
      typedef typename BaseType::OrderType OrderType;
      
      using BaseType::evaluate;
      
      PeriodicFunction() : f(NULL)
      {
      }

      PeriodicFunction(const Function& f_) : f(&f_)
      {
      }
      
      RangeType evaluate(const DomainType& x, const OrderType& d) const
      {
        if(f == NULL)
          DUNE_THROW(Dune::Exception, "Base function was not set prior to evaluation attempt.");
        //~ std::cerr << "periodicFunction requested evaluation in " << x << std::endl;
        return f->evaluate(periodic(x),d);
      }
      
      void setPeriodicBox(const BoundingBox& periodicBox_)
      {
        periodicBox = periodicBox_;
        fPeriodic = true;
      }
        
    private:

      const Function* f;
      BoundingBox periodicBox;
      bool fPeriodic = false;
      
      DomainType periodic(const DomainType& x) const
      {
        if(!fPeriodic)
          return x;
          
        const auto& a = periodicBox.lower();
        const auto& b = periodicBox.upper();
        
        DomainType y(x);
        for(int i = 0; i < x.size(); ++i)
          if(std::isfinite(a[i]) && std::isfinite(b[i]))
            y[i]  = (fmod(x[i]-a[i],b[i]-a[i])+a[i]) + (x[i]<a[i])*(b[i]-a[i]); // Attention: This maps x[i]==b[i] onto a[i].
        return y;
      }
};

#endif
