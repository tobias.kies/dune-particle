// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_FCHO2_PROJECTION_HH
#define DUNE_FCHO2_PROJECTION_HH

#include "IpTNLP.hpp"
#include "IpIpoptApplication.hpp"
#include "IpSolveStatistics.hpp"


// See https://www.coin-or.org/Ipopt/documentation/node22.html for reference.
template<class X, class Points, class Transformation>
class FCHo2ProjectionProblem : public Ipopt::TNLP
{
  protected:
  
    int nUnknowns;
    int nParticles;
    int nParticleDOFs;
    int nConstraints;
    int nAtoms;
  
  public:
  
    X x_final;
  
    FCHo2ProjectionProblem(const X& y, const X& x0, const X& lb, const X& ub, const Points& atoms, const Transformation& transformation, const double minDistance)
    : y(y), x0(x0), ub(ub), lb(lb), atoms(atoms), transformation(transformation), minDistance(minDistance)
    {
      assert(y.size() == x0.size());
      nParticles    = y.size();
      nAtoms        = atoms.size();
      nParticleDOFs = (nParticles > 0) ? x0[0].size() : 0;
      nUnknowns     = nParticles * nParticleDOFs;
      nConstraints  = (nParticles-1)*nParticles*nAtoms*nAtoms/2;
    }
    
    
    /**@name Overloaded from TNLP */
    //@{
    /** Method to return some info about the nlp */
    virtual bool get_nlp_info(Ipopt::Index& n, Ipopt::Index& m, Ipopt::Index& nnz_jac_g,
                              Ipopt::Index& nnz_h_lag, IndexStyleEnum& index_style)
    {
      // Number of unknowns (dimension of x).
      n = nUnknowns;

      // Number of constraints of the problem.
      // Here: (nParticles-1)*nParticles/2*nAtoms^2
      m = nConstraints;
      
      // Number of nonzeros in the Jacobian of the constraint function.
      // Here: In each constraint only exactly two particles interact with each other.
      nnz_jac_g = nConstraints*2*nParticleDOFs;
      
      // Number of nonzeros in the Hessian of the Lagrangian. (With respect to the unknown variable only, not with respect to the Lagrange multiplier; exploiting symmetry.)
      // Here: The Hessian might be dense.
      nnz_h_lag = n*(n+1)/2;
      
      // Numbering style for row/col entries in the sparse matrix format.
      index_style = Ipopt::TNLP::C_STYLE;
      
      return true;
    }


    /** Method to return the bounds for the problem */
    virtual bool get_bounds_info(Ipopt::Index n, Ipopt::Number* x_l, Ipopt::Number* x_u,
                                 Ipopt::Index m, Ipopt::Number* g_l, Ipopt::Number* g_u)
    {
      // Set lower/upper bounds on x.
      toIpoptType(lb, x_l);
      toIpoptType(ub, x_u);
      
      // Set lower bound on g. (Minimum distance between two atoms.)
      for( size_t i = 0; i < m; ++i )
        g_l[i] = minDistance*minDistance;
      
      // Set no upper bound on g.
      for( size_t i = 0; i < m; ++i )
        g_u[i] = std::numeric_limits<double>::max();
        
      return true;
    }


    /** Method to return the starting point for the algorithm */
    virtual bool get_starting_point(Ipopt::Index n, bool init_x, Ipopt::Number* x,
                                    bool init_z, Ipopt::Number* z_L, Ipopt::Number* z_U,
                                    Ipopt::Index m, bool init_lambda,
                                    Ipopt::Number* lambda)
    {
      if( init_x )
        toIpoptType(x0, x);
      
      if( init_z )
        DUNE_THROW( Dune::NotImplemented, "Can not supply z." );
        
      if( init_lambda )
        DUNE_THROW( Dune::NotImplemented, "Can not supply lambda." );
      
      return true;
    }


    /** Method to return the objective value */
    virtual bool eval_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number& obj_value)
    {
      X&& x_hat = toDomainType(x);
      x_hat -= y;
      obj_value = 0.5 * x_hat.two_norm2();
      return true;
    }


    /** Method to return the gradient of the objective */
    virtual bool eval_grad_f(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Number* grad_f)
    {
      X&& x_hat = toDomainType(x);
      x_hat -= y;
      toIpoptType(x_hat, grad_f);
      return true;
    }


    /** Method to return the constraint residuals */
    virtual bool eval_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x, Ipopt::Index m, Ipopt::Number* g)
    {
      // Compute values.
      const auto&& transformedAtoms = transformAtoms(x);
      
      for(int i = 0; i < m; ++i)
      {
        // Determine particle ids and atom ids.
        // The following relations hold:
        //  i = N * (nAtoms^2) + M; 0 <= M < nAtoms^2
        //  M = aid1 * nAtoms + aid2; 0 <= aid1/2 < nAtoms
        //  N = \sum_{k=0}^{pid1-1} k + pid2 = 0.5*p1*(p1-1) + p2;
        //    1 <= pid1 < nParticles; 0 <= pid2 < pid1
        //
        // Explanation for N:
        //  The particle with id k (starting from 0) is responsible for
        //  the interaction with all other particles l < k, ie in total
        //  k other particles.
        //  The index p1 is then the maximum integer with p1^2 - p1 <= 2*N
        //
        // Hope that rounding errors never break anything in the computation of pid1.
        const int N     = i / (nAtoms*nAtoms);
        const int pid1  = std::floor( 0.5*(1 + sqrt(1 + 8*N)) );
        const int pid2  = N - pid1*(pid1-1)/2;
        assert(pid2 < pid1);
        
        const int M     = i % (nAtoms*nAtoms);
        const int aid1  = M / nAtoms;
        const int aid2  = M % nAtoms;
        
        // Compute transformations.
        auto z = transformedAtoms[pid1][aid1];
        z -= transformedAtoms[pid2][aid2];
        
        // Set constraint value.
        g[i] = z.two_norm2();
      }
          
      return true;
    }


    /** Method to return:
     *   1) The structure of the jacobian (if "values" is NULL)
     *   2) The values of the jacobian (if "values" is not NULL)
     */
    virtual bool eval_jac_g(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                            Ipopt::Index m, Ipopt::Index nele_jac, Ipopt::Index* iRow, Ipopt::Index *jCol,
                            Ipopt::Number* values)
    {
      if(values == NULL)
      {
        // Fill in the sparsity pattern.
        int counter = 0;
        int rowId   = 0;
        for(int pid1 = 0; pid1 < nParticles; ++pid1)
          for(int pid2 = 0; pid2 < pid1; ++pid2)
            for(int aid1 = 0; aid1 < nAtoms; ++aid1)
              for(int aid2 = 0; aid2 < nAtoms; ++aid2)
              {
                for(int dof1 = 0; dof1 < nParticleDOFs; ++dof1)
                {
                  iRow[counter] = rowId;
                  jCol[counter] = pid1*nParticleDOFs + dof1;
                  ++counter;
                }
                for(int dof2 = 0; dof2 < nParticleDOFs; ++dof2)
                {
                  iRow[counter] = rowId;
                  jCol[counter] = pid2*nParticleDOFs + dof2;
                  ++counter;
                }
                ++rowId;
              }
      }
      else
      {
        // Evaluate the gradient.
        const auto&& transformedAtoms = transformAtoms(x);
        const auto&& transformedAtomDerivatives = transformAtomDerivatives(x);

        // Fill in the values.
        int counter = 0;
        for(int pid1 = 0; pid1 < nParticles; ++pid1)
          for(int pid2 = 0; pid2 < pid1; ++pid2)
            for(int aid1 = 0; aid1 < nAtoms; ++aid1)
              for(int aid2 = 0; aid2 < nAtoms; ++aid2)
              {
                for(int dof1 = 0; dof1 < nParticleDOFs; ++dof1)
                {
                  auto z = transformedAtoms[pid1][aid1];
                  z -= transformedAtoms[pid2][aid2];
                  values[counter] = 2 * (z * transformedAtomDerivatives[pid1][aid1][dof1]);
                  ++counter;
                }
                for(int dof2 = 0; dof2 < nParticleDOFs; ++dof2)
                {
                  auto z = transformedAtoms[pid1][aid1];
                  z -= transformedAtoms[pid2][aid2];
                  values[counter] = -2 * (z * transformedAtomDerivatives[pid2][aid2][dof2]);
                  ++counter;
                }
              }
      }
      
      
      return true;
    }

    /** Method to return:
     *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
     *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
     */
    virtual bool eval_h(Ipopt::Index n, const Ipopt::Number* x, bool new_x,
                        Ipopt::Number obj_factor, Ipopt::Index m, const Ipopt::Number* lambda,
                        bool new_lambda, Ipopt::Index nele_hess, Ipopt::Index* iRow,
                        Ipopt::Index* jCol, Ipopt::Number* values)
    {
      if( values == NULL )
      {
        size_t counter = 0;
        size_t iCounter = 0;
        for( size_t i = 0; i < x0.size(); ++i )
        {
          for( size_t dir_i = 0; dir_i < x0[i].size(); ++dir_i, ++iCounter )
          {
            size_t jCounter = 0;
            for( size_t j = 0; j <= i; ++j )
            {
              for( size_t dir_j = 0; dir_j < x0[j].size() && ( (i!=j) || dir_j <= dir_i ); ++dir_j, ++counter, ++jCounter )
              {
                iRow[counter] = iCounter;
                jCol[counter] = jCounter;
              }
            }
          }
        }
          
      }
      else
      {
        const auto&& p = toDomainType(x);
        
        // Compute relevant transformations (and derivatives).
        const auto&& transformedAtoms = transformAtoms(x);
        const auto&& transformedAtomDerivatives = transformAtomDerivatives(x);
        const auto&& transformedAtomSecondDerivatives = transformAtomSecondDerivatives(x);
        
        size_t counter = 0;
        for( size_t i = 0; i < x0.size(); ++i )
        {
          for( size_t dir_i = 0; dir_i < x0[i].size(); ++dir_i )
          {
            for( size_t j = 0; j <= i; ++j )
            {
              for( size_t dir_j = 0; dir_j < x0[j].size() && ( (i!=j) || dir_j <= dir_i ); ++dir_j, ++counter )
              {
                values[counter] = 0;
                
                // The Hessian of f is the identity matrix.
                if(i == j && dir_i == dir_j)
                  values[counter] += obj_factor;
                  
                // Compute the actual values.
                // To-do: Find a more elegant way to iterate over the relevant ids.
                for(int k = 0; k < nConstraints; ++k)
                {
                  // Extract relevant identifiers from the constraint id k.
                  const int N     = k / (nAtoms*nAtoms);
                  const int pid1  = std::floor( 0.5*(1 + sqrt(1 + 8*N)) );
                  const int pid2  = N - pid1*(pid1-1)/2;
                  assert(pid2 < pid1);
                  const int M     = k % (nAtoms*nAtoms);
                  const int aid1  = M / nAtoms;
                  const int aid2  = M % nAtoms;
                  
                  // Skip if either i or j are not relevant in the constraint.
                  if( (i != pid1 && i != pid2) || (j != pid1 && j != pid2))
                    continue;
                  
                  // Compute the second derivative.
                  double second_derivative = 0;
                  if(i == j)
                  {
                    bool isFirstInConstraint = (i == pid1);
                    auto z = transformedAtoms[pid1][aid1];
                    z -= transformedAtoms[pid2][aid2];
                    
                    if(isFirstInConstraint)
                      second_derivative = 2 * (transformedAtomDerivatives[i][aid1][dir_i] * transformedAtomDerivatives[j][aid1][dir_j]) + 2 * (z * transformedAtomSecondDerivatives[i][aid1][dir_i][dir_j]);
                    else
                      second_derivative = 2 * (transformedAtomDerivatives[i][aid2][dir_i] * transformedAtomDerivatives[j][aid2][dir_j]) - 2 * (z * transformedAtomSecondDerivatives[i][aid2][dir_i][dir_j]);
                  }
                  else
                  {
                    second_derivative = -2 * (transformedAtomDerivatives[i][aid1][dir_i] * transformedAtomDerivatives[j][aid2][dir_j]);
                  }
                  values[counter] += lambda[k] * second_derivative;
                }
              }
            }
          }
        }
      }
      
      return true;
    }

    //@}

    /** @name Solution Methods */
    //@{
    /** This method is called when the algorithm is complete so the TNLP can store/write the solution */
    virtual void finalize_solution(Ipopt::SolverReturn status,
                                   Ipopt::Index n, const Ipopt::Number* x, const Ipopt::Number* z_L, const Ipopt::Number* z_U,
                                   Ipopt::Index m, const Ipopt::Number* g, const Ipopt::Number* lambda,
                                   Ipopt::Number obj_value,
                                   const Ipopt::IpoptData* ip_data,
                                   Ipopt::IpoptCalculatedQuantities* ip_cq)
    {
      Debug::dprint( "[FCHo2Projection] ErrorCodes:", Ipopt::SUCCESS, Ipopt::MAXITER_EXCEEDED, Ipopt::CPUTIME_EXCEEDED, Ipopt::STOP_AT_TINY_STEP, Ipopt::STOP_AT_ACCEPTABLE_POINT, Ipopt::LOCAL_INFEASIBILITY, Ipopt::USER_REQUESTED_STOP, Ipopt::DIVERGING_ITERATES, Ipopt::RESTORATION_FAILURE, Ipopt::ERROR_IN_STEP_COMPUTATION, Ipopt::INVALID_NUMBER_DETECTED, Ipopt::INTERNAL_ERROR );
      Debug::dprint( "[FCHo2Projection] Summary: nVars =", n, ", nConstraints =", m, ", objectiveValue =", obj_value, ", returnCode=", status );
      
      x_final = toDomainType(x);
      Debug::dprintVector( x0, "[FCHo2Projection] Initial Iterate." );
      Debug::dprintVector( x_final, "[FCHo2Projection] Final Iterate." );
    }
    //@}
    
    
  private:
  
    const X y;
    const X x0;
    const X lb;
    const X ub;
    const Points& atoms;
    const Transformation& transformation;
    const double minDistance;
  
    inline X toDomainType( const Ipopt::Number* x ) const
    {
      // Convert supplied input into domain type.
      X pos(x0.size(),0);
      size_t counter = 0;
      for( size_t i = 0; i < x0.size(); ++i )
        for( size_t j = 0; j < x0[i].size(); ++j, ++counter )
          pos[i][j] = x[counter];
        
      return pos;
    }
    
    inline void toIpoptType(const X& pos, Ipopt::Number* x) const
    {
      size_t counter = 0;
      for( size_t i = 0; i < pos.size(); ++i )
        for( size_t j = 0; j < pos[i].size(); ++j, ++counter )
          x[counter]  = pos[i][j];
    }
    
    auto transformAtoms(const Ipopt::Number* x) const
    {
      const auto&& p = toDomainType(x);
      assert(p.size() == nParticles);
      
      // Compute the transformed atoms.
      std::vector<Points> transformedAtoms(nParticles);
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtoms[i].resize(nAtoms);
        for(int j = 0; j < nAtoms; ++j)
          transformedAtoms[i][j]  = transformation.apply(p[i], atoms[j]);
      }

      return transformedAtoms;
    }
    
    auto transformAtomDerivatives(const Ipopt::Number* x) const
    {
      const auto&& p = toDomainType(x);
      assert(p.size() == nParticles);
      
      typedef typename Points::value_type Point;
      std::vector<std::vector<std::vector<Point>>> transformedAtomDerivatives(nParticles);
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtomDerivatives[i].resize(nAtoms);
        for(int j = 0; j < nAtoms; ++j)
        {
          transformedAtomDerivatives[i][j].resize(nParticleDOFs);
          for(int k = 0; k < nParticleDOFs; ++k)
          {
            transformedAtomDerivatives[i][j][k] = transformation.apply(p[i], atoms[j], {0,k});
          }
        }
      }
      
      return transformedAtomDerivatives;
    }
    
    auto transformAtomSecondDerivatives(const Ipopt::Number* x) const
    {
      const auto&& p = toDomainType(x);
      assert(p.size() == nParticles);
      
      typedef typename Points::value_type Point;
      std::vector<std::vector<std::vector<std::vector<Point>>>> transformedAtomSecondDerivatives(nParticles);
      
      for(int i = 0; i < nParticles; ++i)
      {
        transformedAtomSecondDerivatives[i].resize(nAtoms);
        
        for(int j = 0; j < nAtoms; ++j)
        {
          transformedAtomSecondDerivatives[i][j].resize(nParticleDOFs);
          for(int d1 = 0; d1 < nParticleDOFs; ++d1)
            transformedAtomSecondDerivatives[i][j][d1].resize(nParticleDOFs);

          for(int d1 = 0; d1 < nParticleDOFs; ++d1)
          {
            for(int d2 = 0; d2 <= d1; ++d2)
            {
              transformedAtomSecondDerivatives[i][j][d1][d2] = transformation.second_p_derivative(p[i], atoms[j], d1, d2);
              transformedAtomSecondDerivatives[i][j][d2][d1] = transformedAtomSecondDerivatives[i][j][d1][d2];
            }
          }
        }
      }
      
      return transformedAtomSecondDerivatives;
    }
};

template<class X, class Points, class Transformation>
class FCHo2Projection
{
  public:
  
    FCHo2Projection(const Points& rawAtomPositions, const Transformation& transformation, const X& lb, const X& ub, const double minDistance)
    : rawAtomPositions(rawAtomPositions), transformation(transformation), lb(lb), ub(ub), minDistance(minDistance)
    {
    }
    
    
    auto evaluate(const X& y, const X& x0) const
    {
      Debug::dprintVector( x0, "[FCHo2ProjectionSolver] First iterate." );
      Debug::dprintVector( y, "[FCHo2ProjectionSolver] To be projected." );
      
      // Prepare problem and set config.
      Ipopt::SmartPtr<FCHo2ProjectionProblem<X,Points,Transformation>> nlp = new FCHo2ProjectionProblem<X,Points,Transformation>(y, x0, lb, ub, rawAtomPositions, transformation, minDistance);
      Ipopt::SmartPtr<Ipopt::IpoptApplication> app = new Ipopt::IpoptApplication();
      
      Debug::dprint( "[FCHo2ProjectionSolver] Still need to add config." );
      //~ app->Options()->SetIntegerValue("print_level", 12);
      //~ app->Options()->SetStringValue("derivative_test", "first-order");
      //~ app->Options()->SetStringValue("derivative_test", "second-order");
      //~ app->Options()->SetNumericValue("derivative_test_tol", 1.e-3);
      app->Options()->SetIntegerValue("print_level", 5);
      //~ app->Options()->SetIntegerValue("print_frequency_iter", 100);
      //~ app->Options()->SetStringValue("hessian_approximation", "limited-memory");
      //~ app->Options()->SetIntegerValue("limited_memory_max_history", 10);
      app->Options()->SetIntegerValue("max_iter", 1e3);
      app->Options()->SetNumericValue("tol", 1e-2);
      
      // Initialize solver.
      auto status = app->Initialize();
      if( status != Ipopt::Solve_Succeeded)
        DUNE_THROW( Dune::Exception, "IPOpt: Error during initialization!" );
        //~ Debug::dprint( "[FCHo2ProjectionSolver] IPOpt: Error during initialization!" );
      
      // Apply solver.
      app->OptimizeTNLP(nlp);
      
      return nlp->x_final;
    }
  
  
  protected:
  
    const Points& rawAtomPositions;
    const Transformation& transformation;
    const X& lb;
    const X& ub;
    const double minDistance;
};

#endif
