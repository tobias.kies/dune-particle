// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef DUNE_POINT_CONSTANT_FUNCTION_HH
#define DUNE_POINT_CONSTANT_FUNCTION_HH

#include <dune/particle/helper/functionshelper.hh>

template <class Points, class float_x = double>
class PointConstantFunction
  : public FunctionsHelper::template Function<float_x,2>
{
  private:
    typedef typename FunctionsHelper::template Function<float_x,2> BaseType;

  public:
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::OrderType OrderType;
    
    using BaseType::evaluate;

    PointConstantFunction(const Points& points) : points(points)
    {}
    
    RangeType evaluate(const DomainType& x, const OrderType& d) const
    {
      if(d.one_norm() > 0 || points.size() == 0)
        return 0;
        
      int id = -1;
      auto dist = std::numeric_limits<double>::infinity();
      for(int i = 0; i < points.size(); ++i)
      {
        auto y = x;
        for(int k = 0; k < x.size(); ++k)
          y[k] -= points[i][k];
        auto norm = y.infinity_norm();
        if(norm < dist)
        {
          dist = norm;
          id  = i;
        }
      }
      
      return points[id][x.size()];
    }

  private:
    const Points& points;
};

#endif
