#ifndef DUNE_TRANSFORMED_CONSTRAINT_FUNCTION_HH
#define DUNE_TRANSFORMED_CONSTRAINT_FUNCTION_HH

// Remark "ConstraintFunction" is actually not needed anymore. It's only still there for typedef convenience.
template<class ConstraintFunction, class Intersection>
class TransformedConstraintFunction
  : public Dune::VirtualMultiDifferentiableFunction
    <
      typename ConstraintFunction::DomainType,
      typename ConstraintFunction::RangeType,
      typename ConstraintFunction::OrderType
    >
{
  public:

    typedef Dune::VirtualMultiDifferentiableFunction<
        typename ConstraintFunction::DomainType,
        typename ConstraintFunction::RangeType,
        typename ConstraintFunction::OrderType
      > BaseType;
    
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::OrderType OrderType;
    
    typedef typename std::tuple_element<1,typename Intersection::DomainType>::type Position;


    TransformedConstraintFunction(const ConstraintFunction& constraintFunction, const Intersection& intersection, const Position& position)
      : constraintFunction(constraintFunction), intersection(intersection), position(position)
    {}
    
    
    RangeType evaluate(const DomainType& x, const OrderType& d) const
    {
      RangeType res = 0;
      
      const auto order = orderOf(d);
      switch(order)
      {
        case 0:
          res = evaluateFunction(x);
          //~ std::cerr << x << ": " << res << std::endl;
          //~ res = 1;
          break;
          
        case 1:
          res = evaluateFirstDerivative(x, d);
          //~ std::cerr << "evaluated " << x << "; " << d << " to " << res << std::endl;
          //~ res = x[0]*d[0] + x[1]*d[1];
          //~ res = 1;
          //~ res = 0;
          break;
          
        default:
          DUNE_THROW(Dune::NotImplemented, "Requested order of differentiation (" + std::to_string(order) + ") is not supported.");
      }

      return res;
    }


  private:
  
    const ConstraintFunction& constraintFunction;
    const Intersection& intersection;
    const Position position;
    
    
    const auto orderOf(const OrderType& d) const
    {
      int order = 0;
      for(int i = 0; i < d.size(); ++i)
        order += d[i];
      return order;
    }
    
    const auto directionOf(const OrderType& d) const
    {
      if(orderOf(d) != 1)
        DUNE_THROW(Dune::Exception, "Invalid d.");
      
      int dir = -1;
      for(int i = 0; i < d.size() && dir == -1; ++i)
        if(d[i] != 0)
          dir = i;
      return dir;
    }
    
    const auto evaluateFunction(const DomainType& x) const
    {
      // Get point of evaluation.
      typename Intersection::DomainType xp;
      xp.first      = x;
      xp.second     = position;

      const auto value = intersection.evaluate(xp).second;
      /*if(std::abs(value) > 1e-2)
      {
        //~ intersection.fDebug = true;
        //~ auto isec0 = intersection.evaluate(xp);
        const auto x_ = intersection.evaluate(xp).first;
        std::cerr << "invalid value for " << x << "; " << position << " -> " << x_ << "; " << value << std::endl;
        //~ intersection.fDebug = false;
        
        auto zp = xp;
        zp.second = {-2, -2, 0, 0, 0, 0};
        auto isec = intersection.evaluate(zp);
        std::cerr << "Instead in " << zp.first << "; " << zp.second << " -> " << isec.first << "; " << isec.second << std::endl;
      }*/
      
      return value;
    }
    
    const auto evaluateFirstDerivative(const DomainType& x, const OrderType& d) const
    {
      assert(orderOf(d) == 1);
      
      // Get point of evaluation.
      typename Intersection::DomainType xp;
      xp.first      = x;
      xp.second     = position;
      
      typename Intersection::OrderType d_;
      d_.first  = d;

      //~ if(d[0] == 1)
      //~ {
        //~ const auto y = intersection.evaluate(xp, d_);
        //~ const auto y0 = intersection.evaluate(xp);
        //~ std::cerr << "In " << x << "; " << d << " got " << y.first << "; " << y.second << std::endl;
        //~ std::cerr << "In " << x << " got base " << y0.first << "; " << y0.second << std::endl;
      //~ }

      return intersection.evaluate(xp, d_).second;
    }
    
};

#endif
