// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef VIRTUAL_MULTI_DIFFERENTIABLE_FUNCTION_HH
#define VIRTUAL_MULTI_DIFFERENTIABLE_FUNCTION_HH

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/function.hh>

namespace Dune
{
/*
//! Dummy for DerivativeTypefier
struct DerivativeTypeNotImplemented
{};

//! Traits class for the type of derivative of a VirtualMultiDifferentiableFunction
template <class DomainType, class RangeType>
struct DerivativeTypefier
{
  typedef DerivativeTypeNotImplemented DerivativeType;
};

template <class dctype, int DimDomain>
struct DerivativeTypefier<Dune::FieldVector<dctype, DimDomain>, double >
{
  typedef Dune::FieldMatrix<double, 1, DimDomain> DerivativeType;
};

template <class dctype, int DimDomain, class rctype, int DimRange>
struct DerivativeTypefier<Dune::FieldVector<dctype, DimDomain>, Dune::FieldVector<rctype, DimRange> >
{
  typedef Dune::FieldMatrix<rctype, DimRange, DimDomain> DerivativeType;
};*/

/**
 * \brief Virtual base class template for n-differentiable function
 * 
 * This actually is not really a virtual function but more like an
 * interface.
 *
 * \tparam DType The type of the input variable is 'const DType &'
 *
 * \tparam RType The type of the output variable is 'RType &'
 */
template <class DType, class RType, class OT>
class VirtualMultiDifferentiableFunction :
  public Dune::VirtualFunction<DType, RType>
{
  public:
    typedef DType DomainType;
    typedef RType RangeType;
    typedef OT OrderType;
    //typedef typename DerivativeTypefier<DomainType,RangeType>::DerivativeType DerivativeType;

    virtual ~VirtualMultiDifferentiableFunction() {}

    /** \brief evaluation of arbitrary derivative (or function value)
     *
     *  \param x point at which to evaluate the derivative
     *  \param d describes the order of differentiation
     *  \param y will contain the derivative at x after return
     */
    void evaluate(const DomainType& x, const OrderType& d, RangeType& y) const
    {
        y = evaluate( x, d );
        return;
    }
    
    virtual RangeType evaluate(const DomainType& x, const OrderType& d) const = 0;
    
    // Should be defined by VirtualFunction already?
    void evaluate(const DomainType& x, RangeType& y) const
    {
        y = evaluate(x);
        return;
    }
    
    RangeType evaluate(const DomainType& x) const
    {
        #warning No longer uses: OrderType d(0);
        OrderType d;
        return evaluate(x,d);
    }


    /** \brief evaluation of many points at once
     *  \param x vector of points
     */
    std::vector<RangeType> evaluate( const std::vector<DomainType>& x ) const
    {
        #warning No longer uses: OrderType d(0);
        OrderType d;
        return evaluate(x,d);
    }


    /** \brief evaluation of many points at once
     *  \param x vector of points
     *  \param d describes the order of differentiation
     */
    std::vector<RangeType> evaluate( const std::vector<DomainType>& x, const OrderType& d ) const
    {
        std::vector<RangeType> values(x.size());
        for( size_t i = 0; i < x.size(); ++i )
            values[i]   = evaluate(x[i], d);
        return values;
    }


    //~ /** \brief evaluation of the gradient
     //~ *  \param x point of evaluation
     //~ */
    //~ auto evaluateGradient( const DomainType& x ) const
    //~ {
        //~ DomainType gradient;
        
        //~ // Iterate over all \partial_{i}.
        //~ OrderType d(0);
        //~ for( size_t i = 0; i < x.size(); ++i )
        //~ {
            //~ d[i] = 1;
            //~ gradient[i] = evaluate(x, d);
            //~ d[i] = 0;
        //~ }
        
        //~ return gradient;
    //~ }
    
    // Note: This is not very pretty...
    template<int n>
    auto evaluateGradient(const Dune::FieldVector<double,n>& x) const
    {
      DomainType gradient;
        
        // Iterate over all \partial_{i}.
        Dune::FieldVector<size_t,n> d(0);
        for( size_t i = 0; i < n; ++i )
        {
            d[i] = 1;
            gradient[i] = evaluate(x, (OrderType)d);
            d[i] = 0;
        }
        
        return gradient;
    }
    
    //~ /** \brief evaluation of the Hessian
     //~ *  \param x point of evaluation
     //~ *  \note Could be made more efficient by exploiting symmetry.
     //~ */
    //~ auto evaluateHessian( const DomainType& x ) const
    //~ {
        //~ Dune::FieldMatrix<RangeType,DomainType::dimension,DomainType::dimension> hessian;
        
        //~ // Iterate over all \partial_{ij}.
        //~ OrderType d(0);
        //~ for( size_t i = 0; i < x.size(); ++i )
        //~ {
            //~ d[i]++;
            //~ for( size_t j = 0; j < x.size(); ++j )
            //~ {
                //~ d[j]++;
                //~ hessian[i][j] = evaluate(x, d);
                //~ d[j]--;
            //~ }
            //~ d[i]--;
        //~ }
        
        //~ return hessian;
    //~ }    


    //~ /** \brief evaluation of the Laplacian
     //~ *  \param x point at which to evaluate the Laplacian
     //~ */
    //~ RangeType evaluateLaplacian( const DomainType& x ) const
    //~ {
        //~ RangeType value = 0;
        
        //~ // Iterate over all \partial_{ii} and sum the values.
        //~ OrderType d(0);
        //~ for( size_t i = 0; i < x.size(); ++i )
        //~ {
            //~ d[i]    = 2;
            //~ value   += evaluate(x, d);
            //~ d[i]    = 0;
        //~ }
        
        //~ return value;
    //~ }


}; // end of VirtualMultiDifferentiableFunction class

}

#endif
