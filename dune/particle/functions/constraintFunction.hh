#ifndef DUNE_CONSTRAINT_FUNCTION_HH
#define DUNE_CONSTRAINT_FUNCTION_HH


template<class ConstraintFunctions, class DomainInfo>
class ConstraintFunction
{
  // Templates in this class are completely unnecessary and simply a result of laziness.
  
  public:
  
    ConstraintFunction(const ConstraintFunctions& constraintFunctions, const DomainInfo& domainInfo)
      : constraintFunctions(constraintFunctions), domainInfo(domainInfo)
    {}
    
    template<class Element, class DomainType>
    const auto evaluateLocal(const Element& element, const DomainType& x) const
    {
      const auto id = getContainingId(element);
      const auto&& xGlobal  = element.geometry().global(x);
      
      //~ const double xs = 3, ys = M_PI/2;
      //~ std::cerr << constraintFunctions[id]->evaluate({-3./7+xs,ys}) << " vs "
                //~ << constraintFunctions[id]->evaluate({ 3./7+xs,ys}) << std::endl;
      //~ std::cerr << constraintFunctions[id]->evaluateGradient(DomainType({-3./7+xs,ys})) << " vs "
                //~ << constraintFunctions[id]->evaluateGradient(DomainType({ 3./7+xs,ys})) << std::endl;
      //~ std::cerr << constraintFunctions[id]->evaluateGradient(DomainType({ xs,-3./7+ys})) << " vs "
                //~ << constraintFunctions[id]->evaluateGradient(DomainType({ xs,+3./7+ys})) << std::endl;
      //~ DUNE_THROW(Dune::Exception, "Stop.");
      //~ const auto val1 = constraintFunctions[id]->evaluate(xGlobal);
      //~ const auto val2 = constraintFunctions[id]->evaluate(DomainType({2*xs-xGlobal[0],xGlobal[1]}));
      //~ const auto val3 = constraintFunctions[id]->evaluateGradient(xGlobal);
      //~ const auto val4 = constraintFunctions[id]->evaluateGradient(DomainType({2*xs-xGlobal[0],xGlobal[1]}));
      //~ const auto eps  = 1e-8;
      //~ if( std::abs(val1-val2) > eps || std::abs(val3[0]+val4[0]) > eps || std::abs(val3[1]-val4[1]) > eps )
      //~ {
        //~ std::cerr << xGlobal << ";\t" << val1 << ";\t" << val2 << ";\t" << val3 << ";\t" << val4 << std::endl;
        //~ DUNE_THROW(Dune::Exception, "Discrepancy.");
      //~ }
      
      Dune::FieldVector<double,1> val = 0;
      if(id >= 0)
        val = constraintFunctions[id]->evaluate(xGlobal);
      //~ std::cerr << "value in " << xGlobal << " is " << val << ", mismatch: " << (1.5-1*sin(xGlobal[1]))/sin(xGlobal[1])-val << std::endl;
      return val;
    }
    
    template<class Element, class DomainType>
    const auto evaluateGradientLocal(const Element& element, const DomainType& x) const
    {
      const auto id = getContainingId(element);
      const auto&& xGlobal  = element.geometry().global(x);

      DomainType gradient(0);
      if(id >= 0)
        gradient = constraintFunctions[id]->evaluateGradient(xGlobal);
      return gradient;
    }
    
  private:
  
    const ConstraintFunctions& constraintFunctions;
    const DomainInfo& domainInfo;
    
    template<class Element>
    int getContainingId(const Element& element) const
    {
      int id = -1;
      bool flag = false;

      for(int i = 0; i < domainInfo.numLevelSets(); ++i)
      {
        if( domainInfo.isBoundary(element,i) )
        {
          if(flag)
            DUNE_THROW(Dune::Exception, "One element has multiple level sets.");
          flag  = true;
          id  = i;
        }
      }

      return id;
    }
};

#endif
