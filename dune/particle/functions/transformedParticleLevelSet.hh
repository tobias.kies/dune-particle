#ifndef DUNE_TRANSFORMED_PARTICLE_LEVEL_SET_HH
#define DUNE_TRANSFORMED_PARTICLE_LEVEL_SET_HH

template<class LevelSetFunction, class Intersection, class Surface, class Transformation>
class TransformedParticleLevelSet
  : public Dune::VirtualMultiDifferentiableFunction
    <
      typename LevelSetFunction::DomainType,
      typename LevelSetFunction::RangeType,
      typename LevelSetFunction::OrderType
    >
{
  public:

    typedef Dune::VirtualMultiDifferentiableFunction<
        typename LevelSetFunction::DomainType,
        typename LevelSetFunction::RangeType,
        typename LevelSetFunction::OrderType
      > BaseType;
    
    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::OrderType OrderType;
    
    typedef typename std::tuple_element<1,typename Intersection::DomainType>::type Position;


    TransformedParticleLevelSet(const LevelSetFunction& levelSetFunction, const Intersection& intersection, const Position& position, const Surface& surface, const Transformation& transformation, const bool fPeriodic)
      : levelSetFunction(levelSetFunction), intersection(intersection), position(position), surface(surface), transformation(transformation), fPeriodic(fPeriodic)
    {}
    
    
    RangeType evaluate(const DomainType& x, const OrderType& d) const
    {
      RangeType res = 0;
      
      // Get point of evaluation.
      typename Intersection::DomainType xp;
      xp.first  = x;
      xp.second = position;
      auto&& x_ = intersection.evaluate(xp).first;
      
      //~ if(std::isnan(x_[0]) || std::isnan(x_[1]))
        //~ std::cerr << "Psi: Mapping " << x << " to " << x_ << " using position " << position << std::endl;
      if(fPeriodic)
        x_ = transformation.applyPeriodicity(surface, position, x_);
      //~ if(std::isnan(x_[0]) || std::isnan(x_[1]))
        //~ std::cerr << "Periodicity: Mapping " << x << " to " << x_ << " using position " << position << std::endl;
      
      const auto order = orderOf(d);
      switch(order)
      {
        case 0:
          res = levelSetFunction.evaluate(x_);
          break;
          
        case 1:
          res = evaluateFirstDerivative(x_, xp, d);
          break;
          
        default:
          DUNE_THROW(Dune::NotImplemented, "Requested order of differentiation (" + std::to_string(order) + ") is not supported.");
      }

      return res;
    }


  private:
  
    const LevelSetFunction& levelSetFunction;
    const Intersection& intersection;
    const Position position;
    const Surface& surface;
    const Transformation& transformation;
    const bool fPeriodic;
    
    
    const auto orderOf(const OrderType& d) const
    {
      int order = 0;
      for(int i = 0; i < d.size(); ++i)
        order += d[i];
      return order;
    }
    
    const auto directionOf(const OrderType& d) const
    {
      if(orderOf(d) != 1)
        DUNE_THROW(Dune::Exception, "Invalid d.");
      
      int dir = -1;
      for(int i = 0; i < d.size() && dir == -1; ++i)
        if(d[i] != 0)
          dir = i;
      return dir;
    }
    
    const auto evaluateFirstDerivative(const DomainType& x_, const typename Intersection::DomainType& x0, const OrderType& d) const
    {
      assert(orderOf(d)==1);
      
      const auto&& grad = levelSetFunction.evaluateGradient(x_);
      
      typename Intersection::OrderType d_;
      d_.first  = d;
      const auto&& dx   = intersection.evaluate(x0,d_).first;
      const auto res = grad*dx;

      /// DEBUG
      if(std::isnan(res))
      {
        Debug::dprintVector(x_, "x_");
        Debug::dprintVector(x0.first, "x0.first");
        Debug::dprintVector(x0.second, "x0.second");
        Debug::dprintVector(d, "d");
        Debug::dprintVector(grad, "grad");
        Debug::dprintVector(dx, "dx");
        DUNE_THROW(Dune::Exception, "Got NaN in level set derivative.");
      }
      ///
      
      return res;
    }   
};

#endif
