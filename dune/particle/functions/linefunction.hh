// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef LINE_FUNCTION_HH
#define LINE_FUNCTION_HH

#include <dune/common/fmatrix.hh>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>

template<class Function>
class LineFunction :
    public Dune::VirtualMultiDifferentiableFunction<typename Function::DomainType::value_type,typename Function::RangeType,typename std::array<size_t,1>>
{
    private:

        typedef Dune::VirtualMultiDifferentiableFunction<
                    typename Function::DomainType::value_type,
                    typename Function::RangeType,
                    typename std::array<size_t,1>>
                BaseType;

    public:

        typedef typename Function::DomainType FunctionDomainType;
        typedef typename Function::OrderType FunctionOrderType;
        typedef typename BaseType::DomainType::value_type DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::OrderType OrderType;
        typedef typename DomainType::value_type float_x;
        typedef float_x ScalarType;

        using BaseType::evaluate;


        LineFunction( const Function& function__, const FunctionDomainType& basis__, const FunctionDomainType& direction__ )
        : function_(function__), basis_(basis__), direction_(direction__)
        {}


        // Remark: Evaluation here could also be written down using multi-variate binomial coefficients.
        RangeType evaluate( const DomainType& t, const OrderType& d ) const
        {
            RangeType res = 0;
            
            // Compute point of evaluation.
            FunctionDomainType x(basis_);
            x.axpy(t, direction_);
            
            // Determine differentiation order and reformat differentiation order parameter.
            const size_t difOrder = MultiIndexHelper::shortMultiIndexOrder(d);
            const auto alpha = MultiIndexHelper::toLongMultiIndex(d);
            
            // Iterate over all multi-indices in {1,\dots,dim}^difOrder
            const size_t maxI = std::pow( (size_t) FunctionDomainType::dimension, difOrder );
            for( size_t i = 0; i < maxI; ++i )
            {
                const auto beta = MultiIndexHelper::toLongMultiIndex( i, FunctionDomainType::dimension, difOrder );
                const auto betaShort = MultiIndexHelper::template toShortMultiIndex<OrderType>( beta );
                
                // Compute coefficient.
                float_x c = 1;
                for( size_t j = 0; j < difOrder; ++j )
                    c   *= direction_[beta[j]];
                    
                // Skip if constant is trivial.
                if( c == 0 )
                    continue;
                
                // Evaluate \partial^\beta f in Rx.
                const auto fval = function_.evaluate( x, betaShort );
                    
                // Update res.
                res += fval*c;
            }
            
            return res;
        }


    private:

        const Function& function_;
        const FunctionDomainType basis_;
        const FunctionDomainType direction_;

};

#endif
