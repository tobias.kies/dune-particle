// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=8 sw=2 et sts=2:
#ifndef LINEARLY_REPARAMETERIZED_FUNCTION_HH
#define LINEARLY_REPARAMETERIZED_FUNCTION_HH

#include <math.h>
#include <dune/common/fmatrix.hh>
#include <dune/particle/functions/virtualmultidifferentiablefunction.hh>
#include <dune/particle/helper/multiindexhelper.hh>

/**
 * Represents a function f(A*x + x0).
 */
template<class Function>
class LinearlyReparameterizedFunction :
    public Dune::VirtualMultiDifferentiableFunction<typename Function::DomainType,typename Function::RangeType,typename Function::OrderType>
{
    private:

        typedef Dune::VirtualMultiDifferentiableFunction<
                    typename Function::DomainType,
                    typename Function::RangeType,
                    typename Function::OrderType>
                BaseType;

    public:

        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::OrderType OrderType;
        typedef typename DomainType::value_type float_x;
        typedef float_x ScalarType;
        typedef Dune::FieldMatrix<float_x,DomainType::dimension,DomainType::dimension> MatrixType;

        using BaseType::evaluate;


        LinearlyReparameterizedFunction( const Function& function__, const MatrixType& A__, const DomainType& x0__ )
        : function_(function__), x0_(x0__), A_(A__)
        {}


        RangeType evaluate( const DomainType& x, const OrderType& d ) const
        {
            RangeType res = 0;
            
            // Shift point.
            DomainType Ax_x0(x0_);
            A_.umv( x, Ax_x0 );
            
            // Determine differentiation order and reformat differentiation order parameter.
            const size_t difOrder = MultiIndexHelper::shortMultiIndexOrder(d);
            const auto alpha = MultiIndexHelper::toLongMultiIndex(d);
            
            // Iterate over all multi-indices in {1,\dots,dim}^difOrder
            const size_t maxI = std::pow( (size_t) DomainType::dimension, difOrder );
            for( size_t i = 0; i < maxI; ++i )
            {
                const auto beta = MultiIndexHelper::toLongMultiIndex( i, DomainType::dimension, difOrder );
                const auto betaShort = MultiIndexHelper::template toShortMultiIndex<OrderType>( beta );
                
                // Compute coefficient.
                float_x c = 1;
                for( size_t j = 0; j < difOrder; ++j )
                    c   *= A_[beta[j]][alpha[j]];
                    
                // Skip evaluation if constant is trivial.
                if( c == 0 )
                    continue;
                
                // Evaluate \partial^\beta f in Ax_x0.
                const auto fval = function_.evaluate( Ax_x0, betaShort );
                    
                // Update res.
                res += fval*c;
            }
            
            return res;
        }


    private:

        typedef Dune::FieldMatrix<float_x,DomainType::dimension,DomainType::dimension> RotationMatrix;

        const Function& function_;
        const DomainType x0_;
        const MatrixType A_;

};

#endif
