#ifndef BASIS_MULTI_DIFFERENTIABLE_GRID_FUNCTION_HH
#define BASIS_MULTI_DIFFERENTIABLE_GRID_FUNCTION_HH


#include <dune/common/typetraits.hh>
#include <dune/common/fvector.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/function.hh>

#include <dune/localfunctions/common/virtualinterface.hh>

#include <dune/istl/bvector.hh>

#include <dune/fufem/arithmetic.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/functions/virtualgridfunction.hh>
#include <dune/fufem/functions/basisgridfunction.hh>

#include <dune/particle/helper/multiindexhelper.hh>
#include <dune/particle/functions/virtualmultidifferentiablegridfunction.hh>


template<class Basis, class Coefficients, class RangeType, class OrderType>
struct LocalPartialDerivativeEvaluator
{
    template<class Element, class LocalDomainType>
    static void evaluatePartialDerivativeLocal(const Basis& basis,
                              const Coefficients& coefficients,
                              const Element& e,
                              const LocalDomainType& x,
                              RangeType& y,
                              const OrderType& d)
    {
        typedef typename Basis::LinearCombination LinearCombination;
        typedef typename Basis::LocalFiniteElement::Traits::LocalBasisType LB;
        typedef typename LB::Traits::RangeType LBRangeType;

        const LB& localBasis = basis.getLocalFiniteElement(e).localBasis();

        //~ std::vector<LBRangeType> values(localBasis.size());
        //~ for (size_t i=0; i<localBasis.size(); ++i)
            //~ localBasis.evaluate(x, i, d, values[i]);
        
        auto&& jacobianInverseTransposed = e.geometry().jacobianInverseTransposed(x);
        auto&& dLong = MultiIndexHelper::toLongMultiIndex(d);

        y = 0;
        for (size_t i=0; i<localBasis.size(); ++i)
        {
            LBRangeType value(0);
        
            //~ #warning LocalEvaluation of derivatives in BasisMultiDifferentiableGridFunction highly depends on the usage of HermiteK elements and kind of destroys the point of local evaluation.
            //~ // To-do: replace this part by a general transformation, based on the Jacobian of the transformation.
            //~ for( size_t j = 0; j < d.size(); ++j )
                //~ value    /= std::pow( localBasis.get_h(j), d[j] );
            for( size_t k = 0; k < std::pow( d.size(), dLong.size() ); ++k )
            {
                auto&& betaLong = MultiIndexHelper::toLongMultiIndex( k, d.size(), dLong.size() );
                auto&& beta     = MultiIndexHelper::toShortMultiIndex<OrderType>( betaLong );
                LBRangeType transformationFactor(1);
                for( size_t l = 0; l < dLong.size(); ++l )
                    transformationFactor    *= jacobianInverseTransposed[dLong[l]][betaLong[l]];
                if( std::abs(transformationFactor) > std::numeric_limits<LBRangeType>::epsilon()*32 )
                    value    += transformationFactor * localBasis.evaluate(x, i, beta);
            }

            int index = basis.index(e, i);
            //~ std::cerr << "on local basis function " << i << " on derivative " << d[0] << d[1] << std::endl;
            //~ std::cerr << "accessing coefficients with size " << coefficients.size() << std::endl;
            //~ std::cerr << "\tvalue=" << value << "\tindex=" << index << "\tcoefficient=" << coefficients[index] << std::endl;
            if (basis.isConstrained(index))
            {
                const LinearCombination& constraints = basis.constraints(index);
                for (size_t w=0; w<constraints.size(); ++w)
                    Arithmetic::addProduct(y, value * constraints[w].factor, coefficients[constraints[w].index]);
            }
            else
                Arithmetic::addProduct(y, value, coefficients[index]);
        }
    }
    

    template<class Element, class LocalDomainType>
    static void evaluateLaplacianLocal(const Basis& basis,
                              const Coefficients& coefficients,
                              const Element& e,
                              const LocalDomainType& x,
                              RangeType& y)
    {
        y = 0;
        RangeType partial_derivative;
        OrderType d(0);
        for( size_t i = 0; i < x.size(); ++i )
        {
            d[i]    = 2;
            partial_derivative = 0;
            evaluatePartialDerivativeLocal(basis, coefficients, e, x, partial_derivative, d);
            y += partial_derivative;
            d[i]    = 0;
        }
    }
};

/**
 * \brief Grid function obtained from global basis and coefficient vector
 *
 * \note An object of this class does not store the coefficient vector
 * \tparam B Global function space basis type
 * \tparam C Coefficient vector type
 */
template<class B, class C, class OT = typename B::LocalFiniteElement::Traits::LocalBasisType::OrderType>
class BasisMultiDifferentiableGridFunction :
    public BasisGridFunction<B,C>,
    public VirtualMultiDifferentiableGridViewFunction<typename B::GridView, typename C::value_type, OT>
{
    protected:
        typedef VirtualMultiDifferentiableGridViewFunction<typename B::GridView, typename C::value_type, OT> BaseType;
        typedef BasisGridFunction<B,C> BaseType2;

    public:

        typedef typename BaseType::LocalDomainType LocalDomainType;
        typedef typename BaseType::DomainType DomainType;
        typedef typename BaseType::RangeType RangeType;
        typedef typename BaseType::DerivativeType DerivativeType;
        typedef typename BaseType::Element Element;
        typedef typename BaseType::GridView GridView;
        typedef typename BaseType::Grid Grid;
        typedef typename BaseType::OrderType OrderType;

        //! The type of function space basis
        typedef B Basis;

        //! The type of the coefficient vectors
        typedef C CoefficientVector;
        
        using BaseType::evaluate;
        using BaseType2::evaluate;
        
        using BaseType::isDefinedOn;

        /**
         * \brief Setup grid function
         *
         * \param basis Global function space basis
         * \param coefficients Coefficient vector
         */
        BasisMultiDifferentiableGridFunction(const Basis& basis, const CoefficientVector& coefficients) :
            BasisGridFunction<B,C>( basis, coefficients ),
            BaseType(basis.getGridView())//,
            //~ basis_(basis),
            //~ coefficients_(coefficients)
        {
            //~ std::cerr << "bmdgf initialized " << coefficients_.size() << std::endl;
          assert(basis_.size() == coefficients_.size());
        }



        /**
         * \copydoc VirtualGridFunction::evaluateLocal
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, RangeType& y) const
        {
            LocalEvaluator<B, CoefficientVector, RangeType>::evaluateLocal(basis_, coefficients_, e, x, y);
        }
        
        virtual RangeType evaluateLocal(const Element& e, const LocalDomainType& x) const
        {
            RangeType y;
            evaluateLocal(e,x,y);
            return y;
        }

        
        /**
         * \copydoc VirtualGridFunction::evaluateLocal
         */
        virtual void evaluateLocal(const Element& e, const LocalDomainType& x, const OrderType& d, RangeType& y) const
        {
            LocalPartialDerivativeEvaluator<B, CoefficientVector, RangeType, OrderType>::evaluatePartialDerivativeLocal(basis_, coefficients_, e, x, y, d);
        }
        
        virtual RangeType evaluateLocal(const Element& e, const LocalDomainType& x, const OrderType& d) const
        {
            RangeType y;
            LocalPartialDerivativeEvaluator<B, CoefficientVector, RangeType, OrderType>::evaluatePartialDerivativeLocal(basis_, coefficients_, e, x, y, d);
            return y;
        }
        
        
        /**
         * \brief Evaluate the Laplacian in local coordinates
         * 
         * \param e Element with respect to which the Laplacian is evaluated
         * \param x Point of evaluation in local coordinates
         * \param y Destination where the result will be stored
         */
        virtual void evaluateLaplacianLocal(const Element&e, const LocalDomainType& x, RangeType& y) const
        {
            LocalPartialDerivativeEvaluator<B, CoefficientVector, RangeType, OrderType>::evaluateLaplacianLocal(basis_, coefficients_, e, x, y);
        }
        
        
        /**
         * \brief Evaluate the Laplacian in local coordinates
         * 
         * \param e Element with respect to which the Laplacian is evaluated
         * \param x Point of evaluation in local coordinates
         * 
         * \return Value of the Laplacian
         */
        virtual RangeType evaluateLaplacianLocal(const Element&e, const LocalDomainType& x) const
        {
            RangeType y;
            LocalPartialDerivativeEvaluator<B, CoefficientVector, RangeType, OrderType>::evaluateLaplacianLocal(basis_, coefficients_, e, x, y);
            return y;
        }



        virtual void evaluate(const DomainType& x, RangeType& y) const
        {
            BasisGridFunction<B,C>::evaluate(x,y);
            return;
        }
        
        virtual void evaluate(const DomainType& x, const OrderType& d, RangeType& y) const
        {
            BaseType::evaluate(x,d,y);
        }
        
        virtual RangeType evaluate(const DomainType& x, const OrderType& d) const
        {
            RangeType y;
            evaluate(x,d,y);
            return y;
        }
        
        virtual RangeType evaluate(const DomainType& x) const
        {
            RangeType y;
            BasisGridFunction<B,C>::evaluate(x,y);
            return y;
        }
        
        
        
        
        
        virtual void evaluateDerivative(const DomainType& x, DerivativeType& d) const
        {
            DUNE_THROW(Dune::NotImplemented, "evaluateDerivativeLocal not implemented for this RangeType");
        }


        /**
         * \copydoc VirtualGridFunction::evaluateDerivativeLocal
         */
        void evaluateDerivativeLocal(const Element& e, const LocalDomainType& x, DerivativeType& d) const
        {
            LocalDerivativeEvaluator<B, CoefficientVector, RangeType>::evaluateDerivativeLocal(basis_, coefficients_, e, x, d);
        }


        /**
         * \copydoc BasisGridFunctionInfo::isRefinedLocalFiniteElement
         */
        virtual bool isRefinedLocalFiniteElement(const Element& e) const
        {
            return IsRefinedLocalFiniteElement<typename Basis::LocalFiniteElement>::value(basis_.getLocalFiniteElement(e));
        }



        /**
         * \brief Get QuadratureRuleKey needed to integrate on given element
         */
        virtual QuadratureRuleKey quadratureRuleKey(const Element& e) const
        {
            return QuadratureRuleKey(basis_.getLocalFiniteElement(e));
        }



        /**
         * \brief Export basis
         */
        const Basis& basis() const
        {
            return BasisGridFunction<B,C>::basis();
        }



        /**
         * \brief Export coefficient vector
         */
        const CoefficientVector& coefficientVector() const
        {
            return BasisGridFunction<B,C>::coefficientVector();
        }



        const Grid& grid() const
        {
            return BasisGridFunction<B,C>::grid();
        }
        
        #warning Introduced this for some obscure reasons. Has to be removed again.
        const auto& innerFunction( ) const
        {
            return *this;
        }
        const auto& middleFunction( ) const
        {
            return *this;
        }
        const auto& outerFunction( ) const
        {
            return *this;
        }

    protected:

        //~ const Basis& basis_;
        //~ const CoefficientVector& coefficients_;
        //~ #warning Coefficient vector modified to contain a full copy of the vector. This may often be rather inefficient.
          //~ CoefficientVector coefficients_;
        
        using BaseType2::basis_;
        using BaseType2::coefficients_;
};


namespace Functions
{

template<class B, class C, class O>
BasisMultiDifferentiableGridFunction<B, C, O> makeFunction(const B& basis, const C& coeff)
{
    return BasisMultiDifferentiableGridFunction<B,C,O>(basis, coeff);
}

} // end namespace Functions



#endif

