#ifndef DUNE_RESCALED_MEMBRANE_ENERGY
#define DUNE_RESCALED_MEMBRANE_ENERGY

template<class Problem, class Rescaling, class float_x = double>
class RescaledMembraneEnergy
{
  public:
  
    typedef typename Problem::Position Position;
    
    RescaledMembraneEnergy(Problem& problem, const Rescaling& rescaling, const double h)
      : problem(problem), rescaling(rescaling), h(h)
    {
    }
    
    auto evaluate(const Position& x) const
    {
      if(!isCurrentPosition(x))
        updatePosition(x);
        
      return problem.energy();
    }
    
    auto evaluateGradient(const Position& x) const
    {
      if(!isCurrentPosition(x))
        updatePosition(x);
        
      return applyRescaling(problem.gradient());
    }
    
    auto lowerBound() const
    {
      return applyInverseRescaling(problem.lowerBound());
    }
    
    auto upperBound() const
    {
      return applyInverseRescaling(problem.upperBound());
    }
    
    auto applyInverseRescaling(const Position& x) const
    {
      auto y = x;
      for(int i = 0; i < x.size(); ++i)
        for(int j = 0; j < x[i].size(); ++j)
          y[i][j]  /= rescaling(i,j);
      return y;
    }
    
    auto applyRescaling(const Position& x) const
    {
      auto y = x;
      for(int i = 0; i < x.size(); ++i)
        for(int j = 0; j < x[i].size(); ++j)
          y[i][j]  *= rescaling(i,j);
      return y;
    }
    
    
    Problem& problem;
    double h;
    mutable Position currentPosition;
  private:
    const Rescaling& rescaling;
    
    
    bool isCurrentPosition(const Position& other) const
    {
      if(currentPosition.size() != other.size())
        return false;
        
      for(int i = 0; i < other.size(); ++i)
      {
        if(currentPosition[i].size() != other[i].size())
          return false;

        for(int j = 0; j < other[i].size(); ++j)
          if(currentPosition[i][j] != other[i][j])
            return false;
      }
      
      return true;
    }
    
    mutable int c = 0; // DEBUG PURPOSE ONLY
    void updatePosition(const Position& other) const
    {
      Debug::dprintColumnVector(other, "current position");
      Debug::dprintColumnVector(applyRescaling(other), "current rescaled position");

      currentPosition = other;
      problem.solve(h, applyRescaling(other));
      
      //~ problem.saveMembrane("./iterates/debug_iterate_" + std::to_string(c++));
    }
};

#endif
