#ifndef DUNE_MEMBRANE_MULTIGRID_SOLVER_HH
#define DUNE_MEMBRANE_MULTIGRID_SOLVER_HH

// dune-solver includes
#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/iterationsteps/multigridstep.hh>
#include <dune/solvers/test/common.hh>
#include <dune/solvers/iterationsteps/cgstep.hh>

// Other includes.
#include <dune/particle/constraints/constraintsMachine.hh>
#include <dune/particle/assembler/transferoperatorassemblerextended.hh>

template<class Model, class Basis, class LevelBasis>
class MembraneMultiGridSolver
{
  public:
  
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>> Matrix;
    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;
    typedef Dune::BitSetVector<1> BitVector;
    typedef typename Model::ParameterizedPoints ConstraintPoints;
    
    typedef EnergyNorm<Matrix, Vector> Norm;
    typedef ::LoopSolver<Vector> Solver;
    typedef MultigridStep<Matrix, Vector, BitVector> MGStep;
    typedef MultigridTransfer<Vector, BitVector, Matrix> Transfer;
    typedef CompressedMultigridTransfer<Vector, BitVector, Matrix> TransferImplementation;
    typedef TransferOperatorAssemblerExtended<Basis,LevelBasis> TOA;
    
    typedef typename std::decay<decltype(Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(Dune::Solvers::BlockGS::LocalSolvers::gs()))>::type Smoother;
        
    static bool fVerbose;

    static constexpr double tol = 1e-12;
    static const int maxIterations = 2e2;
    static const int maxIterationsCG = 2e2;
        
    // It does not make much sense to have b in the constructor; is just here for backwards compatibility reasons.
    MembraneMultiGridSolver(const Matrix& A_, const Vector& b, const Model& model, const Basis& basis, const ConstraintPoints& constraintPoints)
      : A(A_), b_(b), model(model), basis(basis), constraintPoints(constraintPoints),
        smoother(Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(Dune::Solvers::BlockGS::LocalSolvers::gs())),
        baseSolverStep(Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(Dune::Solvers::BlockGS::LocalSolvers::gs()))
    {
      // Get ignore DOFs.
      pointIdToDOF     = ConstraintsMachine::mapPointsToDOFs(basis, constraintPoints.first);

      const auto&& ignoreDOFsSurface  = model.getSurface().ignoreDOFs(basis);
      Dune::BitSetVector<1> ignoreDOFsParticle(basis.size(),false);
      for(const auto& rel : pointIdToDOF) // For point value constraints
        ignoreDOFsParticle[rel.second] = true;
      ignoreDOFs   = Misc::joinDOFs(ignoreDOFsSurface, ignoreDOFsParticle);

      // Get grid information.
      const auto& grid = basis.getGridView().grid();

      // Vector of pointers to the transfer operator base class.
      transfer.resize(grid.maxLevel());
      hierarchy.resize(grid.maxLevel());
        
      // Set transfer operators.
      TOA toa(basis);
      toa.assembleOperatorHierarchy(hierarchy);
      for( size_t i = 0; i < transfer.size(); ++i )
        transfer[i] = &hierarchy[i];

      // Set up smoothers and basesolver.
      ///pSmoother       = std::shared_ptr<Smoother>(Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(Dune::Solvers::BlockGS::LocalSolvers::gs()));
      ///pBaseSolverStep = std::shared_ptr<Smoother>(Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(Dune::Solvers::BlockGS::LocalSolvers::gs()));
      ///pBaseNorm       = std::shared_ptr<Norm>(*pBaseSolverStep);
      Norm baseNorm(baseSolverStep);
      //~ pBaseNorm       = std::shared_ptr<Norm>(baseSolverStep);
      pBaseNorm       = std::make_shared<Norm>(baseNorm);
      ///pBaseSolver     = std::shared_ptr<Solver>(pBaseSolverStep.get(), 1, 1e-10, pBaseNorm.get(), Solver::QUIET);
      Solver baseSolver(&baseSolverStep, 1, 1e-10, pBaseNorm.get(), Solver::QUIET);
      //~ pBaseSolver     = std::shared_ptr<Solver>(&baseSolverStep, 1, 1e-10, pBaseNorm.get(), Solver::QUIET);
      pBaseSolver     = std::make_shared<Solver>(baseSolver);
      
      // Create iteration step and set various kinds of data.
      mgStep.setTransferOperators(transfer);
      mgStep.setProblem(A,u,b);
      mgStep.setSmoother(&smoother);
      mgStep.setMGType(1,3,3);
      mgStep.setIgnore(ignoreDOFs);
      mgStep.basesolver_  = pBaseSolver.get();

      // Set energy norm.
      Norm energyNorm;
      energyNorm.setMatrix(&A_);
      //~ pEnergyNorm = std::shared_ptr<Norm>();
      pEnergyNorm = std::make_shared<Norm>(energyNorm);
      //~ pEnergyNorm->setMatrix(&A_);

      // Create loop solver.
      Dune::Solvers::CGStep<Matrix,Vector> cgStep(A, u, b_, mgStep);
      //~ pcgStep = std::shared_ptr<Dune::Solvers::CGStep<Matrix,Vector>>(A, u, b_, mgStep);
      pcgStep = std::make_shared<Dune::Solvers::CGStep<Matrix,Vector>>(cgStep);
      pcgStep->setIgnore(ignoreDOFs);
      //~ pSolver = std::shared_ptr<Solver>(pcgStep.get(), maxIterationsCG, tol, *pEnergyNorm, Solver::FULL);
      Solver solver(&mgStep, maxIterations, tol, pEnergyNorm.get(), Solver::FULL);
      //~ pSolver = std::shared_ptr<Solver>(&mgStep, maxIterations, tol, pEnergyNorm.get(), Solver::FULL);
      pSolver = std::make_shared<Solver>(solver);

      // Preprocess.
      pSolver->check();
      pSolver->preprocess();
    }
    
    
    auto solve() const
    {
      // Set solution (and right hand side) in point constraints.
      u *= 0;
      u.resize(A.M(),0);
      auto b = b_;
      for(const auto& rel : pointIdToDOF)
      {
        b[rel.second] = constraintPoints.second[rel.first]; // Necessary?
        u[rel.second] = constraintPoints.second[rel.first];
      }
      mgStep.setRhs(b);
      pSolver->solve();
      
      return mgStep.getSol();
      //~ return u;
    }
    
    auto solve(const Vector& b)
    {
      setRhs(b);
      return solve();
    }
        
    template<class Rhs>
    void setConstraintPointsRhs(const Rhs& rhs)
    {
      if(rhs.size() != constraintPoints.first.size())
      {
        Debug::dprint(rhs.size(), "vs", constraintPoints.first.size());
        assert(rhs.size() == constraintPoints.first.size());
        DUNE_THROW(Dune::Exception, "rhs and constraintPoints do not match.");
      }
      constraintPoints.second = rhs;
    }
    
    void setRhs(const Vector& b__)
    {
      assert(b__.size() == A.N());
      b_  = b__;
    }

  private:
    const Matrix& A;
          Vector b_;
    mutable Vector u;
    const Model& model;
    const Basis& basis;
          ConstraintPoints constraintPoints;
          BitVector ignoreDOFs;
          std::map<int,int> pointIdToDOF;
    
    mutable MGStep mgStep;
    std::vector<Transfer*> transfer;
    std::vector<TransferImplementation> hierarchy;
    
    std::shared_ptr<Norm> pBaseNorm;
    std::shared_ptr<Norm> pEnergyNorm;
    
    ///std::shared_ptr<Smoother> pSmoother;
    ///std::shared_ptr<Smoother> pBaseSolverStep;
  
    typedef Dune::Solvers::BlockGS::LocalSolvers::GS LocalSolverGS;
    Dune::Solvers::BlockGSStep<LocalSolverGS, Matrix, Vector, BitVector> smoother;
    Dune::Solvers::BlockGSStep<LocalSolverGS, Matrix, Vector, BitVector> baseSolverStep;
  
  
    std::shared_ptr<Dune::Solvers::CGStep<Matrix,Vector>> pcgStep;
    std::shared_ptr<Solver> pBaseSolver;
    std::shared_ptr<Solver> pSolver;
};


template<class Model, class Basis, class LevelBasis>
bool MembraneMultiGridSolver<Model,Basis,LevelBasis>::fVerbose  = false;

#endif
