#ifndef DUNE_MEMBRANE_SOLVER_HH
#define DUNE_MEMBRANE_SOLVER_HH


#include <dune/istl/superlu.hh>
#include <dune/particle/constraints/constraintsMachine.hh>
#include <dune/particle/helper/misc.hh>

template<class Model, class Basis>
class MembraneSolver
{
  public:
  
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>> Matrix;
    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;
    typedef typename Model::ParameterizedPoints ConstraintPoints;
    
    static bool fVerbose;

    // It does not make much sense to have b in the constructor; is just here for backwards compatibility reasons.
    MembraneSolver(const Matrix& A_, const Vector& b, const Model& model, const Basis& basis, const ConstraintPoints& constraintPoints)
      : b_(b), model(model), basis(basis), constraintPoints(constraintPoints)
    {
      // Preprocess system.
      pointIdToDOF     = ConstraintsMachine::mapPointsToDOFs(basis, constraintPoints.first);

      const auto&& ignoreDOFsSurface  = model.getSurface().ignoreDOFs(basis);
      Dune::BitSetVector<1> ignoreDOFsParticle(basis.size(),false);
      for(const auto& rel : pointIdToDOF) // For point value constraints
        ignoreDOFsParticle[rel.second] = true;
      ignoreDOFs   = Misc::joinDOFs(ignoreDOFsSurface, ignoreDOFsParticle);

      A = Misc::incorporateDOFs(A_, b_, basis, ignoreDOFs);
      pSolverSuperLU = std::make_shared<Dune::SuperLU<Matrix>>(A, fVerbose);
    }
    
    
    auto solve() const
    {
      // Preprocess right hand side.
      auto b = b_;
      //~ for(int i = 0; i < b.size(); ++i)
        //~ if(ignoreDOFs[i].any() || basis.isConstrained()[i].any())
          //~ b[i]  = 0;
      for(const auto& rel : pointIdToDOF)
      {
        //~ std::cerr << rel.second << " (point id " << rel.first << ") -> " << constraintPoints.second[rel.first] << std::endl;
        b[rel.second] = constraintPoints.second[rel.first];
      }

      // Solve system.
      Vector u(A.M(), 0);
      Dune::InverseOperatorResult res;
      pSolverSuperLU->apply(u, b, res);
      //~ Debug::dprint("Solved system.");
      
      return u;
    }
    
    void solve(const Vector& b)
    {
      setRhs(b);
      return solve();
    }
        
    template<class Rhs>
    void setConstraintPointsRhs(const Rhs& rhs)
    {
      if(rhs.size() != constraintPoints.first.size())
      {
        Debug::dprint(rhs.size(), "vs", constraintPoints.first.size());
        assert(rhs.size() == constraintPoints.first.size());
        DUNE_THROW(Dune::Exception, "rhs and constraintPoints do not match.");
      }
      constraintPoints.second = rhs;
    }
    
    void setRhs(const Vector& b__)
    {
      assert(b__.size() == A.N());
      b_  = b__;
    }
    
    auto& getBaseSolver()
    {
      if(pSolverSuperLU.get() == nullptr)
        DUNE_THROW(Dune::Exception, "BaseSolver is undefined.");
      return *pSolverSuperLU;
    }
    
    const auto& getPointDOFmap() const
    {
      return pointIdToDOF;
    }

  private:
          Matrix A;
          Vector b_;
    const Model& model;
    const Basis& basis;
          ConstraintPoints constraintPoints;
          Dune::BitSetVector<1> ignoreDOFs;
          std::map<int,int> pointIdToDOF;
          
    std::shared_ptr<Dune::SuperLU<Matrix>> pSolverSuperLU;
};


template<class Model, class Basis>
bool MembraneSolver<Model,Basis>::fVerbose  = false;

#endif
