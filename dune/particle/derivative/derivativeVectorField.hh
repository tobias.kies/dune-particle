#ifndef DUNE_DERIVATIVE_VECTOR_FIELD_HH
#define DUNE_DERIVATIVE_VECTOR_FIELD_HH

#include <dune/particle/functions/basismultidifferentiablegridfunction.hh>

#include <dune/particle/parameterization/perturbationParticleIntersection.hh>
#include <dune/particle/parameterization/perturbationParticleInverseIntersection.hh>

#include <dune/particle/integrand/mongeGaugeIntegrand.hh>

#include <dune/particle/derivative/derivativeVectorFieldBoundaryConditions.hh>

#include <dune/particle/smoother/matrixSmoother.hh>


template<class DomainInfo, class Model, class Solver, class T = Dune::FieldVector<double,3>> // Note: The third component contains the part for preserving the boundary values.
class DerivativeVectorField
  : BasisMultiDifferentiableGridFunction<typename DomainInfo::Basis,Dune::BlockVector<T>>
{
  public:

    typedef typename DomainInfo::Basis Basis;

    typedef Dune::BlockVector<T> Vector;
    typedef BasisMultiDifferentiableGridFunction<Basis,Vector> Base;

    typedef typename Basis::GridType Grid;
    typedef typename Basis::LocalFiniteElement LFE;
    typedef Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1>> Matrix;

    using Base::basis;
    using Base::coefficientVector;

    DerivativeVectorField(const DomainInfo& domainInfo, const Model& model, Solver& solver)
      : Base(domainInfo.getBasis(), Vector(domainInfo.getBasis().size(), 0)), domainInfo(domainInfo), model(model), solver(solver)
    {
      //~ assembleSmoothingMatrix(); --> Not necessary anymore because we use the smoother from the main problem.
    }


    void setLevelSetAndDirection(const int levelSetId, const int directionId)
    {
      // Prepare boundary conditions and integrand.
      typedef typename Model::Surface Surface;
      typedef typename Model::Particle Particle;
      typedef typename Model::Transformation Transformation;
      typedef typename Model::Position::value_type Position;
      typedef typename Model::Mismatch MismatchFunction;
      typedef typename Model::Transformation::DescriptorOT Direction;

      Direction d(0);
      d[directionId]  = 1;

      typedef PerturbationParticleIntersection<MismatchFunction> Phi;
      Phi phi(model.getMismatchFunction());

      typedef PerturbationParticleInverseIntersection<Surface,Particle,Transformation> PhiInv;
      PhiInv phiInv(model.getSurface(), model.getParticle(), model.getTransformation());

      typedef DerivativeVectorFieldBoundaryConditions<Position,Direction,Phi,PhiInv,Particle,Surface,Transformation> F;
      F f(model.getPosition()[levelSetId], d, phi, phiInv, model.getParticle(), model.getSurface(), model.getTransformation(), model.getPeriodicity());

      #warning The integrand coefficients and the choice of functional assembler are currently hard-coded. This is problematic since we re-use the solver from the problem. Therefore, it can happen that right hand side and matrix system are not compatible with each other! 
      typedef MongeGaugeIntegrand<F> Integrand;
      Integrand integrand(1,0,f);

      // Assemble right hand side (only on the corresponding level set dofs).
      typedef FictitiousNitscheFunctionalAssembler<Integrand,DomainInfo,Grid,LFE,T> FunctionalAssembler;
      FunctionalAssembler functionalAssembler(integrand, domainInfo, true, levelSetId+1); // Need to set id+1 because id==0 corresponds to the domain level set.

      Assembler<Basis,Basis> assembler(Base::basis_, Base::basis_);
      assembler.assembleFunctional(functionalAssembler, Base::coefficients_);

      // Compute point constraints.
      const auto&& baseConstraints = model.transformedParameterizedPoints();
      typename MatrixSmoother<Matrix,Model,Basis>::PointConstraints pointConstraints(T::dimension);
      for(int i = 0; i < T::dimension; ++i)
        pointConstraints[i] = baseConstraints;

      for(int j = 0; j < baseConstraints.first.size(); ++j)
      {
        if(model.pointIndexBelongsToLevelSet(j,levelSetId))
        {
          // Evaluate boundary condition.
          const auto boundaryCondition  = f.evaluate(baseConstraints.first[j]);
          for(int i = 0; i < T::dimension; ++i)
            pointConstraints[i].second[j] = boundaryCondition[i];
        }
        else
        {
          for(int i = 0; i < T::dimension; ++i)
            pointConstraints[i].second[j] = 0.;
        }
      }

      // Apply smoother.
      MatrixSmoother<Matrix,Model,Basis> smoother(smoothingMatrix, model, Base::basis_);
      smoother.apply(Base::coefficients_, pointConstraints, solver);
    }


  protected:

    const DomainInfo& domainInfo;
    const Model& model;
    #if USE_OMP
          Solver solver;
    #else
          Solver& solver;
    #endif

    Matrix smoothingMatrix;


    /*void assembleSmoothingMatrix()
    {
      DummyConstraintFunction f;
      typedef MongeGaugeIntegrand<> Integrand;
      Integrand integrand(1,1,f);

      typedef FictitiousNitscheVolumeAssembler<Integrand,DomainInfo,Grid,LFE,LFE> VolumeAssembler;
      typedef FictitiousNitscheTraceAssembler<Integrand,DomainInfo,Grid,LFE,LFE> TraceAssembler;
      typedef StabilizationAssembler<Basis,DomainInfo,Grid,LFE,LFE> StabAssembler;

      VolumeAssembler volumeAssembler(integrand, domainInfo);
      TraceAssembler traceAssembler(integrand, domainInfo);
      StabAssembler stabilizationAssembler(Base::basis_, domainInfo);

      Matrix volumeMatrix, traceMatrix, stabilizationMatrix;

      Assembler<Basis,Basis> assembler(Base::basis_, Base::basis_);
      assembler.assembleOperator(volumeAssembler, volumeMatrix);
      assembler.assembleOperator(traceAssembler, traceMatrix);
      assembler.assembleIntersectionOperator(stabilizationAssembler, stabilizationMatrix);

      const auto&& smoothingMatrix_  = Misc::joinSparseMatrices(volumeMatrix, traceMatrix, stabilizationMatrix);
      
      // Incorporate ignore DOFs etc.
      #warning In the sense of program logic this should be done somewhere else. For the sake of efficiency, however, I put it here.
      const auto& basis = Base::basis_;
      const auto&& constraintPoints = model.transformedParameterizedPoints();
      const auto&& pointIdToDOF     = Misc::mapPointsToDOFs(basis, constraintPoints.first);

      const auto&& ignoreDOFsSurface  = model.getSurface().ignoreDOFs(basis);
      Dune::BitSetVector<1> ignoreDOFsParticle(basis.size(),false);
      for(const auto& rel : pointIdToDOF) // For point value constraints
        ignoreDOFsParticle[rel.second] = true;
      const auto&& ignoreDOFs   = Misc::joinDOFs(ignoreDOFsSurface, ignoreDOFsParticle);

      smoothingMatrix = Misc::incorporateDOFs(smoothingMatrix_, basis, ignoreDOFs);
    }*/
};

#endif
