#ifndef DUNE_PARALLEL_DERIVATIVE_VECTOR_FIELD_HH
#define DUNE_PARALLEL_DERIVATIVE_VECTOR_FIELD_HH

#include <dune/particle/functions/basismultidifferentiablegridfunction.hh>

#include <dune/particle/parameterization/perturbationParticleIntersection.hh>
#include <dune/particle/parameterization/perturbationParticleInverseIntersection.hh>

#include <dune/particle/integrand/mongeGaugeIntegrand.hh>

#include <dune/particle/derivative/derivativeVectorFieldBoundaryConditions.hh>

#include <dune/particle/smoother/matrixSmoother.hh>

#define _DERIVATIVE_PARALLEL_BY_OMP_ 1


// Derivative vector field approach that is better designed to allow the
// parallel computation of extensions of the involved boundary conditions.

// Is a little hacky where we steal the SuperLU solver from the (Membrane)Solver.
// This has no really good reason and is mostly due to attempts to maintain
// compatibility with previously existing code and to re-use certain
// resources efficiently.


template<class Solver, class Basis, class T = Dune::FieldVector<double,3>>
class ParallelDerivativeVectorFieldBase
  : BasisMultiDifferentiableGridFunction<Basis,Dune::BlockVector<T>>
{
  public:

    using ComponentVector = Dune::BlockVector<T>;
    using Base = BasisMultiDifferentiableGridFunction<Basis,ComponentVector>;
    
    using Base::basis;
    using Base::coefficientVector;
    using Base::CoefficientVector;
    
    using DenseMatrix = Dune::Matrix<Dune::FieldVector<double,1>>;
    using S = typename DenseMatrix::block_type;
    
    enum{nComponents = T::dimension};
    
    
    ParallelDerivativeVectorFieldBase(const Basis& basis, Solver& solver)
      : Base(basis, ComponentVector(basis.size(),0)), solver(solver)
    {}
    

    void setLevelSetAndDirection(const int levelSetId, const int directionId)
    {
      if(!fInitialized)
        initialize();
        
      std::array<int,nComponents> id;
      for(int componentId = 0; componentId < nComponents; ++componentId)
        id[componentId] = levelSetDirectionPairToIndex(levelSetId, directionId, componentId);
      
      Base::coefficients_.resize(Base::basis_.size(),0);
      for(int i = 0; i < X.N(); ++i)
        for(int j = 0; j < nComponents; ++j)
          Base::coefficients_[i][j] = (id[j] >= 0) ? X[i][id[j]] : S(0); // A negative id signals a omitted/zero vector.
    }


  protected:
  
    virtual DenseMatrix rightHandSides() const = 0;
    virtual DenseMatrix pointConstraints() const = 0;
    virtual int levelSetDirectionPairToIndex(const int levelSetId, const int directionId, const int componentId) const = 0;
    
    void initialize()
    {
      // Compute all right hand side vectors.
      auto&& B = rightHandSides();
      
      // Compute point constraints.
      const auto&& P = pointConstraints();
      
      // Incorporate point constraints into right hand sides.
      const auto& map = solver.getPointDOFmap();
      std::vector<int> dof;
      dof.reserve(map.size());
      for(const auto& rel : map)
        dof.push_back(rel.second);
        
      for(int i = 0; i < P.N(); ++i)
        for(int j = 0; j < P.M(); ++j)
          B[dof[i]][j] = P[i][j];
      
      // Solve system.
      auto& superLU = solver.getBaseSolver();
      X.setSize(B.N(), B.M());
      #if _DERIVATIVE_PARALLEL_BY_OMP_
        using Vector = Dune::BlockVector<Dune::FieldVector<double,1>>;
        
        //~ #error Does not work because SuperLU does not support this kind of parallelization.
        // ... unless we maybe disable the reusing of already allocated vectors?
        superLU.reuseVectors(false);
        
        #pragma omp parallel for
        // Iterate over columns.
        for(int j = 0; j < X.M(); ++j)
        {
          // Set up temporary vectors.
          Vector x(X.N());
          Vector b(B.N());

          // Extract right hand side.
          for(int i = 0; i < b.size(); ++i)
            b[i] = B[i][j];
            
          // Solve.
          Dune::InverseOperatorResult res;
          superLU.apply(x, b, res);
          
          // Extract solution.
          for(int i = 0; i < x.size(); ++i)
            X[i][j] = x[i];
        }
      #else
        // Solve everything at once.
        Dune::InverseOperatorResult res;
        superLU.apply(X, B, res);
      #endif
      
      // Log initialization.
      fInitialized = true;
    }

    Solver& solver;
    bool fInitialized = false;
    DenseMatrix X;
};



template<class DomainInfo, class Model, class Solver>
class ParallelDerivativeVectorField : public ParallelDerivativeVectorFieldBase<Solver,typename DomainInfo::Basis>
{
  public:
  
    using Basis = typename DomainInfo::Basis;
    using Base = ParallelDerivativeVectorFieldBase<Solver,Basis>;
    using ComponentVector = typename Base::ComponentVector;
    using Base::nComponents;

    using Base::basis;
    using Base::coefficientVector;

    using Vector = Dune::BlockVector<Dune::FieldVector<double,1>>;
    
    enum{nTrafoDofs = Model::Transformation::DescriptorOT::dimension};
    const int nParticles;
    const int nRhs;
    
    ParallelDerivativeVectorField(const DomainInfo& domainInfo, const Model& model, Solver& solver)
      : Base(domainInfo.getBasis(), solver), domainInfo(domainInfo), model(model), nParticles(model.getPosition().size()), nRhs(nParticles*nTrafoDofs*nComponents)
    {
    }
    
    
  protected:
  
    typedef typename Model::Surface Surface;
    typedef typename Model::Particle Particle;
    typedef typename Model::Transformation Transformation;
    typedef typename Model::Position::value_type Position;
    typedef typename Model::Mismatch MismatchFunction;
    typedef typename Model::Transformation::DescriptorOT Direction;
    
    typedef typename Basis::GridType Grid;
    typedef typename Basis::LocalFiniteElement LFE;

    typedef PerturbationParticleIntersection<MismatchFunction> Phi;
    typedef PerturbationParticleInverseIntersection<Surface,Particle,Transformation> PhiInv;
    typedef DerivativeVectorFieldBoundaryConditions<Position,Direction,Phi,PhiInv,Particle,Surface,Transformation> F;
    typedef MongeGaugeIntegrand<F> Integrand;
    typedef typename Base::ComponentVector::value_type T;
    typedef FictitiousNitscheFunctionalAssembler<Integrand,DomainInfo,Grid,LFE,T> FunctionalAssembler;
    
    using DenseMatrix = typename Base::DenseMatrix;
  
    DenseMatrix rightHandSides() const override
    {

      DenseMatrix B(basis().size(), nRhs);
      
      for(int levelSetId = 0; levelSetId < nParticles; ++levelSetId)
      {
        for(int directionId = 0; directionId < nTrafoDofs; ++directionId)
        {
          Direction d(0);
          d[directionId]  = 1;

          // Prepare boundary conditions and integrand.
          Phi phi(model.getMismatchFunction());
          PhiInv phiInv(model.getSurface(), model.getParticle(), model.getTransformation());
          F f(model.getPosition()[levelSetId], d, phi, phiInv, model.getParticle(), model.getSurface(), model.getTransformation(), model.getPeriodicity());
          #warning The integrand coefficients and the choice of functional assembler are currently hard-coded. This is problematic since we re-use the solver from the problem. Therefore, it can happen that right hand side and the matrix system are not compatible with each other! 
          Integrand integrand(1,0,f);

          // Assemble right hand side (only on the corresponding level set dofs).
          FunctionalAssembler functionalAssembler(integrand, domainInfo, true, levelSetId+1); // Need to set id+1 because id==0 corresponds to the domain level set.

          Assembler<Basis,Basis> assembler(basis(), basis());
          ComponentVector b(basis().size(), 0);
          assembler.assembleFunctional(functionalAssembler, b);
          
          std::array<int,nComponents> id;
          for(int componentId = 0; componentId < nComponents; ++componentId)
            id[componentId] = levelSetDirectionPairToIndex(levelSetId, directionId, componentId);
          for(int i = 0; i < b.size(); ++i)
            for(int j = 0; j < nComponents; ++j)
              B[i][id[j]] = b[i][j];
        }
      }
      
      return B;
    }

    DenseMatrix pointConstraints() const override
    {
      // Get the constraint points (together with the membrane's constraint values, which are not important here.)
      const auto&& baseConstraints = model.transformedParameterizedPoints();
      const auto nPointConstraints = baseConstraints.first.size();
      
      // Set matrix for te point constraint values.
      DenseMatrix P(nPointConstraints, nRhs);

      // Define the basic transformation maps.
      Phi phi(model.getMismatchFunction());
      PhiInv phiInv(model.getSurface(), model.getParticle(), model.getTransformation());
      
      for(int levelSetId = 0; levelSetId < nParticles; ++levelSetId)
      {
        for(int directionId = 0; directionId < nTrafoDofs; ++directionId)
        {    
          // Set the function for computing the boundary conditions.
          Direction d(0);
          d[directionId]  = 1;
          F f(model.getPosition()[levelSetId], d, phi, phiInv, model.getParticle(), model.getSurface(), model.getTransformation(), model.getPeriodicity());

          // Extract component ids.
          std::array<int,nComponents> id;
          for(int componentId = 0; componentId < nComponents; ++componentId)
            id[componentId] = levelSetDirectionPairToIndex(levelSetId, directionId, componentId);

          // Set the constraint values.
          for(int i = 0; i < nPointConstraints; ++i)
          {
            if(model.pointIndexBelongsToLevelSet(i,levelSetId))
            {
              // Evaluate boundary condition.
              const auto boundaryCondition  = f.evaluate(baseConstraints.first[i]);
              for(int j = 0; j < nComponents; ++j)
                P[i][id[j]] = boundaryCondition[j];
            }
            else
            {
              for(int j = 0; j < nComponents; ++j)
                P[i][id[j]] = 0;
            }
          }
        }
      }
      
      return P;
    }

    int levelSetDirectionPairToIndex(const int levelSetId, const int directionId, const int componentId) const override
    {
      return levelSetId*nTrafoDofs*nComponents + directionId*nComponents + componentId;
    }
    
    
    const DomainInfo& domainInfo;
    const Model& model;
};



template<class DomainInfo, class Model, class Solver>
class PlanarParallelDerivativeVectorField : public ParallelDerivativeVectorFieldBase<Solver,typename DomainInfo::Basis>
{
  public:
  
    using Basis = typename DomainInfo::Basis;
    using Base = ParallelDerivativeVectorFieldBase<Solver,Basis>;
    using ComponentVector = typename Base::ComponentVector;
    using Base::nComponents;

    using Base::basis;
    using Base::coefficientVector;

    using Vector = Dune::BlockVector<Dune::FieldVector<double,1>>;
    
    enum{nTrafoDofs = Model::Transformation::DescriptorOT::dimension};
    const int nParticles;
    const int nRhs;
    
    PlanarParallelDerivativeVectorField(const DomainInfo& domainInfo, const Model& model, Solver& solver)
      : Base(domainInfo.getBasis(), solver), domainInfo(domainInfo), model(model), nParticles(model.getPosition().size()), nRhs(nParticles*3)
    {
    }
    
    
  protected:
  
    typedef typename Model::Surface Surface;
    typedef typename Model::Particle Particle;
    typedef typename Model::Transformation Transformation;
    typedef typename Model::Position::value_type Position;
    typedef typename Model::Mismatch MismatchFunction;
    typedef typename Model::Transformation::DescriptorOT Direction;
    
    typedef typename Basis::GridType Grid;
    typedef typename Basis::LocalFiniteElement LFE;

    typedef PerturbationParticleIntersection<MismatchFunction> Phi;
    typedef PerturbationParticleInverseIntersection<Surface,Particle,Transformation> PhiInv;
    typedef DerivativeVectorFieldBoundaryConditions<Position,Direction,Phi,PhiInv,Particle,Surface,Transformation> F;
    typedef MongeGaugeIntegrand<F> Integrand;
    typedef typename Base::ComponentVector::value_type T;
    typedef FictitiousNitscheFunctionalAssembler<Integrand,DomainInfo,Grid,LFE,T> FunctionalAssembler;
    
    using DenseMatrix = typename Base::DenseMatrix;
  
    DenseMatrix rightHandSides() const override
    {
      DenseMatrix B(basis().size(), nRhs);
      
      for(int levelSetId = 0; levelSetId < nParticles; ++levelSetId)
      {
        for(int directionId = 0; directionId < nTrafoDofs; ++directionId)
        {
          std::array<int,nComponents> id;
          bool fSkip = true;
          for(int componentId = 0; componentId < nComponents; ++componentId)
          {
            id[componentId] = levelSetDirectionPairToIndex(levelSetId, directionId, componentId);
            if(id[componentId] >= 0)
              fSkip = false;
          }
          // Little hardcoding: Always skip in directionId = 1 because that was essentially already computed for directionId = 0.
          if(directionId == 1)
            fSkip = true;
          if(fSkip)
            continue;

          Direction d(0);
          d[directionId]  = 1;

          // Prepare boundary conditions and integrand.
          Phi phi(model.getMismatchFunction());
          PhiInv phiInv(model.getSurface(), model.getParticle(), model.getTransformation());
          F f(model.getPosition()[levelSetId], d, phi, phiInv, model.getParticle(), model.getSurface(), model.getTransformation(), model.getPeriodicity());
          #warning The integrand coefficients and the choice of functional assembler are currently hard-coded. This is problematic since we re-use the solver from the problem. Therefore, it can happen that right hand side and the matrix system are not compatible with each other! 
          Integrand integrand(1,0,f);

          // Assemble right hand side (only on the corresponding level set dofs).
          FunctionalAssembler functionalAssembler(integrand, domainInfo, true, levelSetId+1); // Need to set id+1 because id==0 corresponds to the domain level set.

          Assembler<Basis,Basis> assembler(basis(), basis());
          ComponentVector b(basis().size(), 0);
          assembler.assembleFunctional(functionalAssembler, b);
          
          for(int j = 0; j < nComponents; ++j)
            if(id[j] >= 0)
              for(int i = 0; i < b.size(); ++i)
                  B[i][id[j]] = b[i][j];
        }
      }
      
      return B;
    }

    DenseMatrix pointConstraints() const override
    {
      // Get the constraint points (together with the membrane's constraint values, which are not important here.)
      const auto&& baseConstraints = model.transformedParameterizedPoints();
      const auto nPointConstraints = baseConstraints.first.size();
      
      // Set matrix for te point constraint values.
      DenseMatrix P(nPointConstraints, nRhs);

      // Define the basic transformation maps.
      Phi phi(model.getMismatchFunction());
      PhiInv phiInv(model.getSurface(), model.getParticle(), model.getTransformation());
      
      for(int levelSetId = 0; levelSetId < nParticles; ++levelSetId)
      {
        for(int directionId = 0; directionId < nTrafoDofs; ++directionId)
        {
          // Extract component ids and decide on skipping.
          std::array<int,nComponents> id;
          bool fSkip = true;
          for(int componentId = 0; componentId < nComponents; ++componentId)
          {
            id[componentId] = levelSetDirectionPairToIndex(levelSetId, directionId, componentId);
            if(id[componentId] >= 0)
              fSkip = false;
          }
          // Little hardcoding: Always skip in directionId = 1 because that was essentially already computed for directionId = 0.
          if(directionId == 1)
            fSkip = true;
          if(fSkip)
            continue;

          // Set the function for computing the boundary conditions.
          Direction d(0);
          d[directionId]  = 1;
          F f(model.getPosition()[levelSetId], d, phi, phiInv, model.getParticle(), model.getSurface(), model.getTransformation(), model.getPeriodicity());

          // Set the constraint values.
          for(int j = 0; j < nComponents; ++j)
          {
            if(id[j] < 0)
              continue;

            for(int i = 0; i < nPointConstraints; ++i)
            {
              if(model.pointIndexBelongsToLevelSet(i,levelSetId))
              {
                // Evaluate boundary condition.
                const auto boundaryCondition  = f.evaluate(baseConstraints.first[i]);
                P[i][id[j]] = boundaryCondition[j];
              }
              else
              {
                P[i][id[j]] = 0;
              }
            }
          }
        }
      }
      
      return P;
    }

    int levelSetDirectionPairToIndex(const int levelSetId, const int directionId, const int componentId) const override
    {
      // Skip degrees of freedom that are not planar at all.
      if(directionId == 2 || directionId == 3 || directionId == 4)
        return -1;
      
      // Skip components that have trivial values.
      if(directionId == 0 && componentId != 0)
        return -1;
        
      if(directionId == 1 && componentId != 1)
        return -1;
        
      if(directionId == 5 && componentId == 2)
        return -1;
        
      // Compute regular ids.
      if(directionId == 0 || directionId == 1) // In this case the solution would be identical
        return levelSetId*3;
      if(directionId == 5 && componentId == 0)
        return levelSetId*3+1;
      if(directionId == 5 && componentId == 1)
        return levelSetId*3+2;
        
      DUNE_THROW(Dune::Exception, "Untreated case!");
      return -1;
    }
    
    
    const DomainInfo& domainInfo;
    const Model& model;
};


#undef _DERIVATIVE_PARALLEL_BY_OMP_

#endif
