#ifndef DUNE_DERIVATIVE_VECTOR_FIELD_BOUNDARY_CONDITIONS_HH
#define DUNE_DERIVATIVE_VECTOR_FIELD_BOUNDARY_CONDITIONS_HH


template<class Position, class Direction, class Phi, class PhiInv, class Particle, class Surface, class Transformation>
class DerivativeVectorFieldBoundaryConditions
{
  // Templates in this class are completely unnecessary and simply a result of laziness.
  
  public:

    typedef Dune::FieldVector<double,3> RangeType;
    mutable bool fDebug = false;
  
  
    DerivativeVectorFieldBoundaryConditions(const Position& p0, const Direction& q, const Phi& phi, const PhiInv& phiInv, const Particle& particle, const Surface& surface, const Transformation& transformation, const bool fPeriodic)
      : p0(p0), q(q), phi(phi), phiInv(phiInv), particle(particle), surface(surface), transformation(transformation), fPeriodic(fPeriodic)
    {}
    
    template<class DomainType>
    const auto evaluate(const DomainType& x) const
    {
      RangeType val(0);

      // Evaluate psi-derivative.
      const auto&& psi_q = psiDerivative(x);
      
      // Evaluate g-derivative.
      const auto&& g_q = gDerivative(x, psi_q);

      val[0]  = psi_q[0];
      val[1]  = psi_q[1];
      val[2]  = g_q;

      if(std::isnan(val.infinity_norm()))
      {
        Debug::dprintVector(p0, "position");
        Debug::dprintVector(q, "direction");
        Debug::dprintVector(x, "point of evaluation");
        Debug::dprintVector(val, "return values");
        DUNE_THROW(Dune::Exception, "Invalid return value on boundary condition function value.");
      }

      return val;
    }
    
    template<class Element, class DomainType>
    const auto evaluateLocal(const Element& element, const DomainType& xLocal) const
    {
      const auto& x = element.geometry().global(xLocal);
      return evaluate(x);
    }
    
    template<class Element, class DomainType>
    const auto evaluateGradientLocal(const Element& element, const DomainType& xLocal) const
    {
      typedef std::array<DomainType,3> GradientType;
      GradientType gradient;
      
      const auto& x = element.geometry().global(xLocal);
      
      //~ /// For debugging: Difference quotient approximation
      //~ #warning TODO UNDO THIS -- or find out if this is justified (neglects some parts regarding the normal)
      //~ for(int i = 0; i < 2; ++i)
      //~ {
        //~ const double t = 1e-4;
        //~ auto xp = x;
        //~ xp[i]  += t;
        //~ auto xm = x;
        //~ xm[i]  -= t;
        //~ const auto&& fxp = evaluate(xp);
        //~ const auto&& fxm = evaluate(xm);
        //~ for(int k = 0; k < 3; ++k)
          //~ gradient[k][i] = (fxp[k]-fxm[k])/(2*t);
      //~ }
      //~ return gradient;
      //~ // ------
      
      // Set up positions/perturbations.
      typename Phi::DomainType z0;
      z0.first     = x;
      z0.second    = p0;
      
      typename Phi::DomainType d;
      d.first     = 0;
      for(int i = 0; i < d.second.size(); ++i)
        d.second[i] = q[i];
      
      // Lambda for evaluation of the joint normal and g_nu.
      const auto&& normal = [&](const typename Phi::DomainType& z)
      { 
        typedef TransformedParticleLevelSet<typename Particle::LevelSetFunction,Phi,Surface,Transformation> TransformedParticleLevelSet_;
        TransformedParticleLevelSet_ f(particle.levelSetFunction(), phi, z.second, surface, transformation, fPeriodic);
        auto nu  = f.evaluateGradient(z.first);
        if(fDebug)
          Debug::dprint("nu pre-normalization:", nu, "from f at", z.first);
        if(nu.two_norm()!=0)
          nu  /= nu.two_norm();
        return nu;
      };
      
      const auto&& func = [&](const typename Phi::DomainType& z)
      {
        // Evaluate psi.
        const auto psi_z = psi(z);
        
        // Set up point of evaluation.
        typename Phi::DomainType w;
        w.first   = psi_z;
        w.second  = z.second;
        
        // Evaluate gradient of transformed level set function and normalize it.
        auto nu = normal(w);

        // Evaluate spatial derivative of r and apply the normal to it.
        typename Phi::OrderType d_1;
        d_1.first   = {1,0};
        d_1.second  = 0;
        
        typename Phi::OrderType d_2;
        d_2.first   = {0,1};
        d_2.second  = 0;

        const auto Dr_1 = phi.evaluate(w, d_1).second;
        const auto Dr_2 = phi.evaluate(w, d_2).second;
        const auto gnu = Dr_1 * nu[0] + Dr_2 * nu[1];        
        
        // Combine values.
        Dune::FieldVector<double,3> fz(0);
        fz[0] = nu[0];
        fz[1] = nu[1];
        fz[2] = gnu;
        
        if(fDebug)
        {
          Debug::dprint("z:", z.first, ";", z.second);
          Debug::dprint("psi_z:", psi_z);
          Debug::dprint("w:", w.first, ";", w.second);
          Debug::dprint("nu", nu);
          Debug::dprint("Dr_i:", Dr_1, ";", Dr_2);
          
          auto f = phi.fDebug;
          phi.fDebug = true;
          phi.evaluate(w, d_1).second;
          phi.evaluate(w, d_2).second;
          phi.fDebug = f;
        }
        
        return fz;
      };
      
      // Compute finite difference.
      const auto func_q = finiteDifference(func, z0, d);
      
      //~ /// TODO REMOVE AGAIN
      //~ auto func_q = finiteDifference(func, z, d);
      //~ const double x0 = x[0];
      //~ const double x1 = x[1];
      //~ const double r = 1./3;
      //~ func_q[0] = 0;
      //~ func_q[1] = 0;
      //~ func_q[2] = (x1*r*r + x1*(x0*x0 + x1*x1))/(r*r*sqrt(x0*x0 + x1*x1));

      
      // Evaluate normal.
      const auto nu = normal(z0);

      //~ /// TODO REMOVE AGAIN
      //~ auto nu = normal(z);
      //~ nu[0] = x0/r;
      //~ nu[1] = x1/r;
      
      // Join information into gradient structure.
      for(int i = 0; i < 3; ++i)
        gradient[i].axpy(func_q[i], nu);


      for(int i = 0; i < 3; ++i)
      {
        if(std::isnan(gradient[i].infinity_norm()))
        {
          phi.fDebug = false;
          
          Debug::dprintVector(p0, "position");
          Debug::dprintVector(q, "direction");
          Debug::dprintVector(x, "point of evaluation");
          typename Phi::DomainType w;
          w.first   = psi(z0);
          w.second  = z0.second;
          typename Phi::OrderType d_1;
          d_1.first   = {1,0};
          d_1.second  = 0;
          typename Phi::OrderType d_2;
          d_2.first   = {0,1};
          d_2.second  = 0;
          Debug::dprintVector(phi.evaluate(w, d_1).first, "phi_dx1.first");
          Debug::dprintVector(phi.evaluate(w, d_1).second, "phi_dx1.second");
          Debug::dprintVector(phi.evaluate(w, d_2).first, "phi_dx2.first");
          Debug::dprintVector(phi.evaluate(w, d_2).second, "phi_dx2.second");
          fDebug = true;
          Debug::dprintVector(finiteDifference(func, z0, d, -1), "finite difference minus");
          fDebug = false;
          Debug::dprintVector(finiteDifference(func, z0, d, +1), "finite difference plus");
          Debug::dprintVector(func_q, "func_q");
          Debug::dprintVector(nu, "nu");
          Debug::dprintVector(psi(z0), "psi_x");
          for(int j = 0; j < 3; ++j)
            Debug::dprintVector(gradient[j], "gradient", j);
          DUNE_THROW(Dune::Exception, "Invalid return value on boundary condition gradient.");
        }
      }

      return gradient;
    }

  private:
  
    const Position p0;
    const Direction q;
    const Phi& phi;
    const PhiInv& phiInv;
    const Particle& particle;
    const Surface& surface;
    const Transformation& transformation;
    const bool fPeriodic;
  
  
    auto psi(const typename Phi::DomainType& z) const
    {
      // Evaluate phi.
      auto z0 = z;
      z0.second = p0;
      const auto phi_x = phi.evaluate(z0);
      
      // Evaluate and return phi inv.
      typename PhiInv::DomainType w;
      w.first   = z.second;
      w.second  = phi_x.first;
      
      return phiInv.evaluate(w).first;
    }
  
    template<class DomainType>
    auto psiDerivative(const DomainType& x) const
    {
      // Evaluate phi.
      typename Phi::DomainType z;
      z.first   = x;
      z.second  = p0;

      const auto phi_x = phi.evaluate(z);

      //~ /// Debugging: Finite difference approximation.
      //~ typename PhiInv::DomainType wp;
      //~ typename PhiInv::DomainType wm;
      
      //~ const double t = 5e-4;
      
      //~ wp.first  = p0;
      //~ wp.first.axpy(t,q);
      //~ wp.second = phi_x.first;
      
      //~ wm.first  = p0;
      //~ wm.first.axpy(-t,q);
      //~ wm.second = phi_x.first;
      
      //~ const auto&& fwp = phiInv.evaluate(wp).first;
      //~ const auto&& fwm = phiInv.evaluate(wm).first;
      
      //~ auto g = fwp;
      //~ g -= fwm;
      //~ g /= (2*t*q.two_norm());
      
      //~ return g;
      //~ ///
      
      
      // Evaluate and return surface-q-derivative.
      typename PhiInv::DomainType w;
      w.first   = p0;
      w.second  = phi_x.first;

      typename PhiInv::OrderType d;
      d.first   = q;
      d.second  = 0;
      
      return phiInv.evaluate(w,d).first;
    }
    
    
    template<class DomainType, class PsiType>
    auto gDerivative(const DomainType& x, const PsiType& psi_q) const
    {
      // Evaluate first derivatives of phi.
      typename Phi::DomainType z;
      z.first     = x;
      z.second    = p0;
      
      //~ /// Debugging: Finite difference approximation.
      //~ const auto phi_x = phi.evaluate(z);

      //~ typename PhiInv::DomainType wp;
      //~ typename PhiInv::DomainType wm;
      
      //~ const double t = 5e-4;
      
      //~ wp.first  = p0;
      //~ wp.first.axpy(t,q);
      //~ wp.second = phi_x.first;
      
      //~ wm.first  = p0;
      //~ wm.first.axpy(-t,q);
      //~ wm.second = phi_x.first;
      
      //~ const auto&& phiInv_p = phiInv.evaluate(wp).first;
      //~ const auto&& phiInv_m = phiInv.evaluate(wm).first;
      
      //~ auto z_p = z;
      //~ z_p.first = phiInv_p;
      //~ z_p.second.axpy(t,q);
      
      //~ auto z_m = z;
      //~ z_m.first = phiInv_m;
      //~ z_m.second.axpy(-t,q);
      
      //~ auto g_q = phi.evaluate(z_p).second;
      //~ g_q -= phi.evaluate(z_m).second;
      //~ g_q /= (2*t);
      //~ return g_q;
      //~ /// =========
      
      typename Phi::OrderType d_q;
      d_q.first   = 0;
      d_q.second  = q;
      
      typename Phi::OrderType d_1;
      d_1.first   = {1,0};
      d_1.second  = 0;
      
      typename Phi::OrderType d_2;
      d_2.first   = {0,1};
      d_2.second  = 0;
      
      const auto r_q  = phi.evaluate(z, d_q).second;
      const auto Dr_1 = phi.evaluate(z, d_1).second;
      const auto Dr_2 = phi.evaluate(z, d_2).second;

      // Return combined result.
      return r_q + Dr_1 * psi_q[0] + Dr_2 * psi_q[1];
    }
    
    template<class F, class X>
    auto finiteDifference(const F& f, const X& x, const X& y, const int only = 0) const
    {
      const double h = 5e-4;
      double norm_y = sqrt(y.first.two_norm2() + y.second.two_norm2());

      X x_m, x_p;
      for(int i = 0; i < x.first.size(); ++i)
      {
        x_m.first[i]  = x.first[i] - h*y.first[i];
        x_p.first[i]  = x.first[i] + h*y.first[i];
      }
      for(int i = 0; i < x.second.size(); ++i)
      {
        x_m.second[i]  = x.second[i] - h*y.second[i];
        x_p.second[i]  = x.second[i] + h*y.second[i];
      }
      
      const auto fp = f(x_p);
      const auto fm = f(x_m);
      
      // For debugging:
      
      if(only == -1)
      {
        Debug::dprintVector(x_m.first, "xm.first");
        Debug::dprintVector(x_m.second, "xm.second");
        return fm;  
      }
      else if(only == 1)
      {
        Debug::dprintVector(x_p.first, "xp.first");
        Debug::dprintVector(x_p.second, "xp.second");
        return fp;
      }
      
      auto dfx = fp;
      dfx -= fm;
      dfx  /= (2*h*norm_y);
      
      return dfx;
    }
};

#endif
