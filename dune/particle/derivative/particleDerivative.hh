#ifndef DUNE_PARTICLE_DERIVATIVE_HH
#define DUNE_PARTICLE_DERIVATIVE_HH

#include <dune/particle/assembler/particleDerivativeAssembler.hh>
#include <omp.h>

template<class Integrand, class VectorField, class DomainInfo, class UVector>
class ParticleDerivative
{
  public:
  
    typedef typename VectorField::Base::CoefficientVector Vector;
    
    static const int dim = 2;
  
    ParticleDerivative(const Integrand& integrand, VectorField& V, const DomainInfo& domainInfo, const UVector& u)
      : V(V)
    {
      // Assemble derivative functional.
      const auto& basis = domainInfo.getBasis();
      typedef typename std::decay<decltype(basis)>::type Basis;
      typedef typename Basis::GridType Grid;
      typedef typename Basis::LocalFiniteElement LFE;

      typedef ParticleDerivativeAssembler<Integrand,DomainInfo,UVector,Grid,LFE> DerivativeAssembler;
      DerivativeAssembler derivativeAssembler(integrand, domainInfo, u);

      Assembler<Basis,Basis> assembler(basis, basis);
      assembler.assembleFunctional(derivativeAssembler, derivativeFunctional);
    }

    // Requires that u and V are respresented with respect to the same basis.
    auto evaluate(const int particleId, const int directionId) const
    {
      // Set level set id and direction.
      V.setLevelSetAndDirection(particleId, directionId);

      // Evaluate derivative.
      return V.coefficientVector() * derivativeFunctional;
    }

    // Requires that u and V are respresented with respect to the same basis.
    auto evaluate(const int particleId, const int directionId, VectorField& W) const
    {
      // Set level set id and direction.
      W.setLevelSetAndDirection(particleId, directionId);

      // Evaluate derivative.
      return W.coefficientVector() * derivativeFunctional;
    }
    
    /*
    // Requires that u and V are respresented with respect to the same basis.
    auto evaluate(const int particleId, const int directionId) const
    {
      typedef Dune::FieldVector<double,dim> DT;
      typedef Dune::FieldMatrix<double,dim,dim> MT;

      const auto& basis = V.basis();
      assert(basis.size() == vec_u.size());

      // Set level set id and direction.
      V.setLevelSetAndDirection(particleId, directionId);
    
      // Set up solution function.
      BasisMultiDifferentiableGridFunction<typename VectorField::Basis,UVector> u(basis, vec_u);

      double returnValue = 0;

      // Do the actual evaluation.
      const auto& gridView = basis.getGridView();
      for(const auto& e : elements(gridView))
      {
        const bool isInside   = domainInfo.isInside(e);
        const bool isOutside  = domainInfo.isOutside(e);
        const auto& geometry  = e.geometry();
        
        // Skip outside elements.
        if(isOutside)
          continue;          
        
        // Get quadrature rule.
        // Because of the special structure of our basis functions we can get away with a lower order here.)
        // Technically not correct for boundary elements, however.
        const auto& fe = basis.getLocalFiniteElement(e);
        const auto& localBasis = fe.localBasis();
        Dune::QuadratureRule<double,2> quadratureRule;
        QuadratureRuleKey quadKeyFE(fe);
        QuadratureRuleKey quadKey = quadKeyFE.square();
        //~ #warning Technically using a too low quadKey for the evaluation of the derivative.
        //~ quadKey.setOrder(localBasis.order()*3);
        quadKey.setOrder(localBasis.order()*2); // Cheating a little here: The order is too low.
        if(!isInside) // element is on boundary
          //~ quadratureRule  = LocalParameterizationQuadratureCache<double>::rule(quadKey, e, domainInfo);
          quadratureRule  = LocalParameterizationQuadrature<double>::rule(quadKey, e, domainInfo);
        else
          quadratureRule  = QuadratureRuleCache<double,2>::rule(quadKey);

        // Iterate over all points and update the return value.
        for(const auto& quadPoint : quadratureRule)
        {
          // Get quadrature point.
          const auto& xLocal  = quadPoint.position();
          const auto& x       = geometry.global(xLocal);
          
          // Pre-evaluate (and scale) all basis functions, including derivatives up to second order.
          const auto val0 = basisValues0(localBasis, e, xLocal);
          const auto val1 = basisValues1(localBasis, e, xLocal);
          const auto val2 = basisValues2(localBasis, e, xLocal);
          
          // Get all derivatives of V.
          const auto&& val_V = [&](const int id)
          {
            const auto index = basis.index(e,id);
            Dune::FieldVector<double,2> val = {V.coefficientVector()[index][0], V.coefficientVector()[index][1]};
            return val;
          };
          const auto V_val  = applyBasisValues(basis, e, val_V, val0);
          const auto DV     = applyBasisValues(basis, e, val_V, val1);
          const auto D2V    = applyBasisValues(basis, e, val_V, val2);
          
          // Get all derivatives of u.
          const auto&& val_u = [&](const int id)
          {
            return vec_u[basis.index(e,id)];
          };
          const auto u   = applyBasisValues(basis, e, val_u, val0)[0];
          const auto Du  = applyBasisValues(basis, e, val_u, val1)[0];
          const auto D2u = applyBasisValues(basis, e, val_u, val2)[0];
          
          // Get all derivatives of xi_p.
          const auto&& val_xi = [&](const int id)
          {
            Dune::FieldVector<double,1> val = {V.coefficientVector()[basis.index(e,id)][2]};
            return val;
          };
          const auto xi_val = applyBasisValues(basis, e, val_xi, val0)[0];
          const auto Dxi    = applyBasisValues(basis, e, val_xi, val1)[0];
          const auto D2xi   = applyBasisValues(basis, e, val_xi, val2)[0];
          
          // Get all derivatives of rho.
          const auto rho_val  = rho.evaluateScalar(x, u, Du, D2u);
          const auto rho_x    = rho.evaluateScalar_x(x, u, Du, D2u);
          const auto rho_u    = rho.evaluateScalar_u(x, u, Du, D2u);
          const auto rho_z    = rho.evaluateScalar_z(x, u, Du, D2u);
          const auto rho_Z    = rho.evaluateScalar_Z(x, u, Du, D2u);
          
          // Define some key values.
          const auto div_V  = DV[0][0] + DV[1][1];
          DT DV_u = { DV[0][0]*Du[0] + DV[0][1]*Du[1],
                      DV[1][0]*Du[0] + DV[1][1]*Du[1] };
          MT csot; // Crazy second order term.
          for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 2; ++j)
              for(int k = 0; k < 2; ++k)
                csot[i][j]  += D2V[k][i][j] * Du[k] + (DV[k][i]*D2u[k][j] + D2u[i][k]*DV[k][j]);

          // Update return value.
          double val = div_V * rho_val;
          val += rho_x * V_val;
          val -= rho_z * DV_u;
          for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 2; ++j)
              val -= rho_Z[i][j] * csot[i][j];

          val += rho_u * xi_val;
          val += rho_z * Dxi;
          for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 2; ++j)
              val += rho_Z[i][j] * D2xi[i][j];
              //~ val += (D2u[0][0]+D2u[1][1])*(i==j)* D2xi[i][j];
          
          const auto integrationFactor  = quadPoint.weight() * geometry.integrationElement(xLocal);
          returnValue += val * integrationFactor;
        }
      }
      
      return returnValue;
    }*/


    // Requires that u and V are respresented with respect to the same basis.
    // Is a little dirty because the position is only used here to sneak out some information that was not supplied earlier.
    template<class Position>
    auto evaluateGradient(const Position& position) const
    {
      auto gradient = position;
      
      #if USE_OMP
        #if FORCE_OMP
          #warning This is broken and does not really work (yet). Probably it is not feasible to use the solver in parallel.
        #else
          #error This is broken and does not work (yet). Probably it is not feasible to use the solver in parallel.
        #endif
        #pragma omp parallel for
        for(int i = 0; i < gradient.size(); ++i)
        {
          auto W = V;
          for(int j = 0; j < gradient[i].size(); ++j)
            gradient[i][j] = evaluate(i,j,W);
        }
      #else
        //Serial execution.
        for(int i = 0; i < gradient.size(); ++i)
          for(int j = 0; j < gradient[i].size(); ++j)
            gradient[i][j] = evaluate(i,j);
      #endif
          
      return gradient;
    }
    
  private:
  
    VectorField& V;
    Vector derivativeFunctional;
    
    /*
    template<class Element, class LocalBasis, class LocalDomainType, class OrderType>
    static auto evaluatePartialDerivativesLocal(
      const LocalBasis& localBasis,
      const Element& e,
      const LocalDomainType& x,
      const OrderType& d)
    {
      typedef typename LocalBasis::Traits::RangeType LBRangeType;

      auto&& jacobianInverseTransposed = e.geometry().jacobianInverseTransposed(x);
      auto&& dLong = MultiIndexHelper::toLongMultiIndex(d);

      Dune::BlockVector<LBRangeType> y(localBasis.size());
      for (size_t i=0; i<localBasis.size(); ++i)
      {
        LBRangeType value(0);
    
        for( size_t k = 0; k < std::pow( d.size(), dLong.size() ); ++k )
        {
          auto&& betaLong = MultiIndexHelper::toLongMultiIndex( k, d.size(), dLong.size() );
          auto&& beta     = MultiIndexHelper::toShortMultiIndex<OrderType>( betaLong );
          LBRangeType transformationFactor(1);
          for( size_t l = 0; l < dLong.size(); ++l )
            transformationFactor    *= jacobianInverseTransposed[dLong[l]][betaLong[l]];
          if( std::abs(transformationFactor) > std::numeric_limits<LBRangeType>::epsilon()*32 )
            value    += transformationFactor * localBasis.evaluate(x, i, beta);
        }

        y[i]  = value;
      }
      
      return y;
    }
    
    template<size_t order, class LocalBasis, class Element, class X>
    auto basisValues(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      DUNE_THROW(Dune::NotImplemented, "Not implemented.");
    }
    
    template<class LocalBasis, class Element, class X>
    auto basisValues0(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      Dune::FieldVector<size_t,X::dimension> d(0);
      return evaluatePartialDerivativesLocal(localBasis, element, x, d);
    }
    
    template<class LocalBasis, class Element, class X>
    auto basisValues1(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      typedef typename LocalBasis::Traits::RangeType RT;
      Dune::BlockVector<Dune::FieldVector<double,X::dimension>> gradients(localBasis.size());
      
      for(int i = 0; i < X::dimension; ++i)
      {
        Dune::FieldVector<size_t,X::dimension> d(0);
        d[i]  = 1;

        const auto&& grad_i = evaluatePartialDerivativesLocal(localBasis, element, x, d);
        
        for(int k = 0; k < grad_i.size(); ++k)
          gradients[k][i] = grad_i[k];
      }

      return gradients;
    }
    
    template<class LocalBasis, class Element, class X>
    auto basisValues2(const LocalBasis& localBasis, const Element& element, const X& x) const
    {
      typedef typename LocalBasis::Traits::RangeType RT;
      Dune::BlockVector<Dune::FieldMatrix<double,X::dimension,X::dimension>> hessians(localBasis.size());
      
      for(int i = 0; i < X::dimension; ++i)
      {
        for(int j = 0; j <= i; ++j)
        {
          Dune::FieldVector<size_t,X::dimension> d(0);
          ++d[i];
          ++d[j];

          const auto&& hess_ij = evaluatePartialDerivativesLocal(localBasis, element, x, d);
          
          for(int k = 0; k < hess_ij.size(); ++k)
          {
            hessians[k][i][j] = hess_ij[k];
            hessians[k][j][i] = hess_ij[k];
          }
        }
      }

      return hessians;
    }


    template<class Basis, class Element, class FC, class BC>
    auto applyBasisValues(const Basis& basis, const Element& e, const FC& funcCoeffs, const BC& basisCoeffs) const
    {
      const auto& fe = basis.getLocalFiniteElement(e);
      const auto& localBasis = fe.localBasis();

      typedef typename std::decay<decltype(funcCoeffs(0))>::type FuncRT;
      typedef typename std::decay<decltype(basisCoeffs[0])>::type BasisRT;
      
      typedef Dune::FieldVector<BasisRT,FuncRT::dimension> RetType;
      RetType y;
      for(int k = 0; k < y.size(); ++k)
        y[k] *= 0;
      
      // Helper: try to find a local alternative for constrained DOFs.
      const auto&& localConstraintId = [&](const int index)
      {
        for(int localId = 0; localId < localBasis.size(); ++localId)
          if(basis.index(e,localId) == index)
            return localId;
        DUNE_THROW(Dune::Exception, "Constraint is non-local.");
        return -1;
      };

      for(int localId = 0; localId < localBasis.size(); ++localId)
      {
        const auto index = basis.index(e,localId);
        if(basis.isConstrained(index))
        {
          const auto& constraints = basis.constraints(index);
          for (size_t w=0; w<constraints.size(); ++w)
            for(int k = 0; k < y.size(); ++k)
              y[k].axpy(constraints[w].factor * funcCoeffs(localId)[k], basisCoeffs[localConstraintId(constraints[w].index)]);
        }
        else
          for(int k = 0; k < y.size(); ++k)
            y[k].axpy(funcCoeffs(localId)[k], basisCoeffs[localId]);
      }
      
      return y;
    }*/
};

#endif
