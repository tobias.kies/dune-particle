#ifndef DUNE_QUADRATURE_CACHES_HH
#define DUNE_QUADRATURE_CACHES_HH

#include <unordered_map>
#include <dune/particle/quadrature/localparameterizationquadrature.hh>
#include <dune/particle/quadrature/curvequadrature.hh>

template<class float_x>
class LocalParameterizationQuadratureCache
{
  private:
    typedef Dune::QuadratureRule<float_x,2> Rule;
    typedef std::pair<QuadratureRuleKey,size_t> RuleIndex;
    //~ typedef size_t RuleIndex;
    
    typedef std::map<RuleIndex,Rule> ContainerType;
    static ContainerType quadRules;

  public:

    template<class Element, class DomainInfo>
    static Rule& rule(const QuadratureRuleKey& index, const Element& element, const DomainInfo& domainInfo)
    {
      auto ruleIndex  = std::make_pair(index,domainInfo.getBasis().getGridView().grid().globalIdSet().id(element));
      auto qrule_it   = quadRules.find(ruleIndex);

      if(qrule_it != quadRules.end())
        return qrule_it->second;

      qrule_it = quadRules.insert(
                  std::make_pair(
                    ruleIndex,
                    LocalParameterizationQuadrature<float_x>::rule(index, element, domainInfo)
                  ) ).first;
                  
      return qrule_it->second;
    }
    
    static void clear()
    {
      quadRules.clear();
    }
};

template<class float_x>
std::map<
  std::pair<QuadratureRuleKey,size_t>,
  Dune::QuadratureRule<float_x,2>
> LocalParameterizationQuadratureCache<float_x>::quadRules;




template<class float_x, size_t order>
class CurveQuadratureCache
{
  private:
    typedef Dune::QuadratureRule<float_x,2> Rule;
    typedef std::pair<QuadratureRuleKey,size_t> RuleIndex;
    //~ typedef size_t RuleIndex;
    
    typedef std::map<RuleIndex,Rule> ContainerType;
    static ContainerType quadRules;

  public:

    template<class Element, class DomainInfo>
    static Rule& rule(const QuadratureRuleKey& index, const Element& element, const DomainInfo& domainInfo)
    {
      auto ruleIndex  = std::make_pair(index,domainInfo.getBasis().getGridView().grid().globalIdSet().id(element));
      auto qrule_it   = quadRules.find(ruleIndex);

      if(qrule_it != quadRules.end())
        return qrule_it->second;

      qrule_it = quadRules.insert(
                  std::make_pair(
                    ruleIndex,
                    CurveQuadrature<float_x>::template rule<order>(index, element, domainInfo)
                  ) ).first;
                  
      return qrule_it->second;
    }
    
    static void clear()
    {
      quadRules.clear();
    }
};

template<class float_x, size_t order>
std::map<
  std::pair<QuadratureRuleKey,size_t>,
  Dune::QuadratureRule<float_x,2>
> CurveQuadratureCache<float_x,order>::quadRules;

#endif
