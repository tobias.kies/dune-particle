#ifndef DUNE_POLYGON_QUADRATURE_HH
#define DUNE_POLYGON_QUADRATURE_HH

#include "dune/fufem/quadraturerules/quadraturerulecache.hh"
#include <dune/particle/poly2tri/geometry.hh>

/**
 *  This class computes quadrature points for a polygon by triangulating it and applying standard quadrature rules to each triangle.
 *  This class works only in two dimensions.
 */
template<class float_x = double>
class PolygonQuadrature
{    
    
    private:
    
        static const size_t dim = 2;
        
        PolygonQuadrature(){}        
        
        /**
         * Triangulate a polygon.
         * Requires that the polygon has no duplicate points and the points must be ordered counter-clockwise; see documentation of the library poly2tri for further information).
         * The return value is a list of points which form the triangles (instead of just a list of indices referring to the corresponding points in the polygon-array).
         */
        template<class PolygonT>
        static auto triangulatePolygon( const PolygonT& polygon )
        {
            typedef std::array<typename PolygonT::value_type,3> Triangle;
            typedef std::vector<Triangle> Triangles;
            typedef std::vector<PolygonT> BoundaryComponents;
            Triangles triangles;
            
            // Do nothing if polygon is degenerate.
            if( polygon.size() <= 2 )
                return triangles;
            
            // Invoke poly2tri triangulation method.
            // It expects a vector of polygons which describe the most outer polygon in the 0-index (ordered counter-clockwise) and its holes in the remaining ones (ordered clockwise).
            BoundaryComponents boundaryComponents;
            boundaryComponents.push_back( polygon );
            using namespace Poly2Tri;
            Polygon<BoundaryComponents> poly(boundaryComponents);
            poly.triangulation();
            const auto&& triangleIndices = poly.triangles();
            
            for( const auto& triangleIndex : triangleIndices )
            {
                Triangle triangle;
                for( size_t i = 0; i < triangleIndex.size(); ++i ) // would be a place for a python-style map-loop
                    triangle[i] = polygon[triangleIndex[i]];
                triangles.push_back(triangle);
            }
            
            // Return triangles.
            return triangles;
        }
        
    public:
    
        /**
         * Performs quadrature for a polygon.
         * The polygon must be provided as an array-like structure of points.
         * Compare documentation of poly2tri for more details on what the list of points has to look like.
         */
        template<class QuadKey, class PolygonT>
        static auto rule( const QuadKey& quadKey, const PolygonT& polygon )
        {
            // Make sure that we are in the two-dimensional setting.
            static_assert( PolygonT::value_type::dimension == 2, "Polygon quadrature only works in two dimensions." );
            
            // Triangulate the polygon.
            const auto&& triangles = triangulatePolygon( polygon );
            
            // Get reference quadrature rule.
            Dune::GeometryType geometryType;
            geometryType.makeTriangle();
            QuadratureRuleKey referenceQuadKey( geometryType, quadKey.order(), quadKey.refinement(), quadKey.lumping() );
            const auto& referenceRule = QuadratureRuleCache<float_x, dim>::rule(referenceQuadKey);
            
            // Create quadrature rule by applying the reference rule to each triangle.
            Dune::QuadratureRule<float_x,dim> quadRule;
            for( const auto& triangle : triangles )
            {
                // Prepare transformations of reference points onto the triangle.
                const auto& shift   = triangle[0];
                Dune::FieldMatrix<float_x,dim,dim> T;
                Dune::FieldVector<float_x,dim> Tx;
                for( size_t i = 0; i < dim; ++i )
                    for( size_t j = 1; j < 3; ++j )
                        T[i][j-1]    = triangle[j][i] - shift[i];
                const auto& det     = std::abs(T[0][0]*T[1][1] - T[0][1] * T[1][0]);
                
                // Perform actual transformation.
                for( const auto& point : referenceRule )
                {
                    T.mv( point.position(), Tx );
                    quadRule.push_back( Dune::QuadraturePoint<float_x,dim>( Tx+shift, point.weight()*det )  );
                }
            }
            
            // Return final result.
            return quadRule;
        }
};

#endif
