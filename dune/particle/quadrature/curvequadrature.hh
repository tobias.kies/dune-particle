#ifndef DUNE_CURVE_QUADRATURE_HH
#define DUNE_CURVE_QUADRATURE_HH


#include <dune/particle/functions/linearlyreparameterizedfunction.hh>
#include <dune/particle/functions/funcpolz.hh>

#include <dune/geometry/type.hh>
#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include <dune/particle/quadrature/quadraturehelper.hh>

template<class float_x = double>
class CurveQuadrature
{
    private:
        
        CurveQuadrature(){}

        
    public:
    
        template<size_t dim = 2, class QuadKey, class Element, class DomainInfo>
        static auto rule( const QuadKey& quadKey, const Element& element, const DomainInfo& domainInfo )
        {
            static_assert( dim == 2 && Element::dimension == dim, "CurveQuadrature only supports dim=2 in ellipse quadrature." );
            Dune::QuadratureRule<float_x,dim> quadraturePoints;
            
            for( size_t levelSetId = 0; levelSetId < domainInfo.numLevelSets(); ++levelSetId )
            {
                const auto& intersectionPairs = domainInfo.intersectionPairs(element, levelSetId);
                for( const auto& intersectionPair : intersectionPairs )
                {
                    addCurveQuadraturePoints<dim>(quadKey, element, intersectionPair.first, intersectionPair.second, domainInfo.levelSetFunction(levelSetId), quadraturePoints );
                }
            }

            return quadraturePoints;
        }
        
    
    private:
    
        template<size_t dim, class QuadKey, class Element, class DomainType, class LevelSetFunction, class QuadraturePoints>
        static void addCurveQuadraturePoints(const QuadKey& quadKey, const Element& element, const DomainType& p, const DomainType& q, const LevelSetFunction& levelSetFunction, QuadraturePoints& quadraturePoints )
        {
            // Set some variables that may be useful later on.
            const auto&& z  = q-p;
            auto zT = z;
            zT[0]   = -z[1];
            zT[1]   = z[0];
            const auto& norm_z      = z.two_norm2();
            const auto& norm_zT     = zT.two_norm2();
            
            // Transform variables onto reference element.
            const auto&& geometry   = element.geometry();
            const auto&& local_p    = geometry.local(p);
            const auto&& local_q    = geometry.local(q);
            const auto&& local_z    = local_q - local_p;
            auto local_zT           = local_z;
            local_zT[0]             = -local_z[1];
            local_zT[1]             = local_z[0];
                
            // Check local points and abort if distance is too small.
            if( local_z.two_norm() < 1e-5 )
                return;

            // Parameterize the level set function via (s,t) |-> s*(q-p) + t*normal + p = A(s;t) + p
            Dune::FieldMatrix<float_x,dim,dim> parameterizationMatrix;
            for( size_t rowId = 0; rowId < dim; ++rowId )
            {
                parameterizationMatrix[rowId][0]    = z[rowId];
                parameterizationMatrix[rowId][1]    = zT[rowId];
            }
            LinearlyReparameterizedFunction<LevelSetFunction> parameterizedLevelSetFunction( levelSetFunction, parameterizationMatrix, p );
            
            // Interpolate the level set function parameterized over the segment [p,q].
            #warning Need to include a paremeter for the geometric approximation order.
            const size_t geomOrder = 3;
            const auto&& interpolatedParameterizedLevelSetFunction = QuadratureHelper::levelSetFunctionToGraph( parameterizedLevelSetFunction, geomOrder );            
                
            // Retrieve 1D quadrature rule on the unit segment. We use the QuadratureRuleKey that is induced by quadKey.
            Dune::GeometryType geometryType;
            geometryType.makeLine();
            QuadratureRuleKey oneDimensionalQuadKey( geometryType, quadKey.order() + geomOrder, quadKey.refinement(), quadKey.lumping() );
            const auto& localRule   = QuadratureRuleCache<float_x, 1>::rule(oneDimensionalQuadKey);
            
            // Create quadrature rule.
            for( const auto& localPoint : localRule )
            {
                const auto&& gamma_s = interpolatedParameterizedLevelSetFunction.evaluate(localPoint.position());
                const auto&& d_gamma_s = interpolatedParameterizedLevelSetFunction.evaluate(localPoint.position(), {1});
                
                // Map 1D point to point on reference element.
                auto pos  = local_p;
                pos.axpy( localPoint.position(), local_z );
                pos.axpy( gamma_s, local_zT );
                
                // Compute weight.
                auto weight = localPoint.weight() * std::sqrt( norm_z + d_gamma_s*d_gamma_s*norm_zT );
                
                // Add quadrature point.
                //~ if( std::abs(pos[0]) > 1 || std::abs(pos[1]) > 1 ) /// Debugging.
                  //~ Debug::dprint("Invalid quadrature point detected.");
                quadraturePoints.push_back( Dune::QuadraturePoint<float_x,dim>( pos, weight ) );
            }
        }
};

#endif
