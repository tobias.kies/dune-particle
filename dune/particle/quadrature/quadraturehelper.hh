#ifndef DUNE_QUADRATURE_HELPER_HH
#define DUNE_QUADRATURE_HELPER_HH

#include <dune/particle/functions/funcpolz.hh>

namespace QuadratureHelper
{
  // Tries to extract from a 2D level set function f a graph g such that f(s,g(s)) = 0 for 0 <= s <= 1.
  // This is done approximately by interpolation.
  //
  // This is so far only implemented for geometric order = 3 and order = 5. (The general case could be done using Hermite interpolation and the implicit functions theorem.)
  template<class LevelSetFunction>
  auto levelSetFunctionToGraph( const LevelSetFunction& parameterizedLevelSetFunction, const size_t geomOrder_ )
  {
    //~ #warning Fixed geometric order to 3.
    //~ const size_t geomOrder = 3;
    const size_t geomOrder = geomOrder_;
    
    // Some convenient type definitions.
    typedef typename LevelSetFunction::DomainType DomainType;
    typedef typename LevelSetFunction::OrderType OrderType;
    typedef typename DomainType::field_type float_x;
    typedef FuncPolZ<Dune::FieldVector<float_x,1>,float_x,Dune::FieldVector<size_t,1>> Polynomial;
    typedef typename Polynomial::PolTuple PT;
    
    // Compute some important values used for interpolation.
    #warning Should make this dependent on the geometric order.
    const DomainType
        a   = {0,0}, b = {1,0};
    const OrderType
        ds = {1,0}, dt = {0,1}, dst = {1,1}, dss = {2,0}, dtt = {0,2};
        
    const auto f = [&parameterizedLevelSetFunction]( const auto& x, const auto& d )
    {
      return parameterizedLevelSetFunction.evaluate(x,d);  
    };

    const auto df = [&f, &ds, &dt]( const auto& x )
    {
      return -f(x,ds)/f(x,dt);  
    };
    
    const auto ddf = [&f, &df, &ds, &dt, &dss, &dst, &dtt]( const auto& x )
    {
      const auto&& d_x = df(x);
      const auto&& numerator = f(x,dss) + 2*f(x,dst)*d_x +f(x,dtt)*d_x*d_x;
      return -numerator/f(x,dt);
    };
    
    const auto&& d_a = df(a);
    const auto&& d_b = df(b);

    // Compute the graph by Hermite interpolation.
    Polynomial interpolatedParameterizedLevelSetFunction;
    switch( geomOrder )
    {
        case 3:
            interpolatedParameterizedLevelSetFunction = Polynomial(
                {
                    std::make_pair( d_a,          PT({1}) ),
                    std::make_pair( -2*d_a-d_b, PT({2}) ),
                    std::make_pair( d_a+d_b,    PT({3}) )
                }
            );
            break;
            
        case 5:
        {
            const auto&& dd_a = ddf(a);
            const auto&& dd_b = ddf(b);
            
            if( !std::isfinite(dd_a) || !std::isfinite(dd_b) )
                DUNE_THROW( Dune::Exception, "Quadrature failed when trying to perform local interpolation. It may help to refine the grid." );

            interpolatedParameterizedLevelSetFunction = Polynomial(
                {
                    std::make_pair( d_a,                                    PT({1}) ),
                    std::make_pair( 0.5*dd_a,                               PT({2}) ),
                    std::make_pair( 0.5*dd_b - 4*d_b - 1.5*dd_a - 6*d_a,    PT({3}) ),
                    std::make_pair( 8*d_a + 7*d_b + 1.5*dd_a - dd_b,        PT({4}) ),
                    std::make_pair( 0.5*dd_b - 3*d_b - 0.5*dd_a - 3*d_a,    PT({5}) )
                }
            );
            break;
        }
          
        default:
            DUNE_THROW( Dune::Exception, "Curve quadrature of desired geometric order is not available." );
            break;
    }
    
    // React if val_a or val_b are infeasible.
    if( !std::isfinite(d_a) /*|| !std::isfinite(dd_a)*/ || !std::isfinite(d_b) /*|| !std::isfinite(dd_b)*/ )
    {
        DUNE_THROW( Dune::Exception, "Quadrature failed when trying to perform local interpolation. It may help to refine the grid." );
    }
    
    return interpolatedParameterizedLevelSetFunction;
  }
}

#endif
