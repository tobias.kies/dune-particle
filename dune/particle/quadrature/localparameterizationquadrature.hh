#ifndef DUNE_LOCAL_PARAMETERIZATION_QUADRATURE_HH
#define DUNE_LOCAL_PARAMETERIZATION_QUADRATURE_HH


//#include <dune/membrane/helper/helper.hh>

#include <dune/geometry/type.hh>
#include "dune/fufem/quadraturerules/quadraturerulecache.hh"

#include <dune/particle/functions/linearlyreparameterizedfunction.hh>
#include <dune/particle/functions/linefunction.hh>
#include <dune/particle/functions/funcpolz.hh>
#include <dune/particle/quadrature/polygonquadrature.hh>
#include <dune/particle/quadrature/quadraturehelper.hh>

#include <dune/particle/helper/geometryhelper.hh>

/**
 *  This class computes quadrature points according to the locally parameterized quadrature method.
 *  See also
 *      M. A. Olshanskii and D. Safin: Numerical integration over implicitly defined domains for higher order unfitted finite element methods
 *
 *  The general idea is that the method "rule" returns a list of quadrature points, just as the QuadratureRuleCache does.
 *  This also means that the weights and the quadrature points are given with respect to the reference element.
 *
 *  It is remarkable how similar the implementations for ellipses and for general level sets are.
 *  One could probably reduce the code by introducing some more helper methods.
 */
template<class float_x = double>
class LocalParameterizationQuadrature
{
    public:

        /**
         *  This flag is used to decide whether the quadrature is done with respect to the interior domain (i.e. level set < 0) or with respect to the outer or both.
         */
        enum Flags
        {
            Inner,
            Outer,
            InnerAndOuter
        };


    private:

        LocalParameterizationQuadrature(){}

        #warning Still need to ensure that the quadrature orders are chosen properly.

        /**
         * Add quadrature points to an existing rule for the parameterized part of a domain.
         * This is a specialization for the case of an ellipse.
         * (Again, this only works for 2D.)
         */
        template<class Point, class LevelSetFunction, class Element, class QuadKey, class QuadratureRule>
        static void addParametricQuadraturePoints( const Point& p, const Point& q, const LevelSetFunction& levelSetFunction, const Element& element, const QuadKey& quadKey, const Flags& flag, QuadratureRule& quadRule )
        {
            static_assert( QuadratureRule::d == 2, "LPQ only supports dim=2." );
            const size_t dim = QuadratureRule::d;

            // Retrieve 1D quadrature rule on the unit segment. We use the QuadratureRuleKey that is induced by quadKey.
            Dune::GeometryType geometryType;
            geometryType.makeLine();
            QuadratureRuleKey oneDimensionalQuadKey( geometryType, quadKey.order(), quadKey.refinement(), quadKey.lumping() );
            const auto& localRule   = QuadratureRuleCache<float_x, 1>::rule(oneDimensionalQuadKey);

            // Transform rule onto the line segment [local_p,local_q] where local_p and local_q are representations of p and q on the reference element, respectively.
            const auto& local_p = element.geometry().local(p);
            const auto& local_q = element.geometry().local(q);
            const auto&& local_q_p   = local_q-local_p;
            const auto local_qp_norm    = local_q_p.two_norm();
            if( local_qp_norm < 1e-5 )
                return;
            const auto& pqRule = transform1DRuleOntoLineSegment( local_p, local_q, localRule );

            // Compute a vector in normal direction. Note: That vector is not necessarily normalized.
            const Point&& q_p   = q-p;
            const auto qp_norm  = q_p.two_norm();
            Point normal;
            normal[0]   = -q_p[1];
            normal[1]   = q_p[0];

            // Parameterize the level set function via (s,t) |-> s*(q-p) + t*normal + p = A(s;t) + p
            Dune::FieldMatrix<float_x,dim,dim> parameterizationMatrix;
            for( size_t rowId = 0; rowId < dim; ++rowId )
            {
                parameterizationMatrix[rowId][0]    = q_p[rowId];
                parameterizationMatrix[rowId][1]    = normal[rowId];
            }
            LinearlyReparameterizedFunction<LevelSetFunction> parameterizedLevelSetFunction( levelSetFunction, parameterizationMatrix, p );

            // Interpolate the level set function
            #warning Need to include a paremeter for the geometric approximation order.
            const size_t geomOrder = 3;
            const auto&& interpolatedParameterizedLevelSetFunction = QuadratureHelper::levelSetFunctionToGraph( parameterizedLevelSetFunction, geomOrder );

            // Iterate over the points on pq and add compute the rays in normal direction that intersect
            for( const auto& pqPoint : pqRule )
            {
                // Extract the coordinate part and its global representation.
                const auto& local_x = pqPoint.position();
                const auto& x = element.geometry().global(local_x);

                // Approximate the ray {x + t * normal}.
                const auto&& tempPoint = x-p;
                auto intersection = x;
                intersection.axpy( interpolatedParameterizedLevelSetFunction.evaluate(tempPoint.two_norm()/qp_norm), normal );

                // Transform the 1D quadrature onto the segment [local_x,local_y], where local_x and local_y are the representations of x and of the intersection point on the reference element, respectively.
                const auto& y = intersection;
                const auto& local_y = element.geometry().local(y);
                const auto& xyRule = transform1DRuleOntoLineSegment( local_x, local_y, localRule );

                // Compute the sign-factor in front of the integrand.
                // We will incorporate this factor into the quadrature weights directly.
                // Also we assume that the whole segment [x,y] is either outside or inside the domain, so we just test for the position of x.
                float_x signFactor = (levelSetFunction.evaluate(x) <= 0) ? 1 : -1;
                if( flag == Flags::Outer ) signFactor *= -1;   // flip sign if the outside-flag is specified.

                // Collect the 2D quadrature points (these are given with respect to the current element).
                for( const auto& xyPoint : xyRule )
                {
                    quadRule.push_back( Dune::QuadraturePoint<float_x,dim>( xyPoint.position(), xyPoint.weight() * pqPoint.weight() * signFactor ) );
                }
            }
        }


        /**
         * Add quadrature points to an existing rule for the parameterized part of a domain.
         * This is a specialization for the case of an ellipse.
         * (Again, this only works for 2D.)
         */
        /*template<class Point, class Element, class QuadKey, class QuadratureRule>
        static void addParametricQuadraturePoints( const Point& p, const Point& q, const Geometries::Ellipse<float_x>& ellipse, const Element& element, const QuadKey& quadKey, const Flags& flag, QuadratureRule& quadRule )
        {
            static_assert( QuadratureRule::d == 2, "LPQ only supports dim=2 for element-ellipse quadrature." );
            const size_t dim = QuadratureRule::d;

            // Retrieve 1D quadrature rule on the unit segment. We use the QuadratureRuleKey that is induced by quadKey.
            Dune::GeometryType geometryType;
            geometryType.makeLine();
            QuadratureRuleKey oneDimensionalQuadKey( geometryType, quadKey.order(), quadKey.refinement(), quadKey.lumping() );
            const auto& localRule   = QuadratureRuleCache<float_x, 1>::rule(oneDimensionalQuadKey);

            // Transform rule onto the line segment [local_p,local_q] where local_p and local_q are representations of p and q on the reference element, respectively.
            const auto& local_p = element.geometry().local(p);
            const auto& local_q = element.geometry().local(q);
            const auto& pqRule = transform1DRuleOntoLineSegment( local_p, local_q, localRule );

            // Compute a vector in normal direction.
            Point normal;
            normal[0]   = p[1] - q[1];
            normal[1]   = q[0] - p[0];

            // Iterate over the points on pq and add compute the rays in normal direction that intersect
            for( const auto& pqPoint : pqRule )
            {
                // Extract the coordinate part and its global representation.
                const auto& local_x = pqPoint.position();
                const auto& x = element.geometry().global(local_x);

                // Compute the intersection of the line
                //      {x + t*normal}
                // with the ellipse.
                // Raise an error if more than one intersection point is inside the element
                // and ignore this point if there is no intersection point.
                Geometries::Line<float_x> line( normal, x );
                auto&& intersections = GeometryHelper::intersections( ellipse, line );
                GeometryHelper::removePointsOutsideElement(element, intersections );

                if( intersections.size() >= 2 )
                    DUNE_THROW( Dune::NotImplemented, "LPQ found at least two intersection points when computing the rays. This is not supported." );
                if( intersections.size() != 1 )
                    continue;
                const auto& intersection = intersections[0];

                // Transform the 1D quadrature onto the segment [local_x,local_y], where local_x and local_y are the representations of x and of the intersection point on the reference element, respectively.
                const auto& y = intersection;
                const auto& local_y = element.geometry().local(y);
                const auto& xyRule = transform1DRuleOntoLineSegment( local_x, local_y, localRule );

                // Compute the sign-factor in front of the integrand.
                // We will incorporate this factor into the quadrature weights directly.
                // Also we assume that the whole segment [x,y] is either outside or inside the domain, so we just test for the position of x.
                float_x signFactor = (ellipse.contains(x)) ? 1 : -1;
                if( flag == Flags::Outer ) signFactor *= -1;   // flip sign if the outside-flag is specified.

                // Collect the 2D quadrature points (these are given with respect to the current element).
                for( const auto& xyPoint : xyRule )
                {
                    quadRule.push_back( Dune::QuadraturePoint<float_x,dim>( xyPoint.position(), xyPoint.weight() * pqPoint.weight() * signFactor ) );
                }
            }
        }*/


        /**
         * Transform quadrature 1D points onto a line segment.
         */
        template<class Point, class QuadRule>
        static auto transform1DRuleOntoLineSegment( const Point& x, const Point& y, const QuadRule& quadRule )
        {
            const size_t dim = Point::dimension;
            const auto& z = y-x;
            const auto& norm_z = z.two_norm();

            Dune::QuadratureRule<float_x,dim> transformedRule;
            for( const auto& point : quadRule )
            {
                const auto& t   = point.position();
                auto dummy = z;
                dummy   *= t;
                transformedRule.push_back( Dune::QuadraturePoint<float_x,dim>( x + dummy, point.weight() * norm_z ) );
            }

            return transformedRule;
        }


    public:

        /**
         * LPQ for arbitrary level sets.
         * Note that this method will not detect all possible intersections! (But usually most of them.)
         *
         * Remark:
         *  - The computation of the roots may be sub-optimal.
         */
        template<size_t dim = 2, class QuadKey, class Element, class DomainInfo>
        static auto rule( const QuadKey& quadKey, const Element& element, const DomainInfo& domainInfo, const Flags& flag = Flags::Inner )
        {
            static_assert( dim == 2, "LPQ only supports dim=2." );

            // If the corresponding flag is specified we just return the standard quadrature rules.
            if( flag == Flags::InnerAndOuter )
                return QuadratureRuleCache<float_x, dim>::rule(quadKey);
                
            Dune::QuadratureRule<float_x,dim> quadraturePoints;

            // Get corners of the element. Note that those corners will be ordered already.
            Geometries::Polygon<float_x> elementCorners = Geometries::Polygon<float_x>::createPolygonFromElement(element);
            
            bool fHaveIntersections = false;
            for( size_t levelSetId = 0; levelSetId < domainInfo.numLevelSets(); ++levelSetId )
            {
                const auto& intersections = domainInfo.intersectionPairs(element,levelSetId);
                const auto& levelSetFunction = domainInfo.levelSetFunction(levelSetId);
                if( intersections.size() < 1 )
                    continue;
                    
                if( fHaveIntersections )
                    DUNE_THROW( Dune::NotImplemented, "LPQ quadrature does not support quadrature rules for elements that have intersections with multiple level sets." );
                    
                if( intersections.size() > 1 )
                {
                    std::cerr << "WARNING: LPQ quadrature detected element with more than two intersections. This may be handled poorly." << std::endl;
                    for( const auto& intersectionPair : intersections )
                    {
                        std::cerr << intersectionPair.first << " and " << intersectionPair.second << std::endl;
                    }
                }
                    
                fHaveIntersections = true;

                // Create a list containing the intersections plus the corners of the element which are inside/outside the ellipse.
                // Define polygon that approximates the domain of integration.
                Geometries::Polygon<float_x> domainPoints;

                for( const auto& intersectionPair : intersections )
                {
                    domainPoints.push_back(intersectionPair.first);
                    domainPoints.push_back(intersectionPair.second);
                }

                for( size_t cornerId = 0; cornerId < element.geometry().corners(); cornerId++ )
                {
                    const auto& point = element.geometry().corner(cornerId);
                    bool isInside = ( levelSetFunction.evaluate(point) <= 0 );
                    if( (flag == Flags::Inner && isInside)
                        || (flag == Flags::Outer && !isInside ) )
                        domainPoints.push_back(point);
                }

                // Remove duplicates.
                GeometryHelper::sortPointsByCenterOfElement( domainPoints, element );
                GeometryHelper::makePointsUnique( domainPoints );

                // Transform the points back to the reference element.
                const auto& localPoints = GeometryHelper::transformOntoReferenceElement( element, domainPoints );

                // Get quadrature points for the given polygon on the reference element.
                // (This will give us a quadrature rule that also lives on the reference element.)
                quadraturePoints = PolygonQuadrature<float_x>::rule( quadKey, localPoints );

                // Compute the quadrature points for the locally parameterized part.
                // This is only necessary if there are (at least) two intersection points.
                for( const auto& intersectionPair : intersections )
                    addParametricQuadraturePoints( intersectionPair.first, intersectionPair.second, levelSetFunction, element, quadKey, flag, quadraturePoints );
            }

            return quadraturePoints;
        }


        /**
         * A specialized version of the LPQ for the case that the domain is given by an ellipse (or an complement thereof).
         * The interior of the domain is seen as the interior of the ellipse.
         * This method only works for grids in 2D.
         */
        /*template<size_t dim = 2, class QuadKey, class Element>
        static auto rule( const QuadKey& quadKey, const Element& element, const Geometries::Ellipse<float_x>& ellipse, const Flags& flag = Flags::Inner )
        {
            static_assert( dim == 2, "LPQ only supports dim=2 in ellipse quadrature." );

            // If the corresponding flag is specified we just return the standard quadrature rules.
            if( flag == Flags::InnerAndOuter )
                return QuadratureRuleCache<float_x, dim>::rule(quadKey);

            // Compute the intersection of the element with the ellipse.
            // Throw an error if there are more than two intersections.
            Geometries::Polygon<float_x> elementCorners = Geometries::Polygon<float_x>::createPolygonFromElement(element);
            const auto intersections = GeometryHelper::intersections( elementCorners, ellipse );
            if( intersections.size() > 2 )
                DUNE_THROW( Dune::NotImplemented, "LPQ quadrature does not support quadrature rules for elements that have more than two intersections with an ellipse." );

            // Create a list containing the intersections plus the corners of the element which are inside/outside the ellipse.
            // Define polygon that approximates the domain of integration.
            Geometries::Polygon<float_x> domainPoints;

            for( const auto& point : intersections )
                domainPoints.push_back(point);

            for( const auto& point : elementCorners )
            {
                auto isInside = ellipse.contains(point);
                if( (flag == Flags::Inner && isInside)
                    || (flag == Flags::Outer && !isInside ) )
                    domainPoints.push_back(point);
            }

            // Remove duplicates.
            GeometryHelper::sortPointsByCenterOfElement( domainPoints, element );
            GeometryHelper::makePointsUnique( domainPoints );

            // Transform the points back to the reference element.
            const auto& localPoints = GeometryHelper::transformOntoReferenceElement( element, domainPoints );

            // Get quadrature points for the given polygon on the reference element.
            // (This will give us a quadrature rule that also lives on the reference element.)
            auto quadraturePoints = PolygonQuadrature<float_x>::rule( quadKey, localPoints );

            // Compute the quadrature points for the locally parameterized part.
            // This is only necessary if there are (at least) two intersection points.
            if( intersections.size() >= 2 )
                addParametricQuadraturePoints( intersections[0], intersections[1], ellipse, element, quadKey, flag, quadraturePoints );

            return quadraturePoints;
        }*/
};

#endif
