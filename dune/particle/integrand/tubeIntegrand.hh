#ifndef DUNE_TUBE_INTEGRAND_HH
#define DUNE_TUBE_INTEGRAND_HH

#include <dune/istl/matrix.hh>


class TubeDummyConstraintFunction
{
  public:
  
    TubeDummyConstraintFunction(){}
    
    template<class Element, class DomainType>
    const double evaluateLocal(const Element& element, const DomainType& x) const
    {
      DUNE_THROW(Dune::Exception, "Can not evaluate dummy.");
      return 0.;
    }
    
    template<class Element, class DomainType>
    const auto evaluateGradientLocal(const Element& element, const DomainType& x) const
    {
      DUNE_THROW(Dune::Exception, "Can not evaluate dummy.");
      return DomainType(0);
    }  
};

template<class F = TubeDummyConstraintFunction>
class TubeIntegrand
{
  public:
  
    typedef double float_x;
    typedef Dune::Matrix<Dune::FieldMatrix<float_x,1,1>> DenseMatrix;
  
    TubeIntegrand(const float_x& kappa, const float_x& sigma, const float_x& r, const F& f)
      : kappa(kappa), sigma(sigma), r(r), f(f)
    {
      if(sigma != 0)
        DUNE_THROW(Dune::NotImplemented, "Currently the concept of 'evaluateVolumeFunctionalLocal' is missing.");
    }

    
    //
    // Evaluation functions of actual integrand.
    //
    
    template<class X, class U, class DU, class DDU>
    float_x evaluateScalar(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return 0.5 * r * (kappa*kappaTerm(u, D2u)*kappaTerm(u,D2u) + sigma*(sigmaTerm(Du)*sigmaTerm(Du)) + 2./r*sigma*u);
    }
    
    template<class X, class U, class DU, class DDU>
    Dune::FieldVector<float_x,2> evaluateScalar_x(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return {0.,0.};
    }
    
    template<class X, class U, class DU, class DDU>
    float_x evaluateScalar_u(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return r*kappa*kappaTerm(u,D2u) + sigma;
    }
    
    template<class X, class U, class DU, class DDU>
    Dune::FieldVector<float_x,2> evaluateScalar_z(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return {sigma*Du[0]*r, sigma*Du[1]/r};
    }
    
    template<class X, class U, class DU, class DDU>
    Dune::FieldMatrix<float_x,2,2> evaluateScalar_Z(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      Dune::FieldMatrix<float_x,2,2> A;
      const auto kT = kappaTerm(u,D2u);
      A[0][0] = r*kappa*kT;
      A[0][1] = 0;
      A[1][0] = 0;
      A[1][1] = kappa/r*kT;
      return A;
    }
    
    
    //
    // Evaluation of operator/functional during matrix/vector assembly.
    //
    
    template<class E, class X, class U, class DU, class DDU>
    auto evaluateVolumeLocal(const E& e, const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      const auto&& func   = [&](const int i, const int j)
      {
        return r * ( kappa * kappaTerm(u[i],D2u[i]) * kappaTerm(u[j], D2u[j]) + sigma * (sigmaTerm(Du[i]) * sigmaTerm(Du[j])) );
      };

      return fillSymmetricMatrix(func, u.size());
    }
    
    
    template<class E, class X, class Ny, class U, class DU, class DDU, class DDDU>
    auto evaluateNitscheLocal(const E& e, const X& x, const Ny& ny, const U& u_, const DU& Du, const DDU& D2u, const DDDU& D3u) const
    {
      assert(x.size() == 2);
      const auto&& func   = [&](const int i, const int j)
      {
        const auto& u = u_[i];
        const auto& v = u_[j];
        
        const auto& u1 = Du[i][0];
        const auto& u2 = Du[i][1];
        
        const auto& v1 = Du[j][0];
        const auto& v2 = Du[j][1];
        
        const auto& u11 = D2u[i][0][0];
        const auto& u22 = D2u[i][1][1];
        
        const auto& u111 = D3u[i][0][0][0];
        const auto& u112 = D3u[i][0][0][1];
        const auto& u122 = D3u[i][0][1][1];
        const auto& u222 = D3u[i][1][1][1];
        
        const auto& n1 = ny[0];
        const auto& n2 = ny[1];
        
        const auto r2 = r*r;
        const auto r4 = r2*r2;
        
        const auto kappaTerms =
          -u11*v1*n1 + u111*v*n1
          + (-u*v1*n1+u1*v*n1-u22*v1*n1+u122*v*n1-u11*v2*n2+u112*v*n2)/r2
          + (-u*v2*n2+u2*v*n2-u22*v2*n2+u222*v*n2)/r4;
          
        const auto sigmaTerms = -u1*v*n1-u2*v*n2/r2;
       
        if(std::isnan(kappaTerms))
        {
          Debug::dprint(i, j, u, v, u1, u2, v1, v2, u11, u22, u111, u112, u122, u222, n1, n2, r2, r4, kappaTerms, sigmaTerms, r, r2, r4);
          DUNE_THROW(Dune::Exception, "Here 0");
        }
        
        return kappa*kappaTerms + sigma*sigmaTerms;
      };

      return fillMatrixMakeSymmetric(func, u_.size());
    }
    
    
    template<class E, class X, class Ny, class U, class DU, class DDU, class DDDU>
    auto evaluateNitscheFunctionalLocal(const E& e, const X& x, const Ny& ny, const U& u_, const DU& Du, const DDU& D2u, const DDDU& D3u) const
    {
      assert(x.size() == 2);
      
      const auto&& f_value  = f.evaluateLocal(e, x);
      const auto&& grad     = f.evaluateGradientLocal(e,x); // Is either a field vector or an array of field vectors.
      
      const auto&& func   = [&](const int i)
      {
        const auto& u = u_[i];
        
        const auto& u1 = Du[i][0];
        const auto& u2 = Du[i][1];
        
        const auto& u11 = D2u[i][0][0];
        const auto& u22 = D2u[i][1][1];
        
        const auto& u111 = D3u[i][0][0][0];
        const auto& u112 = D3u[i][0][0][1];
        const auto& u122 = D3u[i][0][1][1];
        const auto& u222 = D3u[i][1][1][1];
        
        const auto& n1 = ny[0];
        const auto& n2 = ny[1];
        
        const auto r2 = r*r;
        const auto r4 = r2*r2;
        
        auto y = f_value;
        y *= 0;

        for(int j = 0; j < y.size(); ++j)
        {
          const auto& v = f_value[j];
          const auto& v1 = gradVal(grad, j, 0); // either g[0] or g[j][0]
          const auto& v2 = gradVal(grad, j, 1); // either g[1] or g[j][1]
          
          const auto kappaTerms =
            -u11*v1*n1 + u111*v*n1
            + (-u*v1*n1+u1*v*n1-u22*v1*n1+u122*v*n1-u11*v2*n2+u112*v*n2)/r2
            + (-u*v2*n2+u2*v*n2-u22*v2*n2+u222*v*n2)/r4;
          
          const auto sigmaTerms = -u1*v*n1-u2*v*n2/r2;
          
          y[j] = kappa*kappaTerms + sigma*sigmaTerms;
          if(std::isnan(y[j]))
          {
            Debug::dprint(j, v, v1, v2, kappaTerms, sigmaTerms, r2, r4, u, u1, u2, u11, u22, u111, u112, u122, u222);
            DUNE_THROW(Dune::Exception, "Here 1");
          }
        }
        return y;
      };

      return fillVector(func, u_.size());
    }

    
    template<class E, class X, class Ny, class U, class DU>
    auto evaluatePenaltyLocal(const E& e, const X& x, const float_x& h, const Ny& ny, const U& u, const DU& Du) const
    {
      assert(x.size() == 2);
      const float_x h1 = std::pow(h, -3);
      const float_x h2 = std::pow(h, -1);
      const auto&& func   = [&](const int i, const int j)
      {
        return  (lambda * h1 * u[i] * u[j] + lambda * h2 * (Du[i]*ny) * (Du[j]*ny));
      };

      return fillSymmetricMatrix(func, u.size());
    }

    
    template<class E, class X, class Ny, class U, class DU>
    auto evaluatePenaltyFunctionalLocal(const E& e, const X& x, const float_x& h, const Ny& ny, const U& u, const DU& Du) const
    {
      assert(x.size() == 2);
      const float_x h1 = std::pow(h, -3);
      const float_x h2 = std::pow(h, -1);
      
      const auto&& f_value  = f.evaluateLocal(e, x);
      const auto&& f_normal = normalGradient(f, e, x, ny);
      
      const auto&& func   = [&](const int i)
      {
        auto y = f_value;
        y *= lambda * h1 * u[i];
        y.axpy(lambda * h2 * (Du[i]*ny), f_normal);
        return y;
      };

      return fillVector(func, u.size());
    }
    
      
      
  private:
  
    template<class M>
    static auto trace(const M& m)
    {
      typename M::value_type tr = 0;
      const int n = std::min(m.M(),m.N());
      for(int i = 0; i < n; ++i)
        tr  += m[i][i];
      return tr;
    }
    
    template<class F_>
    static auto fillSymmetricMatrix(const F_& f, const int n)
    {
      DenseMatrix matrix(n,n);
      
      for(int i = 0; i < n; ++i)
      {
        for(int j = 0; j <= i; ++j)
        {          
          matrix[i][j]  = f(i,j);
          matrix[j][i]  = matrix[i][j];
        }
      }

      return matrix;
    }
    
    template<class F_>
    static auto fillMatrixMakeSymmetric(const F_& f, const int n)
    {
      DenseMatrix matrix(n,n);
      
      for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
          matrix[i][j]  = f(i,j) + f(j,i);

      return matrix;
    }
    
    template<class F_>
    static auto fillVector(const F_& f, const int n)
    {
      typedef Dune::BlockVector<typename std::decay<decltype(f(0))>::type> Vector;
      Vector vector(n);
      
      for(int i = 0; i < n; ++i)
        vector[i] = f(i);

      return vector;
    }
    
    template<int n>
    Dune::FieldVector<float_x,1> gradVal(const Dune::FieldVector<float_x,n>&g, const int dummy, const int id) const
    {
      return g[id];
    }
    
    template<int n>
    Dune::FieldVector<float_x,1> gradVal(const std::array<Dune::FieldVector<float_x,2>,n>&g, const int j, const int id) const
    {
      return g[j][id];
    }
    
    
    template<class Element, class X, class Ny>
    auto normalGradient(const F& f, const Element& e, const X& x, const Ny& ny) const
    {
      const auto&& g = f.evaluateGradientLocal(e,x);
      return applyNormal(g, ny);
    }
    
    template<int n, class Ny>
    Dune::FieldVector<float_x,1> applyNormal( const Dune::FieldVector<float_x,n>& g, const Ny& ny ) const
    {
      return g*ny;
    }
    
    template<size_t n, class Ny>
    auto applyNormal( const std::array<Dune::FieldVector<float_x,2>,n> & g, const Ny& ny ) const
    {
      typedef typename Dune::FieldVector<double,n> DNormal;
      DNormal f_normal(0);
      
      for(int i = 0; i < f_normal.size(); ++i)
        f_normal[i] = g[i]*ny;
      return f_normal;
    }

    template<class U, class DDU>
    float_x kappaTerm(const U& u,const DDU& D2u) const
    {
      return D2u[0][0] + (u+D2u[1][1])/(r*r);
    }

    template<class DU>
    Dune::FieldVector<float_x,2> sigmaTerm(const DU& Du) const
    {
      return {Du[0], Du[1]/r};
    }

    const float_x kappa;  // Bending rigidity.
    const float_x sigma;  // Surface tension.
    const float_x r;      // Tube radius
    const F& f;           // Constraint function.
    
    const float_x lambda = 1024;
};

#endif
