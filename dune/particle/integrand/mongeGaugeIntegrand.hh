#ifndef DUNE_MONGE_GAUGE_INTEGRAND_HH
#define DUNE_MONGE_GAUGE_INTEGRAND_HH

#include <dune/istl/matrix.hh>


class DummyConstraintFunction
{
  public:
  
    DummyConstraintFunction(){}
    
    template<class Element, class DomainType>
    const double evaluateLocal(const Element& element, const DomainType& x) const
    {
      DUNE_THROW(Dune::Exception, "Can not evaluate dummy.");
      return 0.;
    }
    
    template<class Element, class DomainType>
    const auto evaluateGradientLocal(const Element& element, const DomainType& x) const
    {
      DUNE_THROW(Dune::Exception, "Can not evaluate dummy.");
      return DomainType(0);
    }  
};

template<class F = DummyConstraintFunction>
class MongeGaugeIntegrand
{
  public:
  
    typedef double float_x;
    typedef Dune::Matrix<Dune::FieldMatrix<float_x,1,1>> DenseMatrix;
  
    MongeGaugeIntegrand(const float_x& kappa, const float_x& sigma, const F& f)
      : kappa(kappa), sigma(sigma), f(f)
    {}
    
    //
    // Evaluation functions of actual integrand.
    //
    
    template<class X, class U, class DU, class DDU>
    float_x evaluateScalar(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return 0.5 * (kappa*trace(D2u)*trace(D2u) + sigma * (Du*Du));
    }
    
    template<class X, class U, class DU, class DDU>
    Dune::FieldVector<float_x,2> evaluateScalar_x(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return {0,0};
    }
    
    template<class X, class U, class DU, class DDU>
    float_x evaluateScalar_u(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return 0;
    }
    
    template<class X, class U, class DU, class DDU>
    Dune::FieldVector<float_x,2> evaluateScalar_z(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      return {sigma*Du[0], sigma*Du[1]};
    }
    
    template<class X, class U, class DU, class DDU>
    Dune::FieldMatrix<float_x,2,2> evaluateScalar_Z(const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      Dune::FieldMatrix<float_x,2,2> A;
      A[0][0] = kappa*trace(D2u);
      A[0][1] = 0;
      A[1][0] = 0;
      A[1][1] = A[0][0];
      return A;
    }
    
    
    //
    // Evaluation of operator/functional during matrix/vector assembly.
    //
    
    template<class E, class X, class U, class DU, class DDU>
    auto evaluateVolumeLocal(const E& e, const X& x, const U& u, const DU& Du, const DDU& D2u) const
    {
      const auto&& func   = [&](const int i, const int j)
      {
        return kappa * trace(D2u[i]) * trace(D2u[j]) + sigma * (Du[i] * Du[j]);
      };

      return fillSymmetricMatrix(func, u.size());
    }
    
    
    template<class E, class X, class Ny, class U, class DU, class DDU, class DDDU>
    auto evaluateNitscheLocal(const E& e, const X& x, const Ny& ny, const U& u, const DU& Du, const DDU& D2u, const DDDU& D3u) const
    {
      assert(x.size() == 2);
      const auto&& func   = [&](const int i, const int j)
      {
        auto v = (- kappa * trace(D2u[i]) * (Du[j]*ny)
                + kappa * ( (D3u[i][0][0][0] + D3u[i][0][1][1])*ny[0] + (D3u[i][0][0][1] + D3u[i][1][1][1])*ny[1] ) * u[j] // This line is a little too hard-coded.
                - sigma * (Du[i]*ny) * u[j]);
        return static_cast<double>(v); // DOES THIS WORK PROPERLY IN NON-SCALAR CASES?
        // Note: I had to do this since otherwise a FieldMatrix<double, 1, 1> would be assigned from a FieldVector<double, 1> which one might expect to work.
        // But in this specific instance this did not work since the called DenseMatrixAssigner in densematrix.hh tried to access nested iterators for the rhs
        // as if it was a matrix (which is not the case here as it is a vector).
      };

      return fillMatrixMakeSymmetric(func, u.size());
    }
    
    
    template<class E, class X, class Ny, class U, class DU, class DDU, class DDDU>
    auto evaluateNitscheFunctionalLocal(const E& e, const X& x, const Ny& ny, const U& u, const DU& Du, const DDU& D2u, const DDDU& D3u) const
    {
      assert(x.size() == 2);
      
      const auto&& f_value  = f.evaluateLocal(e, x);
      const auto&& f_normal = normalGradient(f, e, x, ny);
      
      const auto&& func   = [&](const int i)
      {
        auto y = f_normal;
        y *= - kappa * trace(D2u[i]);
        y.axpy( kappa * ( (D3u[i][0][0][0] + D3u[i][0][1][1])*ny[0] + (D3u[i][0][0][1] + D3u[i][1][1][1])*ny[1] ), f_value );
        y.axpy( - sigma * (Du[i]*ny), f_value );
        return y;
        //~ return (- kappa * trace(D2u[i]) * f_normal
                //~ + kappa * ( (D3u[i][0][0][0] + D3u[i][0][1][1])*ny[0] + (D3u[i][1][0][0] + D3u[i][1][1][1])*ny[1] ) * f_value // This line is a little too hard-coded.
                //~ - sigma * (Du[i]*ny) * f_value);
      };

      return fillVector(func, u.size());
    }

    
    template<class E, class X, class Ny, class U, class DU>
    auto evaluatePenaltyLocal(const E& e, const X& x, const float_x& h, const Ny& ny, const U& u, const DU& Du) const
    {
      assert(x.size() == 2);
      const float_x h1 = std::pow(h, -3);
      const float_x h2 = std::pow(h, -1);
      const auto&& func   = [&](const int i, const int j)
      {
        return  (lambda * h1 * u[i] * u[j] + lambda * h2 * (Du[i]*ny) * (Du[j]*ny));
      };

      return fillSymmetricMatrix(func, u.size());
    }

    
    template<class E, class X, class Ny, class U, class DU>
    auto evaluatePenaltyFunctionalLocal(const E& e, const X& x, const float_x& h, const Ny& ny, const U& u, const DU& Du) const
    {
      assert(x.size() == 2);
      const float_x h1 = std::pow(h, -3);
      const float_x h2 = std::pow(h, -1);
      
      const auto&& f_value  = f.evaluateLocal(e, x);
      const auto&& f_normal = normalGradient(f, e, x, ny);
      //~ const Dune::FieldVector<float_x,1> f_normal = (f.evaluateGradientLocal(e, x) * ny);
      
      const auto&& func   = [&](const int i)
      {
        auto y = f_value;
        y *= lambda * h1 * u[i];
        y.axpy(lambda * h2 * (Du[i]*ny), f_normal);
        return y;
        //~ return  (lambda * h1 * u[i] * f_value + lambda * h2 * (Du[i]*ny) * f_normal);
      };

        
      //~ std::cerr << "Penalty evaluation in " << e.geometry().global(x) << " with normal " << ny << std::endl;
      //~ std::cerr << "Yielded " << f_value << " and " << f_normal << std::endl;
      //~ DUNE_THROW(Dune::Exception, "Break here.");

      return fillVector(func, u.size());
    }
    
      
      
  private:
  
    template<class M>
    static auto trace(const M& m)
    {
      typename M::value_type tr = 0;
      const int n = std::min(m.M(),m.N());
      for(int i = 0; i < n; ++i)
        tr  += m[i][i];
      return tr;
    }
    
    template<class F_>
    static auto fillSymmetricMatrix(const F_& f, const int n)
    {
      DenseMatrix matrix(n,n);
      
      for(int i = 0; i < n; ++i)
      {
        for(int j = 0; j <= i; ++j)
        {          
          matrix[i][j]  = f(i,j);
          matrix[j][i]  = matrix[i][j];
        }
      }

      return matrix;
    }
    
    template<class F_>
    static auto fillMatrixMakeSymmetric(const F_& f, const int n)
    {
      DenseMatrix matrix(n,n);
      
      for(int i = 0; i < n; ++i)
        for(int j = 0; j < n; ++j)
          matrix[i][j]  = f(i,j) + f(j,i);

      return matrix;
    }
    
    template<class F_>
    static auto fillVector(const F_& f, const int n)
    {
      typedef Dune::BlockVector<typename std::decay<decltype(f(0))>::type> Vector;
      Vector vector(n);
      
      for(int i = 0; i < n; ++i)
        vector[i] = f(i);

      return vector;
    }
    
    template<class Element, class X, class Ny>
    auto normalGradient(const F& f, const Element& e, const X& x, const Ny& ny) const
    {
      const auto&& g = f.evaluateGradientLocal(e,x);
      return applyNormal(g, ny);
    }
    
    template<int n, class Ny>
    Dune::FieldVector<float_x,1> applyNormal( const Dune::FieldVector<float_x,n>& g, const Ny& ny ) const
    {
      return g*ny;
    }
    
    template<size_t n, class Ny>
    auto applyNormal( const std::array<Dune::FieldVector<float_x,2>,n> & g, const Ny& ny ) const
    {
      typedef typename Dune::FieldVector<double,n> DNormal;
      DNormal f_normal(0);
      
      for(int i = 0; i < f_normal.size(); ++i)
        f_normal[i] = g[i]*ny;
      return f_normal;
    }


    const float_x kappa;  // Bending rigidity.
    const float_x sigma;  // Surface tension.
    const F& f;           // Constraint function.
    
    const float_x lambda = 1024;
};

#endif
