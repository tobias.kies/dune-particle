#ifndef DUNE_MEMBRANE_TEST_HH
#define DUNE_MEMBRANE_TEST_HH

#include <dune/grid/io/file/vtk/subsamplingvtkwriter.hh>
#include <dune/fufem/functions/vtkbasisgridfunction.hh>

#include <dune/grid/utility/gridinfo.hh>

class Test
{
  public:
    typedef Dune::BlockVector<Dune::FieldVector<double,1>> Vector;

    template<class Basis>
    static void plotBasisFunctions(const Basis& basis, const int nSubsampling = 0)
    {
      const auto N = basis.size();
      Vector v(N,0);
      
      for( int i = 3; i < N; i+=4 )
      {
        v[i]  = 1;
        writeDiscreteFunction(basis, v, "basisFunction_" + std::to_string(i), nSubsampling);
        v[i]  = 0;
        
        std::cerr << "Wrote basis function " << (i+1) << " of " << N << std::endl;
      }
    }
    
    
    template<class GridView>
    static void printGridViewElements(const GridView& gridView)
    {
      for( const auto& e : elements(gridView) )
      {
        const auto&& g = e.geometry();
        const int n = g.corners();
        for( int i = 0; i < n; ++i )
        {
          auto x = g.corner(i);
          std::cerr << x[0] << "\t" << x[1] << std::endl;
        }
        std::cerr << std::endl;
      }
    }

    template<class DomainInfo, class float_x = double>
    static auto curveQuadratureSum(const DomainInfo& domainInfo)
    {
      float_x sum(0);

      // Initialize some variables.
      static const size_t dim = 2;
      const auto& basis       = domainInfo.getBasis();
      const auto& gridView    = basis.getGridView();
      
      // Iterate over all elements.
      for(const auto& element : elements(gridView))
      {
        // Skip non-boundary elements.
        if( !domainInfo.isBoundary(element) )
          continue;

        // Init variables..
        const auto& tFE         = basis.getLocalFiniteElement(element);
        const auto& geometry    = element.geometry();
        
        // Get rule.
        QuadratureRuleKey quadFE(tFE);
        QuadratureRuleKey quadKey   = quadFE.square();
        const auto& quadratureRule  = CurveQuadrature<float_x>::template rule<dim>(quadKey, element, domainInfo);
        
        // Update sum.
        for( const auto& quadPoint : quadratureRule )
          sum += quadPoint.weight();
      }
      
      return sum;
    }
    
    
    // Writes information on curve quadrature, normal on curve and on boundary conditions
    template<class DomainInfo, class F, class float_x = double>
    static auto writeCurveInfo( const DomainInfo& domainInfo, const F& f, const std::string& filename = "curveInfo.txt" )
    {
      // Initialize some variables.
      static const size_t dim = 2;
      const auto& basis       = domainInfo.getBasis();
      const auto& gridView    = basis.getGridView();
      size_t elementCounter = 0;
      std::ofstream file;
      file.open(filename);
      
      // Iterate over all elements.
      for(const auto& element : elements(gridView))
      {
        // Skip non-boundary elements.
        if( !domainInfo.isBoundary(element) )
          continue;

        // Init variables..
        const auto& tFE         = basis.getLocalFiniteElement(element);
        const auto& geometry    = element.geometry();
        
        // Get rule.
        QuadratureRuleKey quadFE(tFE);
        QuadratureRuleKey quadKey   = quadFE.square();
        const auto& quadratureRule  = CurveQuadrature<float_x>::template rule<dim>(quadKey, element, domainInfo);
        
        // Get level set.
        int levelSetId = -1;
        for(int i = 0; i < domainInfo.numLevelSets(); ++i)
          if( domainInfo.isBoundary(element,i) )
          {
            levelSetId  = i;
            break;
          }
        if(levelSetId == -1)
          DUNE_THROW(Dune::Exception, "Unexpected error.");
        const auto& levelSetFunction = domainInfo.levelSetFunction(levelSetId);
        
        // Write data.
        for(const auto& quadPoint : quadratureRule)
        {
          const auto& xLocal  = quadPoint.position();
          const auto& xGlobal = geometry.global(xLocal);
          
          // Element id.
          file << elementCounter;
          
          // Position (global).
          for( const auto& val : xGlobal )
            file << "\t" << val;
            
          // Weight + integration element.
          file << "\t" << quadPoint.weight()
             << "\t" << geometry.integrationElement( quadPoint.position() );
          
          
          // Normal vector of level set:
          auto normal = levelSetFunction.evaluateGradient(xGlobal);
          normal /= normal.two_norm();
          for( const auto& val : normal )
            file << "\t" << val;
            
            
          // Constraints:
          const auto&& f_value  = f.evaluateLocal(element, xLocal);
          const auto&& f_normal = f.evaluateGradientLocal(element, xLocal) * normal;
          file << "\t" << f_value << "\t" << f_normal << std::endl;

          // New line.
          file << std::endl;
        }
        
        elementCounter++;
      }
      
      file.close();
      return;
    }
    

    template<class Basis, class Vector>
    static void writeDiscreteFunction(const typename Basis::GridView& gridView, const Basis& basis, const Vector& u, const std::string& filename, const int nSubsampling = 0)
    {
      Dune::SubsamplingVTKWriter<typename Basis::GridView> writer(gridView, nSubsampling);
      typedef VTKBasisGridFunction<Basis, Vector> VTKFunction;
      std::shared_ptr<VTKFunction> vtkFunction = std::make_shared<VTKFunction>(basis, u, "u(x,y)");
      writer.addVertexData(vtkFunction);
      writer.write(filename, Dune::VTK::OutputType::base64);
    }


    template<class Basis, class Vector>
    static auto writeDiscreteFunction(const Basis& basis, const Vector& u, const std::string& filename, const int nSubsampling)
    {
        return writeDiscreteFunction(basis.getGridView(), basis, u, filename, nSubsampling);
    }
};

#endif
