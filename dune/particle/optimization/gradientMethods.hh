#ifndef DUNE_GRADIENT_METHODS_HH
#define DUNE_GRADIENT_METHODS_HH

#include <random>

class GradientMethods
{
  public:
  
    static bool fRethrow;

    /*
     * TODO: Routine is not finished yet (misses e.g. computation of whiteNoise and the callback is placed wrongly.
    template<class F, class X, class Callback>
    static auto eulerMaruyama(const F& f, const X& x0, const double c0, const double tau, const int n, const Callback& callback)
    {
      auto x = x0;
      
      for(int i = 1; i <= n; ++i)
      {
        const auto grad_f = f.evaluateGradient(x);
        double T = c0/log(2+(i-1)*tau);
        
        x.axpy(-tau, grad_f);
        x.axpy(sqrt(2*tau*T), whiteNoise);
        
        callback(i, x, grad_f, whiteNoise);
      }
      
      return x;
    }*/

    template<class F, class X, class Callback>
    static void reflectiveEulerMaruyama(const F& f, const X& x0, const double c0, const X& l, const X& u, const double tau, const int n, const Callback& callback, const double maxGradNorm = 0)
    {
      auto x = x0;
      auto grad_f = f.evaluateGradient(x0);
      double t  = 0;
      
      std::default_random_engine generator;
      std::normal_distribution<double> distribution(0,1);
      
      const auto&& projection = [&l, &u](const X& x)
      {
        auto y = x;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            y[i][j]  = std::max(l[i][j], std::min(u[i][j],y[i][j]));
        return y;
      };
      
      const auto&& whiteNoise = [&x0, &generator, &distribution]()
      {
        auto noise = x0;
        for(int i = 0; i < noise.size(); ++i)
          for(int j = 0; j < noise[i].size(); ++j)
            noise[i][j] = distribution(generator);
        return noise;
      };
      
      const auto&& reflect = [&l, &u](const X& x, const X& xi)
      {
        auto y = x;
        y += xi;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            if(std::isfinite(l[i][j]) && std::isfinite(u[i][j]))
              y[i][j] += std::max(0., 2.*(l[i][j]-y[i][j])) + std::min(0., 2.*(u[i][j]-y[i][j]));
        return y;
      };
      
      for(int i = 0; i <= n; ++i)
      {
        double reduction = 1;
        
        while(true){
          try{
            double T = c0/log(2+t);

            auto noise = whiteNoise();
            noise   *= sqrt(2*tau*reduction*T);

            callback(i, t, x, grad_f, noise); // Bug here: If the below catch ever triggers, the callback will possibly not know what to do -- e.g., it technically a the wrong x-value.
            
            x.axpy(-tau*reduction, grad_f);
            x = projection(x);
            x = projection(reflect(x, noise)); // Tiny bug: Technically, the reflection may cause that we leave the domain in the other direction. Therefore we add another projection, which technically may no longer be compatible with the reflection. In most cases this should not be a real issue.

            if(i != n)
              grad_f  = f.evaluateGradient(x);
            else
              grad_f  *= std::numeric_limits<double>::quiet_NaN();
              
            //~ if(maxGradNorm != 0 && grad_f.infinity_norm() > maxGradNorm)
              //~ grad_f  *= maxGradNorm/grad_f.infinity_norm();
              
            for(int k = 0; k < grad_f.size(); ++k)
              if(maxGradNorm != 0 && grad_f[k].infinity_norm() > maxGradNorm)
                grad_f[k]  *= maxGradNorm/grad_f[k].infinity_norm();
              
            t += tau*reduction;
            break;
          }catch(const Dune::NotImplemented& e){
          //~ }catch(...){ // The following part does not work as expected for arbitrary errors because it causes segmentation faults when somehow saveMembrane is called although u is not defined yet. I guess this is due to the way the callbacks are implemented.
            reduction *= 0.5;
            std::cerr << "Encountered error. Reducing original step length by factor " << reduction << "." << std::endl;
            std::cout << "Encountered error in " << i << ". Reducing original step length by factor " << reduction << "." << std::endl;
            if(reduction < 0.125)
              throw;
          }
        }
      }
    }



    // Temporary variant. Needs to be deleted.
    template<class F, class X, class Callback>
    static void reflectiveEulerMaruyama(const F& f, const X& x0, const double c0, const X& l, const X& u, const double tau, const int n, const Callback& callback, const int skipId, const double maxGradNorm = 0)
    {
      assert(skipId >= 0);
      auto x = x0;
      auto grad_f = f.evaluateGradient(x0);
      double t  = 0;
      
      std::default_random_engine generator;
      std::normal_distribution<double> distribution(0,1);
      
      const auto&& projection = [&l, &u](const X& x)
      {
        auto y = x;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            y[i][j]  = std::max(l[i][j], std::min(u[i][j],y[i][j]));
        return y;
      };
      
      const auto&& whiteNoise = [&x0, &generator, &distribution]()
      {
        auto noise = x0;
        for(int i = 0; i < noise.size(); ++i)
          for(int j = 0; j < noise[i].size(); ++j)
            noise[i][j] = distribution(generator);
        return noise;
      };
      
      const auto&& reflect = [&l, &u](const X& x, const X& xi)
      {
        auto y = x;
        y += xi;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            y[i][j] += std::max(0., 2.*(l[i][j]-y[i][j])) + std::min(0., 2.*(u[i][j]-y[i][j]));
        return y;
      };

      for(int i = 0; i <= n; ++i)
      {
        double reduction = 1;

        while(true){
          try{
            double T = c0/log(2+t);

            auto noise = whiteNoise();
            noise   *= sqrt(2*tau*reduction*T);
            
            if(i <= skipId)
            {
              t += tau*reduction;
              break;
            }

            callback(i, t, x, grad_f, noise); // Bug here: If the below catch ever triggers, the callback will possibly not know what to do -- e.g., it technically a the wrong x-value.

            x.axpy(-tau*reduction, grad_f);
            x = projection(x);
            x = projection(reflect(x, noise)); // Tiny bug: Technically, the reflection may cause that we leave the domain in the other direction. Therefore we add another projection, which technically may no longer be compatible with the reflection. In most cases this should not be a real issue.

            if(i != n)
              grad_f  = f.evaluateGradient(x);
            else
              grad_f  *= std::numeric_limits<double>::quiet_NaN();

            if(maxGradNorm != 0 && grad_f.infinity_norm() > maxGradNorm)
              grad_f  *= maxGradNorm/grad_f.infinity_norm();

            t += tau*reduction;
          }catch(const Dune::NotImplemented& e){
            reduction *= 0.5;
            std::cerr << "Encountered error. Reducing original step length by factor " << reduction << "." << std::endl;
            if(reduction < 0.25)
              throw;
          }
          break;
        }
      }
    }
    
    
    
    
    
    
    
    // A modification of the algorithm presented in M. Solodov, B. Svaiter "Desecent methods with linesearch in the presence of perturbations".
    // The modification is done such that it works with projected directions.
    // (So technically the results from the paper may no longer be applicable; but there are related other papers from M. Solodov on perturbed dscent methods.)
    //
    // Another modification admits to add artificial noise in the gradient.
    template<class F, class X, class Callback>
    static auto perturbedProjectedGradient(const F& f, const X& x0, const X& l, const X& u, const int maxIter, const Callback& callback, const double noiseSigma = 0, const double maxGradNorm = 0, const double forceFlowTau = 0)
    {
      const auto&& projection = [&l, &u](const X& x)
      {
        auto y = x;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            y[i][j]  = std::max(l[i][j], std::min(u[i][j],y[i][j]));
        return y;
      };
      
      return perturbedProjectedGradient(f, x0, projection, maxIter, callback, noiseSigma, maxGradNorm, forceFlowTau);
    }
    
    template<class F, class X, class Callback, class Projection>
    static auto perturbedProjectedGradient(const F& f, const X& x0, const Projection& projection, const int maxIter, const Callback& callback, const double noiseSigma = 0, const double maxGradNorm = 0, const double forceFlowTau = 0)
    {
      // Preliminaries.
      auto x = x0;
      auto fx = f.evaluate(x);
      
      const double gradientLipschitzConstant = 1e3;
      
      const double reductionFactor  = 0.5;
      const double armijoConstant   = 0.01;
      const int maxReductionPower   = std::ceil(std::log(gradientLipschitzConstant/(0.95-armijoConstant)) / std::log(1/reductionFactor));
      
      int k0 = 0;
      
      std::cerr << "Perturbed projected gradient method set maxReductionPower = " << maxReductionPower << std::endl;
      
      std::default_random_engine generator;
      std::normal_distribution<double> distribution(0,noiseSigma);
      const auto&& whiteNoise = [&x0, &generator, &distribution]()
      {
        auto noise = x0;
        for(int i = 0; i < noise.size(); ++i)
          for(int j = 0; j < noise[i].size(); ++j)
            noise[i][j] = distribution(generator);
        return noise;
      };

      // A little hacky: First callback.
      auto x_NaN = x;
      x_NaN *= std::numeric_limits<double>::quiet_NaN();
      const auto NaN = std::numeric_limits<double>::quiet_NaN();
      callback(0, 0, fx, NaN, NaN, x, x_NaN, x_NaN, x_NaN, true);

      // Compute iterates.
      for(int i = 0; i <= maxIter; ++i)
      {
        double stepLength = std::pow(reductionFactor, std::max(k0-1,0));

        // Compute search direction:
        //  d = proj(x - grad_f - noise) - x
        auto grad_f = f.evaluateGradient(x);
        const auto normGrad = grad_f.infinity_norm();
        if(maxGradNorm != 0 && normGrad > maxGradNorm)
          for(int k = 0; k < grad_f.size(); ++k)
            if(grad_f[k].infinity_norm() > maxGradNorm)
              grad_f[k]  *= maxGradNorm/grad_f[k].infinity_norm();
        const auto noise = whiteNoise();
        
        auto projectionPoint = x;
        projectionPoint.axpy(-stepLength, grad_f);
        projectionPoint.axpy(-stepLength, noise);
        
        auto searchDirection = projection(projectionPoint);
        searchDirection -= x;
        const auto searchDirection_twoNorm2 = searchDirection.two_norm2();
        
        // Super-hacky!!!
        #warning super hacky...
        if(grad_f * searchDirection > 0)
        {
          Debug::dprint( "Non-descent direction detected. Attempting manual fix. Value: ", grad_f*searchDirection );
          searchDirection *= -1;
        }
        
        // Compute step length.
        for(int k = std::max(k0-1,0); k <= std::max(k0-1,0)+maxReductionPower; ++k)
        {
          // Evaluate function f at new candidate point xk.
          auto xk = x;
          xk.axpy(stepLength, searchDirection);
          try{
            const auto fxk = f.evaluate(xk);
            bool accepted = false;
          
            // Update and break if armijo criterion is fulfilled.
            if( fx - fxk >= armijoConstant * stepLength * searchDirection_twoNorm2 )
            {
              callback(i+1, k, fx, fxk, normGrad, x, xk, grad_f, noise, true);
            
              x = xk;
              fx = fxk;
              k0 = k;
              std::cerr << "Accepted step " << i << " " << k << std::endl;
              accepted = true;
            }
            else
              callback(i+1, k, fx, fxk, normGrad, x, xk, grad_f, noise, false);
            
            if(accepted)
              break;
          }catch(...){
            std::cerr << "Evaluation " << i << " " << k << " failed." << std::endl;
            if(fRethrow)
              throw;
          }
          
          // Else: reduce the step length and try again.
          stepLength  *= reductionFactor;
          
          // Abort the method completely if no step length is found.
          if(k == maxReductionPower)
          {
            std::cerr << "Abort: No valid step length exists." << std::endl;
            if(forceFlowTau > 0)
            {
              xk = x;
              xk.axpy(forceFlowTau, searchDirection);
              const auto fxk = f.evaluate(xk);
              
              callback(i+1, -1, fx, fxk, normGrad, x, xk, grad_f, noise, true);
              if(fxk < fx)
                k0 = k;
              else
                k0 = 0;
              x  = xk;
              fx = fxk;
              break;
            }
            else
              return x;
          }
        }
      }
      
      return x;
    }
    
    
    
    
    // Transitions from a perturbed gradient method to a gradient flow method.
    template<class F, class X, class Callback, class Projection>
    static auto transitioningGradient(const F& f, const X& x0, const Projection& projection, const int maxIterGradient, const int maxIterFlow, const Callback& callback, const double noiseSigma = 0, const double maxGradNorm = 0, const double forceFlowTau = 0)
    {
      assert(forceFlowTau > 0);
      
      // Ensure this way that the callback is invoked for the initial iterate.
      assert(maxIterGradient >= 0);
      auto x = perturbedProjectedGradient(f, x0, projection, maxIterGradient, callback, noiseSigma, maxGradNorm, 0);
      auto fx = f.evaluate(x);
      
      std::default_random_engine generator;
      std::normal_distribution<double> distribution(0,noiseSigma);
      const auto&& whiteNoise = [&x0, &generator, &distribution]()
      {
        auto noise = x0;
        for(int i = 0; i < noise.size(); ++i)
          for(int j = 0; j < noise[i].size(); ++j)
            noise[i][j] = distribution(generator);
        return noise;
      };
      
      for(int i = 0; i < maxIterFlow; ++i)
      {
        // Evaluate and project gradient.
        // By doing so, compute the new x.
        auto grad_f = f.evaluateGradient(x);
        const auto normGrad = grad_f.infinity_norm();
        if(maxGradNorm != 0 && normGrad > maxGradNorm)
          for(int k = 0; k < grad_f.size(); ++k)
            if(grad_f[k].infinity_norm() > maxGradNorm)
              grad_f[k]  *= maxGradNorm/grad_f[k].infinity_norm();
        const auto noise = whiteNoise();
        
        auto projectionPoint = x;
        projectionPoint.axpy(-forceFlowTau, grad_f);
        projectionPoint.axpy(-forceFlowTau, noise);

        auto xk = projection(projectionPoint);
        const auto fxk = f.evaluate(xk);

        // Invoke callback.
        callback(maxIterGradient+i+2, -1, fx, fxk, normGrad, x, xk, grad_f, noise, true);
        
        // Update x.
        x = xk;
        fx = fxk;        
      }
      
      return x;
    }
    

    template<class F, class X, class Callback>
    static auto transitioningGradient(const F& f, const X& x0, const X& l, const X& u, const int maxIterGradient, const int maxIterFlow, const Callback& callback, const double noiseSigma = 0, const double maxGradNorm = 0, const double forceFlowTau = 0)
    {
      const auto&& projection = [&l, &u](const X& x)
      {
        auto y = x;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            y[i][j]  = std::max(l[i][j], std::min(u[i][j],y[i][j]));
        return y;
      };
      
      return transitioningGradient(f, x0, projection, maxIterGradient, maxIterFlow, callback, noiseSigma, maxGradNorm, forceFlowTau);
    }



    // Box constrained BFGS Quasi Newton method with optional gradient flow restart.
    // B0_inv must be a dense matrix.
    // Only works with a restricted class of matrices... cf. the Binv update function.
    template<class F, class X, class M, class Callback>
    static auto bfgs(const F& f, const X& x0, const X& l, const X& u, const M& B0_inv, const int maxIter, const Callback& callback, const double forceFlowTau = 0)
    {
      // Initialize relevant variables.
      auto x        = x0;
      auto fx       = f.evaluate(x);
      auto grad_fx  = f.evaluateGradient(x);
      auto Binv     = B0_inv;

      // A little hacky: First callback.
      const auto NaN = std::numeric_limits<double>::quiet_NaN();
      auto x_NaN = x; x_NaN *= NaN;
      callback(0, 0, "C", fx, NaN, NaN, NaN, NaN, x_NaN, true);
      
      // Box projection function.      
      const auto&& projection = [&l, &u](const X& x)
      {
        auto y = x;
        for(int i = 0; i < x.size(); ++i)
          for(int j = 0; j < x[i].size(); ++j)
            y[i][j]  = std::max(l[i][j], std::min(u[i][j],y[i][j]));
        return y;
      };
      
      // (Strict) Wolfe-Powell step length routine.
      // Some of the parameters are only supplied for logging reasons.
      const auto&& stepLength = [&callback](const int nIter, const F& f, const X& x0, const auto& f0, const X& grad_f, const X& p, X& xt_export, auto& f_export, auto& Df_export)
      {
        // General configuration.
        const double sigma = 1e-4;
        const double rho = 0.9;
        const double gamma = 2;
        const double tau_1 = 0.25;
        const double tau_2 = 0.25;
        const int iMax = 10;
        
        // Initialization and feasibility check.
        double t = 1;
        double a = 0, b = 0;
        const auto directional_derivative = grad_f*p;
        if(directional_derivative >= 0)
        {
          Debug::dprint("Directional derivative is not negative:", directional_derivative, "for grad_f =", grad_f, " and p =", p);
          return std::numeric_limits<double>::quiet_NaN();
        }
        
        // Essential routines for simplicity.
        const auto&& x  = [&](const double t){ auto xt = x0; xt.axpy(t, p); xt_export = xt; return xt; };
        const auto&& phi  = [&](const double t){ f_export = f.evaluate(x(t)); return f_export; };
        const auto&& dphi = [&](const double t){ Df_export = f.evaluateGradient(x(t)); return Df_export * p; };
        const auto&& psi = [&](const double t){ return phi(t) - f0 - sigma*t*directional_derivative; };
        const auto&& dpsi = [&](const double t){ return dphi(t) - sigma*directional_derivative; };
        
        // Phase A.
        for(int i = 0; i <= iMax; ++i)
        {
          if(i == iMax)
          {
            Debug::dprint("Phase A failed");
            return std::numeric_limits<double>::quiet_NaN();
          }
            
          const auto psi_t = psi(t);
          callback(nIter, i, "A", f0, f.evaluate(x(t)), t, a, b, x0, false);
          
          if(psi_t >= 0)
          {
            a = 0;
            b = t;
            break;
          }
          
          const auto dpsi_t = dpsi(t);
          if(std::abs(dpsi_t) <= (rho-sigma)*std::abs(directional_derivative))
          {
            return t;
          }
          
          if(dpsi_t > 0)
          {
            a = t;
            b = 0;
            break;
          }
          
          t = gamma*t;
        }
        
        // Phase B.
        auto psi_a = psi(a);
        for(int i = 0; i <= iMax; ++i)
        {
          if(i == iMax)
          {
            Debug::dprint("Phase B failed");
            return std::numeric_limits<double>::quiet_NaN();
          }
            
          t = (a+tau_1*(b-a) + b-tau_2*(b-a))/2;
          const auto psi_t = psi(t);
          callback(nIter, i, "B", f0, f.evaluate(x(t)), t, a, b, x0, false);
          if(psi_t >= psi_a)
          {
            b = t;
            continue;
          }
          
          const auto dpsi_t = dpsi(t);
          if(std::abs(dpsi_t) <= (rho-sigma) * std::abs(directional_derivative))
            return t;
            
          if((t-a)*dpsi_t > 0)
            b = a;
          a = t;
          psi_a = psi_t;
        }
        
        return t;
      };


      // Update routine for inverse of the approximation of the Hessian.
      const auto&& updateBinv = [&Binv](const auto& s, const auto& y)
      {
        // Pre-compute some relevant values.
        const int n = s.size();
        const auto sy = s*y;
        const auto sy2 = sy*sy;
        auto Binv_y = y; Binv_y *= 0; Binv.mv(y, Binv_y);
        const auto y_Binv_y = Binv_y * y;
        const auto c = (sy + y_Binv_y)/sy2;
        
        // Perform actual update.
        for(int i = 0; i < n; ++i)
          for(int j = 0; j < n; ++j)
            for(int k = 0; k < s[i].size(); ++k)
              for(int l = 0; l < s[j].size(); ++l)
                Binv[i][j][k][l] += c*s[i][k]*s[j][l] - (Binv_y[i][k]*s[j][l]+s[i][k]*Binv_y[j][l])/sy;
                
        return;
      };
      
      
      // Reset Binv (after no feasible steps were found anymore).
      const auto&& resetBinv = [&Binv]()
      {
        for(int i = 0; i < Binv.N(); ++i)
          for(int j = 0; j < Binv.M(); ++j)
            for(int k = 0; k < Binv[i][j].N(); ++k)
              for(int l = 0; l < Binv[i][j].M(); ++l)
                Binv[i][j][k][l] = (i==j && k==l);
        return;
      };
      
      // Actual iteration loop.
      for(int i = 0; i <= maxIter; ++i)
      {
        // Compute new search direction. (Yes, looks somewhat ugly.)
        auto projectionPoint = x;
        Binv.mmv(grad_fx, projectionPoint);
        auto p = projection(projectionPoint);
        p -= x;
        
        // Compute step length.
        auto xt = x;
        auto fx_new = fx;
        auto grad_fx_new = grad_fx;
        auto alpha = stepLength(i, f, x, fx, grad_fx, p, xt, fx_new, grad_fx_new);
        
        // If no feasible step length was found (sufficiently fast):
        //   Abort or do a small gradient flow step.
        if(alpha == 0 || std::isnan(alpha))
        {
          Debug::dprint("No feasible step length found.");
          if(forceFlowTau == 0)
            break;
            
          x.axpy(-forceFlowTau, grad_fx);
          fx_new = f.evaluate(x);
          grad_fx_new = f.evaluateGradient(x);
          resetBinv();
        }
        // Else: Do standard BFGS stuff.
        else
        {
          // Compute secant and y-vector. Then update B_inv.
          auto s = p; s *= alpha;
          auto y = grad_fx_new; y -= grad_fx;
          updateBinv(s, y);
        
          // Update iterate. (Other values have already been computed.)
          x = xt;
        }
        
        
        // Invoke callback.
        callback(i, 0, "C", fx, fx_new, alpha, 0, 0, x0, true);
        
        // Prepare next iterate.
        fx      = fx_new;
        grad_fx = grad_fx_new;
      }
      
      return x;
    }
};

bool GradientMethods::fRethrow = false;
#endif
