#ifndef DUNE_POINT_CONSTRAINT_PARTICLE_HH
#define DUNE_POINT_CONSTRAINT_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>

template<class float_x = double>
class PointConstraintParticle
{
  public:
  
    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;
    
    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    typedef Polynomial LevelSetFunction;
    typedef Polynomial HeightProfileFunction;
    
    //~ RESTATE static const typename ParticleType::Type type = ParticleType::Point;
    static const typename ParticleType::Type type = ParticleType::Curve;
  
    PointConstraintParticle(const Points& points_)
      //~ RESTATE : points_(points_), levelSetFunction_(FunctionsHelper::constant(-1)), heightProfileFunction_(FunctionsHelper::constant(1))
      //~ : levelSetFunction_(FunctionsHelper::ellipse(0.47,0.3))
      : levelSetFunction_(FunctionsHelper::ellipse(0.3000123,0.3000123))
    {
      //~ heightProfileFunction_ = FunctionsHelper::constant(0);
      
      typedef typename Polynomial::PolTuple P;
            
      const float_x r = 0.3000123;
            
      heightProfileFunction_ =
        Polynomial( { 
          std::make_pair(   0+0.5*r, P({0,0}) ),
          std::make_pair( -1./(2*r), P({2,0}) ),
          std::make_pair( -1./(2*r), P({0,2}) ),
        } );
    }
    
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }


  private:
  
    const Points points_;
    const Polynomial levelSetFunction_;
          Polynomial heightProfileFunction_;
};

#endif
