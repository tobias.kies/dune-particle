#ifndef DUNE_ELLIPSOIDAL_PARTICLE_HH
#define DUNE_ELLIPSOIDAL_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>
#include <dune/particle/functions/periodicFunction.hh>

template<class float_x = double>
class EllipsoidalParticle
{
  public:
  
    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;
    
    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    typedef Polynomial LevelSetFunctionBase;
    typedef Polynomial HeightProfileFunctionBase;
    typedef PeriodicFunction<LevelSetFunctionBase> LevelSetFunction;
    typedef PeriodicFunction<HeightProfileFunctionBase> HeightProfileFunction;
    
    static const typename ParticleType::Type type = ParticleType::Curve;
  
    EllipsoidalParticle(const double a, const double b, const double height, const double slope)
      : levelSetFunctionBase(FunctionsHelper::ellipse(a,b))
    {
      typedef typename Polynomial::PolTuple P;

      heightProfileFunctionBase =
        Polynomial( { 
          std::make_pair( height+slope*a/2, P({0,0}) ),
          std::make_pair( -slope*a/(2*a*a), P({2,0}) ),
          std::make_pair( -slope*a/(2*b*b), P({0,2}) ),
        } );
        
      levelSetFunction_       = LevelSetFunction(levelSetFunctionBase);
      heightProfileFunction_  = HeightProfileFunction(heightProfileFunctionBase);
    }
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }
    
    template<class Surface>
    void makePeriodic(const Surface& surface)
    {
      levelSetFunction_.setPeriodicBox(surface.periodicBox());
      heightProfileFunction_.setPeriodicBox(surface.periodicBox());
    }


  private:
  
    const Points points_;
    const LevelSetFunctionBase levelSetFunctionBase;
          HeightProfileFunctionBase heightProfileFunctionBase;
          LevelSetFunction levelSetFunction_;
          HeightProfileFunction heightProfileFunction_;
};

#endif
