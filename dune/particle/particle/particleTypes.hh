#ifndef DUNE_PARTICLE_TYPES_HH
#define DUNE_PARTICLE_TYPES_HH

namespace ParticleType{
    enum Type {Point, Curve};
}

#endif
