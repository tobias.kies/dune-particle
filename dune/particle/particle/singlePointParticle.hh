#ifndef DUNE_SINGLE_POINT_PARTICLE_HH
#define DUNE_SINGLE_POINT_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>

template<class float_x = double>
class SinglePointParticle
{
  public:
  
    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;
    
    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    typedef Polynomial LevelSetFunction;
    typedef Polynomial HeightProfileFunction;
    
    static const typename ParticleType::Type type = ParticleType::Point;
  
    SinglePointParticle(const double height = 0)
      : points_({{0,0,height}}), levelSetFunction_(FunctionsHelper::constant(-1)), heightProfileFunction_(FunctionsHelper::constant(0))
    {
    }
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }


  private:
  
    const Points points_;
    const Polynomial levelSetFunction_;
          Polynomial heightProfileFunction_;
};

#endif
