#ifndef DUNE_EXAMPLE_POINT_CONSTRAINTS_HH
#define DUNE_EXAMPLE_POINT_CONSTRAINTS_HH

class ExamplePointConstraints
{
  public:
  
    typedef Dune::FieldVector<double,3> Point;
    typedef std::vector<Point> Points;
  
    static Points testPoints( )
    {
      Points points(4);

      points[0] = {-0.5, -0.5, -1};
      points[1] = {+0.5, -0.5, +1};
      points[2] = {+0.5, +0.5, -1};
      points[3] = {-0.5, +0.5, +1};

      return points;
    }
    

  private:
  
    ExamplePointConstraints(){};
};

#endif
