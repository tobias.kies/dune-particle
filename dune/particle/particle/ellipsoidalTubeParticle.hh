#ifndef DUNE_ELLIPSOIDAL_TUBE_PARTICLE_HH
#define DUNE_ELLIPSOIDAL_TUBE_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>
#include <dune/particle/functions/periodicFunction.hh>



class TubeParticleHeightProfileFunction
  : public FunctionsHelper::template Function<double,2>
{
  public:

    typedef typename FunctionsHelper::template Function<double,2> BaseType;

    typedef typename BaseType::DomainType DomainType;
    typedef typename BaseType::RangeType RangeType;
    typedef typename BaseType::OrderType OrderType;
    
    using BaseType::evaluate;

    TubeParticleHeightProfileFunction(const double a, const double r)
      : a(a), r(r)
    {}

    RangeType evaluate( const DomainType& x, const OrderType& d ) const
    {
      RangeType res(0);
      
      const auto r2 = r*r;
      const auto x2 = x[0]*x[0];
      
      if(d[0]==0 && d[1] == 0){
        res = sqrt(r2-x2)-sqrt(r2-a*a);
      }else if(d[1]>0){
        res = 0;
      }else if(d[0] == 1){
        res = -x[0]/sqrt(r2-x2);
      }else if(d[0] == 2){
        res = -r2/std::pow(r2-x2,1.5);
      }else{
        DUNE_THROW(Dune::Exception, "Not implemented.");
      }
      
      return res;
    }
    
  private:
    const double a;
    const double r;
};
    
    
template<class float_x = double>
class EllipsoidalTubeParticle
{
  public:

    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;

    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    
    static const typename ParticleType::Type type = ParticleType::Curve;
  
    typedef Polynomial LevelSetFunctionBase;
    typedef TubeParticleHeightProfileFunction HeightProfileFunctionBase;
    typedef PeriodicFunction<LevelSetFunctionBase> LevelSetFunction;
    typedef PeriodicFunction<HeightProfileFunctionBase> HeightProfileFunction;
  
    EllipsoidalTubeParticle(const double a, const double b, const double r)
      : levelSetFunctionBase(FunctionsHelper::ellipse(a,b)), heightProfileFunctionBase(a, r)
    {
      levelSetFunction_       = LevelSetFunction(levelSetFunctionBase);
      heightProfileFunction_  = HeightProfileFunction(heightProfileFunctionBase);
    }
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }
    
    template<class Surface>
    void makePeriodic(const Surface& surface)
    {
      levelSetFunction_.setPeriodicBox(surface.periodicBox());
      heightProfileFunction_.setPeriodicBox(surface.periodicBox());
    }


  private:  
  
    const Points points_;
    const LevelSetFunctionBase levelSetFunctionBase;
          HeightProfileFunctionBase heightProfileFunctionBase;
          LevelSetFunction levelSetFunction_;
          HeightProfileFunction heightProfileFunction_;
};

#endif
