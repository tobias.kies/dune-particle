#ifndef DUNE_FCHO2_PARTICLE_HH
#define DUNE_FCHO2_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>
#include <dune/particle/particle/FCHo2Particle_raw.hh>
#include <dune/particle/functions/periodicFunction.hh>
#include <dune/particle/functions/pointConstantFunction.hh>

template<class float_x = double>
class FCHo2Particle
{
  public:
  
    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;
    
    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    typedef Polynomial LevelSetFunctionBase;
    typedef PointConstantFunction<Points,float_x> HeightProfileFunctionBase;
    typedef PeriodicFunction<LevelSetFunctionBase> LevelSetFunction;
    typedef PeriodicFunction<HeightProfileFunctionBase> HeightProfileFunction;
    
    static const typename ParticleType::Type type = ParticleType::Point;
  
    FCHo2Particle()
      : points_(
          {
            { 78.3993,  7.4521,  3.7144},
            { 81.5369, -3.0805, -1.1676},
            { 83.1268, -4.0080, -2.5367},
            {-78.5279, -7.7386,  3.5408},
            {-81.6674,  2.9629, -1.1191},
            {-82.8677,  4.4120, -2.4317}
          }
        ),
        levelSetFunctionBase(FunctionsHelper::constant(-1)),
        heightProfileFunctionBase(points_)
    {
      levelSetFunction_       = LevelSetFunction(levelSetFunctionBase);
      heightProfileFunction_  = HeightProfileFunction(heightProfileFunctionBase);
    }
    
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }
    
    template<class Surface>
    void makePeriodic(const Surface& surface)
    {
      levelSetFunction_.setPeriodicBox(surface.periodicBox());
      heightProfileFunction_.setPeriodicBox(surface.periodicBox());
    }


  private:
  
    const Points points_;
    const LevelSetFunctionBase levelSetFunctionBase;
    const HeightProfileFunctionBase heightProfileFunctionBase;
          LevelSetFunction levelSetFunction_;
          HeightProfileFunction heightProfileFunction_;
};

#endif
