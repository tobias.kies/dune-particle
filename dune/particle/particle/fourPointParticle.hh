#ifndef DUNE_FOUR_POINT_PARTICLE_HH
#define DUNE_FOUR_POINT_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>
#include <dune/particle/functions/pointConstantFunction.hh>

template<class float_x = double>
class FourPointParticle
{
  public:
  
    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;
    
    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    typedef Polynomial LevelSetFunction;
    typedef PointConstantFunction<Points,float_x> HeightProfileFunction;
    
    static const typename ParticleType::Type type = ParticleType::Point;
  
    FourPointParticle()
      : points_(
          {
            { -0.35, -0.35,  0.3},
            {  0.35, -0.35, -0.3},
            {  0.35,  0.35,  0.3},
            { -0.35,  0.35, -0.3}
          }
        ),
        levelSetFunction_(FunctionsHelper::constant(-1)),
        heightProfileFunction_(points_)
    { }
    
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }


  private:
  
    const Points points_;
    const LevelSetFunction levelSetFunction_;
    const HeightProfileFunction heightProfileFunction_;
};

#endif
