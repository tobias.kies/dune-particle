#ifndef DUNE_CIRCULAR_PARTICLE_HH
#define DUNE_CIRCULAR_PARTICLE_HH

#include <dune/particle/particle/particleTypes.hh>

template<class float_x = double>
class CircularParticle
{
  public:
  
    typedef Dune::FieldVector<float_x,3> Point;
    typedef std::vector<Point> Points;

    typedef Dune::FieldVector<float_x,2> ParameterizedPoint;
    
    typedef typename FunctionsHelper::template Polynomial<float_x,2> Polynomial;
    typedef Polynomial LevelSetFunction;
    typedef Polynomial HeightProfileFunction;
    
    static const typename ParticleType::Type type = ParticleType::Curve;
  
    CircularParticle(const double radius, const double height, const double slope)
      : levelSetFunction_(FunctionsHelper::circle(radius))
    {
      typedef typename Polynomial::PolTuple P;
            
      heightProfileFunction_ =
        Polynomial( { 
          std::make_pair( height+slope*radius/2  , P({0,0}) ),
          std::make_pair(       -slope/(2*radius), P({2,0}) ),
          std::make_pair(       -slope/(2*radius), P({0,2}) ),
        } );
    }
    
    const auto& points() const
    {
      return points_;
    }
    
    const auto& levelSetFunction() const
    {
      return levelSetFunction_;
    }
    
    const auto& heightProfileFunction() const
    {
      return heightProfileFunction_;
    }


  private:
  
    const Points points_;
    const Polynomial levelSetFunction_;
          Polynomial heightProfileFunction_;
};

#endif
