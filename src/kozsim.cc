// This program is associated to the simulations done for the paper
// associated to the special issue of the journal "Interfaces and Free
// Boundaries" on "Free Boundary Problems in Biology".

//~ #define USE_OMP 1
//~ #define FORCE_OMP 1
//~ #define DISABLE_FBUDDY_ERROR 1
//~ #define NO_PERIODIC_CONSTRAINTS 1 // Disables the periodic constraints in the surface. Can only be used if the particles are in turn restricted to reside within the domain. -- Appears to not be a good idea.
#define USE_PLANAR_SPECIALIZATION 0 // Uses a specialization for the evaluation of derivatives that only works if no tilts are present.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <random>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/common/timer.hh>

#include <dune/particle/problem/fcho2IFBProblem.hh>
#include <dune/particle/problem/fcho2KozlovProblem.hh>
#include <dune/particle/functions/rescaledMembraneEnergy.hh>
#include <dune/particle/optimization/gradientMethods.hh>

#include <numeric>
#include <chrono>

template<class Problem = FCHo2KozlovProblem>
static auto run(const int nRef = 0, const int nPart = 24)
{
  // Set up base problem.
  //~ typedef FCHo2KozlovProblem Problem;
  //~ typedef FCHo2IFBProblem Problem;
  Problem problem;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  //~ const auto&& rescaling = [&problem, &tau](const int particleId, const int componentId){ return (componentId < 3) ? 100/tau : 1.; }; // old value (up to IFB16)
  const auto&& rescaling = [&problem](const int particleId, const int componentId){ return (componentId < 3) ? 100 : 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);
  
  // Set initial iterate.
  auto x0 = f.problem.initialPosition();
  /*
  std::vector<double>
    x0_ = {
      0, 1900,
      0, 1800
    };
  x0.resize(x0_.size()/2); x0 *= 0;
  for(int i = 0; i < x0.size(); ++i)
    x0[i] = {x0_[2*i], x0_[2*i+1], 0, 0, 0, 0};
  */
  x0 = f.applyInverseRescaling(x0);
  x0.resize(nPart);

  // Set some further problem configuration.
  f.problem.nMaxGridSize = 60e3;
  f.problem.nRefinementSteps = nRef;
  
  // Evaluate.
  try{
    f.evaluate(x0);
  }catch(...){
    std::cerr << "FAILED" << std::endl;
    //~ problem.saveMembrane("./test", 0);
    throw;
  }
  //~ problem.saveMembrane("./test", 0);
  
  auto&& grad = f.evaluateGradient(x0);
  Debug::dprintVector(grad, "gradient");
  
  return grad;
}

static void annealing(const double tau, const double c0, const int nRef)
{
  // Set up base problem.
  typedef FCHo2KozlovProblem Problem;
  Problem problem;

  // Annealing configuration.
  const int maxIter = 1e5;
  const double maxGradNorm = 1e5;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  //~ const auto&& rescaling = [&problem, &tau](const int particleId, const int componentId){ return (componentId < 3) ? 100/tau : 1.; }; // old value (up to IFB16)
  const auto&& rescaling = [&problem, &tau](const int particleId, const int componentId){ return (componentId < 3) ? 100 : 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);

  
  // Set initial iterate.
  std::vector<double>
    x0_ = {
      -1000, -1000,
      -546, -695,
      -237, -978,
      132, -735,
      582, -972,
      952, -706,
      -986, -416,
      -571, -112,
      -225, -354,
      195, -48,
      582, -354,
      1000, 0,
      -986, 265,
      -500, 500,
      -211, 178,
      141, 494,
      599, 209,
      1000, 500,
      -980, 729,
      -500, 1000,
      -125, 754,
      333, 1002,
      686, 742,
      1000, 1000
    };

  std::default_random_engine generator;
  std::uniform_real_distribution<double> distribution(0, 2*M_PI);

  auto x0 = f.problem.initialPosition();
  x0.resize(x0_.size()/2); x0 *= 0;
  for(int i = 0; i < x0.size(); ++i)
    x0[i] = {x0_[2*i], x0_[2*i+1], 0, 0, 0, distribution(generator)};
  x0 = f.applyInverseRescaling(x0);
    
  // Set callback function.
  /*std::ofstream file;
  file.open("iterates.txt");
  file << std::setprecision(15);
  const auto writeVector = [&file](const auto vec)
  {
    for(int i = 0; i < vec.size(); ++i)
      for(int j = 0; j < vec[i].size(); ++j)
        file << " " << vec[i][j];
  };
  
  const auto callback = [&file, &problem, &writeVector, &f](const int i, const double t, const auto& x_i, const auto& grad_i, const auto& noise_i)
  {
    std::cerr << "Hit callback " << i << " at time " << t << std::endl;
    
    // Update info in file.
    file << i << " " << t << " " << problem.energy();
    writeVector(f.applyRescaling(x_i));
    file << std::endl;
  };*/
  
  std::fstream file;
  std::string fname;
  fname.reserve(255);
  sprintf(&fname[0], "iterates_%04d_%04d_%02d.bin", (int) (tau*1000), (int) (c0*1000), nRef);
  file = std::fstream(fname, std::ios::out | std::ios::binary);
  
  const int nParticles = x0.size();
  const int nDof = 3;
  
  file.write((char*) &tau, sizeof(tau));
  file.write((char*) &c0, sizeof(c0));
  file.write((char*) &nRef, sizeof(nRef));
  file.write((char*) &nParticles, sizeof(nParticles));
  file.write((char*) &nDof, sizeof(nDof));
  file.write((char*) &maxIter, sizeof(maxIter));
  file.flush();
  
  const auto writeVector = [&file](const auto vec)
  {
    for(int i = 0; i < vec.size(); ++i)
    {
      for(int j = 0; j < vec[i].size(); ++j)
      {
        if(j == 2 || j == 3 || j == 4)
          continue;
        const double val = vec[i][j];
        file.write((char*) &val, sizeof(val));
      }
    }
    file.flush();
  };
  
  const auto callback = [&file, &problem, &writeVector, &f](const int i, const double t, const auto& x_i, const auto& grad_i, const auto& noise_i)
  {
    std::cerr << "Hit callback " << i << " at time " << t << std::endl;
    
    // Update info in file.
    const double d_i = i;
    file.write((char*) &d_i, sizeof(d_i));
    file.write((char*) &t, sizeof(t));
    
    const double e = problem.energy();
    file.write((char*) &e, sizeof(e));
    writeVector(f.applyRescaling(x_i));
  };

  // Set some further problem configuration.
  f.problem.nMaxGridSize = 60e3;
  f.problem.nRefinementSteps = nRef;
  
  // Run a simulated annealing method.
  auto l_ = f.problem.lowerBound();
  auto u_ = f.problem.upperBound();
  auto l = f.applyInverseRescaling(l_);
  auto u = f.applyInverseRescaling(u_);
  GradientMethods::reflectiveEulerMaruyama(f, x0, c0, l, u, tau, maxIter, callback, maxGradNorm);
  
  file.close();
}

int main(int argc, char** argv)
{
  /*Dune::Matrix<Dune::FieldVector<double,2>> mat;
  
  mat.setSize(3,2);
  
  mat[0][0] = {0,1};
  mat[0][1] = {2,3};
  mat[1][0] = {4,5};
  mat[1][1] = {6,7};
  mat[2][0] = {8,9};
  mat[2][1] = {10,11};
  
  double* a = &mat[0][0][0];
  for(int i = 0; i < mat.N()*mat.M()*2; ++i, ++a)
    std::cerr << *a << " ";
  std::cerr << std::endl;*/
  
  /*using M = Dune::FieldMatrix<double,1,1>;
  using V = Dune::FieldVector<double,1>;
  using Mat = Dune::BCRSMatrix<M>;
  Mat A(3,3,9,Mat::row_wise);
  
  for(auto row=A.createbegin(); row!=A.createend(); ++row)
    for(int j = 0; j < A.M(); ++j)
      row.insert(j);
      
  A[0][0] = 1;
  A[0][1] = 1;
  A[0][2] = 1;
  A[1][0] = 1;
  A[1][1] = 2;
  A[1][2] = 3;
  A[2][0] = 1;
  A[2][1] = 5;
  A[2][2] = 6;
  
  Dune::Matrix<V> X(3,2);
  Dune::Matrix<V> B(3,2);
  
  B[0][0] = 1;
  B[1][0] = 2;
  B[2][0] = 3;
  B[0][1] = 4;
  B[1][1] = 5;
  B[2][1] = 6;
  
  Dune::SuperLU<Mat> solver(A);
  Dune::InverseOperatorResult res;
  Debug::dprintSMatrix(B, "BPre");
  solver.apply(X,B,res);
  
  Debug::dprintSMatrix(A, "A");
  Debug::dprintSMatrix(X, "X");
  Debug::dprintSMatrix(B, "B");*/
  
  
  // Compare efficiency of new and old implementation
  /*
  Dune::Timer timer;
  
  for(int i = 1; i <= 24; ++i)
  {
    timer.reset();
    run<FCHo2KozlovProblem>(8, i);
    std::cout << "Version 1 " << i << " took " << timer.elapsed() << std::endl << std::flush;

    timer.reset();
    run<FCHo2IFBProblem>(8, i);
    std::cout << "Version 2 " << i << " took " << timer.elapsed() << std::endl << std::flush;
  }
  */
  
  // Gradient in dependence of refinement.
  /*for(int i = 5; i <= 10; ++i)
  {
    Dune::Timer timer;
    auto g = run(i);
    Debug::dprintVector(g, "igradient", i, "took", timer.elapsed());
  }*/
  
  // Do a parameter study.
  if(argc < 4)
    DUNE_THROW(Dune::Exception, "Insufficient number of arguments.");
    
  const double tau = strtod(argv[1], nullptr);
  const double c0 = strtod(argv[2], nullptr);
  const int nRef = strtol(argv[3], nullptr, 0);
  Debug::dprint("tau", tau, "c0", c0, "nRef", nRef);
  annealing(tau, c0, nRef);
  

  return 0;
}
