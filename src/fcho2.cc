// This program is associated to the simulations done for the paper
// associated to the special issue of the journal "Interfaces and Free
// Boundaries" on "Free Boundary Problems in Biology".

//~ #define USE_OMP 1
//~ #define FORCE_OMP 1
#define DISABLE_FBUDDY_ERROR 1
//~ #define NO_PERIODIC_CONSTRAINTS 1 // Disables the periodic constraints in the surface. Can only be used if the particles are in turn restricted to reside within the domain. -- Appears to not be a good idea.
#define USE_PLANAR_SPECIALIZATION 0 // Uses a specialization for the evaluation of derivatives that only works if no tilts are present.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <vector>
#include <stdlib.h>
#include <random>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions

#include <dune/particle/problem/fcho2IFBProblem.hh>
#include <dune/particle/functions/rescaledMembraneEnergy.hh>
#include <dune/particle/optimization/gradientMethods.hh>

#include <numeric>
#include <chrono>

static void FCHo2Optimization()
{
  // General configuration.
  const int nMaxSteps = 1e3;

  // Set up base problem.
  typedef FCHo2IFBProblem Problem;
  Problem problem;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  const auto&& rescaling = [&problem](const int particleId, const int componentId){ return (componentId < 3) ? 1000. : 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);

  // Set callback function.
  std::ofstream file;
  file.open("FCHo2_IFB.txt");
  file << std::setprecision(15);
  const auto writeVector = [&file](const auto vec)
  {
    for(int i = 0; i < vec.size(); ++i)
      for(int j = 0; j < vec[i].size(); ++j)
        file << vec[i][j] << " ";
  };

  const auto callback = [&file, &problem, &writeVector](const int i, const int k, const auto& fx_i, const auto& fx_ik, const auto& norm_grad, const auto& x_i, const auto& x_ik, const auto& grad_i, const auto& noise_i, const bool accepted)
  {
    std::cerr << "Hit callback " << i << " at step length iteration " << k << std::endl;

    // Update info in file.
    file << i << " " << k << " " << fx_i << " " << fx_ik << " " << norm_grad << " ";
    writeVector(x_i);
    writeVector(x_ik);
    writeVector(grad_i);
    writeVector(noise_i);
    file << std::endl;

    Debug::dprintColumnVector(x_i, "x_i");
    Debug::dprintColumnVector(grad_i, "grad_i");
    Debug::dprintColumnVector(noise_i, "noise_i");
    Debug::dprint(problem.energy(), "xk energy");

    // Save iterate.
    if(accepted)
      problem.saveMembrane("./iterates/FCHo2_IFB_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
  };

  // Run gradient method.
  GradientMethods::fRethrow = true;
  GradientMethods::perturbedProjectedGradient(f, f.applyInverseRescaling(problem.initialPosition()), f.lowerBound(), f.upperBound(), nMaxSteps, callback, 0., 0., 0.05);
}


static void FCHo2Optimization_BFGS(const int arbitraryInitialPosition = 0, const bool removeLimits = false)
{
  // General configuration.
  const int nMaxSteps = 1e4;

  // Set up base problem.
  typedef FCHo2IFBProblem Problem;
  Problem problem;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  const auto&& rescaling = [&problem](const int particleId, const int componentId){ return 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);

  // Set callback function.
  std::ofstream file;
  file.open("FCHo2_IFB.txt");
  file << std::setprecision(15);
  const auto writeVector = [&file](const auto vec)
  {
    for(int i = 0; i < vec.size(); ++i)
      for(int j = 0; j < vec[i].size(); ++j)
        file << vec[i][j] << " ";
  };

  const auto callback = [&file, &problem, &writeVector](const int i, const int k, const std::string& phase, const auto& fx0, const auto& fxt, const double t, const double a, const double b, const auto& x, const bool accepted)
  {
    std::cerr << "Hit callback " << i << " at step length iteration " << k << " of phase " << phase << std::endl;

    // Update info in file.
    file << i << " " << k << " " << phase << " " << fx0 << " " << fxt << " " << t << " " << a << " " << b << " ";
    writeVector(x);
    file << std::endl;
    Debug::dprint(problem.energy(), "xt energy");

    // Save iterate.
    if(accepted)
      problem.saveMembrane("./iterates/FCHo2_IFB_low_iterate_" + std::string(floor(log10(nMaxSteps+0.5))-floor(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
  };
  
  // Compute an initial Binv. This is done by approximating the diagonal of the Hessian. (A little hacky.)
  const int n = problem.initialPosition().size();
  auto x0 = f.applyInverseRescaling(problem.initialPosition());
  
  typedef Dune::Matrix<Dune::FieldMatrix<double,6,6>> DenseMatrix;
  DenseMatrix Binv(n,n);
  
  /*problem.nRefinementSteps = 7;
  problem.repulsionConstant = 0;
  const auto grad_fx = f.evaluateGradient(x0);
  for(int i = 0; i < n; ++i)
  {
    for(int j = 0; j < x0[i].size(); ++j)
    {
      // Skip x3-related entries.
      if(j == 2 || j == 3 || j == 4)
        continue;
        
      // Approximate second derivative.
      const double t = (i <= 2) ? 10 : M_PI/180;
      auto xp = x0; xp[i][j] += t;
      //~ const auto grad_fxp = f.evaluateGradient(xp); // Problem: Did not yet implement evaluation of partial derivatives.
      //~ Binv[i][i][j][j] = t/(grad_fxp[i][j]-grad_fx[i][j]);
      f.evaluate(xp); // Solve in xp.
      Binv[i][i][j][j] = std::abs(t/(problem.partialDerivativeWoRepulsion(i)[j]-grad_fx[i][j])); // ATTENTION: Does not support rescaling!
      std::cerr << "Approximated inverse Hessian in " << i << "," << j << " by " << Binv[i][i][j][j] << std::endl;
      std::cout << i << " " << j << " " << Binv[i][i][j][j] << std::endl;
    }
  }*/
  // Hard coded diagonal from a previous run to save time.
  /*std::vector<double> diagonal = {
    6.9413e+06, -2.8513e+08, 112.921,
    579721, -2.17303e+06, 70.301,
    1.54861e+06, 3.28211e+06, 110.797,
    166280, 544446, 5.4616,
    -526650, 45546.2, 1.15126,
    -224522, 55268.6, 275.172,
    414946, -185896, -9.88251,
    141716, 466058, 6.08712,
    174580, 166832, -5.3467,
    513598, 115795, 12.0432,
    308228, -249705, 2.84565,
    80364.2, -285282, 169.024,
    121688, 237014, 3.56426,
    2.47831e+06, -384412, 2.31031,
    160690, 184442, 16.4973,
    -209359, 61014.3, 1687.64,
    47476, 145299, -5.87014,
    242172, -89450.6, 6.80713,
    -110569, -224697, -2.4125,
    -638975, -74606.6, -1.82482
  };
  for(int i = 0; i < n*3; ++i)
    Binv[i/3][i/3][i%3+(i%3==2)*3][i%3+(i%3==2)*3] = std::abs(diagonal[i]) / 100.;*/
  /*std::vector<double> diagonal = {
    111995, 307678, 31.8171,
    11362.6, -81757.6, 12.0202,
    38573.2, 116715, 15.6485,
    60256.7, 10360.9, 2.32765,
    20344, 6084.87, 0.501951,
    -68972, 4453.74, 0.67816,
    43037.2, 9566.72, 1.00584,
    7368.47, 17338.7, 0.714806,
    15718.2, -7594.78, -2.15233,
    -23158.3, -16219.3, 1.04523,
    18514.4, -41563.4, 1.90623,
    -42069.6, -126701, -1.05516,
    -80601.9, -223633, 2.3048,
    9544.85, -90176, 0.977724,
    27540.6, -19406.8, 3.72294,
    66796.5, 10394.7, 2.32765,
    20315.3, 6042.11, 0.501951,
    -63989.3, 4391.78, 0.67816,
    41760, 9494.74, 1.00584,
    7606.91, 16476.2, 0.714806,
    15682.7, -7359.9, -2.15233,
    -21268.7, -15235.5, 1.04523,
    18281.3, -42090.3, 1.90623,
    -39359.1, -268365, -1.05516
  };
  for(int i = 0; i < n*3; ++i)
    Binv[i/3][i/3][i%3+(i%3==2)*3][i%3+(i%3==2)*3] = std::abs(diagonal[i]) / 100.;
  problem.repulsionConstant = 1;
  problem.nRefinementSteps = 9;*/

  // Just initialize by identity matrix.
  if(removeLimits)
    for(int i = 0; i < n*6; ++i)
      Binv[i/6][i/6][i%6][i%6] = 1;
  else
    for(int i = 0; i < n*3; ++i)
      Binv[i/3][i/3][i%3+(i%3==2)*3][i%3+(i%3==2)*3] = 1;
    
  
  if(arbitraryInitialPosition != 0)
  {
    std::vector<double> x0_;
    
    if(arbitraryInitialPosition == 1)
    {
      x0_ = {
        -790, -787,
        -474, -803,
        -162, -792,
        176, -776,
        514, -782,
        842, -787,
        -839, -396,
        -389, -295,
        139, -311,
        847, -137,
        546, -433,
        863, -454,
        -786, -21,
        -680, 328,
        -342, 386,
        197, 211,
        757, 396,
        858, 148,
        -796, 682,
        -394, 740,
        -51, 772,
        318, 793,
        408, 476,
        784, 788
      };
    }else if(arbitraryInitialPosition == 2){
      x0_ = {
        -1000, -1000,
        -546, -695,
        -237, -978,
        132, -735,
        582, -972,
        952, -706,
        -986, -416,
        -571, -112,
        -225, -354,
        195, -48,
        582, -354,
        1000, 0,
        -986, 265,
        -500, 500,
        -211, 178,
        141, 494,
        599, 209,
        1000, 500,
        -980, 729,
        -500, 1000,
        -125, 754,
        333, 1002,
        686, 742,
        1000, 1000
      };
      problem.nRefinementSteps = 9;
      problem.nMaxGridSize = 80e3;
      
      // Set a different initial Binv.
      for(int i = 0; i < n*3; ++i)
        Binv[i/3][i/3][i%3+(i%3==2)*3][i%3+(i%3==2)*3] = (i%3==2) ? 1 : 10000;
    }

    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0, 2*M_PI);

    x0.resize(x0_.size()/2); x0 *= 0;
    for(int i = 0; i < x0.size(); ++i)
      x0[i] = {x0_[2*i], x0_[2*i+1], 0, 0, 0, distribution(generator)};
    x0 = f.applyInverseRescaling(x0);
  }
    
  // Run optimization method.
  GradientMethods::fRethrow = true;
  auto l = f.lowerBound();
  auto u = f.upperBound();
  if(removeLimits)
  {
    for(int i = 0; i < n; ++i)
    {
      for(int j = 0; j < u[i].size(); ++j)
      {
        u[i][j] = std::numeric_limits<double>::infinity();
        l[i][j] = -u[i][j];
      }
    }
  }
  
  // Set some further problem configuration.
  f.problem.nMaxGridSize = 60e3;
  f.problem.nRefinementSteps = 13;
  
  GradientMethods::bfgs(f, x0, l, u, Binv, nMaxSteps, callback, 0.01);
}



// Computes some energies for special configurations.
static void FCHo2Optimization_TMP()
{
  // General configuration.
  const int nMaxSteps = 1e4;

  // Set up base problem.
  typedef FCHo2IFBProblem Problem;
  Problem problem;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  const auto&& rescaling = [&problem](const int particleId, const int componentId){ return 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);
  
  
  // f evaluated at some interesting positions.
  std::cout << "single particle energy" << std::endl << f.evaluate( {{0,0,0,0,0,0}} ) << std::endl << std::endl;
  
  std::cout << "distance energy" << std::endl;
  std::vector<double> distances = {50, 100, 150, 200, 250, 300, 500};
  for(int i = 0; i < distances.size(); ++i)
    std::cout << distances[i] << " " << f.evaluate( {{0,0,0,0,0,0}, {0,distances[i],0,0,0,0}} ) << std::endl;
  std::cout << std::endl;
  
  std::cout << "angular energy" << std::endl;
  const double DEG = M_PI/180;
  const double r = 150;
  std::vector<double> angles = {0, 15, 30, 45, 60, 75, 90};
  for(int i = 0; i < angles.size(); ++i)
    std::cout << angles[i] << " " << f.evaluate( {{0,0,0,0,0,0}, {-r*sin(angles[i]),r*cos(angles[i]),0,0,0,angles[i]}} ) << std::endl;
  std::cout << std::endl;
  
  std::cout << "joined angular/distance energy" << std::endl;
  for(int j = 0; j < distances.size(); ++j)
    for(int i = 0; i < angles.size(); ++i)
      std::cout << angles[i] << " " << distances[j] << " " << f.evaluate( {{0,0,0,0,0,0}, {-distances[j]*sin(angles[i]),distances[j]*cos(angles[i]),0,0,0,angles[i]}} ) << std::endl;
  std::cout << std::endl;
}


// Computes how the refinement impacts the energy.
static void FCHo2Optimization_EnergyByRefinement()
{
  // Set up base problem.
  typedef FCHo2IFBProblem Problem;
  Problem problem;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  const auto&& rescaling = [&problem](const int particleId, const int componentId){ return 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);

  // Set callback function.
  std::ofstream file;
  file.open("FCHo2_IFB.txt");
  file << std::setprecision(15);
  
  // Set initial iterate.
  std::vector<double>
    x0_ = {
      -1000, -1000,
      -546, -695,
      -237, -978,
      132, -735,
      582, -972,
      952, -706,
      -986, -416,
      -571, -112,
      -225, -354,
      195, -48,
      582, -354,
      1000, 0,
      -986, 265,
      -500, 500,
      -211, 178,
      141, 494,
      599, 209,
      1000, 500,
      -980, 729,
      -500, 1000,
      -125, 754,
      333, 1002,
      686, 742,
      1000, 1000
    };
    
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0, 2*M_PI);
    
    auto x0 = f.problem.initialPosition();
    x0.resize(x0_.size()/2); x0 *= 0;
    for(int i = 0; i < x0.size(); ++i)
      x0[i] = {x0_[2*i], x0_[2*i+1], 0, 0, 0, distribution(generator)};
    x0 = f.applyInverseRescaling(x0);
    

    f.problem.nMaxGridSize = 100e3;
    for(int i = 4; i <= 11; ++i)
    {
      f.problem.nRefinementSteps = i;
      const auto start = std::chrono::system_clock::now();
      
      const auto fx = f.evaluate(x0);
      const auto end = std::chrono::system_clock::now();
      const std::chrono::duration<double> diff = end-start;
      
      const auto grad_fx = f.evaluateGradient(x0);
      const auto end2 = std::chrono::system_clock::now();
      const std::chrono::duration<double> diff2 = end2-start;
      
      file << i << " " << fx << " " << diff.count() << " " << diff2.count();
      for(int j = 0; j < grad_fx.size(); ++j)
        for(int k = 0; k < grad_fx[j].size(); ++k)
          file << " " << grad_fx[j][k];
      file << std::endl;
      f.currentPosition *= std::numeric_limits<double>::quiet_NaN();
    }
    
    file.close();
}


static void FCHo2Optimization_Annealing()
{
  // Set up base problem.
  typedef FCHo2IFBProblem Problem;
  Problem problem;

  // Annealing configuration.
  //~ const double c0 = log(2) * 0.1; // Old value. (up to IFB17)
  //~ const double c0 = 1; // IFB 18
  const double c0 = 0.2; // IFB 19
  const double tau = 0.05;
  const int maxIter = 1e5;
  const double maxGradNorm = 1;

  // Define rescaling. (Allows to rescale single degrees of freedom.)
  //~ const auto&& rescaling = [&problem, &tau](const int particleId, const int componentId){ return (componentId < 3) ? 100/tau : 1.; }; // old value (up to IFB16)
  const auto&& rescaling = [&problem, &tau](const int particleId, const int componentId){ return (componentId < 3) ? 100 : 1.; };

  // Define optimization functional.
  const double h = Problem::length/8;
  RescaledMembraneEnergy<Problem,decltype(rescaling)> f(problem, rescaling, h);

  // Set callback function.
  std::ofstream file;
  file.open("FCHo2_IFB.txt");
  file << std::setprecision(15);
  const auto writeVector = [&file](const auto vec)
  {
    for(int i = 0; i < vec.size(); ++i)
      for(int j = 0; j < vec[i].size(); ++j)
        file << " " << vec[i][j];
  };
  
  const auto callback = [&file, &problem, &writeVector](const int i, const double t, const auto& x_i, const auto& grad_i, const auto& noise_i)
  {
    std::cerr << "Hit callback " << i << " at time " << t << std::endl;
    
    // Update info in file.
    file << i << " " << t << " " << problem.energy();
    writeVector(x_i);
    writeVector(grad_i);
    writeVector(noise_i);
    file << std::endl;
    
    // Save iterate.
    problem.saveMembrane("./iterates/iterate_" + std::string(ceil(log10(maxIter+0.5))-ceil(log10(i+(i==0)+0.5)),'0') + std::to_string(i), 0);
  };
  
  // Set initial iterate.
  std::vector<double>
    x0_ = {
      -1000, -1000,
      //~ -2000, -2000, // Temporary for testing of across-border buddy finding.
      -546, -695,
      -237, -978,
      132, -735,
      582, -972,
      952, -706,
      -986, -416,
      -571, -112,
      -225, -354,
      195, -48,
      582, -354,
      1000, 0,
      -986, 265,
      -500, 500,
      -211, 178,
      141, 494,
      599, 209,
      1000, 500,
      -980, 729,
      -500, 1000,
      -125, 754,
      333, 1002,
      686, 742,
      1000, 1000
    };
    
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(0, 2*M_PI);
    
    auto x0 = f.problem.initialPosition();
    x0.resize(x0_.size()/2); x0 *= 0;
    for(int i = 0; i < x0.size(); ++i)
      x0[i] = {x0_[2*i], x0_[2*i+1], 0, 0, 0, distribution(generator)};
    x0 = f.applyInverseRescaling(x0);


    // Set some further problem configuration.
    f.problem.nMaxGridSize = 60e3;
    f.problem.nRefinementSteps = 8;
    
    // Run a simulated annealing method.
    auto l_ = f.problem.lowerBound();
    auto u_ = f.problem.upperBound();
    /*for(int i = 0; i < l_.size(); ++i) // Active for IFB16, IFB17
    {
      for(int j = 0; j < 2; ++j)
      {
        l_[i][j] = 200-f.problem.length;
        u_[i][j] = -l_[i][j];
      }
    }*/
    auto l = f.applyInverseRescaling(l_);
    auto u = f.applyInverseRescaling(u_);
    GradientMethods::reflectiveEulerMaruyama(f, x0, c0, l, u, tau, maxIter, callback, maxGradNorm);
}

int main(int argc, char** argv)
{
  //~ FCHo2Optimization();
  //~ FCHo2Optimization_BFGS();
  //~ FCHo2Optimization_BFGS(1);
  //~ FCHo2Optimization_BFGS(2); /// CURRENT ACTIVE VERSION!
  //~ FCHo2Optimization_TMP();
  //~ FCHo2Optimization_EnergyByRefinement();
  
  // IFB Paper Simulations
  //FCHo2Optimization_Annealing(); /// To-do: There are potentially different c0 values!
  FCHo2Optimization_BFGS(0,true); /// NEWER CURRENT ACTIVE VERSION!

  return 0;
}
