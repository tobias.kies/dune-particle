// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <vector>
#include <stdlib.h>

#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/common/parametertreeparser.hh>

#include <dune/particle/problem/testProblems.hh>


int main(int argc, char** argv)
{
  // Read configuration.
  Dune::ParameterTree params;
  
  int mode;
  
  if(argc < 2)
  {
    Dune::ParameterTreeParser::readINITree("particle.ini", params);
    mode = params.template get<int>("mode");
  }
  else
  {
    mode = atoi(argv[1]);
  }

  switch(mode)
  {
    
    //
    // Curve tests.
    //

    // Solution membrane: Rates of convergence.
    case 11: // Annulus example (with known exact solution).
      TestProblems::convergenceTestCurveExact();
      break;

    case 12: // Three ellipses on a tube. (Too expensive, I think.)
      TestProblems::convergenceTestCurveInexact();
      break;
      
    case 13: // Two "arbitrary" ellipses in classical Monge-gauge.
      TestProblems::convergenceTestCurveInexact2();
      break;
      
    // Derivative: Fit and rates of convergence.
    case 21: // Elliptical particle around origin.
      TestProblems::derivativeTestCurve();
      break;

    case 22: // Test convergence rates for a circle in the origin. -- Rather boring.
      TestProblems::derivativeConvergenceTestCurve();
      break;
    
    case 23: // Test convergence rates for the "arbitrary" ellipses from earlier.
      TestProblems::derivativeConvergenceTestCurve2();
      break;


    //
    // Point tests.
    //
    
    // Solution membrane: Rates of convergence.
    case 31:
      TestProblems::convergenceTestPointExact();
      break;
      
    case 32:
      TestProblems::convergenceTestPointInexact();
      break;
      
    case 33:
      TestProblems::convergenceTestPointInexact2();
      break;
      
    // Derivative: Fit and rates of convergence.
    case 41:
      TestProblems::derivativeTestPoint();
      break;
      
    case 42:
      TestProblems::derivativeConvergenceTestPoint();
      break;


    //
    // Gradient flows.
    //
    
    case 51:
      // This is the pre-version of the egg-carton test.
      // Here we only have two circular particles in Monge-gauge with
      // periodic boundary conditions.
      TestProblems::eggCartonTest();
      break;

    case 52:
      // The actual egg-carton test: "Many" particles in Monge-gauge
      // with periodic boundary conditions.
      TestProblems::eggCartonTest2();
      break;

    case 522:
      // The egg carton test that tries to get back into the 52-test
      // after it crashed due to out-of-memory issues.
      TestProblems::eggCartonTest2_Backup();
      break;

    case 53:
      // The actual egg-carton test: "Many" particles in Monge-gauge
      // with periodic boundary conditions.
      // Here with an Armijo-type perturbed gradient method (instead
      // of a gradient flow).
      TestProblems::eggCartonTest3();
      break;

    case 5301:
      // Like 532 but checks some pre-defined candidates instead.
      TestProblems::eggCartonTest3_checkCandidates();
      break;

    case 532:
      // Like 53 but with saddle point particles.
      TestProblems::eggCartonTest4();
      break;

    case 5321:
      // Like 532 but checks some pre-defined candidates instead.
      TestProblems::eggCartonTest4_checkCandidates();
      break;
      
    case 54:
      TestProblems::FCHo2Test();
      break;
      
    case 542:
      // Like 54 but different starting configuration.
      TestProblems::FCHo2Test(1);
      break;
      
    case 543:
      // Like 54 but also with a repulsion term.
      TestProblems::FCHo2Test(2, 1.0);
      break;
      
    //~ case 55:
      //~ TestProblems::FCHo2_Test2();
      //~ break;

    case 56:
      TestProblems::tubeInteractionTest();
      break;

    case 562:
      TestProblems::tubeInteractionTest(0.2, 45);
      break;

    case 563:
      TestProblems::tubeInteractionTest(-0.3, 45);
      //~ TestProblems::tubeInteractionTest(-0.3, 170);
      break;
      
      
    //
    // Gradient tests.
    //
    case 61:
      // Gradients from the exact curve problem.
      TestProblems::gradientsCurveExact();
      break;
      
    case 62:
      // Gradients from the inexact curve problem.
      TestProblems::gradientsCurveInexact();
      break;
      
    case 63:
      // Gradients from the exact point problem.
      TestProblems::gradientsPointExact();
      break;
      
    case 64:
      // Gradients from the inexact point problem.
      TestProblems::gradientsPointInexact();
      break;
    
    
    //
    // Difference quotient tests.
    //
    case 72:
      // Difference quotients from the inexact curve problem.
      TestProblems::differenceQuotientsCurveInexact();
      break;

    case 74:
      // Difference quotients from the inexact curve problem.
      TestProblems::differenceQuotientsPointInexact();
      break;
      
      
    //
    // Free energy tests.
    //
    case 81:
      TestProblems::freeEnergySlope();
      break;
    
    
    //
    // Temporary/Other tests.
    //
    case 99:
      //~ std::cout << "No temporary tests defined at the moment." << std::endl;
      TestProblems::checkFiniteDifferenceConvergence();
      break;

    case 98:
      //~ std::cout << "No temporary tests defined at the moment." << std::endl;
      TestProblems::checkFiniteDifferenceConvergenceExt();
      break;

    case 97:
      //~ std::cout << "No temporary tests defined at the moment." << std::endl;
      TestProblems::gradientsCurveInexactTmp();
      break;
      
    case 96:
      TestProblems::saveSolutions();
      break;
      
    default:
      std::cout << "Mode not recognized." << std::endl;
      break;
  }


  /*// Set initial position.
  #if SURFACE == 1
    typename MembraneParticleProblem::Position position(1);
    position[0] = {0,0,1,0,0,0};
  #else
    typename MembraneParticleProblem::Position position(1);
    const double alpha = 0*M_PI/180;
    {
      const int i = 3;
      auto elev   = 1.5;
      auto rowId  = (i < 2) ? 0 : (i < 5) ? 1 : 2;
      auto colId  = (i < 2) ? i : (i < 5) ? i-2 : i-5;
      auto x = (rowId != 1) ? (colId+1)*5./4+5./8 : (colId+1)*5./4;
      auto y = sin(-rowId*alpha)*elev;
      auto z = cos(-rowId*alpha)*elev;
      position[0] = {x, y, z, rowId*alpha, 0,  0*M_PI/180};
    }
  #endif
  Debug::dprintVector(position, "Initial position");
  */
  return 0;
}
