# About
This [DUNE](https://dune-project.org/) module came into existence as part of the
[CRC-1114](http://sfb1114.de/) project [particles in lipid bilayers](http://www.mi.fu-berlin.de/en/math/groups/ag-numerik/projects/Particles-lipid-bilayers/index.html),
which is concerned with the simulation of interactions between biological membranes
(modeled as continuous surfaces, similar to very thin mechanical plates)
and discrete finite-sized particles (which induce local deformations and propagate
"mechanically" through the whole domain).

This repository is a fork/backup of the original repository which is hosted at
the [Freie Universität Berlin](http://www.mi.fu-berlin.de/).
As such, it may potentially be somewhat out of date.

If you are curious about examples on what simulations and results can
be produced with the help of this code, you can check out
[my PhD thesis](https://refubium.fu-berlin.de/handle/fub188/24940).
